clear all

********************************************************************************
* Without economic bloc, October the 15th.
//WORKING
* use "/home/javiervg/QED/6 TrabajoCueva/DATA/Quest-HL-WithoutBlock-201015.dta",clear
********************************************************************************
********************************************************************************
* With Economic Block, September the 16th
 use "/home/javiervg/QED/6 TrabajoCueva/DATA/Quest-HL-WithBlock-200916.dta", clear
********************************************************************************
********************************************************************************
* With Economic Block, October the 15th
* use "/home/javiervg/QED/6 TrabajoCueva/DATA/Quest-HL-WithBlock-201015.dta", clear
********************************************************************************
********************************************************************************


********************************************************************************
* Generate variables:
********************************************************************************
gen cuidad = 0
label variable cuidad "El sujeto referencio la cuidad en la que curso el bachillerato"
gen instituto = 0
label variable instituto "El sujeto referencio el instituto en la que curso el bachillerato"
gen SocioeconStatPers = 0
label variable SocioeconStatPers "Estatus socioecomico de la cuidad/distrito en 2017 por persona"
gen SocioeconStatHog = 0
label variable SocioeconStatHog "Estatus socioecomico de la cuidad/distrito en 2017 por hogar"
gen poblacion = 0
label variable SocioeconStatHog "Población en la cuidad a 1 de enero de 2019"
********************************************************************************
********************************************************************************


********************************************************************************
* Assignment by Provinces/Autonomic Cities
********************************************************************************
********************************************************************************
*    A Coruña
********************************************************************************
********************************************************************************
*    Álava
********************************************************************************
********************************************************************************
*    Albacete
********************************************************************************
// Albacete
// albacete
replace cuidad = 1 if LocalidadInstitutoenelcursas=="albacete"
replace SocioeconStatPers = 11223 if LocalidadInstitutoenelcursas=="albacete"
replace SocioeconStatHog = 30173 if LocalidadInstitutoenelcursas=="albacete"
replace poblacion = 173329 if LocalidadInstitutoenelcursas=="albacete"
// Almansa
// Almansa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almansa"
replace SocioeconStatPers = 9776 if LocalidadInstitutoenelcursas=="Almansa"
replace SocioeconStatHog = 25469 if LocalidadInstitutoenelcursas=="Almansa"
replace poblacion = 24419 if LocalidadInstitutoenelcursas=="Almansa"
// Almansa 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almansa "
replace SocioeconStatPers = 9776 if LocalidadInstitutoenelcursas=="Almansa "
replace SocioeconStatHog = 25469 if LocalidadInstitutoenelcursas=="Almansa "
replace poblacion = 24419 if LocalidadInstitutoenelcursas=="Almansa "
// Almansa/IES Herminio Almendros
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
replace SocioeconStatPers = 9776 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
replace SocioeconStatHog = 25469 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
replace poblacion = 24419 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
// Bonillo, El
// IES Las Sabinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
replace SocioeconStatPers = 8569 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
replace SocioeconStatHog = 21546 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
replace poblacion = 2779 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
********************************************************************************
*    Alicante
********************************************************************************
// Albatera
// Albatera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Albatera"
replace SocioeconStatPers = 8157 if LocalidadInstitutoenelcursas=="Albatera"
replace SocioeconStatHog = 22661 if LocalidadInstitutoenelcursas=="Albatera"
replace poblacion = 12279 if LocalidadInstitutoenelcursas=="Albatera"
// IES Antonio Serna Serna (Albatera, Alicante)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
replace SocioeconStatPers = 8157 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
replace SocioeconStatHog = 22661 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
replace poblacion = 12279 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
// IES COX
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES COX"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES COX"
replace SocioeconStatPers = 8157 if LocalidadInstitutoenelcursas=="IES COX"
replace SocioeconStatHog = 22661 if LocalidadInstitutoenelcursas=="IES COX"
replace poblacion = 12279 if LocalidadInstitutoenelcursas=="IES COX"
// Alcoy
// Alcoi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoi"
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoi"
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoi"
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoi"
// Alcoy
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoy"
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoy"
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoy"
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoy"
// Alicante
// Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante"
// alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="alicante"
// alicante 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="alicante "
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="alicante "
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="alicante "
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="alicante "
// ALICANTE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ALICANTE"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="ALICANTE"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="ALICANTE"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="ALICANTE"
// CEU JESÚS Y MARÍA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
// Alicante/Colegio Altozano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
// IES cavanilles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES cavanilles"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES cavanilles"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES cavanilles"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES cavanilles"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES cavanilles"
// Colegio Inmaculada Jesuitas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
// Alicante / Colegio San José de Carolinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
// Alicante/Colegio Sagrado Corazón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
// IES Figueras Pacheco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
// Alicante/Figueras Pacheco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
// liceo francés internacional alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
// IES 8 de marzo Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
// IES Mare Nostrum Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
// Alicante, Ies Mare Nostrum
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
// Alicante, Ies Mare Nostrum
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
// Maristas de Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
// Alicante, colegio HH Maristas sagrado corazón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
// Salesianos - Don Bosco, Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
// Inmaculada Jesuitas Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
// I.E.S Cabo de la Huerta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
// ies cabo de las huertas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
// IES SAN BLAS
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
// IES San Blay y HH Maristas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
// IES Virgen del Remedio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
// Aspe
// ASPE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ASPE"
replace SocioeconStatPers = 8073 if LocalidadInstitutoenelcursas=="ASPE"
replace SocioeconStatHog = 21893 if LocalidadInstitutoenelcursas=="ASPE"
replace poblacion = 20714 if LocalidadInstitutoenelcursas=="ASPE"
// Banyeres de Mariola
// IES Professor Manuel Broseta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
replace SocioeconStatPers = 10364 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
replace SocioeconStatHog = 25903 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
replace poblacion = 7068 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
// Benejúzar
// IES Benejúzar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Benejúzar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Benejúzar"
replace SocioeconStatPers = 8130 if LocalidadInstitutoenelcursas=="IES Benejúzar"
replace SocioeconStatHog = 22875 if LocalidadInstitutoenelcursas=="IES Benejúzar"
replace poblacion = 5402 if LocalidadInstitutoenelcursas=="IES Benejúzar"
// Benidorm
// Benidorm
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Benidorm"
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="Benidorm"
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="Benidorm"
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="Benidorm"
// IES Mediterrania
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Mediterrania"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Mediterrania"
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="IES Mediterrania"
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="IES Mediterrania"
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="IES Mediterrania"
//Bigastro
// IES Paco Ruiz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
replace SocioeconStatPers = 8341 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
replace SocioeconStatHog = 23348 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
replace poblacion = 6733 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
// Callosa de segura
// Callosa de segura
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Callosa de segura"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="Callosa de segura"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="Callosa de segura"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="Callosa de segura"
// Callosa de Segura, IES Vega Baja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
// IES Vega Baja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Vega Baja"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Vega Baja"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="IES Vega Baja"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="IES Vega Baja"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="IES Vega Baja"
// Campello, El
// El Campello
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Campello"
replace SocioeconStatPers = 10850 if LocalidadInstitutoenelcursas=="El Campello"
replace SocioeconStatHog = 26650 if LocalidadInstitutoenelcursas=="El Campello"
replace poblacion = 28349 if LocalidadInstitutoenelcursas=="El Campello"
// El Campello, Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Campello, Alicante"
replace SocioeconStatPers = 10850 if LocalidadInstitutoenelcursas=="El Campello, Alicante"
replace SocioeconStatHog = 26650 if LocalidadInstitutoenelcursas=="El Campello, Alicante"
replace poblacion = 28349 if LocalidadInstitutoenelcursas=="El Campello, Alicante"
// El Campello. I.E.S Enric Valor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
replace SocioeconStatPers = 10850 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
replace SocioeconStatHog = 26650 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
replace poblacion = 28349 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
// Cocentaina
// Cocentaina
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cocentaina"
replace SocioeconStatPers = 9623 if LocalidadInstitutoenelcursas=="Cocentaina"
replace SocioeconStatHog = 24800 if LocalidadInstitutoenelcursas=="Cocentaina"
replace poblacion = 11511 if LocalidadInstitutoenelcursas=="Cocentaina"
// cocentaina
replace cuidad = 1 if LocalidadInstitutoenelcursas=="cocentaina"
replace SocioeconStatPers = 9623 if LocalidadInstitutoenelcursas=="cocentaina"
replace SocioeconStatHog = 24800 if LocalidadInstitutoenelcursas=="cocentaina"
replace poblacion = 11511 if LocalidadInstitutoenelcursas=="cocentaina"
// Crevillente
// crevillent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="crevillent"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="crevillent"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="crevillent"
replace poblacion = 28952 if LocalidadInstitutoenelcursas=="crevillent"
// Crevillente IES Maciá Abela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
replace poblacion = 28952 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
// IES Macià Abela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Macià Abela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Macià Abela"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="IES Macià Abela"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="IES Macià Abela"
replace poblacion = 28952 if LocalidadInstitutoenelcursas=="IES Macià Abela"
// Denia
// Denia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Denia"
replace SocioeconStatPers = 9675 if LocalidadInstitutoenelcursas=="Denia"
replace SocioeconStatHog = 24734 if LocalidadInstitutoenelcursas=="Denia"
replace poblacion = 42166 if LocalidadInstitutoenelcursas=="Denia"
// Dolores
// Dolores
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Dolores"
replace SocioeconStatPers = 7222 if LocalidadInstitutoenelcursas=="Dolores"
replace SocioeconStatHog = 19287 if LocalidadInstitutoenelcursas=="Dolores"
replace poblacion = 7470 if LocalidadInstitutoenelcursas=="Dolores"
// Dolores (Alicante)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Dolores (Alicante)"
replace SocioeconStatPers = 7222 if LocalidadInstitutoenelcursas=="Dolores (Alicante)"
replace SocioeconStatHog = 19287 if LocalidadInstitutoenelcursas=="Dolores (Alicante)"
replace poblacion = 7470 if LocalidadInstitutoenelcursas=="Dolores (Alicante)"
// Elche
// Elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche"
// elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="elche"
// elche 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="elche "
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="elche "
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="elche "
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="elche "
// ELCHE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ELCHE"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="ELCHE"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="ELCHE"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="ELCHE"
// Elche/IES Carrus
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
// Elche/ IES Carrús
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
// colegio la devesa Elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
// la devesa school elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="la devesa school elche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="la devesa school elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="la devesa school elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="la devesa school elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="la devesa school elche"
// IES Joanot Martorell
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
// La Foia, Elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Foia, Elche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Foia, Elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="La Foia, Elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="La Foia, Elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="La Foia, Elche"
// Elche/ IES Misteri d'Elx
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
// Elche/ Victoria Kent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
// Elche, IES Victoria Kent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
// Santa María-Jesuitinas de Elche, Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
// Sixto Marco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sixto Marco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sixto Marco"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Sixto Marco"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Sixto Marco"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Sixto Marco"
// Elda
// Elda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elda"
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="Elda"
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="Elda"
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="Elda"
// Elda 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elda "
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="Elda "
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="Elda "
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="Elda "
// Elda / IES Monastil
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
// Ibi
// ibi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ibi"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="ibi"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="ibi"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="ibi"
// IBI / LA FOIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
// Mutxamel
// Mutxamel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mutxamel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="Mutxamel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="Mutxamel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="Mutxamel"
// Mutxamel - C.E.B.AT.
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
replace instituto = 1 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
// , ElI.E.S Mutxamel
replace cuidad = 1 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
replace instituto = 1 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
// I.E.S Mutxamel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
// Novelda
// Novelda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Novelda"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Novelda"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Novelda"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Novelda"
// Colegio padre dehon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
// Colegio padre dehon 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
// IES LA MOLA, NOVELDA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
// Nucia, La
// IES LA NUCIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
replace SocioeconStatPers = 9007 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
replace SocioeconStatHog = 25499 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
replace poblacion = 18603 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
// LA NUCIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="LA NUCIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="LA NUCIA"
replace SocioeconStatPers = 9007 if LocalidadInstitutoenelcursas=="LA NUCIA"
replace SocioeconStatHog = 25499 if LocalidadInstitutoenelcursas=="LA NUCIA"
replace poblacion = 18603 if LocalidadInstitutoenelcursas=="LA NUCIA"
// Onil
// Ies la Creueta Onil
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace SocioeconStatPers = 10109 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace SocioeconStatHog = 25027 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace poblacion = 7507 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
// Ies la Creueta Onil
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace SocioeconStatPers = 10109 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace SocioeconStatHog = 25027 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace poblacion = 7507 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
// Orihuela
// Orihuela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Orihuela"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="Orihuela"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="Orihuela"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="Orihuela"
// ORIHUELA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ORIHUELA"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="ORIHUELA"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="ORIHUELA"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="ORIHUELA"
// IES Gabriel Miró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
// IES Tháder
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Tháder"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Tháder"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="IES Tháder"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="IES Tháder"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="IES Tháder"
// Petrer
// Petrer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Petrer"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="Petrer"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="Petrer"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="Petrer"
// IES La Canal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES La Canal"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES La Canal"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="IES La Canal"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="IES La Canal"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="IES La Canal"
// Petrer/IES Poeta Paco Mollá
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
// ies paco molla(petrer)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
// Redován
// Redovan
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Redovan"
replace SocioeconStatPers = 7708 if LocalidadInstitutoenelcursas=="Redovan"
replace SocioeconStatHog = 21653 if LocalidadInstitutoenelcursas=="Redovan"
replace poblacion = 7869 if LocalidadInstitutoenelcursas=="Redovan"
// redován
replace cuidad = 1 if LocalidadInstitutoenelcursas=="redován"
replace SocioeconStatPers = 7708 if LocalidadInstitutoenelcursas=="redován"
replace SocioeconStatHog = 21653 if LocalidadInstitutoenelcursas=="redován"
replace poblacion = 7869 if LocalidadInstitutoenelcursas=="redován"
//San Vicente del Raspeig
// San Vicente del Raspeig
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
// San vicente del Raspeig
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San vicente del Raspeig"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San vicente del Raspeig"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San vicente del Raspeig"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San vicente del Raspeig"
// San Vicente del Raspeig / I.E.S. San Vicente
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
// IES Gaia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Gaia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Gaia"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES Gaia"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES Gaia"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES Gaia"
// IES Gaia 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Gaia "
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Gaia "
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES Gaia "
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES Gaia "
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES Gaia "
// ÍES GAIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES GAIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES GAIA"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="ÍES GAIA"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="ÍES GAIA"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="ÍES GAIA"
// San Vicente del Raspeig/IES María Blasco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
// IES MARIA BLASCO
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
// ÍES SAN Vicente
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
// ÍES SAN Vicente 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
// Sant Joan d'Alacant
// Sant Joan d'Alacant/IES Luís García Berlanga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
replace SocioeconStatPers = 11217 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
replace SocioeconStatHog = 30000 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
replace poblacion = 23915 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
// IES Lloixa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace SocioeconStatPers = 11217 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace SocioeconStatHog = 30000 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace poblacion = 23915 if LocalidadInstitutoenelcursas=="IES Lloixa"
// IES Lloixa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace SocioeconStatPers = 11217 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace SocioeconStatHog = 30000 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace poblacion = 23915 if LocalidadInstitutoenelcursas=="IES Lloixa"
// Santa Pola
// SANTAB POLA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="SANTAB POLA"
replace SocioeconStatPers = 9146 if LocalidadInstitutoenelcursas=="SANTAB POLA"
replace SocioeconStatHog = 22371 if LocalidadInstitutoenelcursas=="SANTAB POLA"
replace poblacion = 32306 if LocalidadInstitutoenelcursas=="SANTAB POLA"
// IES Santa Pola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Santa Pola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Santa Pola"
replace SocioeconStatPers = 9146 if LocalidadInstitutoenelcursas=="IES Santa Pola"
replace SocioeconStatHog = 22371 if LocalidadInstitutoenelcursas=="IES Santa Pola"
replace poblacion = 32306 if LocalidadInstitutoenelcursas=="IES Santa Pola"
// Torrevieja
// TORREVIEJA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="TORREVIEJA"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="TORREVIEJA"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="TORREVIEJA"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="TORREVIEJA"
// La Purisima Torrevieja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
// Torrevieja/ IES Torrevigía Nº5
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
// ies libertas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies libertas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies libertas"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="ies libertas"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="ies libertas"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="ies libertas"
// Villajoyosa/Vila Joiosa, la
// La Vila Joiosa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Vila Joiosa"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="La Vila Joiosa"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="La Vila Joiosa"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="La Vila Joiosa"
// Villajoyosa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villajoyosa"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="Villajoyosa"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="Villajoyosa"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="Villajoyosa"
// Villajoyosa/IES La Malladeta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
// Villena
// Villena
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villena"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Villena"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="Villena"
replace poblacion = 33964 if LocalidadInstitutoenelcursas=="Villena"
********************************************************************************
*    Almería
********************************************************************************
// Almería
// Almeria/IES Maestro Padilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
replace SocioeconStatPers = 9968 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
replace SocioeconStatHog = 27090 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
replace poblacion = 198533 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
********************************************************************************
*    Asturias
********************************************************************************
********************************************************************************
*    Ávila
********************************************************************************
********************************************************************************
*    Badajoz
********************************************************************************
// Don Benito
// Don Benito
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Don Benito"
replace SocioeconStatPers = 9654 if LocalidadInstitutoenelcursas=="Don Benito"
replace SocioeconStatHog = 25048 if LocalidadInstitutoenelcursas=="Don Benito"
replace poblacion = 37151 if LocalidadInstitutoenelcursas=="Don Benito"
// Don Benito 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Don Benito "
replace SocioeconStatPers = 9654 if LocalidadInstitutoenelcursas=="Don Benito "
replace SocioeconStatHog = 25048 if LocalidadInstitutoenelcursas=="Don Benito "
replace poblacion = 37151 if LocalidadInstitutoenelcursas=="Don Benito "
********************************************************************************
*    Baleares
********************************************************************************
// Eivissa
// Eivissa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Eivissa"
replace SocioeconStatPers = 13314 if LocalidadInstitutoenelcursas=="Eivissa"
replace SocioeconStatHog = 37822 if LocalidadInstitutoenelcursas=="Eivissa"
replace poblacion = 49783 if LocalidadInstitutoenelcursas=="Eivissa"
// Manacor
// IES Manacor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Manacor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Manacor"
replace SocioeconStatPers = 11333 if LocalidadInstitutoenelcursas=="IES Manacor"
replace SocioeconStatHog = 32589 if LocalidadInstitutoenelcursas=="IES Manacor"
replace poblacion = 43808 if LocalidadInstitutoenelcursas=="IES Manacor"
// Palma
// Sant Josep Obrer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
replace SocioeconStatPers = 12514 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
replace SocioeconStatHog = 35110 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
replace poblacion = 416065 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
********************************************************************************
*    Barcelona
********************************************************************************
// Badalona
// Badalona/Maristes Champanyat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
replace SocioeconStatPers = 11495 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
replace SocioeconStatHog = 30705 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
replace poblacion = 220440 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
// Barcelona
// BARCELONA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BARCELONA"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="BARCELONA"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="BARCELONA"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="BARCELONA"
// Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona"
// Barcelona 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona "
// barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="barcelona"
// Abat Oliba Spinola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
// Barcelona/Abat Oliba Cardenal Spínola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
// CIC
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CIC"
replace instituto = 1 if LocalidadInstitutoenelcursas=="CIC"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="CIC"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="CIC"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="CIC"
// Barcelona, betania patmos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
// Cic Batxillerats, Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
// Barcelona/Dominicas Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
// Barcelona, Frederic Mistral Tecnica Eulalia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
// Barcelona, Frederic Mistral-Tècnic Eulàlia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
// Barcelona / Jesus María Sant Andreu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
// Institut Menéndez y Pelayo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
* Assuming Escola Pia Balmes
// Escola Ia Balmes (Barcelona)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
// Escola Ia Balmes (Barcelona) 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
// Escola Pia Sarrià
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
// Barcelona/Príncep de Viana
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
// Frederic mistral tècnic eulàlia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
* Assuming Sant Ignasi
// Dant Ignasi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Dant Ignasi"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Dant Ignasi"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Dant Ignasi"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Dant Ignasi"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Dant Ignasi"
// Escuela Suiza de Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
// IPSI
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IPSI"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IPSI"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="IPSI"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="IPSI"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="IPSI"
// Jesuïtes Casp
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
// ípsilons Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
// ípsilons Barcelona 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
replace instituto = 1 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
// Sagrado Corazón Sarria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
// Sagrado Corazón Sarria 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
// Sarrià
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sarrià"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sarrià"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sarrià"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sarrià"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sarrià"
// Sarrià 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sarrià "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sarrià "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sarrià "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sarrià "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sarrià "
// Santa Teresa Ganduxer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
// Garriga, La
// La Garriga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Garriga"
replace SocioeconStatPers = 14110 if LocalidadInstitutoenelcursas=="La Garriga"
replace SocioeconStatHog = 38100 if LocalidadInstitutoenelcursas=="La Garriga"
replace poblacion = 16514 if LocalidadInstitutoenelcursas=="La Garriga"
// Gironella
// Ies Pere Fontdevila
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace SocioeconStatPers = 12203 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace SocioeconStatHog = 30035 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace poblacion = 4826 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
// Ies Pere Fontdevila
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace SocioeconStatPers = 12203 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace SocioeconStatHog = 30035 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace poblacion = 4826 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
// Hospitalet de Llobregat
// Tecla Sala
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tecla Sala"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Tecla Sala"
replace SocioeconStatPers = 10917 if LocalidadInstitutoenelcursas=="Tecla Sala"
replace SocioeconStatHog = 28985 if LocalidadInstitutoenelcursas=="Tecla Sala"
replace poblacion = 264923 if LocalidadInstitutoenelcursas=="Tecla Sala"
// Xaloc
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Xaloc"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Xaloc"
replace SocioeconStatPers = 10917 if LocalidadInstitutoenelcursas=="Xaloc"
replace SocioeconStatHog = 28985 if LocalidadInstitutoenelcursas=="Xaloc"
replace poblacion = 264923 if LocalidadInstitutoenelcursas=="Xaloc"
// Malgrat de Mar
// INS Ramon Turro i Darder
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
replace SocioeconStatPers = 11251 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
replace SocioeconStatHog = 28985 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
replace poblacion = 18579 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
// Martorell
// Martorell
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Martorell"
replace SocioeconStatPers = 11891 if LocalidadInstitutoenelcursas=="Martorell"
replace SocioeconStatHog = 34056 if LocalidadInstitutoenelcursas=="Martorell"
replace poblacion = 28189 if LocalidadInstitutoenelcursas=="Martorell"
// Masnou, El
// El Masnou
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Masnou"
replace SocioeconStatPers = 15135 if LocalidadInstitutoenelcursas=="El Masnou"
replace SocioeconStatHog = 39690 if LocalidadInstitutoenelcursas=="El Masnou"
replace poblacion = 23515 if LocalidadInstitutoenelcursas=="El Masnou"
// Mollet del Vallès
// mollet del valles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="mollet del valles"
replace SocioeconStatPers = 12075 if LocalidadInstitutoenelcursas=="mollet del valles"
replace SocioeconStatHog = 32350 if LocalidadInstitutoenelcursas=="mollet del valles"
replace poblacion = 51318 if LocalidadInstitutoenelcursas=="mollet del valles"
// mollet/vicenç plantada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
replace instituto = 1 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
replace SocioeconStatPers = 12075 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
replace SocioeconStatHog = 32350 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
replace poblacion = 51318 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
// Montgat
// montgat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="montgat"
replace SocioeconStatPers = 14346 if LocalidadInstitutoenelcursas=="montgat"
replace SocioeconStatHog = 37657 if LocalidadInstitutoenelcursas=="montgat"
replace poblacion = 12041 if LocalidadInstitutoenelcursas=="montgat"
// Sant Cugat
// Sant Cugat del Vallès
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallès"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallès"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallès"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallès"
// Sant Cugat del Vallés
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallés"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallés"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallés"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallés"
// La Farga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Farga"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Farga"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="La Farga"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="La Farga"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="La Farga"
// La Farga, Sant Cugat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
// Viaró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Viaró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Viaró"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="Viaró"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="Viaró"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="Viaró"
// Sant Pere de Ribes
// INS Can Puig
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS Can Puig"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS Can Puig"
replace SocioeconStatPers = 11918 if LocalidadInstitutoenelcursas=="INS Can Puig"
replace SocioeconStatHog = 32766 if LocalidadInstitutoenelcursas=="INS Can Puig"
replace poblacion = 30719 if LocalidadInstitutoenelcursas=="INS Can Puig"
// Santa Eulàlia de Ronçana
// INS La Vall del Tenes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
replace SocioeconStatPers = 13219 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
replace SocioeconStatHog = 37517 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
replace poblacion = 7288 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
// Sant Feliu de Llobregat
// Sant feliu de llobregat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant feliu de llobregat"
replace SocioeconStatPers = 13897 if LocalidadInstitutoenelcursas=="Sant feliu de llobregat"
replace SocioeconStatHog = 37265 if LocalidadInstitutoenelcursas=="Sant feliu de llobregat"
replace poblacion = 44860 if LocalidadInstitutoenelcursas=="Sant feliu de llobregat"
// Sant Feliu de Llobregat/ Verge de la Salut
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
replace SocioeconStatPers = 13897 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
replace SocioeconStatHog = 37265 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
replace poblacion = 44860 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
// Sant Feliu de Llobregat/ Verge de la Salut 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
replace SocioeconStatPers = 13897 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
replace SocioeconStatHog = 37265 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
replace poblacion = 44860 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
//Sant Joan Despí
// Sant Joan Despí
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Joan Despí"
replace SocioeconStatPers = 14565 if LocalidadInstitutoenelcursas=="Sant Joan Despí"
replace SocioeconStatHog = 39846 if LocalidadInstitutoenelcursas=="Sant Joan Despí"
replace poblacion = 34123 if LocalidadInstitutoenelcursas=="Sant Joan Despí"
// Vic
// Vic
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vic"
replace SocioeconStatPers = 12708 if LocalidadInstitutoenelcursas=="Vic"
replace SocioeconStatHog = 34720 if LocalidadInstitutoenelcursas=="Vic"
replace poblacion = 46214 if LocalidadInstitutoenelcursas=="Vic"
//Vilanova i la Geltrú
// Vilanova i la Geltrú, IES baixamar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace SocioeconStatPers = 12777 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace SocioeconStatHog = 32551 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace poblacion = 67086 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
// Vilanova i la Geltrú, IES baixamar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace SocioeconStatPers = 12777 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace SocioeconStatHog = 32551 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace poblacion = 67086 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
********************************************************************************
*    Burgos
********************************************************************************
// Aranda de Duero
// Aranda de Duero
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aranda de Duero"
replace SocioeconStatPers = 11939 if LocalidadInstitutoenelcursas=="Aranda de Duero"
replace SocioeconStatHog = 29101 if LocalidadInstitutoenelcursas=="Aranda de Duero"
replace poblacion = 32856 if LocalidadInstitutoenelcursas=="Aranda de Duero"
// Burgos
// Burgos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Burgos"
replace SocioeconStatPers = 13347 if LocalidadInstitutoenelcursas=="Burgos"
replace SocioeconStatHog = 32285 if LocalidadInstitutoenelcursas=="Burgos"
replace poblacion = 175821 if LocalidadInstitutoenelcursas=="Burgos"
********************************************************************************
*    Cáceres
********************************************************************************
// Cáceres
// IES Norba Caesarina
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
replace SocioeconStatPers = 11649 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
replace SocioeconStatHog = 29356 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
replace poblacion = 96126 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
// Navalmoral de la Mata
// Navalmoral de la mata
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Navalmoral de la mata"
replace SocioeconStatPers = 10404 if LocalidadInstitutoenelcursas=="Navalmoral de la mata"
replace SocioeconStatHog = 27365 if LocalidadInstitutoenelcursas=="Navalmoral de la mata"
replace poblacion = 17129 if LocalidadInstitutoenelcursas=="Navalmoral de la mata"
// Navalmoral de la Mata
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
replace SocioeconStatPers = 10404 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
replace SocioeconStatHog = 27365 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
replace poblacion = 17129 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
********************************************************************************
*    Cádiz
********************************************************************************
// Población Cáidz 116027
********************************************************************************
*    Cantabria
********************************************************************************
********************************************************************************
*    Castellón
********************************************************************************
********************************************************************************
*    Ceuta
********************************************************************************
********************************************************************************
*    Ciudad Real
********************************************************************************
********************************************************************************
*    Córdoba
********************************************************************************
********************************************************************************
*    Cuenca
********************************************************************************
// Cuenca
// Cuenca/ IES Alfonso VIII
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
replace SocioeconStatPers = 11827 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
replace SocioeconStatHog = 29877 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
replace poblacion = 54690 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
********************************************************************************
*    Girona
********************************************************************************
// Girona
// Girona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Girona"
replace SocioeconStatPers = 13527 if LocalidadInstitutoenelcursas=="Girona"
replace SocioeconStatHog = 35968 if LocalidadInstitutoenelcursas=="Girona"
replace poblacion = 101852 if LocalidadInstitutoenelcursas=="Girona"
********************************************************************************
*    Granada
********************************************************************************
// Granada
// Granada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Granada"
replace SocioeconStatPers = 12127 if LocalidadInstitutoenelcursas=="Granada"
replace SocioeconStatHog = 29373 if LocalidadInstitutoenelcursas=="Granada"
replace poblacion = 232462 if LocalidadInstitutoenelcursas=="Granada"
// Peligros
// Peligros (Granada)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Peligros (Granada)"
replace SocioeconStatPers = 9250 if LocalidadInstitutoenelcursas=="Peligros (Granada)"
replace SocioeconStatHog = 26468 if LocalidadInstitutoenelcursas=="Peligros (Granada)"
replace poblacion = 11394 if LocalidadInstitutoenelcursas=="Peligros (Granada)"
********************************************************************************
*    Guadalajara
********************************************************************************
// Guadalajara
// GUADALAJARA/ I.E.S CASTILLA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
replace SocioeconStatPers = 12466 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
replace SocioeconStatHog = 32442 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
replace poblacion = 85871 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
********************************************************************************
*    Gipuzkoa
********************************************************************************
// Donostia/San Sebastián
// Donostia/San Sebastián
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Donostia/San Sebastián"
replace SocioeconStatPers = 17187 if LocalidadInstitutoenelcursas=="Donostia/San Sebastián"
replace SocioeconStatHog = 41051 if LocalidadInstitutoenelcursas=="Donostia/San Sebastián"
replace poblacion = 184415 if LocalidadInstitutoenelcursas=="Donostia/San Sebastián"
********************************************************************************
*    Huelva
********************************************************************************
********************************************************************************
*    Huesca
********************************************************************************
// Huesca
// huesca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="huesca"
replace SocioeconStatPers = 12665 if LocalidadInstitutoenelcursas=="huesca"
replace SocioeconStatHog = 31278 if LocalidadInstitutoenelcursas=="huesca"
replace poblacion = 53132 if LocalidadInstitutoenelcursas=="huesca"
********************************************************************************
*    Jaén
********************************************************************************
// Jaén
// el valle
replace cuidad = 1 if LocalidadInstitutoenelcursas=="el valle"
replace instituto = 1 if LocalidadInstitutoenelcursas=="el valle"
replace SocioeconStatPers = 11366 if LocalidadInstitutoenelcursas=="el valle"
replace SocioeconStatHog = 30452 if LocalidadInstitutoenelcursas=="el valle"
replace poblacion = 112999 if LocalidadInstitutoenelcursas=="el valle"
********************************************************************************
*    La Rioja
********************************************************************************
********************************************************************************
*    Las Palmas
********************************************************************************
// Santa Brígida
// American School of Las Palmas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
replace SocioeconStatPers = 14072 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
replace SocioeconStatHog = 41367 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
replace poblacion = 18263 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
********************************************************************************
*    León
********************************************************************************
********************************************************************************
*    Lérida
********************************************************************************
// Lérida
// Lleida
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lleida"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Lleida"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Lleida"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Lleida"
// Lleida / Màrius Torres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
// Lleida/Ins Marius Torres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
// Torrefarrera
// Institut Joan Solà
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
replace SocioeconStatPers = 12748 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
replace SocioeconStatHog = 35673 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
replace poblacion = 4605 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
// Tremp
// Tremp
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tremp"
replace SocioeconStatPers = 11674 if LocalidadInstitutoenelcursas=="Tremp"
replace SocioeconStatHog = 27434 if LocalidadInstitutoenelcursas=="Tremp"
replace poblacion = 5844 if LocalidadInstitutoenelcursas=="Tremp"
********************************************************************************
*    Lugo
********************************************************************************
********************************************************************************
*    Madrid
********************************************************************************
// Alcorcón
// Alcorcón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcón"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcón"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcón"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcón"
// Alcorcón/Colegio Amanecer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
// Boadilla del Monte
// Boadilla del Monte
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
// Carabanchel
// Escolapias de Carabanchel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
replace SocioeconStatPers = 10988 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
replace SocioeconStatHog = 28721 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
replace poblacion = 243998 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
// Griñón
// Griñon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Griñon"
replace SocioeconStatPers = 12472 if LocalidadInstitutoenelcursas=="Griñon"
replace SocioeconStatHog = 38627 if LocalidadInstitutoenelcursas=="Griñon"
replace poblacion = 10319 if LocalidadInstitutoenelcursas=="Griñon"
// La Salle Griñón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Griñón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Griñón"
replace SocioeconStatPers = 12472 if LocalidadInstitutoenelcursas=="La Salle Griñón"
replace SocioeconStatHog = 38627 if LocalidadInstitutoenelcursas=="La Salle Griñón"
replace poblacion = 10319 if LocalidadInstitutoenelcursas=="La Salle Griñón"
// Colegio Villa de Griñón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
replace SocioeconStatPers = 12472 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
replace SocioeconStatHog = 38627 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
replace poblacion = 10319 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
// Leganés
// Leganés
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Leganés"
replace SocioeconStatPers = 11546 if LocalidadInstitutoenelcursas=="Leganés"
replace SocioeconStatHog = 30833 if LocalidadInstitutoenelcursas=="Leganés"
replace poblacion =  189861 if LocalidadInstitutoenelcursas=="Leganés"
// Majadahonda
// Majadahonda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Majadahonda"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Majadahonda"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Majadahonda"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Majadahonda"
// Majadahonda 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Majadahonda "
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Majadahonda "
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Majadahonda "
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Majadahonda "
// Majadahonda, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Majadahonda, Madrid"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Majadahonda, Madrid"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Majadahonda, Madrid"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Majadahonda, Madrid"
// Móstoles
// Móstoles, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Móstoles, Madrid"
replace SocioeconStatPers = 11004 if LocalidadInstitutoenelcursas=="Móstoles, Madrid"
replace SocioeconStatHog = 30826 if LocalidadInstitutoenelcursas=="Móstoles, Madrid"
replace poblacion = 209184 if LocalidadInstitutoenelcursas=="Móstoles, Madrid"
// Paracuellos de Jarama
// Paracuellos de Jarama
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
replace poblacion = 25269 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
// Pinto
// Colegio Mirasur. Pinto (Madrid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
replace SocioeconStatPers = 11957 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
replace SocioeconStatHog = 33342 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
replace poblacion = 52526 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
// Pozuelo de Alarcón
// Everest School Monteclaro
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
// IES Gerardo Diego
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
// Rivas
// Rivas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Rivas"
replace SocioeconStatPers = 14002 if LocalidadInstitutoenelcursas=="Rivas"
replace SocioeconStatHog = 42234 if LocalidadInstitutoenelcursas=="Rivas"
replace poblacion = 88150 if LocalidadInstitutoenelcursas=="Rivas"
// Rozas, las
// Las Rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las Rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las Rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las Rozas"
// Las rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las rozas"
// Valdemoro
// Juncarejo Marqués de Vallejo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
replace SocioeconStatPers = 11593 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
replace SocioeconStatHog = 34829 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
replace poblacion = 75983 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
// Villa de Madrid
// Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid"
// madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="madrid"
// IES Crdenal Cisneros
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
// Colegio Jesus Maria Garcia Noblejas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
// Colegio Santa Maria de la Hispanidad, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
// Colegio Valdefuentes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
// IES Ciudad de los poetas, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
// IES Isabel La Católica
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Isabel La Católica"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Isabel La Católica"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Isabel La Católica"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Isabel La Católica"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Isabel La Católica"
// las rosas 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="las rosas "
replace instituto = 1 if LocalidadInstitutoenelcursas=="las rosas "
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="las rosas "
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="las rosas "
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="las rosas "
// Liceo Francés de Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
// Colegio Lourdes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
// Lourdes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lourdes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lourdes"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Lourdes"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Lourdes"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Lourdes"
// Madrid, Nuestra Señora del Recuerdo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
// Madrid/I.E.S. Ramiro de Maeztu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
// Madrid/San Mateo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
// San José del Parque
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San José del Parque"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San José del Parque"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="San José del Parque"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="San José del Parque"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="San José del Parque"
// Madrid, San Saturio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
// IES Madrid Sur
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
// Madrid / Jesús-María
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
********************************************************************************
*    Málaga
********************************************************************************
// Málaga
// Málaga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Málaga"
replace SocioeconStatPers = 10119 if LocalidadInstitutoenelcursas=="Málaga"
replace SocioeconStatHog = 27525 if LocalidadInstitutoenelcursas=="Málaga"
replace poblacion = 574654 if LocalidadInstitutoenelcursas=="Málaga"
// Málaga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Málaga"
replace SocioeconStatPers = 10119 if LocalidadInstitutoenelcursas=="Málaga"
replace SocioeconStatHog = 27525 if LocalidadInstitutoenelcursas=="Málaga"
replace poblacion = 574654 if LocalidadInstitutoenelcursas=="Málaga"
********************************************************************************
*    Melilla
********************************************************************************
********************************************************************************
*    Murcia
********************************************************************************
// Murcia 
// Murcia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Murcia"
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Murcia"
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Murcia"
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Murcia"
// Murcia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Murcia"
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Murcia"
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Murcia"
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Murcia"
// Murcia/ ies alquibla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
// Colegio Nelva
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Nelva"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Nelva"
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Colegio Nelva"
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Colegio Nelva"
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Colegio Nelva"
// Colegio Nelva 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Nelva "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Nelva "
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Colegio Nelva "
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Colegio Nelva "
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Colegio Nelva "
// San Javier
// ies mar menor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies mar menor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies mar menor"
replace SocioeconStatPers = 9560 if LocalidadInstitutoenelcursas=="ies mar menor"
replace SocioeconStatHog = 26969 if LocalidadInstitutoenelcursas=="ies mar menor"
replace poblacion = 32489 if LocalidadInstitutoenelcursas=="ies mar menor"
********************************************************************************
*    Navarra
********************************************************************************
********************************************************************************
*    Ourense
********************************************************************************
********************************************************************************
*    Palencia
********************************************************************************
********************************************************************************
*    Pontevedra
********************************************************************************
********************************************************************************
*    Salamanca
********************************************************************************
// Salamanca
// IES vaguada de la palma
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
replace SocioeconStatPers = 12225 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
replace SocioeconStatHog = 27811 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
replace poblacion = 144228 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
// IES vaguada de la palma 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
replace SocioeconStatPers = 12225 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
replace SocioeconStatHog = 27811 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
replace poblacion = 144228 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
********************************************************************************
*    Segovia
********************************************************************************
********************************************************************************
*    Sevilla
********************************************************************************
********************************************************************************
*    Soria
********************************************************************************
********************************************************************************
*    Tarragona
********************************************************************************
// Reus
// Reus
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Reus"
replace SocioeconStatPers = 11530 if LocalidadInstitutoenelcursas=="Reus"
replace SocioeconStatHog = 30179 if LocalidadInstitutoenelcursas=="Reus"
replace poblacion = 104373 if LocalidadInstitutoenelcursas=="Reus"
// Salou
// Salou/Escola Elisabeth
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
replace SocioeconStatPers = 11091 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
replace SocioeconStatHog = 28351 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
replace poblacion = 27476 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
// Tarragona
// TARRAGONA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="TARRAGONA"
replace SocioeconStatPers = 12967 if LocalidadInstitutoenelcursas=="TARRAGONA"
replace SocioeconStatHog = 33547 if LocalidadInstitutoenelcursas=="TARRAGONA"
replace poblacion = 134515 if LocalidadInstitutoenelcursas=="TARRAGONA"
// Tarragona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tarragona"
replace SocioeconStatPers = 12967 if LocalidadInstitutoenelcursas=="Tarragona"
replace SocioeconStatHog = 33547 if LocalidadInstitutoenelcursas=="Tarragona"
replace poblacion = 134515 if LocalidadInstitutoenelcursas=="Tarragona"
// La Salle Tarragona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
replace SocioeconStatPers = 12967 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
replace SocioeconStatHog = 33547 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
replace poblacion = 134515 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
********************************************************************************
*    Santa Cruz de Tenerife
********************************************************************************
// Santa Cruz de la Palma
// Luis Cobiella Cuevas (S/C de La Palma)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
replace SocioeconStatPers = 10475 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
replace SocioeconStatHog = 29555 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
replace poblacion = 15716 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
// Luis Cobiella Cuevas/ S/C de La Palma
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
replace SocioeconStatPers = 10475 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
replace SocioeconStatHog = 29555 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
replace poblacion = 15716 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
// Villa de Mazo
// villa de mazo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="villa de mazo"
replace SocioeconStatPers = 9515 if LocalidadInstitutoenelcursas=="villa de mazo"
replace SocioeconStatHog = 26688 if LocalidadInstitutoenelcursas=="villa de mazo"
replace poblacion = 4843 if LocalidadInstitutoenelcursas=="villa de mazo"
// villa de mazo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="villa de mazo"
replace SocioeconStatPers = 9515 if LocalidadInstitutoenelcursas=="villa de mazo"
replace SocioeconStatHog = 26688 if LocalidadInstitutoenelcursas=="villa de mazo"
replace poblacion = 4843 if LocalidadInstitutoenelcursas=="villa de mazo"
********************************************************************************
*    Teruel
********************************************************************************
********************************************************************************
*    Toledo
********************************************************************************
********************************************************************************
*    Valencia
********************************************************************************
// Cullera
// Cullera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cullera"
replace SocioeconStatPers = 9762 if LocalidadInstitutoenelcursas=="Cullera"
replace SocioeconStatHog = 24236 if LocalidadInstitutoenelcursas=="Cullera"
replace poblacion = 22145 if LocalidadInstitutoenelcursas=="Cullera"
// Foios o Foyos
// Foios
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Foios"
replace SocioeconStatPers = 11590 if LocalidadInstitutoenelcursas=="Foios"
replace SocioeconStatHog = 29111 if LocalidadInstitutoenelcursas=="Foios"
replace poblacion = 7367 if LocalidadInstitutoenelcursas=="Foios"
// Gandia
// Gandia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gandia"
replace SocioeconStatPers = 9631 if LocalidadInstitutoenelcursas=="Gandia"
replace SocioeconStatHog = 25307 if LocalidadInstitutoenelcursas=="Gandia"
replace poblacion = 74562 if LocalidadInstitutoenelcursas=="Gandia"
// Godella
// Godella
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Godella"
replace SocioeconStatPers = 14468 if LocalidadInstitutoenelcursas=="Godella"
replace SocioeconStatHog = 43179 if LocalidadInstitutoenelcursas=="Godella"
replace poblacion = 13088 if LocalidadInstitutoenelcursas=="Godella"
// Godella 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Godella "
replace SocioeconStatPers = 14468 if LocalidadInstitutoenelcursas=="Godella "
replace SocioeconStatHog = 43179 if LocalidadInstitutoenelcursas=="Godella "
replace poblacion = 13088 if LocalidadInstitutoenelcursas=="Godella "
********************************************************************************
*    Valladolid
********************************************************************************
// Valladolid
// Colegio San José, Valladolid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
// IES Jose Jiménez Lozano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
********************************************************************************
*    Vizcaya
********************************************************************************
********************************************************************************
*    Zamora
********************************************************************************
********************************************************************************
*    Zaragoza
********************************************************************************
// Zaragoza
// Zaragoza
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Zaragoza"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="Zaragoza"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="Zaragoza"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="Zaragoza"
// Zaragoza, Montearagón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
********************************************************************************
********************************************************************************
*** Institutos Extranjeros, otras Regiones, desconocidos o ambiguos 
********************************************************************************
********************************************************************************
//prueba
//No cursado;;0;;0;;0;;0;;0
//uned;;0;;0;;0;;0;;0
//UNED;;0;;0;;0;;0;;0
//Andorra
//Colegio Holandés de Tres Arroyos, Argentina
//Riobamba - Ecuador
//Catalunya
//Ceapucv
//Emiratos Árabes Unidos
//Future language school-egypt
// EL IES Jose Luis Sampedro puede ser tanto de Guadalajara como de Madrid
//IES Jose Luis Sampedro
//Marruecos
//Miro
//No cursado
//Rosenheim, Alemania
//Santa Teresa de Lisieux
//Santo Domingo
//St Pauls School
********************************************************************************
********************************************************************************
