def process_files(read_file, write_file):
    # Opens input file for reading and output file for writing.
    # Output file is overwritten
    # Tells to drop the unasigned ones, for testing purposes in Stata
    dropping = 0
    with open(read_file, 'r') as reader, open(write_file, 'w') as writer:
        writer.write(create_file_header())
        # For each line in the input file
        for line in reader.readlines():
            # Cuts the endline character at the end, will allow to clean the last field
            line = line.replace('\n', '')
            # Check whether the line is a comment (start with // or *)
            comment = is_comment(line)
            # If it is a comment writes it in the output file
            if comment:
                writer.write(line + '\n')
            else:
                # Discards lines with only 1 character
                if len(line) >= 1:
                    # print(line, end='')
                    # print('\n')
                    # Splits the line in the fields separated by ';;'
                    # 1st field: nombre instituto.
                    # 2nd field: dummy cuidad
                    # 3rd field: dummy instituto
                    # 4rd field: SocioeconStatPers
                    # 5th field: SocioeconStatHog
                    # 6th field: poblacion
                    aux = line.split(";;")
                    # Checks if the line contained the 6 fields
                    if len(aux) == 6:
                        stata_replace = create_institute_block(aux[0], aux[1], aux[2], aux[3], aux[4], aux[5])
                        writer.write(stata_replace)
                    else:
                        # For testing purposes, drops the unasigned observations
                        if len(aux) == 1 & dropping:
                            writer.write(create_drop_block(aux[0]))
                        else:
                            str1 = ""
                            for element in aux:
                                str1 += element
                            writer.write("// " + str1 + "\n")
                            #print(str1)

        # crear fin de archivo
        writer.write(create_file_tail())


def is_comment(line):
    return line.startswith('//') or line.startswith('*')


def create_file_header():
    # Creates the header for the .do file
    s = ("********************************************************************************\n"
         "* The python code and the license can be found at:\n"
         "* https://gitlab.com/zT7jkDCzsc/socioeconstatusstata\n"
         "* Any comment, clarification or mistake in the data might be addressed to\n"
         "* javier.vazquez@ua.es\n"         
         "********************************************************************************\n"
         "* Los datos referentes al estatus socioeconómico fueron obtenidos en la web el \n"
         "* INE a nivel de municipio para el 2017:\n"
         "* https://www.ine.es/experimental/atlas/exp_atlas_tab.htm\n"
         "* Los datos referentes a la población  fueron obtenidos en la web del INE para el 2019\n"
         "* https://www.ine.es/nomen2/index.do\n"
         "* En ocasiones, los datos de población fueron obtenidos de la página de wikipedia \n"
         "* en español correspondiente al municipio, la cuál usá a su vez los datos del INE del \n"
         "* 2019 referenciados previamente.\n"
         "********************************************************************************\n"
         "* Generate variables:\n"
         "********************************************************************************\n"
         "gen cuidad = 0\n"
         'label variable cuidad "El sujeto referencio la cuidad en la que curso el bachillerato"\n'
         "gen instituto = 0\n"
         'label variable instituto "El sujeto referencio el instituto en la que curso el bachillerato"\n'
         "gen SocioeconStatPers = 0\n"
         'label variable SocioeconStatPers "Estatus socioecomico de la cuidad/distrito en 2017 por persona"\n'
         "gen SocioeconStatHog = 0\n"
         'label variable SocioeconStatHog "Estatus socioecomico de la cuidad/distrito en 2017 por hogar"\n'
         "gen poblacion = 0\n"
         'label variable SocioeconStatHog "Población en la cuidad a 1 de enero de 2019"\n'
         "gen cuidad_grande = 0\n"
         'label variable cuidad_grande "Dummy, 1 si la cuidad se considera grande"\n'
         "********************************************************************************\n"
         "********************************************************************************\n"
         "\n"
         "\n"
         )
    return s


def create_file_tail():
    # Creates the tail for the .do file
    # Includes the dummy cuidad_grande
    s = ("\n"
         "\n"
         "********************************************************************************\n"
         "* Dummy para cuidad_grande si población > 50000\n"
         "********************************************************************************\n"
         "replace cuidad_grande = 1 if poblacion > 50000\n"
         "********************************************************************************\n"
         "********************************************************************************\n"
         "\n"
         "\n"
         )
    return s


def create_institute_block(nombre, cuidad, instituto, persona, hogar, poblacion):
    # print(nombre, "++", cuidad, "++", instituto, "++", persona, "++", hogar)
    rep_cuidad = ''
    rep_instituto = ''
    # Controls for special case in which " is in the name of the school
    if '"' in nombre:
        if cuidad == '1':
            rep_cuidad = 'replace cuidad = 1 if LocalidadInstitutoenelcursas==`"' + nombre + '"\'\n'
        if instituto == '1':
            rep_instituto = 'replace instituto = 1 if LocalidadInstitutoenelcursas==`"' + nombre + '"\'\n'
        rep_persona = 'replace SocioeconStatPers = ' + persona + ' if LocalidadInstitutoenelcursas==`"' + nombre + '"\'\n'
        rep_hogar = 'replace SocioeconStatHog = ' + hogar + ' if LocalidadInstitutoenelcursas==`"' + nombre + '"\'\n'
        rep_poblacion = 'replace poblacion = ' + poblacion + ' if LocalidadInstitutoenelcursas==`"' + nombre + '"\'\n'
    else:
        if cuidad == '1':
            rep_cuidad = 'replace cuidad = 1 if LocalidadInstitutoenelcursas=="' + nombre + '"\n'
        if instituto == '1':
            rep_instituto = 'replace instituto = 1 if LocalidadInstitutoenelcursas=="' + nombre + '"\n'
        rep_persona = 'replace SocioeconStatPers = ' + persona + ' if LocalidadInstitutoenelcursas=="' + nombre + '"\n'
        rep_hogar = 'replace SocioeconStatHog = ' + hogar + ' if LocalidadInstitutoenelcursas=="' + nombre + '"\n'
        rep_poblacion = 'replace poblacion = ' + poblacion + ' if LocalidadInstitutoenelcursas=="' + nombre + '"\n'
    s = (
            "// " + nombre + "\n"
            + rep_cuidad + rep_instituto + rep_persona + rep_hogar + rep_poblacion
    )
    # print(s)
    return s


def create_drop_block(nombre):
    # prints the drop block
    drop_block = 'drop if LocalidadInstitutoenelcursas=="' + nombre + '"\n'
    s = (
            "// " + nombre + "\n"
            + drop_block
    )
    return s


if __name__ == '__main__':
    # Equals 1 if short testing input file, any other number otherwise
    testing = 0
    if testing == 1:
        input_file = 'institutos_short'
        output_file = 'stata/testing.do.do'
    else:
        input_file = 'institutos'
        output_file = 'stata/filter.do'
    process_files(input_file, output_file)
