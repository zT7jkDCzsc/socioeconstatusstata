********************************************************************************
* The python code and the license can be found at:
* https://gitlab.com/zT7jkDCzsc/socioeconstatusstata
* Any comment, clarification or mistake in the data might be addressed to
* javier.vazquez@ua.es
********************************************************************************
* Los datos referentes al estatus socioeconómico fueron obtenidos en la web el 
* INE a nivel de municipio para el 2017:
* https://www.ine.es/experimental/atlas/exp_atlas_tab.htm
* Los datos referentes a la población  fueron obtenidos en la web del INE para el 2019
* https://www.ine.es/nomen2/index.do
* En ocasiones, los datos de población fueron obtenidos de la página de wikipedia 
* en español correspondiente al municipio, la cuál usá a su vez los datos del INE del 
* 2019 referenciados previamente.
********************************************************************************
* Generate variables:
********************************************************************************
gen cuidad = 0
label variable cuidad "El sujeto referencio la cuidad en la que curso el bachillerato"
gen instituto = 0
label variable instituto "El sujeto referencio el instituto en la que curso el bachillerato"
gen SocioeconStatPers = 0
label variable SocioeconStatPers "Estatus socioecomico de la cuidad/distrito en 2017 por persona"
gen SocioeconStatHog = 0
label variable SocioeconStatHog "Estatus socioecomico de la cuidad/distrito en 2017 por hogar"
gen poblacion = 0
label variable SocioeconStatHog "Población en la cuidad a 1 de enero de 2019"
gen cuidad_grande = 0
label variable cuidad_grande "Dummy, 1 si la cuidad se considera grande"
********************************************************************************
********************************************************************************


********************************************************************************
* Assignment by Provinces/Autonomic Cities
********************************************************************************
********************************************************************************
*    A Coruña
********************************************************************************
// A Coruña
// A Coruña
replace cuidad = 1 if LocalidadInstitutoenelcursas=="A Coruña"
replace SocioeconStatPers = 13516 if LocalidadInstitutoenelcursas=="A Coruña"
replace SocioeconStatHog = 32374 if LocalidadInstitutoenelcursas=="A Coruña"
replace poblacion = 245711 if LocalidadInstitutoenelcursas=="A Coruña"
// La Coruña
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Coruña"
replace SocioeconStatPers = 13516 if LocalidadInstitutoenelcursas=="La Coruña"
replace SocioeconStatHog = 32374 if LocalidadInstitutoenelcursas=="La Coruña"
replace poblacion = 245711 if LocalidadInstitutoenelcursas=="La Coruña"
// Colegio Internacional Eiris
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Internacional Eiris"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Internacional Eiris"
replace SocioeconStatPers = 13516 if LocalidadInstitutoenelcursas=="Colegio Internacional Eiris"
replace SocioeconStatHog = 32374 if LocalidadInstitutoenelcursas=="Colegio Internacional Eiris"
replace poblacion = 245711 if LocalidadInstitutoenelcursas=="Colegio Internacional Eiris"
// A Coruña/Montespiño
replace cuidad = 1 if LocalidadInstitutoenelcursas=="A Coruña/Montespiño"
replace instituto = 1 if LocalidadInstitutoenelcursas=="A Coruña/Montespiño"
replace SocioeconStatPers = 13516 if LocalidadInstitutoenelcursas=="A Coruña/Montespiño"
replace SocioeconStatHog = 32374 if LocalidadInstitutoenelcursas=="A Coruña/Montespiño"
replace poblacion = 245711 if LocalidadInstitutoenelcursas=="A Coruña/Montespiño"
// Ferrol
// IES CONCEPCIÓN ARENAL FERROL
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES CONCEPCIÓN ARENAL FERROL"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES CONCEPCIÓN ARENAL FERROL"
replace SocioeconStatPers = 11956 if LocalidadInstitutoenelcursas=="IES CONCEPCIÓN ARENAL FERROL"
replace SocioeconStatHog = 26918 if LocalidadInstitutoenelcursas=="IES CONCEPCIÓN ARENAL FERROL"
replace poblacion = 66065 if LocalidadInstitutoenelcursas=="IES CONCEPCIÓN ARENAL FERROL"
// Oleiros
// Oleiros, A Coruña
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Oleiros, A Coruña"
replace SocioeconStatPers = 15765 if LocalidadInstitutoenelcursas=="Oleiros, A Coruña"
replace SocioeconStatHog = 4223 if LocalidadInstitutoenelcursas=="Oleiros, A Coruña"
replace poblacion = 36075 if LocalidadInstitutoenelcursas=="Oleiros, A Coruña"
// Ribeira
// IES Leliadoura
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Leliadoura"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Leliadoura"
replace SocioeconStatPers = 10135 if LocalidadInstitutoenelcursas=="IES Leliadoura"
replace SocioeconStatHog = 28519 if LocalidadInstitutoenelcursas=="IES Leliadoura"
replace poblacion = 26886 if LocalidadInstitutoenelcursas=="IES Leliadoura"
// Santiago de Compostela
// Santiago de Compostela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santiago de Compostela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santiago de Compostela"
replace SocioeconStatPers = 13517 if LocalidadInstitutoenelcursas=="Santiago de Compostela"
replace SocioeconStatHog = 34706 if LocalidadInstitutoenelcursas=="Santiago de Compostela"
replace poblacion = 97260 if LocalidadInstitutoenelcursas=="Santiago de Compostela"
// La Salle Santiago de Compostela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela"
replace SocioeconStatPers = 13517 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela"
replace SocioeconStatHog = 34706 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela"
replace poblacion = 97260 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela"
// La Salle Santiago de Compostela 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela "
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela "
replace SocioeconStatPers = 13517 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela "
replace SocioeconStatHog = 34706 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela "
replace poblacion = 97260 if LocalidadInstitutoenelcursas=="La Salle Santiago de Compostela "
********************************************************************************
*    Álava
********************************************************************************
// Vitoria Gasteiz
// Vitoria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vitoria"
replace SocioeconStatPers = 14367 if LocalidadInstitutoenelcursas=="Vitoria"
replace SocioeconStatHog = 33632 if LocalidadInstitutoenelcursas=="Vitoria"
replace poblacion = 251774 if LocalidadInstitutoenelcursas=="Vitoria"
// Colegio Sagrado Corazón Corazonistas Vitoria-Gasteuz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Corazonistas Vitoria-Gasteuz"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Corazonistas Vitoria-Gasteuz"
replace SocioeconStatPers = 14367 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Corazonistas Vitoria-Gasteuz"
replace SocioeconStatHog = 33632 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Corazonistas Vitoria-Gasteuz"
replace poblacion = 251774 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Corazonistas Vitoria-Gasteuz"
********************************************************************************
*    Albacete
********************************************************************************
// Albacete
// albacete
replace cuidad = 1 if LocalidadInstitutoenelcursas=="albacete"
replace SocioeconStatPers = 11223 if LocalidadInstitutoenelcursas=="albacete"
replace SocioeconStatHog = 30173 if LocalidadInstitutoenelcursas=="albacete"
replace poblacion = 173329 if LocalidadInstitutoenelcursas=="albacete"
// Albacete
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Albacete"
replace SocioeconStatPers = 11223 if LocalidadInstitutoenelcursas=="Albacete"
replace SocioeconStatHog = 30173 if LocalidadInstitutoenelcursas=="Albacete"
replace poblacion = 173329 if LocalidadInstitutoenelcursas=="Albacete"
// Albacete/CEDES
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Albacete/CEDES"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Albacete/CEDES"
replace SocioeconStatPers = 11223 if LocalidadInstitutoenelcursas=="Albacete/CEDES"
replace SocioeconStatHog = 30173 if LocalidadInstitutoenelcursas=="Albacete/CEDES"
replace poblacion = 173329 if LocalidadInstitutoenelcursas=="Albacete/CEDES"
// Almansa
// Almansa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almansa"
replace SocioeconStatPers = 9776 if LocalidadInstitutoenelcursas=="Almansa"
replace SocioeconStatHog = 25469 if LocalidadInstitutoenelcursas=="Almansa"
replace poblacion = 24419 if LocalidadInstitutoenelcursas=="Almansa"
// Almansa 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almansa "
replace SocioeconStatPers = 9776 if LocalidadInstitutoenelcursas=="Almansa "
replace SocioeconStatHog = 25469 if LocalidadInstitutoenelcursas=="Almansa "
replace poblacion = 24419 if LocalidadInstitutoenelcursas=="Almansa "
// Almansa/IES Herminio Almendros
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
replace SocioeconStatPers = 9776 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
replace SocioeconStatHog = 25469 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
replace poblacion = 24419 if LocalidadInstitutoenelcursas=="Almansa/IES Herminio Almendros"
// IES Herminio Almendros
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Herminio Almendros"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Herminio Almendros"
replace SocioeconStatPers = 9776 if LocalidadInstitutoenelcursas=="IES Herminio Almendros"
replace SocioeconStatHog = 25469 if LocalidadInstitutoenelcursas=="IES Herminio Almendros"
replace poblacion = 24419 if LocalidadInstitutoenelcursas=="IES Herminio Almendros"
// Bonillo, El
// IES Las Sabinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
replace SocioeconStatPers = 8569 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
replace SocioeconStatHog = 21546 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
replace poblacion = 2779 if LocalidadInstitutoenelcursas=="IES Las Sabinas"
// Caudete
// Caudete
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Caudete"
replace SocioeconStatPers = 8603 if LocalidadInstitutoenelcursas=="Caudete"
replace SocioeconStatHog = 22191 if LocalidadInstitutoenelcursas=="Caudete"
replace poblacion = 9963 if LocalidadInstitutoenelcursas=="Caudete"
// IES Pintor Rafael Requena
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Pintor Rafael Requena"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Pintor Rafael Requena"
replace SocioeconStatPers = 8603 if LocalidadInstitutoenelcursas=="IES Pintor Rafael Requena"
replace SocioeconStatHog = 22191 if LocalidadInstitutoenelcursas=="IES Pintor Rafael Requena"
replace poblacion = 9963 if LocalidadInstitutoenelcursas=="IES Pintor Rafael Requena"
// Hellín
// hellin
replace cuidad = 1 if LocalidadInstitutoenelcursas=="hellin"
replace SocioeconStatPers = 8250 if LocalidadInstitutoenelcursas=="hellin"
replace SocioeconStatHog = 22138 if LocalidadInstitutoenelcursas=="hellin"
replace poblacion = 30306 if LocalidadInstitutoenelcursas=="hellin"
// HELLIN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="HELLIN"
replace SocioeconStatPers = 8250 if LocalidadInstitutoenelcursas=="HELLIN"
replace SocioeconStatHog = 22138 if LocalidadInstitutoenelcursas=="HELLIN"
replace poblacion = 30306 if LocalidadInstitutoenelcursas=="HELLIN"
// HELLÍN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="HELLÍN"
replace SocioeconStatPers = 8250 if LocalidadInstitutoenelcursas=="HELLÍN"
replace SocioeconStatHog = 22138 if LocalidadInstitutoenelcursas=="HELLÍN"
replace poblacion = 30306 if LocalidadInstitutoenelcursas=="HELLÍN"
// Hellin/ IES Cristobal Lozano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Hellin/ IES Cristobal Lozano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Hellin/ IES Cristobal Lozano"
replace SocioeconStatPers = 8250 if LocalidadInstitutoenelcursas=="Hellin/ IES Cristobal Lozano"
replace SocioeconStatHog = 22138 if LocalidadInstitutoenelcursas=="Hellin/ IES Cristobal Lozano"
replace poblacion = 30306 if LocalidadInstitutoenelcursas=="Hellin/ IES Cristobal Lozano"
// Pozuelo
// Pozuelo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pozuelo"
replace SocioeconStatPers = 10237 if LocalidadInstitutoenelcursas=="Pozuelo"
replace SocioeconStatHog = 22157 if LocalidadInstitutoenelcursas=="Pozuelo"
replace poblacion = 481 if LocalidadInstitutoenelcursas=="Pozuelo"
// Escolapios pozuelo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escolapios pozuelo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escolapios pozuelo"
replace SocioeconStatPers = 10237 if LocalidadInstitutoenelcursas=="Escolapios pozuelo"
replace SocioeconStatHog = 22157 if LocalidadInstitutoenelcursas=="Escolapios pozuelo"
replace poblacion = 481 if LocalidadInstitutoenelcursas=="Escolapios pozuelo"
// escolapios Pozuelo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="escolapios Pozuelo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="escolapios Pozuelo"
replace SocioeconStatPers = 10237 if LocalidadInstitutoenelcursas=="escolapios Pozuelo"
replace SocioeconStatHog = 22157 if LocalidadInstitutoenelcursas=="escolapios Pozuelo"
replace poblacion = 481 if LocalidadInstitutoenelcursas=="escolapios Pozuelo"
// Monte Tabor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Monte Tabor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Monte Tabor"
replace SocioeconStatPers = 10237 if LocalidadInstitutoenelcursas=="Monte Tabor"
replace SocioeconStatHog = 22157 if LocalidadInstitutoenelcursas=="Monte Tabor"
replace poblacion = 481 if LocalidadInstitutoenelcursas=="Monte Tabor"
// Roda, La
// La Roda (Albacete)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Roda (Albacete)"
replace SocioeconStatPers = 8938 if LocalidadInstitutoenelcursas=="La Roda (Albacete)"
replace SocioeconStatHog = 24154 if LocalidadInstitutoenelcursas=="La Roda (Albacete)"
replace poblacion = 15476 if LocalidadInstitutoenelcursas=="La Roda (Albacete)"
// I.E.S Alarcón Santón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Alarcón Santón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Alarcón Santón"
replace SocioeconStatPers = 8938 if LocalidadInstitutoenelcursas=="I.E.S Alarcón Santón"
replace SocioeconStatHog = 24154 if LocalidadInstitutoenelcursas=="I.E.S Alarcón Santón"
replace poblacion = 15476 if LocalidadInstitutoenelcursas=="I.E.S Alarcón Santón"
// Villarrobledo
// Villarrobledo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villarrobledo"
replace SocioeconStatPers = 8994 if LocalidadInstitutoenelcursas=="Villarrobledo"
replace SocioeconStatHog = 25146 if LocalidadInstitutoenelcursas=="Villarrobledo"
replace poblacion = 25184 if LocalidadInstitutoenelcursas=="Villarrobledo"
// IES Virrey Morcillo (Villarrobledo)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Virrey Morcillo (Villarrobledo)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Virrey Morcillo (Villarrobledo)"
replace SocioeconStatPers = 8994 if LocalidadInstitutoenelcursas=="IES Virrey Morcillo (Villarrobledo)"
replace SocioeconStatHog = 25146 if LocalidadInstitutoenelcursas=="IES Virrey Morcillo (Villarrobledo)"
replace poblacion = 25184 if LocalidadInstitutoenelcursas=="IES Virrey Morcillo (Villarrobledo)"
// ÍES Virrey Morcillo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES Virrey Morcillo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES Virrey Morcillo"
replace SocioeconStatPers = 8994 if LocalidadInstitutoenelcursas=="ÍES Virrey Morcillo"
replace SocioeconStatHog = 25146 if LocalidadInstitutoenelcursas=="ÍES Virrey Morcillo"
replace poblacion = 25184 if LocalidadInstitutoenelcursas=="ÍES Virrey Morcillo"
********************************************************************************
*    Alicante
********************************************************************************
// Albatera
// Albatera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Albatera"
replace SocioeconStatPers = 8157 if LocalidadInstitutoenelcursas=="Albatera"
replace SocioeconStatHog = 22661 if LocalidadInstitutoenelcursas=="Albatera"
replace poblacion = 12279 if LocalidadInstitutoenelcursas=="Albatera"
// albatera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="albatera"
replace SocioeconStatPers = 8157 if LocalidadInstitutoenelcursas=="albatera"
replace SocioeconStatHog = 22661 if LocalidadInstitutoenelcursas=="albatera"
replace poblacion = 12279 if LocalidadInstitutoenelcursas=="albatera"
// IES Antonio Serna Serna (Albatera, Alicante)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
replace SocioeconStatPers = 8157 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
replace SocioeconStatHog = 22661 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
replace poblacion = 12279 if LocalidadInstitutoenelcursas=="IES Antonio Serna Serna (Albatera, Alicante)"
// Cox / Coix
// Cox
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cox"
replace SocioeconStatPers = 8123 if LocalidadInstitutoenelcursas=="Cox"
replace SocioeconStatHog = 23638 if LocalidadInstitutoenelcursas=="Cox"
replace poblacion = 7297 if LocalidadInstitutoenelcursas=="Cox"
// IES COX
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES COX"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES COX"
replace SocioeconStatPers = 8123 if LocalidadInstitutoenelcursas=="IES COX"
replace SocioeconStatHog = 23638 if LocalidadInstitutoenelcursas=="IES COX"
replace poblacion = 7297 if LocalidadInstitutoenelcursas=="IES COX"
// cox/ IES COX
replace cuidad = 1 if LocalidadInstitutoenelcursas=="cox/ IES COX"
replace instituto = 1 if LocalidadInstitutoenelcursas=="cox/ IES COX"
replace SocioeconStatPers = 8123 if LocalidadInstitutoenelcursas=="cox/ IES COX"
replace SocioeconStatHog = 23638 if LocalidadInstitutoenelcursas=="cox/ IES COX"
replace poblacion = 7297 if LocalidadInstitutoenelcursas=="cox/ IES COX"
// IES COX
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES COX"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES COX"
replace SocioeconStatPers = 8123 if LocalidadInstitutoenelcursas=="IES COX"
replace SocioeconStatHog = 23638 if LocalidadInstitutoenelcursas=="IES COX"
replace poblacion = 7297 if LocalidadInstitutoenelcursas=="IES COX"
// IES cox
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES cox"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES cox"
replace SocioeconStatPers = 8123 if LocalidadInstitutoenelcursas=="IES cox"
replace SocioeconStatHog = 23638 if LocalidadInstitutoenelcursas=="IES cox"
replace poblacion = 7297 if LocalidadInstitutoenelcursas=="IES cox"
// Alcoy
// Alcoi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoi"
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoi"
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoi"
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoi"
// Alcoy
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoy"
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoy"
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoy"
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoy"
// Alcoy
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoy"
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoy"
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoy"
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoy"
// Alcoy 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoy "
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoy "
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoy "
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoy "
// Alcoy, La Salle
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoy, La Salle"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcoy, La Salle"
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoy, La Salle"
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoy, La Salle"
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoy, La Salle"
// Alcoy/ IES Cotes Baixes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoy/ IES Cotes Baixes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcoy/ IES Cotes Baixes"
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoy/ IES Cotes Baixes"
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoy/ IES Cotes Baixes"
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoy/ IES Cotes Baixes"
// Alcoy/ Instituto Cotes Baixes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoy/ Instituto Cotes Baixes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcoy/ Instituto Cotes Baixes"
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoy/ Instituto Cotes Baixes"
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoy/ Instituto Cotes Baixes"
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoy/ Instituto Cotes Baixes"
// Alcoy/La Salle
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcoy/La Salle"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcoy/La Salle"
replace SocioeconStatPers = 10288 if LocalidadInstitutoenelcursas=="Alcoy/La Salle"
replace SocioeconStatHog = 25190 if LocalidadInstitutoenelcursas=="Alcoy/La Salle"
replace poblacion = 58994 if LocalidadInstitutoenelcursas=="Alcoy/La Salle"
// Alfaz del Pi / Alfàs del Pi
// IES l'arabí
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES l'arabí"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES l'arabí"
replace SocioeconStatPers = 8661 if LocalidadInstitutoenelcursas=="IES l'arabí"
replace SocioeconStatHog = 23291 if LocalidadInstitutoenelcursas=="IES l'arabí"
replace poblacion = 20482 if LocalidadInstitutoenelcursas=="IES l'arabí"
// Alicante
// Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante"
// alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="alicante"
// alicante 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="alicante "
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="alicante "
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="alicante "
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="alicante "
// ALICANTE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ALICANTE"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="ALICANTE"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="ALICANTE"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="ALICANTE"
// Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante"
// Alicanye
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicanye"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicanye"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicanye"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicanye"
// Alicante 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante "
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante "
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante "
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante "
// Inmaculada Jesuitas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas"
// Salesianos Don Bosco de Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesianos Don Bosco de Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesianos Don Bosco de Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Salesianos Don Bosco de Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Salesianos Don Bosco de Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Salesianos Don Bosco de Alicante"
// Maristas Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Maristas Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Maristas Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Maristas Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Maristas Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Maristas Alicante"
// ceu jesus maria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ceu jesus maria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ceu jesus maria"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="ceu jesus maria"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="ceu jesus maria"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="ceu jesus maria"
// ceu jesús maría
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ceu jesús maría"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ceu jesús maría"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="ceu jesús maría"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="ceu jesús maría"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="ceu jesús maría"
// Bahia de Babel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Bahia de Babel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Bahia de Babel"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Bahia de Babel"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Bahia de Babel"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Bahia de Babel"
// Alicante y el IES JAIME II
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante y el IES JAIME II"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante y el IES JAIME II"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante y el IES JAIME II"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante y el IES JAIME II"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante y el IES JAIME II"
// Santa Teresa- Vistahermosa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Teresa- Vistahermosa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Teresa- Vistahermosa"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Santa Teresa- Vistahermosa"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Santa Teresa- Vistahermosa"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Santa Teresa- Vistahermosa"
// San Jose de Carolinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Jose de Carolinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Jose de Carolinas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="San Jose de Carolinas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="San Jose de Carolinas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="San Jose de Carolinas"
// San José de Carolinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San José de Carolinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San José de Carolinas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="San José de Carolinas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="San José de Carolinas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="San José de Carolinas"
// San Agustín, Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Agustín, Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Agustín, Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="San Agustín, Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="San Agustín, Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="San Agustín, Alicante"
// Alicante, Colegio Santa Teresa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, Colegio Santa Teresa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, Colegio Santa Teresa"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, Colegio Santa Teresa"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, Colegio Santa Teresa"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, Colegio Santa Teresa"
// Alicante, Figueras Pacheco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, Figueras Pacheco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, Figueras Pacheco"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, Figueras Pacheco"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, Figueras Pacheco"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, Figueras Pacheco"
// Alicante, IES Cabo de la Huerta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, IES Cabo de la Huerta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, IES Cabo de la Huerta"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, IES Cabo de la Huerta"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, IES Cabo de la Huerta"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, IES Cabo de la Huerta"
// Alicante, San José de Carolinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, San José de Carolinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, San José de Carolinas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, San José de Carolinas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, San José de Carolinas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, San José de Carolinas"
// Alicante/8 de marzo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/8 de marzo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/8 de marzo"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/8 de marzo"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/8 de marzo"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/8 de marzo"
// Alicante/I.E.S Doctor Balmis
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/I.E.S Doctor Balmis"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/I.E.S Doctor Balmis"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/I.E.S Doctor Balmis"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/I.E.S Doctor Balmis"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/I.E.S Doctor Balmis"
// IES DOCTOR BALMIS
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES DOCTOR BALMIS"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES DOCTOR BALMIS"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES DOCTOR BALMIS"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES DOCTOR BALMIS"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES DOCTOR BALMIS"
// Alicante/IES Cabo de la Huerta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/IES Cabo de la Huerta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/IES Cabo de la Huerta"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/IES Cabo de la Huerta"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/IES Cabo de la Huerta"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/IES Cabo de la Huerta"
// Alicante/IES Miguel Hernández
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández"
// Alicante/IES Miguel Hernández 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández "
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández "
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández "
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/IES Miguel Hernández "
// Alicante/IES San Blas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/IES San Blas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/IES San Blas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/IES San Blas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/IES San Blas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/IES San Blas"
// Alicante/San José de Carolinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/San José de Carolinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/San José de Carolinas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/San José de Carolinas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/San José de Carolinas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/San José de Carolinas"
// Alicante/Virgen del Remedio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/Virgen del Remedio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/Virgen del Remedio"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/Virgen del Remedio"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/Virgen del Remedio"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/Virgen del Remedio"
// CEU JESÚS Y MARÍA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="CEU JESÚS Y MARÍA"
// CEU Jesús María Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CEU Jesús María Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="CEU Jesús María Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="CEU Jesús María Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="CEU Jesús María Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="CEU Jesús María Alicante"
// Alicante/Colegio Altozano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/Colegio Altozano"
// IES cavanilles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES cavanilles"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES cavanilles"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES cavanilles"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES cavanilles"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES cavanilles"
// Antonio José Cavanilles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles"
// Antonio José Cavanilles 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles "
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles "
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles "
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Antonio José Cavanilles "
// Colegio Inmaculada Jesuitas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Colegio Inmaculada Jesuitas"
// Alicante / Colegio San José de Carolinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante / Colegio San José de Carolinas"
// Alicante/Colegio Sagrado Corazón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/Colegio Sagrado Corazón"
// IES Figueras Pacheco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES Figueras Pacheco"
// Alicante/Figueras Pacheco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante/Figueras Pacheco"
// liceo francés internacional alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="liceo francés internacional alicante"
// IES 8 de marzo Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES 8 de marzo Alicante"
// Liceo Francés Internacional de Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Liceo Francés Internacional de Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Liceo Francés Internacional de Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Liceo Francés Internacional de Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Liceo Francés Internacional de Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Liceo Francés Internacional de Alicante"
// Liceo Francés de Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Liceo Francés de Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Liceo Francés de Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Liceo Francés de Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Liceo Francés de Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Liceo Francés de Alicante"
// IES Mare Nostrum Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES Mare Nostrum Alicante"
// Alicante, Ies Mare Nostrum
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
// Alicante, Ies Mare Nostrum
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, Ies Mare Nostrum"
// Maristas de Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Maristas de Alicante"
// Alicante, colegio HH Maristas sagrado corazón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Alicante, colegio HH Maristas sagrado corazón"
// Salesianos - Don Bosco, Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Salesianos - Don Bosco, Alicante"
// Inmaculada Jesuitas Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Inmaculada Jesuitas Alicante"
// I.E.S Cabo de la Huerta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="I.E.S Cabo de la Huerta"
// ies cabo de las huertas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="ies cabo de las huertas"
// IES SAN BLAS
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES SAN BLAS"
// IES San Blay y HH Maristas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES San Blay y HH Maristas"
// IES Virgen del Remedio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES Virgen del Remedio"
// Agustinos Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Agustinos Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Agustinos Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Agustinos Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Agustinos Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Agustinos Alicante"
// Agustinos Alicante 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Agustinos Alicante "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Agustinos Alicante "
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Agustinos Alicante "
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Agustinos Alicante "
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Agustinos Alicante "
// COLEGIO SAN AGUSTÍN ALICANTE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="COLEGIO SAN AGUSTÍN ALICANTE"
replace instituto = 1 if LocalidadInstitutoenelcursas=="COLEGIO SAN AGUSTÍN ALICANTE"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="COLEGIO SAN AGUSTÍN ALICANTE"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="COLEGIO SAN AGUSTÍN ALICANTE"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="COLEGIO SAN AGUSTÍN ALICANTE"
// Ceu Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ceu Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ceu Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Ceu Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Ceu Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Ceu Alicante"
// Colegio "El Valle", Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas==`"Colegio "El Valle", Alicante"'
replace instituto = 1 if LocalidadInstitutoenelcursas==`"Colegio "El Valle", Alicante"'
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas==`"Colegio "El Valle", Alicante"'
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas==`"Colegio "El Valle", Alicante"'
replace poblacion = 334887 if LocalidadInstitutoenelcursas==`"Colegio "El Valle", Alicante"'
// Colegio Aitana
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Aitana"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Aitana"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Colegio Aitana"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Colegio Aitana"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Colegio Aitana"
// Colegio Calasancio Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Calasancio Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Calasancio Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Colegio Calasancio Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Colegio Calasancio Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Colegio Calasancio Alicante"
// Colegio Diocesano San José de Carolinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Diocesano San José de Carolinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Diocesano San José de Carolinas"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Colegio Diocesano San José de Carolinas"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Colegio Diocesano San José de Carolinas"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Colegio Diocesano San José de Carolinas"
// Colegio de Fomento Altozano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio de Fomento Altozano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio de Fomento Altozano"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Colegio de Fomento Altozano"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Colegio de Fomento Altozano"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Colegio de Fomento Altozano"
// Don Bosco salesianos alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Don Bosco salesianos alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Don Bosco salesianos alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Don Bosco salesianos alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Don Bosco salesianos alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Don Bosco salesianos alicante"
// El Pla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Pla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="El Pla"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="El Pla"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="El Pla"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="El Pla"
// IES El pla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES El pla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES El pla"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES El pla"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES El pla"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES El pla"
// Escuela Europea de Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escuela Europea de Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escuela Europea de Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Escuela Europea de Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Escuela Europea de Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Escuela Europea de Alicante"
// Gran via
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gran via"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Gran via"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Gran via"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Gran via"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Gran via"
// IES Playa San Juan
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Playa San Juan"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Playa San Juan"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES Playa San Juan"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES Playa San Juan"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES Playa San Juan"
// IES PLAYA SAN JUAN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES PLAYA SAN JUAN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES PLAYA SAN JUAN"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES PLAYA SAN JUAN"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES PLAYA SAN JUAN"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES PLAYA SAN JUAN"
// SAN JUAN DE ALICANTE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="SAN JUAN DE ALICANTE"
replace instituto = 1 if LocalidadInstitutoenelcursas=="SAN JUAN DE ALICANTE"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="SAN JUAN DE ALICANTE"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="SAN JUAN DE ALICANTE"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="SAN JUAN DE ALICANTE"
// San juan playa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San juan playa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San juan playa"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="San juan playa"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="San juan playa"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="San juan playa"
// alicante/ IES radio exterior
replace cuidad = 1 if LocalidadInstitutoenelcursas=="alicante/ IES radio exterior"
replace instituto = 1 if LocalidadInstitutoenelcursas=="alicante/ IES radio exterior"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="alicante/ IES radio exterior"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="alicante/ IES radio exterior"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="alicante/ IES radio exterior"
// San Juan, Alicante/IES RADIO EXTERIOR
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Juan, Alicante/IES RADIO EXTERIOR"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Juan, Alicante/IES RADIO EXTERIOR"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="San Juan, Alicante/IES RADIO EXTERIOR"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="San Juan, Alicante/IES RADIO EXTERIOR"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="San Juan, Alicante/IES RADIO EXTERIOR"
// Salesianos Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesianos Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesianos Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Salesianos Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Salesianos Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Salesianos Alicante"
// Santa María Del Carmen (Carmelitas de Alicante)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa María Del Carmen (Carmelitas de Alicante)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa María Del Carmen (Carmelitas de Alicante)"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="Santa María Del Carmen (Carmelitas de Alicante)"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="Santa María Del Carmen (Carmelitas de Alicante)"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="Santa María Del Carmen (Carmelitas de Alicante)"
// IES Miguel Hernández, Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Miguel Hernández, Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Miguel Hernández, Alicante"
replace SocioeconStatPers = 10802 if LocalidadInstitutoenelcursas=="IES Miguel Hernández, Alicante"
replace SocioeconStatHog = 27759 if LocalidadInstitutoenelcursas=="IES Miguel Hernández, Alicante"
replace poblacion = 334887 if LocalidadInstitutoenelcursas=="IES Miguel Hernández, Alicante"
// Almoradí
// Almoradi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almoradi"
replace SocioeconStatPers = 7485 if LocalidadInstitutoenelcursas=="Almoradi"
replace SocioeconStatHog = 21214 if LocalidadInstitutoenelcursas=="Almoradi"
replace poblacion = 20803 if LocalidadInstitutoenelcursas=="Almoradi"
// ALMORADÍ IES ANTONIO SEQUEROS
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ALMORADÍ IES ANTONIO SEQUEROS"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ALMORADÍ IES ANTONIO SEQUEROS"
replace SocioeconStatPers = 7485 if LocalidadInstitutoenelcursas=="ALMORADÍ IES ANTONIO SEQUEROS"
replace SocioeconStatHog = 21214 if LocalidadInstitutoenelcursas=="ALMORADÍ IES ANTONIO SEQUEROS"
replace poblacion = 20803 if LocalidadInstitutoenelcursas=="ALMORADÍ IES ANTONIO SEQUEROS"
// I.E.S Antonio Sequeros (Almoradí)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Antonio Sequeros (Almoradí)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Antonio Sequeros (Almoradí)"
replace SocioeconStatPers = 7485 if LocalidadInstitutoenelcursas=="I.E.S Antonio Sequeros (Almoradí)"
replace SocioeconStatHog = 21214 if LocalidadInstitutoenelcursas=="I.E.S Antonio Sequeros (Almoradí)"
replace poblacion = 20803 if LocalidadInstitutoenelcursas=="I.E.S Antonio Sequeros (Almoradí)"
// Altea
// ALTEA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ALTEA"
replace SocioeconStatPers = 9560 if LocalidadInstitutoenelcursas=="ALTEA"
replace SocioeconStatHog = 24744 if LocalidadInstitutoenelcursas=="ALTEA"
replace poblacion = 22290 if LocalidadInstitutoenelcursas=="ALTEA"
// Altea
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Altea"
replace SocioeconStatPers = 9560 if LocalidadInstitutoenelcursas=="Altea"
replace SocioeconStatHog = 24744 if LocalidadInstitutoenelcursas=="Altea"
replace poblacion = 2229 if LocalidadInstitutoenelcursas=="Altea"
// Altea, IES Bellaguarda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Altea, IES Bellaguarda"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Altea, IES Bellaguarda"
replace SocioeconStatPers = 9560 if LocalidadInstitutoenelcursas=="Altea, IES Bellaguarda"
replace SocioeconStatHog = 24744 if LocalidadInstitutoenelcursas=="Altea, IES Bellaguarda"
replace poblacion = 2229 if LocalidadInstitutoenelcursas=="Altea, IES Bellaguarda"
// IES ALTAIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES ALTAIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES ALTAIA"
replace SocioeconStatPers = 9560 if LocalidadInstitutoenelcursas=="IES ALTAIA"
replace SocioeconStatHog = 24744 if LocalidadInstitutoenelcursas=="IES ALTAIA"
replace poblacion = 2229 if LocalidadInstitutoenelcursas=="IES ALTAIA"
// IES Bellaguarda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Bellaguarda"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Bellaguarda"
replace SocioeconStatPers = 9560 if LocalidadInstitutoenelcursas=="IES Bellaguarda"
replace SocioeconStatHog = 24744 if LocalidadInstitutoenelcursas=="IES Bellaguarda"
replace poblacion = 2229 if LocalidadInstitutoenelcursas=="IES Bellaguarda"
// Aspe
// ASPE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ASPE"
replace SocioeconStatPers = 8073 if LocalidadInstitutoenelcursas=="ASPE"
replace SocioeconStatHog = 21893 if LocalidadInstitutoenelcursas=="ASPE"
replace poblacion = 20714 if LocalidadInstitutoenelcursas=="ASPE"
// IES VILLA DE ASPE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES VILLA DE ASPE"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES VILLA DE ASPE"
replace SocioeconStatPers = 8073 if LocalidadInstitutoenelcursas=="IES VILLA DE ASPE"
replace SocioeconStatHog = 21893 if LocalidadInstitutoenelcursas=="IES VILLA DE ASPE"
replace poblacion = 20714 if LocalidadInstitutoenelcursas=="IES VILLA DE ASPE"
// Banyeres de Mariola
// Banyeres de mariola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Banyeres de mariola"
replace SocioeconStatPers = 10364 if LocalidadInstitutoenelcursas=="Banyeres de mariola"
replace SocioeconStatHog = 25903 if LocalidadInstitutoenelcursas=="Banyeres de mariola"
replace poblacion = 7068 if LocalidadInstitutoenelcursas=="Banyeres de mariola"
// Banyeres de mariola 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Banyeres de mariola "
replace SocioeconStatPers = 10364 if LocalidadInstitutoenelcursas=="Banyeres de mariola "
replace SocioeconStatHog = 25903 if LocalidadInstitutoenelcursas=="Banyeres de mariola "
replace poblacion = 7068 if LocalidadInstitutoenelcursas=="Banyeres de mariola "
// Banyeres de Mariola / IES Professor Manuel Broseta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Banyeres de Mariola / IES Professor Manuel Broseta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Banyeres de Mariola / IES Professor Manuel Broseta"
replace SocioeconStatPers = 10364 if LocalidadInstitutoenelcursas=="Banyeres de Mariola / IES Professor Manuel Broseta"
replace SocioeconStatHog = 25903 if LocalidadInstitutoenelcursas=="Banyeres de Mariola / IES Professor Manuel Broseta"
replace poblacion = 7068 if LocalidadInstitutoenelcursas=="Banyeres de Mariola / IES Professor Manuel Broseta"
// IES Professor Manuel Broseta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
replace SocioeconStatPers = 10364 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
replace SocioeconStatHog = 25903 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
replace poblacion = 7068 if LocalidadInstitutoenelcursas=="IES Professor Manuel Broseta"
// Benejúzar
// IES Benejúzar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Benejúzar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Benejúzar"
replace SocioeconStatPers = 8130 if LocalidadInstitutoenelcursas=="IES Benejúzar"
replace SocioeconStatHog = 22875 if LocalidadInstitutoenelcursas=="IES Benejúzar"
replace poblacion = 5402 if LocalidadInstitutoenelcursas=="IES Benejúzar"
// Benidorm
// Benidorm
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Benidorm"
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="Benidorm"
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="Benidorm"
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="Benidorm"
// Benidorm
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Benidorm"
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="Benidorm"
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="Benidorm"
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="Benidorm"
// BENIDORM
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BENIDORM"
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="BENIDORM"
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="BENIDORM"
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="BENIDORM"
// Benidorm 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Benidorm "
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="Benidorm "
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="Benidorm "
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="Benidorm "
// BENIDORM,ALICANTE. IES BERNAT DE SARRIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BENIDORM,ALICANTE. IES BERNAT DE SARRIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="BENIDORM,ALICANTE. IES BERNAT DE SARRIA"
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="BENIDORM,ALICANTE. IES BERNAT DE SARRIA"
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="BENIDORM,ALICANTE. IES BERNAT DE SARRIA"
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="BENIDORM,ALICANTE. IES BERNAT DE SARRIA"
// IES Mediterrania
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Mediterrania"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Mediterrania"
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="IES Mediterrania"
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="IES Mediterrania"
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="IES Mediterrania"
// IES L'ALMADRAVA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES L'ALMADRAVA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES L'ALMADRAVA"
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="IES L'ALMADRAVA"
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="IES L'ALMADRAVA"
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="IES L'ALMADRAVA"
// IES PERE MARIA ORTS I BOSCH
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES PERE MARIA ORTS I BOSCH"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES PERE MARIA ORTS I BOSCH"
replace SocioeconStatPers = 9724 if LocalidadInstitutoenelcursas=="IES PERE MARIA ORTS I BOSCH"
replace SocioeconStatHog = 22793 if LocalidadInstitutoenelcursas=="IES PERE MARIA ORTS I BOSCH"
replace poblacion = 68721 if LocalidadInstitutoenelcursas=="IES PERE MARIA ORTS I BOSCH"
// Benissa
// Benissa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Benissa"
replace SocioeconStatPers = 9047 if LocalidadInstitutoenelcursas=="Benissa"
replace SocioeconStatHog = 22650 if LocalidadInstitutoenelcursas=="Benissa"
replace poblacion = 11005 if LocalidadInstitutoenelcursas=="Benissa"
// Biar
// BIAR
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BIAR"
replace SocioeconStatPers = 10290 if LocalidadInstitutoenelcursas=="BIAR"
replace SocioeconStatHog = 25669 if LocalidadInstitutoenelcursas=="BIAR"
replace poblacion = 3671 if LocalidadInstitutoenelcursas=="BIAR"
// Biar/ IES Biar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Biar/ IES Biar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Biar/ IES Biar"
replace SocioeconStatPers = 10290 if LocalidadInstitutoenelcursas=="Biar/ IES Biar"
replace SocioeconStatHog = 25669 if LocalidadInstitutoenelcursas=="Biar/ IES Biar"
replace poblacion = 3671 if LocalidadInstitutoenelcursas=="Biar/ IES Biar"
//Bigastro
// IES Paco Ruiz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
replace SocioeconStatPers = 8341 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
replace SocioeconStatHog = 23348 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
replace poblacion = 6733 if LocalidadInstitutoenelcursas=="IES Paco Ruiz"
// Callosa de Ensarriá / Callosa d'en Sarrià
// Callosa d'en Sarrià/Colegio Almedia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Callosa d'en Sarrià/Colegio Almedia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Callosa d'en Sarrià/Colegio Almedia"
replace SocioeconStatPers = 8751 if LocalidadInstitutoenelcursas=="Callosa d'en Sarrià/Colegio Almedia"
replace SocioeconStatHog = 23627 if LocalidadInstitutoenelcursas=="Callosa d'en Sarrià/Colegio Almedia"
replace poblacion = 7373 if LocalidadInstitutoenelcursas=="Callosa d'en Sarrià/Colegio Almedia"
// Almedia, Callosa d'En Sarrià
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià"
replace SocioeconStatPers = 8751 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià"
replace SocioeconStatHog = 23627 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià"
replace poblacion = 7373 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià"
// Almedia, Callosa d'En Sarrià 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià "
replace SocioeconStatPers = 8751 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià "
replace SocioeconStatHog = 23627 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià "
replace poblacion = 7373 if LocalidadInstitutoenelcursas=="Almedia, Callosa d'En Sarrià "
// Callosa de segura
// callosa de seguro
replace cuidad = 1 if LocalidadInstitutoenelcursas=="callosa de seguro"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="callosa de seguro"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="callosa de seguro"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="callosa de seguro"
// Callosa de segura
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Callosa de segura"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="Callosa de segura"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="Callosa de segura"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="Callosa de segura"
// Callosa de Segura
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Callosa de Segura"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="Callosa de Segura"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="Callosa de Segura"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="Callosa de Segura"
// Callosa de Segura, IES Vega Baja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="Callosa de Segura, IES Vega Baja"
// IES Vega Baja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Vega Baja"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Vega Baja"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="IES Vega Baja"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="IES Vega Baja"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="IES Vega Baja"
// CALLOSA DE SEGURA IES VEGA BAJA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CALLOSA DE SEGURA IES VEGA BAJA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="CALLOSA DE SEGURA IES VEGA BAJA"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="CALLOSA DE SEGURA IES VEGA BAJA"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="CALLOSA DE SEGURA IES VEGA BAJA"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="CALLOSA DE SEGURA IES VEGA BAJA"
// Ies Santiago grisolia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Santiago grisolia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Santiago grisolia"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="Ies Santiago grisolia"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="Ies Santiago grisolia"
replace poblacion = 19038 if LocalidadInstitutoenelcursas=="Ies Santiago grisolia"
// Calpe
// Calpe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Calpe"
replace SocioeconStatPers = 8663 if LocalidadInstitutoenelcursas=="Calpe"
replace SocioeconStatHog = 22369 if LocalidadInstitutoenelcursas=="Calpe"
replace poblacion = 22725 if LocalidadInstitutoenelcursas=="Calpe"
// Calpe/ Ies Ifach
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Calpe/ Ies Ifach"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Calpe/ Ies Ifach"
replace SocioeconStatPers = 8663 if LocalidadInstitutoenelcursas=="Calpe/ Ies Ifach"
replace SocioeconStatHog = 22369 if LocalidadInstitutoenelcursas=="Calpe/ Ies Ifach"
replace poblacion = 22725 if LocalidadInstitutoenelcursas=="Calpe/ Ies Ifach"
// Campello, El
// El Campello
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Campello"
replace SocioeconStatPers = 10850 if LocalidadInstitutoenelcursas=="El Campello"
replace SocioeconStatHog = 26650 if LocalidadInstitutoenelcursas=="El Campello"
replace poblacion = 28349 if LocalidadInstitutoenelcursas=="El Campello"
// El Campello, Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Campello, Alicante"
replace SocioeconStatPers = 10850 if LocalidadInstitutoenelcursas=="El Campello, Alicante"
replace SocioeconStatHog = 26650 if LocalidadInstitutoenelcursas=="El Campello, Alicante"
replace poblacion = 28349 if LocalidadInstitutoenelcursas=="El Campello, Alicante"
// El Campello. I.E.S Enric Valor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
replace SocioeconStatPers = 10850 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
replace SocioeconStatHog = 26650 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
replace poblacion = 28349 if LocalidadInstitutoenelcursas=="El Campello. I.E.S Enric Valor"
// Clot del Illot
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Clot del Illot"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Clot del Illot"
replace SocioeconStatPers = 10850 if LocalidadInstitutoenelcursas=="Clot del Illot"
replace SocioeconStatHog = 26650 if LocalidadInstitutoenelcursas=="Clot del Illot"
replace poblacion = 28349 if LocalidadInstitutoenelcursas=="Clot del Illot"
// I.E.S Enric Valor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Enric Valor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Enric Valor"
replace SocioeconStatPers = 10850 if LocalidadInstitutoenelcursas=="I.E.S Enric Valor"
replace SocioeconStatHog = 26650 if LocalidadInstitutoenelcursas=="I.E.S Enric Valor"
replace poblacion = 28349 if LocalidadInstitutoenelcursas=="I.E.S Enric Valor"
// IES CLOT DE L'ILLOT
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES CLOT DE L'ILLOT"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES CLOT DE L'ILLOT"
replace SocioeconStatPers = 10850 if LocalidadInstitutoenelcursas=="IES CLOT DE L'ILLOT"
replace SocioeconStatHog = 26650 if LocalidadInstitutoenelcursas=="IES CLOT DE L'ILLOT"
replace poblacion = 28349 if LocalidadInstitutoenelcursas=="IES CLOT DE L'ILLOT"
// Castalla
// Castalla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Castalla"
replace SocioeconStatPers = 9597 if LocalidadInstitutoenelcursas=="Castalla"
replace SocioeconStatHog = 23657 if LocalidadInstitutoenelcursas=="Castalla"
replace poblacion = 10124 if LocalidadInstitutoenelcursas=="Castalla"
// Castalla 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Castalla "
replace SocioeconStatPers = 9597 if LocalidadInstitutoenelcursas=="Castalla "
replace SocioeconStatHog = 23657 if LocalidadInstitutoenelcursas=="Castalla "
replace poblacion = 10124 if LocalidadInstitutoenelcursas=="Castalla "
// Catral
// Catral
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Catral"
replace SocioeconStatPers = 7401 if LocalidadInstitutoenelcursas=="Catral"
replace SocioeconStatHog = 20415 if LocalidadInstitutoenelcursas=="Catral"
replace poblacion = 8639 if LocalidadInstitutoenelcursas=="Catral"
// catral
replace cuidad = 1 if LocalidadInstitutoenelcursas=="catral"
replace SocioeconStatPers = 7401 if LocalidadInstitutoenelcursas=="catral"
replace SocioeconStatHog = 20415 if LocalidadInstitutoenelcursas=="catral"
replace poblacion = 8639 if LocalidadInstitutoenelcursas=="catral"
// Catral-Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Catral-Alicante"
replace SocioeconStatPers = 7401 if LocalidadInstitutoenelcursas=="Catral-Alicante"
replace SocioeconStatHog = 20415 if LocalidadInstitutoenelcursas=="Catral-Alicante"
replace poblacion = 8639 if LocalidadInstitutoenelcursas=="Catral-Alicante"
// IES Catral
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Catral"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Catral"
replace SocioeconStatPers = 7401 if LocalidadInstitutoenelcursas=="IES Catral"
replace SocioeconStatHog = 20415 if LocalidadInstitutoenelcursas=="IES Catral"
replace poblacion = 8639 if LocalidadInstitutoenelcursas=="IES Catral"
// Cocentaina
// Cocentaina
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cocentaina"
replace SocioeconStatPers = 9623 if LocalidadInstitutoenelcursas=="Cocentaina"
replace SocioeconStatHog = 24800 if LocalidadInstitutoenelcursas=="Cocentaina"
replace poblacion = 11511 if LocalidadInstitutoenelcursas=="Cocentaina"
// cocentaina
replace cuidad = 1 if LocalidadInstitutoenelcursas=="cocentaina"
replace SocioeconStatPers = 9623 if LocalidadInstitutoenelcursas=="cocentaina"
replace SocioeconStatHog = 24800 if LocalidadInstitutoenelcursas=="cocentaina"
replace poblacion = 11511 if LocalidadInstitutoenelcursas=="cocentaina"
// Crevillente
// crevillent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="crevillent"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="crevillent"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="crevillent"
replace poblacion = 28952 if LocalidadInstitutoenelcursas=="crevillent"
// Crevillente IES Maciá Abela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
replace poblacion = 28952 if LocalidadInstitutoenelcursas=="Crevillente IES Maciá Abela"
// IES Macià Abela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Macià Abela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Macià Abela"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="IES Macià Abela"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="IES Macià Abela"
replace poblacion = 28952 if LocalidadInstitutoenelcursas=="IES Macià Abela"
// IES Macia Abela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Macia Abela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Macia Abela"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="IES Macia Abela"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="IES Macia Abela"
replace poblacion = 28952 if LocalidadInstitutoenelcursas=="IES Macia Abela"
// Maciá Abela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Maciá Abela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Maciá Abela"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="Maciá Abela"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="Maciá Abela"
replace poblacion = 28952 if LocalidadInstitutoenelcursas=="Maciá Abela"
// IES Maciá Abela Crevillent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Maciá Abela Crevillent"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Maciá Abela Crevillent"
replace SocioeconStatPers = 7849 if LocalidadInstitutoenelcursas=="IES Maciá Abela Crevillent"
replace SocioeconStatHog = 21774 if LocalidadInstitutoenelcursas=="IES Maciá Abela Crevillent"
replace poblacion = 28952 if LocalidadInstitutoenelcursas=="IES Maciá Abela Crevillent"
// Denia / Dénia
// Denia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Denia"
replace SocioeconStatPers = 9675 if LocalidadInstitutoenelcursas=="Denia"
replace SocioeconStatHog = 24734 if LocalidadInstitutoenelcursas=="Denia"
replace poblacion = 42166 if LocalidadInstitutoenelcursas=="Denia"
// Dénia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Dénia"
replace SocioeconStatPers = 9675 if LocalidadInstitutoenelcursas=="Dénia"
replace SocioeconStatHog = 24734 if LocalidadInstitutoenelcursas=="Dénia"
replace poblacion = 42166 if LocalidadInstitutoenelcursas=="Dénia"
// Dolores
// Dolores
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Dolores"
replace SocioeconStatPers = 7222 if LocalidadInstitutoenelcursas=="Dolores"
replace SocioeconStatHog = 19287 if LocalidadInstitutoenelcursas=="Dolores"
replace poblacion = 7470 if LocalidadInstitutoenelcursas=="Dolores"
// Dolores (Alicante)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Dolores (Alicante)"
replace SocioeconStatPers = 7222 if LocalidadInstitutoenelcursas=="Dolores (Alicante)"
replace SocioeconStatHog = 19287 if LocalidadInstitutoenelcursas=="Dolores (Alicante)"
replace poblacion = 7470 if LocalidadInstitutoenelcursas=="Dolores (Alicante)"
// Elche
// Elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche"
// elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="elche"
// elche 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="elche "
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="elche "
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="elche "
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="elche "
// ELCHE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ELCHE"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="ELCHE"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="ELCHE"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="ELCHE"
// Elche/IES Carrus
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/IES Carrus"
// IES CARRUS
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES CARRUS"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES CARRUS"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="IES CARRUS"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="IES CARRUS"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="IES CARRUS"
// Elche/ IES Carrús
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/ IES Carrús"
// elche/carrus
replace cuidad = 1 if LocalidadInstitutoenelcursas=="elche/carrus"
replace instituto = 1 if LocalidadInstitutoenelcursas=="elche/carrus"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="elche/carrus"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="elche/carrus"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="elche/carrus"
// colegio la devesa Elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="colegio la devesa Elche"
// la devesa school elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="la devesa school elche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="la devesa school elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="la devesa school elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="la devesa school elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="la devesa school elche"
// IES Joanot Martorell
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="IES Joanot Martorell"
// La Foia, Elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Foia, Elche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Foia, Elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="La Foia, Elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="La Foia, Elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="La Foia, Elche"
// Elche/ IES Misteri d'Elx
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/ IES Misteri d'Elx"
// Elche/ Victoria Kent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/ Victoria Kent"
// Elche, IES Victoria Kent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche, IES Victoria Kent"
// Santa María-Jesuitinas de Elche, Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Santa María-Jesuitinas de Elche, Alicante"
// Sixto Marco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sixto Marco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sixto Marco"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Sixto Marco"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Sixto Marco"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Sixto Marco"
// ELCHE / NTRA. SRA. DEL CARMEN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ELCHE / NTRA. SRA. DEL CARMEN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ELCHE / NTRA. SRA. DEL CARMEN"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="ELCHE / NTRA. SRA. DEL CARMEN"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="ELCHE / NTRA. SRA. DEL CARMEN"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="ELCHE / NTRA. SRA. DEL CARMEN"
//  ELCHE | NUESTRA SEÑORA DEL CARMEN 
replace cuidad = 1 if LocalidadInstitutoenelcursas==" ELCHE | NUESTRA SEÑORA DEL CARMEN "
replace instituto = 1 if LocalidadInstitutoenelcursas==" ELCHE | NUESTRA SEÑORA DEL CARMEN "
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas==" ELCHE | NUESTRA SEÑORA DEL CARMEN "
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas==" ELCHE | NUESTRA SEÑORA DEL CARMEN "
replace poblacion = 232517 if LocalidadInstitutoenelcursas==" ELCHE | NUESTRA SEÑORA DEL CARMEN "
// Elche (Ies la Asunción)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche (Ies la Asunción)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche (Ies la Asunción)"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche (Ies la Asunción)"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche (Ies la Asunción)"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche (Ies la Asunción)"
// Ies la Asunción
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies la Asunción"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies la Asunción"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Ies la Asunción"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Ies la Asunción"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Ies la Asunción"
// Elche, Nuestra Señora del Carmen
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche, Nuestra Señora del Carmen"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche, Nuestra Señora del Carmen"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche, Nuestra Señora del Carmen"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche, Nuestra Señora del Carmen"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche, Nuestra Señora del Carmen"
// Elche/ Carmelitas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/ Carmelitas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/ Carmelitas"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/ Carmelitas"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/ Carmelitas"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/ Carmelitas"
// Elche/ IES LA ASUNCIÓN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/ IES LA ASUNCIÓN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/ IES LA ASUNCIÓN"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/ IES LA ASUNCIÓN"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/ IES LA ASUNCIÓN"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/ IES LA ASUNCIÓN"
// Elche/Salesianos San José Artesano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elche/Salesianos San José Artesano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elche/Salesianos San José Artesano"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Elche/Salesianos San José Artesano"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Elche/Salesianos San José Artesano"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Elche/Salesianos San José Artesano"
// ELCHE | NUESTRA SEÑORA DEL CARMEN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ELCHE | NUESTRA SEÑORA DEL CARMEN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ELCHE | NUESTRA SEÑORA DEL CARMEN"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="ELCHE | NUESTRA SEÑORA DEL CARMEN"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="ELCHE | NUESTRA SEÑORA DEL CARMEN"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="ELCHE | NUESTRA SEÑORA DEL CARMEN"
// ELCHE/JESUITINAS
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ELCHE/JESUITINAS"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ELCHE/JESUITINAS"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="ELCHE/JESUITINAS"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="ELCHE/JESUITINAS"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="ELCHE/JESUITINAS"
// TIRANT LO BLANC ELCHE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="TIRANT LO BLANC ELCHE"
replace instituto = 1 if LocalidadInstitutoenelcursas=="TIRANT LO BLANC ELCHE"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="TIRANT LO BLANC ELCHE"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="TIRANT LO BLANC ELCHE"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="TIRANT LO BLANC ELCHE"
// Cayetano Sempere Elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cayetano Sempere Elche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cayetano Sempere Elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Cayetano Sempere Elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Cayetano Sempere Elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Cayetano Sempere Elche"
// Cayetano sempere
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cayetano sempere"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cayetano sempere"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Cayetano sempere"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Cayetano sempere"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Cayetano sempere"
// IES Misteri d'Elx
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Misteri d'Elx"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Misteri d'Elx"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="IES Misteri d'Elx"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="IES Misteri d'Elx"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="IES Misteri d'Elx"
// IES Misteri d’Elx
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Misteri d’Elx"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Misteri d’Elx"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="IES Misteri d’Elx"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="IES Misteri d’Elx"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="IES Misteri d’Elx"
// IES MISTERI D'ELX
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES MISTERI D'ELX"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES MISTERI D'ELX"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="IES MISTERI D'ELX"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="IES MISTERI D'ELX"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="IES MISTERI D'ELX"
// ies misteri elx
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies misteri elx"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies misteri elx"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="ies misteri elx"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="ies misteri elx"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="ies misteri elx"
// IES TORRELLANO
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES TORRELLANO"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES TORRELLANO"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="IES TORRELLANO"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="IES TORRELLANO"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="IES TORRELLANO"
// IES Torrellano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Torrellano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Torrellano"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="IES Torrellano"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="IES Torrellano"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="IES Torrellano"
// IES Torrellano - Torrellano (Elche)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Torrellano - Torrellano (Elche)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Torrellano - Torrellano (Elche)"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="IES Torrellano - Torrellano (Elche)"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="IES Torrellano - Torrellano (Elche)"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="IES Torrellano - Torrellano (Elche)"
// Jesuitinas Elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jesuitinas Elche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jesuitinas Elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Jesuitinas Elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Jesuitinas Elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Jesuitinas Elche"
// Jesuitinas,Elche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jesuitinas,Elche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jesuitinas,Elche"
replace SocioeconStatPers = 8743 if LocalidadInstitutoenelcursas=="Jesuitinas,Elche"
replace SocioeconStatHog = 24047 if LocalidadInstitutoenelcursas=="Jesuitinas,Elche"
replace poblacion = 232517 if LocalidadInstitutoenelcursas=="Jesuitinas,Elche"
// Elda
// Elda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elda"
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="Elda"
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="Elda"
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="Elda"
// Elda 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elda "
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="Elda "
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="Elda "
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="Elda "
// ELDA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ELDA"
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="ELDA"
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="ELDA"
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="ELDA"
// elda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="elda"
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="elda"
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="elda"
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="elda"
// Elda / IES Monastil
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="Elda / IES Monastil"
// Elda . Centro Santa María del Carmen.
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elda . Centro Santa María del Carmen."
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elda . Centro Santa María del Carmen."
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="Elda . Centro Santa María del Carmen."
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="Elda . Centro Santa María del Carmen."
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="Elda . Centro Santa María del Carmen."
// Elda. IES La Melva
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Elda. IES La Melva"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Elda. IES La Melva"
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="Elda. IES La Melva"
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="Elda. IES La Melva"
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="Elda. IES La Melva"
// I.E.S La Melva
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S La Melva"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S La Melva"
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="I.E.S La Melva"
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="I.E.S La Melva"
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="I.E.S La Melva"
// La Melva, ciudad de Elda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Melva, ciudad de Elda"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Melva, ciudad de Elda"
replace SocioeconStatPers = 8609 if LocalidadInstitutoenelcursas=="La Melva, ciudad de Elda"
replace SocioeconStatHog = 22032 if LocalidadInstitutoenelcursas=="La Melva, ciudad de Elda"
replace poblacion = 52618 if LocalidadInstitutoenelcursas=="La Melva, ciudad de Elda"
// Guardamar del Segura
// Guardamar del Segura - IES Les Dunes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Guardamar del Segura - IES Les Dunes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Guardamar del Segura - IES Les Dunes"
replace SocioeconStatPers = 9255 if LocalidadInstitutoenelcursas=="Guardamar del Segura - IES Les Dunes"
replace SocioeconStatHog = 22707 if LocalidadInstitutoenelcursas=="Guardamar del Segura - IES Les Dunes"
replace poblacion = 15358 if LocalidadInstitutoenelcursas=="Guardamar del Segura - IES Les Dunes"
// Ibi
// ibi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ibi"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="ibi"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="ibi"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="ibi"
// Ibi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ibi"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="Ibi"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="Ibi"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="Ibi"
// ibi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ibi"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="ibi"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="ibi"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="ibi"
// Ibi, IES Nou Derramador
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ibi, IES Nou Derramador"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ibi, IES Nou Derramador"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="Ibi, IES Nou Derramador"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="Ibi, IES Nou Derramador"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="Ibi, IES Nou Derramador"
// Ibi/ IES NOU DERRAMADOR
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ibi/ IES NOU DERRAMADOR"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ibi/ IES NOU DERRAMADOR"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="Ibi/ IES NOU DERRAMADOR"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="Ibi/ IES NOU DERRAMADOR"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="Ibi/ IES NOU DERRAMADOR"
// IBI / LA FOIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="IBI / LA FOIA"
// Fray Ignacio Barrachina
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Fray Ignacio Barrachina"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Fray Ignacio Barrachina"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="Fray Ignacio Barrachina"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="Fray Ignacio Barrachina"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="Fray Ignacio Barrachina"
// IBI/ IES La Foia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IBI/ IES La Foia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IBI/ IES La Foia"
replace SocioeconStatPers = 10439 if LocalidadInstitutoenelcursas=="IBI/ IES La Foia"
replace SocioeconStatHog = 26744 if LocalidadInstitutoenelcursas=="IBI/ IES La Foia"
replace poblacion = 23489 if LocalidadInstitutoenelcursas=="IBI/ IES La Foia"
// Jávea / Xàbia
// Jávea
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jávea"
replace SocioeconStatPers = 8842 if LocalidadInstitutoenelcursas=="Jávea"
replace SocioeconStatHog = 22713 if LocalidadInstitutoenelcursas=="Jávea"
replace poblacion = 27604 if LocalidadInstitutoenelcursas=="Jávea"
// Jijona / Xixona
// IES XIXONA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES XIXONA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES XIXONA"
replace SocioeconStatPers = 11484 if LocalidadInstitutoenelcursas=="IES XIXONA"
replace SocioeconStatHog = 26622 if LocalidadInstitutoenelcursas=="IES XIXONA"
replace poblacion = 6865 if LocalidadInstitutoenelcursas=="IES XIXONA"
// Los Montesinos
// Los Montesinos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Los Montesinos"
replace SocioeconStatPers = 7786 if LocalidadInstitutoenelcursas=="Los Montesinos"
replace SocioeconStatHog = 19795 if LocalidadInstitutoenelcursas=="Los Montesinos"
replace poblacion = 4968 if LocalidadInstitutoenelcursas=="Los Montesinos"
// Monforte del Cid
// Monforte del Cid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Monforte del Cid"
replace SocioeconStatPers = 9038 if LocalidadInstitutoenelcursas=="Monforte del Cid"
replace SocioeconStatHog = 23547 if LocalidadInstitutoenelcursas=="Monforte del Cid"
replace poblacion = 8165 if LocalidadInstitutoenelcursas=="Monforte del Cid"
// Monóvar / Monòver
// Monóvar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Monóvar"
replace SocioeconStatPers = 8187 if LocalidadInstitutoenelcursas=="Monóvar"
replace SocioeconStatHog = 20568 if LocalidadInstitutoenelcursas=="Monóvar"
replace poblacion = 12167 if LocalidadInstitutoenelcursas=="Monóvar"
// Monovar/ IES Enric Valor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Monovar/ IES Enric Valor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Monovar/ IES Enric Valor"
replace SocioeconStatPers = 8187 if LocalidadInstitutoenelcursas=="Monovar/ IES Enric Valor"
replace SocioeconStatHog = 20568 if LocalidadInstitutoenelcursas=="Monovar/ IES Enric Valor"
replace poblacion = 12167 if LocalidadInstitutoenelcursas=="Monovar/ IES Enric Valor"
// Mutxamel
// Mutxamel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mutxamel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="Mutxamel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="Mutxamel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="Mutxamel"
// mutxamel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="mutxamel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="mutxamel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="mutxamel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="mutxamel"
// Muchamiel, Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Muchamiel, Alicante"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="Muchamiel, Alicante"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="Muchamiel, Alicante"
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="Muchamiel, Alicante"
// Mutxamel - C.E.B.AT.
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
replace instituto = 1 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="Mutxamel - C.E.B.AT."
// , ElI.E.S Mutxamel
replace cuidad = 1 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
replace instituto = 1 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas==", ElI.E.S Mutxamel"
// I.E.S Mutxamel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="I.E.S Mutxamel"
// Ies Mutxamel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Mutxamel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Mutxamel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="Ies Mutxamel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="Ies Mutxamel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="Ies Mutxamel"
// IES Mutxamel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Mutxamel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Mutxamel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="IES Mutxamel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="IES Mutxamel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="IES Mutxamel"
// Ies muchamiel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies muchamiel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies muchamiel"
replace SocioeconStatPers = 10540 if LocalidadInstitutoenelcursas=="Ies muchamiel"
replace SocioeconStatHog = 29259 if LocalidadInstitutoenelcursas=="Ies muchamiel"
replace poblacion = 25352 if LocalidadInstitutoenelcursas=="Ies muchamiel"
// Novelda
// Novelda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Novelda"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Novelda"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Novelda"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Novelda"
// novelda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="novelda"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="novelda"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="novelda"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="novelda"
// Colegio padre dehon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Colegio padre dehon"
// Colegio padre dehon 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Colegio padre dehon "
// IES LA MOLA, NOVELDA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="IES LA MOLA, NOVELDA"
// IES LA MOLA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES LA MOLA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES LA MOLA"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="IES LA MOLA"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="IES LA MOLA"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="IES LA MOLA"
// IES LA MOLA (NOVELDA)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES LA MOLA (NOVELDA)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES LA MOLA (NOVELDA)"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="IES LA MOLA (NOVELDA)"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="IES LA MOLA (NOVELDA)"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="IES LA MOLA (NOVELDA)"
// Colegio Padre Dehon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Padre Dehon"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Padre Dehon"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Colegio Padre Dehon"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Colegio Padre Dehon"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Colegio Padre Dehon"
// Novelda IES La Mola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Novelda IES La Mola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Novelda IES La Mola"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Novelda IES La Mola"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Novelda IES La Mola"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Novelda IES La Mola"
// Novelda Padre DEHON
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Novelda Padre DEHON"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Novelda Padre DEHON"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Novelda Padre DEHON"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Novelda Padre DEHON"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Novelda Padre DEHON"
// Novelda,Padre Dehon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Novelda,Padre Dehon"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Novelda,Padre Dehon"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Novelda,Padre Dehon"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Novelda,Padre Dehon"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Novelda,Padre Dehon"
// Novelda/ IES El Vinalopó
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Novelda/ IES El Vinalopó"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Novelda/ IES El Vinalopó"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Novelda/ IES El Vinalopó"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Novelda/ IES El Vinalopó"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Novelda/ IES El Vinalopó"
// Novelda/Colegio Padre Dehon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Novelda/Colegio Padre Dehon"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Novelda/Colegio Padre Dehon"
replace SocioeconStatPers = 9745 if LocalidadInstitutoenelcursas=="Novelda/Colegio Padre Dehon"
replace SocioeconStatHog = 25999 if LocalidadInstitutoenelcursas=="Novelda/Colegio Padre Dehon"
replace poblacion = 25651 if LocalidadInstitutoenelcursas=="Novelda/Colegio Padre Dehon"
// Nucia, La
// IES LA NUCIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
replace SocioeconStatPers = 9007 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
replace SocioeconStatHog = 25499 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
replace poblacion = 18603 if LocalidadInstitutoenelcursas=="IES LA NUCIA"
// LA NUCIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="LA NUCIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="LA NUCIA"
replace SocioeconStatPers = 9007 if LocalidadInstitutoenelcursas=="LA NUCIA"
replace SocioeconStatHog = 25499 if LocalidadInstitutoenelcursas=="LA NUCIA"
replace poblacion = 18603 if LocalidadInstitutoenelcursas=="LA NUCIA"
// Onil
// Onil
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Onil"
replace SocioeconStatPers = 10109 if LocalidadInstitutoenelcursas=="Onil"
replace SocioeconStatHog = 25027 if LocalidadInstitutoenelcursas=="Onil"
replace poblacion = 7507 if LocalidadInstitutoenelcursas=="Onil"
// Ies la Creueta Onil
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace SocioeconStatPers = 10109 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace SocioeconStatHog = 25027 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace poblacion = 7507 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
// Ies la Creueta Onil
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace SocioeconStatPers = 10109 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace SocioeconStatHog = 25027 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
replace poblacion = 7507 if LocalidadInstitutoenelcursas=="Ies la Creueta Onil"
// IES La Creueta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES La Creueta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES La Creueta"
replace SocioeconStatPers = 10109 if LocalidadInstitutoenelcursas=="IES La Creueta"
replace SocioeconStatHog = 25027 if LocalidadInstitutoenelcursas=="IES La Creueta"
replace poblacion = 7507 if LocalidadInstitutoenelcursas=="IES La Creueta"
// Orihuela
// Orihuela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Orihuela"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="Orihuela"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="Orihuela"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="Orihuela"
// ORIHUELA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ORIHUELA"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="ORIHUELA"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="ORIHUELA"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="ORIHUELA"
// Orihuela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Orihuela"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="Orihuela"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="Orihuela"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="Orihuela"
// Orihuela 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Orihuela "
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="Orihuela "
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="Orihuela "
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="Orihuela "
// Orihuela Gabriel Miró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Orihuela Gabriel Miró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Orihuela Gabriel Miró"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="Orihuela Gabriel Miró"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="Orihuela Gabriel Miró"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="Orihuela Gabriel Miró"
// Thader Orihuela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Thader Orihuela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Thader Orihuela"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="Thader Orihuela"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="Thader Orihuela"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="Thader Orihuela"
// IES Gabriel Miró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="IES Gabriel Miró"
// IES Tháder
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Tháder"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Tháder"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="IES Tháder"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="IES Tháder"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="IES Tháder"
// Colegio Diocesano Santo Domingo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo"
// Colegio Diocesano Santo Domingo 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo "
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo "
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo "
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="Colegio Diocesano Santo Domingo "
// Escuela de arte y superior de diseño de Orihuela (EASDO)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escuela de arte y superior de diseño de Orihuela (EASDO)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escuela de arte y superior de diseño de Orihuela (EASDO)"
replace SocioeconStatPers = 8610 if LocalidadInstitutoenelcursas=="Escuela de arte y superior de diseño de Orihuela (EASDO)"
replace SocioeconStatHog = 22274 if LocalidadInstitutoenelcursas=="Escuela de arte y superior de diseño de Orihuela (EASDO)"
replace poblacion = 77414 if LocalidadInstitutoenelcursas=="Escuela de arte y superior de diseño de Orihuela (EASDO)"
// Petrer
// Petrer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Petrer"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="Petrer"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="Petrer"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="Petrer"
// petrer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="petrer"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="petrer"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="petrer"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="petrer"
// IES La Canal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES La Canal"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES La Canal"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="IES La Canal"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="IES La Canal"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="IES La Canal"
// Petrer/IES Poeta Paco Mollá
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="Petrer/IES Poeta Paco Mollá"
// ies paco molla(petrer)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="ies paco molla(petrer)"
// Petrer, IES Poeta Paco Mollá
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Petrer, IES Poeta Paco Mollá"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Petrer, IES Poeta Paco Mollá"
replace SocioeconStatPers = 8575 if LocalidadInstitutoenelcursas=="Petrer, IES Poeta Paco Mollá"
replace SocioeconStatHog = 22752 if LocalidadInstitutoenelcursas=="Petrer, IES Poeta Paco Mollá"
replace poblacion = 34276 if LocalidadInstitutoenelcursas=="Petrer, IES Poeta Paco Mollá"
// Pinoso / Pinós, el
// Pinoso
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pinoso"
replace SocioeconStatPers = 7972 if LocalidadInstitutoenelcursas=="Pinoso"
replace SocioeconStatHog = 20703 if LocalidadInstitutoenelcursas=="Pinoso"
replace poblacion = 7966 if LocalidadInstitutoenelcursas=="Pinoso"
// Rafal
// Rafal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Rafal"
replace SocioeconStatPers = 7534 if LocalidadInstitutoenelcursas=="Rafal"
replace SocioeconStatHog = 21712 if LocalidadInstitutoenelcursas=="Rafal"
replace poblacion = 4498 if LocalidadInstitutoenelcursas=="Rafal"
// Redován
// Redovan
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Redovan"
replace SocioeconStatPers = 7708 if LocalidadInstitutoenelcursas=="Redovan"
replace SocioeconStatHog = 21653 if LocalidadInstitutoenelcursas=="Redovan"
replace poblacion = 7869 if LocalidadInstitutoenelcursas=="Redovan"
// redován
replace cuidad = 1 if LocalidadInstitutoenelcursas=="redován"
replace SocioeconStatPers = 7708 if LocalidadInstitutoenelcursas=="redován"
replace SocioeconStatHog = 21653 if LocalidadInstitutoenelcursas=="redován"
replace poblacion = 7869 if LocalidadInstitutoenelcursas=="redován"
// Redován
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Redován"
replace SocioeconStatPers = 7708 if LocalidadInstitutoenelcursas=="Redován"
replace SocioeconStatHog = 21653 if LocalidadInstitutoenelcursas=="Redován"
replace poblacion = 7869 if LocalidadInstitutoenelcursas=="Redován"
// Redovan IES juame de sant angel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Redovan IES juame de sant angel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Redovan IES juame de sant angel"
replace SocioeconStatPers = 7708 if LocalidadInstitutoenelcursas=="Redovan IES juame de sant angel"
replace SocioeconStatHog = 21653 if LocalidadInstitutoenelcursas=="Redovan IES juame de sant angel"
replace poblacion = 7869 if LocalidadInstitutoenelcursas=="Redovan IES juame de sant angel"
// Rojales
// Rojales
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Rojales"
replace SocioeconStatPers = 7384 if LocalidadInstitutoenelcursas=="Rojales"
replace SocioeconStatHog = 17749 if LocalidadInstitutoenelcursas=="Rojales"
replace poblacion = 16963 if LocalidadInstitutoenelcursas=="Rojales"
// IES La Encantà Rojales
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES La Encantà Rojales"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES La Encantà Rojales"
replace SocioeconStatPers = 7384 if LocalidadInstitutoenelcursas=="IES La Encantà Rojales"
replace SocioeconStatHog = 17749 if LocalidadInstitutoenelcursas=="IES La Encantà Rojales"
replace poblacion = 16963 if LocalidadInstitutoenelcursas=="IES La Encantà Rojales"
//San Vicente del Raspeig
// San Vicente del Raspeig
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
// San vicente del Raspeig
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San vicente del Raspeig"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San vicente del Raspeig"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San vicente del Raspeig"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San vicente del Raspeig"
// San Vicente del Raspeig
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig"
// San Vicente del raspeig
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del raspeig"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del raspeig"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del raspeig"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del raspeig"
// San Vicente
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente"
// ÍES San Vicente
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES San Vicente"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES San Vicente"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="ÍES San Vicente"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="ÍES San Vicente"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="ÍES San Vicente"
// IES Gaia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Gaia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Gaia"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES Gaia"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES Gaia"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES Gaia"
// IES.GAIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES.GAIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES.GAIA"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES.GAIA"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES.GAIA"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES.GAIA"
// ies gaia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies gaia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies gaia"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="ies gaia"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="ies gaia"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="ies gaia"
// IES Gaia 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Gaia "
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Gaia "
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES Gaia "
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES Gaia "
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES Gaia "
// ÍES GAIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES GAIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES GAIA"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="ÍES GAIA"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="ÍES GAIA"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="ÍES GAIA"
// San Vicente del Raspeig/ Instituto Maria Blasco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/ Instituto Maria Blasco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/ Instituto Maria Blasco"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/ Instituto Maria Blasco"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/ Instituto Maria Blasco"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/ Instituto Maria Blasco"
// San Vicente del Raspeig/IES María Blasco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/IES María Blasco"
// IES MARIA BLASCO
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES MARIA BLASCO"
// IES María Blasco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES María Blasco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES María Blasco"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES María Blasco"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES María Blasco"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES María Blasco"
// IES Maria Blasco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Maria Blasco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Maria Blasco"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES Maria Blasco"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES Maria Blasco"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES Maria Blasco"
// San Vicente del Raspeig/Ies San Vicente
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/Ies San Vicente"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/Ies San Vicente"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/Ies San Vicente"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/Ies San Vicente"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig/Ies San Vicente"
// San Vicente del Raspeig / I.E.S. San Vicente
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="San Vicente del Raspeig / I.E.S. San Vicente"
// ÍES SAN Vicente
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente"
// ÍES SAN Vicente 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="ÍES SAN Vicente "
// IES SAN VICENTE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES SAN VICENTE"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES SAN VICENTE"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES SAN VICENTE"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES SAN VICENTE"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES SAN VICENTE"
// IES Haygon, San Vicente del Raspeig
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Haygon, San Vicente del Raspeig"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Haygon, San Vicente del Raspeig"
replace SocioeconStatPers = 9781 if LocalidadInstitutoenelcursas=="IES Haygon, San Vicente del Raspeig"
replace SocioeconStatHog = 27143 if LocalidadInstitutoenelcursas=="IES Haygon, San Vicente del Raspeig"
replace poblacion = 58385 if LocalidadInstitutoenelcursas=="IES Haygon, San Vicente del Raspeig"
// San Miguel de Salinas
// san miguel de salinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="san miguel de salinas"
replace SocioeconStatPers = 7747 if LocalidadInstitutoenelcursas=="san miguel de salinas"
replace SocioeconStatHog = 19449 if LocalidadInstitutoenelcursas=="san miguel de salinas"
replace poblacion = 6034 if LocalidadInstitutoenelcursas=="san miguel de salinas"
// San Miguel de Salinas / I.E.S. Los Alcores
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Miguel de Salinas / I.E.S. Los Alcores"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Miguel de Salinas / I.E.S. Los Alcores"
replace SocioeconStatPers = 7747 if LocalidadInstitutoenelcursas=="San Miguel de Salinas / I.E.S. Los Alcores"
replace SocioeconStatHog = 19449 if LocalidadInstitutoenelcursas=="San Miguel de Salinas / I.E.S. Los Alcores"
replace poblacion = 6034 if LocalidadInstitutoenelcursas=="San Miguel de Salinas / I.E.S. Los Alcores"
// Sant Joan d'Alacant
// Sant Joan d'Alacant/IES Luís García Berlanga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
replace SocioeconStatPers = 11217 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
replace SocioeconStatHog = 30000 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
replace poblacion = 23915 if LocalidadInstitutoenelcursas=="Sant Joan d'Alacant/IES Luís García Berlanga"
// IES Lloixa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace SocioeconStatPers = 11217 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace SocioeconStatHog = 30000 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace poblacion = 23915 if LocalidadInstitutoenelcursas=="IES Lloixa"
// IES Lloixa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace SocioeconStatPers = 11217 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace SocioeconStatHog = 30000 if LocalidadInstitutoenelcursas=="IES Lloixa"
replace poblacion = 23915 if LocalidadInstitutoenelcursas=="IES Lloixa"
// Santa Pola
// Santa Pola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Pola"
replace SocioeconStatPers = 9146 if LocalidadInstitutoenelcursas=="Santa Pola"
replace SocioeconStatHog = 22371 if LocalidadInstitutoenelcursas=="Santa Pola"
replace poblacion = 32306 if LocalidadInstitutoenelcursas=="Santa Pola"
// Santa pola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa pola"
replace SocioeconStatPers = 9146 if LocalidadInstitutoenelcursas=="Santa pola"
replace SocioeconStatHog = 22371 if LocalidadInstitutoenelcursas=="Santa pola"
replace poblacion = 32306 if LocalidadInstitutoenelcursas=="Santa pola"
// SANTAB POLA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="SANTAB POLA"
replace SocioeconStatPers = 9146 if LocalidadInstitutoenelcursas=="SANTAB POLA"
replace SocioeconStatHog = 22371 if LocalidadInstitutoenelcursas=="SANTAB POLA"
replace poblacion = 32306 if LocalidadInstitutoenelcursas=="SANTAB POLA"
// Santa Pola - IES Santa Pola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Pola - IES Santa Pola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Pola - IES Santa Pola"
replace SocioeconStatPers = 9146 if LocalidadInstitutoenelcursas=="Santa Pola - IES Santa Pola"
replace SocioeconStatHog = 22371 if LocalidadInstitutoenelcursas=="Santa Pola - IES Santa Pola"
replace poblacion = 32306 if LocalidadInstitutoenelcursas=="Santa Pola - IES Santa Pola"
// IES Santa Pola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Santa Pola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Santa Pola"
replace SocioeconStatPers = 9146 if LocalidadInstitutoenelcursas=="IES Santa Pola"
replace SocioeconStatHog = 22371 if LocalidadInstitutoenelcursas=="IES Santa Pola"
replace poblacion = 32306 if LocalidadInstitutoenelcursas=="IES Santa Pola"
// Ies cap del aljub
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies cap del aljub"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies cap del aljub"
replace SocioeconStatPers = 9146 if LocalidadInstitutoenelcursas=="Ies cap del aljub"
replace SocioeconStatHog = 22371 if LocalidadInstitutoenelcursas=="Ies cap del aljub"
replace poblacion = 32306 if LocalidadInstitutoenelcursas=="Ies cap del aljub"
// Teulada
// Teulada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Teulada"
replace SocioeconStatPers = 8470 if LocalidadInstitutoenelcursas=="Teulada"
replace SocioeconStatHog = 20935 if LocalidadInstitutoenelcursas=="Teulada"
replace poblacion = 11112 if LocalidadInstitutoenelcursas=="Teulada"
// IES Teulada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Teulada"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Teulada"
replace SocioeconStatPers = 8470 if LocalidadInstitutoenelcursas=="IES Teulada"
replace SocioeconStatHog = 20935 if LocalidadInstitutoenelcursas=="IES Teulada"
replace poblacion = 11112 if LocalidadInstitutoenelcursas=="IES Teulada"
// Torrevieja
// TORREVIEJA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="TORREVIEJA"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="TORREVIEJA"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="TORREVIEJA"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="TORREVIEJA"
// Torrevieja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrevieja"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="Torrevieja"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="Torrevieja"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="Torrevieja"
// torrevieja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="torrevieja"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="torrevieja"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="torrevieja"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="torrevieja"
// torrevieja la purisima
replace cuidad = 1 if LocalidadInstitutoenelcursas=="torrevieja la purisima"
replace instituto = 1 if LocalidadInstitutoenelcursas=="torrevieja la purisima"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="torrevieja la purisima"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="torrevieja la purisima"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="torrevieja la purisima"
// Torrevieja/ La Purísima
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrevieja/ La Purísima"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Torrevieja/ La Purísima"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="Torrevieja/ La Purísima"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="Torrevieja/ La Purísima"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="Torrevieja/ La Purísima"
// La Purisima Torrevieja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="La Purisima Torrevieja"
// Torrevieja/ IES Torrevigía Nº5
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="Torrevieja/ IES Torrevigía Nº5"
// ies libertas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies libertas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies libertas"
replace SocioeconStatPers = 7643 if LocalidadInstitutoenelcursas=="ies libertas"
replace SocioeconStatHog = 18484 if LocalidadInstitutoenelcursas=="ies libertas"
replace poblacion = 83337 if LocalidadInstitutoenelcursas=="ies libertas"
// Villajoyosa/Vila Joiosa, la
// La Vila Joiosa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Vila Joiosa"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="La Vila Joiosa"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="La Vila Joiosa"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="La Vila Joiosa"
// Vilajoiosa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vilajoiosa"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="Vilajoiosa"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="Vilajoiosa"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="Vilajoiosa"
// Villajoiosa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villajoiosa"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="Villajoiosa"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="Villajoiosa"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="Villajoiosa"
// Villajoyosa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villajoyosa"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="Villajoyosa"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="Villajoyosa"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="Villajoyosa"
// Villajoyosa/IES La Malladeta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="Villajoyosa/IES La Malladeta"
// I.E.S MARINA BAIXA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S MARINA BAIXA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S MARINA BAIXA"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="I.E.S MARINA BAIXA"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="I.E.S MARINA BAIXA"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="I.E.S MARINA BAIXA"
// IES MARINA BAIXA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES MARINA BAIXA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES MARINA BAIXA"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="IES MARINA BAIXA"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="IES MARINA BAIXA"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="IES MARINA BAIXA"
// IES La Malladeta/Villajoiosa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES La Malladeta/Villajoiosa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES La Malladeta/Villajoiosa"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="IES La Malladeta/Villajoiosa"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="IES La Malladeta/Villajoiosa"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="IES La Malladeta/Villajoiosa"
// Ies la malladeta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies la malladeta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies la malladeta"
replace SocioeconStatPers = 9639 if LocalidadInstitutoenelcursas=="Ies la malladeta"
replace SocioeconStatHog = 24035 if LocalidadInstitutoenelcursas=="Ies la malladeta"
replace poblacion = 34673 if LocalidadInstitutoenelcursas=="Ies la malladeta"
// Villena
// Villena
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villena"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Villena"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="Villena"
replace poblacion = 33964 if LocalidadInstitutoenelcursas=="Villena"
// villena
replace cuidad = 1 if LocalidadInstitutoenelcursas=="villena"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="villena"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="villena"
replace poblacion = 33964 if LocalidadInstitutoenelcursas=="villena"
// VILLENA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="VILLENA"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="VILLENA"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="VILLENA"
replace poblacion = 33964 if LocalidadInstitutoenelcursas=="VILLENA"
// Villena (Alicante)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villena (Alicante)"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Villena (Alicante)"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="Villena (Alicante)"
replace poblacion = 33964 if LocalidadInstitutoenelcursas=="Villena (Alicante)"
// Villena / Alicante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villena / Alicante"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Villena / Alicante"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="Villena / Alicante"
replace poblacion = 33964 if LocalidadInstitutoenelcursas=="Villena / Alicante"
// IES A. Navarro Santafé Villena (Alicante)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES A. Navarro Santafé Villena (Alicante)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES A. Navarro Santafé Villena (Alicante)"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="IES A. Navarro Santafé Villena (Alicante)"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="IES A. Navarro Santafé Villena (Alicante)"
replace poblacion = 33964 if LocalidadInstitutoenelcursas=="IES A. Navarro Santafé Villena (Alicante)"
********************************************************************************
*    Almería
********************************************************************************
// Almería
// Almería
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almería"
replace SocioeconStatPers = 9968 if LocalidadInstitutoenelcursas=="Almería"
replace SocioeconStatHog = 27090 if LocalidadInstitutoenelcursas=="Almería"
replace poblacion = 198533 if LocalidadInstitutoenelcursas=="Almería"
// Almería
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almería"
replace SocioeconStatPers = 9968 if LocalidadInstitutoenelcursas=="Almería"
replace SocioeconStatHog = 27090 if LocalidadInstitutoenelcursas=="Almería"
replace poblacion = 198533 if LocalidadInstitutoenelcursas=="Almería"
// Almería 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almería "
replace SocioeconStatPers = 9968 if LocalidadInstitutoenelcursas=="Almería "
replace SocioeconStatHog = 27090 if LocalidadInstitutoenelcursas=="Almería "
replace poblacion = 198533 if LocalidadInstitutoenelcursas=="Almería "
// Almeria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almeria"
replace SocioeconStatPers = 9968 if LocalidadInstitutoenelcursas=="Almeria"
replace SocioeconStatHog = 27090 if LocalidadInstitutoenelcursas=="Almeria"
replace poblacion = 198533 if LocalidadInstitutoenelcursas=="Almeria"
// Almeria/IES Maestro Padilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
replace SocioeconStatPers = 9968 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
replace SocioeconStatHog = 27090 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
replace poblacion = 198533 if LocalidadInstitutoenelcursas=="Almeria/IES Maestro Padilla"
// Stella Maris, Almería
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Stella Maris, Almería"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Stella Maris, Almería"
replace SocioeconStatPers = 9968 if LocalidadInstitutoenelcursas=="Stella Maris, Almería"
replace SocioeconStatHog = 27090 if LocalidadInstitutoenelcursas=="Stella Maris, Almería"
replace poblacion = 198533 if LocalidadInstitutoenelcursas=="Stella Maris, Almería"
// Colegio Diocesano San Ildefonso de Almería
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Diocesano San Ildefonso de Almería"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Diocesano San Ildefonso de Almería"
replace SocioeconStatPers = 9968 if LocalidadInstitutoenelcursas=="Colegio Diocesano San Ildefonso de Almería"
replace SocioeconStatHog = 27090 if LocalidadInstitutoenelcursas=="Colegio Diocesano San Ildefonso de Almería"
replace poblacion = 198533 if LocalidadInstitutoenelcursas=="Colegio Diocesano San Ildefonso de Almería"
// La Salle Virgen del Mar, Almería
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Virgen del Mar, Almería"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Virgen del Mar, Almería"
replace SocioeconStatPers = 9968 if LocalidadInstitutoenelcursas=="La Salle Virgen del Mar, Almería"
replace SocioeconStatHog = 27090 if LocalidadInstitutoenelcursas=="La Salle Virgen del Mar, Almería"
replace poblacion = 198533 if LocalidadInstitutoenelcursas=="La Salle Virgen del Mar, Almería"
// Ejido, El
// Almería/SEK Alborán
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Almería/SEK Alborán"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Almería/SEK Alborán"
replace SocioeconStatPers = 7552 if LocalidadInstitutoenelcursas=="Almería/SEK Alborán"
replace SocioeconStatHog = 22481 if LocalidadInstitutoenelcursas=="Almería/SEK Alborán"
replace poblacion = 83594 if LocalidadInstitutoenelcursas=="Almería/SEK Alborán"
// Huércal de Almería
// Centro Educativo Agave
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Centro Educativo Agave"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Centro Educativo Agave"
replace SocioeconStatPers = 9016 if LocalidadInstitutoenelcursas=="Centro Educativo Agave"
replace SocioeconStatHog = 25820 if LocalidadInstitutoenelcursas=="Centro Educativo Agave"
replace poblacion = 17651 if LocalidadInstitutoenelcursas=="Centro Educativo Agave"
// Roquetas de Mar
// Aguadulce/ Portocarrero
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aguadulce/ Portocarrero"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Aguadulce/ Portocarrero"
replace SocioeconStatPers = 8567 if LocalidadInstitutoenelcursas=="Aguadulce/ Portocarrero"
replace SocioeconStatHog = 24092 if LocalidadInstitutoenelcursas=="Aguadulce/ Portocarrero"
replace poblacion = 96800 if LocalidadInstitutoenelcursas=="Aguadulce/ Portocarrero"
// IES Sabinar, Roquetas de Mar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Sabinar, Roquetas de Mar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Sabinar, Roquetas de Mar"
replace SocioeconStatPers = 8567 if LocalidadInstitutoenelcursas=="IES Sabinar, Roquetas de Mar"
replace SocioeconStatHog = 24092 if LocalidadInstitutoenelcursas=="IES Sabinar, Roquetas de Mar"
replace poblacion = 96800 if LocalidadInstitutoenelcursas=="IES Sabinar, Roquetas de Mar"
********************************************************************************
*    Asturias
********************************************************************************
// Cangas de Onis
// Cangas de Onis
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cangas de Onis"
replace SocioeconStatPers = 9581 if LocalidadInstitutoenelcursas=="Cangas de Onis"
replace SocioeconStatHog = 22791 if LocalidadInstitutoenelcursas=="Cangas de Onis"
replace poblacion = 6163 if LocalidadInstitutoenelcursas=="Cangas de Onis"
// Cangas de Onis 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cangas de Onis "
replace SocioeconStatPers = 9581 if LocalidadInstitutoenelcursas=="Cangas de Onis "
replace SocioeconStatHog = 22791 if LocalidadInstitutoenelcursas=="Cangas de Onis "
replace poblacion = 6163 if LocalidadInstitutoenelcursas=="Cangas de Onis "
// Gijón
// Gijón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gijón"
replace SocioeconStatPers = 12882 if LocalidadInstitutoenelcursas=="Gijón"
replace SocioeconStatHog = 29306 if LocalidadInstitutoenelcursas=="Gijón"
replace poblacion = 271780 if LocalidadInstitutoenelcursas=="Gijón"
// Gozón
// Luanco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Luanco"
replace SocioeconStatPers = 12296 if LocalidadInstitutoenelcursas=="Luanco"
replace SocioeconStatHog = 27335 if LocalidadInstitutoenelcursas=="Luanco"
replace poblacion = 10333 if LocalidadInstitutoenelcursas=="Luanco"
// Langreo
// langreo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="langreo"
replace SocioeconStatPers = 11697 if LocalidadInstitutoenelcursas=="langreo"
replace SocioeconStatHog = 26210 if LocalidadInstitutoenelcursas=="langreo"
replace poblacion = 39420 if LocalidadInstitutoenelcursas=="langreo"
// Oviedo
// Oviedo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Oviedo"
replace SocioeconStatPers = 13704 if LocalidadInstitutoenelcursas=="Oviedo"
replace SocioeconStatHog = 31951 if LocalidadInstitutoenelcursas=="Oviedo"
replace poblacion = 220406 if LocalidadInstitutoenelcursas=="Oviedo"
// Oviedo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Oviedo"
replace SocioeconStatPers = 13704 if LocalidadInstitutoenelcursas=="Oviedo"
replace SocioeconStatHog = 31951 if LocalidadInstitutoenelcursas=="Oviedo"
replace poblacion = 220406 if LocalidadInstitutoenelcursas=="Oviedo"
// Oviedo, Asturias
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Oviedo, Asturias"
replace SocioeconStatPers = 13704 if LocalidadInstitutoenelcursas=="Oviedo, Asturias"
replace SocioeconStatHog = 31951 if LocalidadInstitutoenelcursas=="Oviedo, Asturias"
replace poblacion = 220406 if LocalidadInstitutoenelcursas=="Oviedo, Asturias"
// Colegio San Ignacio - Jesuitas - Oviedo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio San Ignacio - Jesuitas - Oviedo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio San Ignacio - Jesuitas - Oviedo"
replace SocioeconStatPers = 13704 if LocalidadInstitutoenelcursas=="Colegio San Ignacio - Jesuitas - Oviedo"
replace SocioeconStatHog = 31951 if LocalidadInstitutoenelcursas=="Colegio San Ignacio - Jesuitas - Oviedo"
replace poblacion = 220406 if LocalidadInstitutoenelcursas=="Colegio San Ignacio - Jesuitas - Oviedo"
// Colegio San Ignacio de Oviedo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio San Ignacio de Oviedo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio San Ignacio de Oviedo"
replace SocioeconStatPers = 13704 if LocalidadInstitutoenelcursas=="Colegio San Ignacio de Oviedo"
replace SocioeconStatHog = 31951 if LocalidadInstitutoenelcursas=="Colegio San Ignacio de Oviedo"
replace poblacion = 220406 if LocalidadInstitutoenelcursas=="Colegio San Ignacio de Oviedo"
********************************************************************************
*    Ávila
********************************************************************************
// Ávila
// Ávila
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ávila"
replace SocioeconStatPers = 11990 if LocalidadInstitutoenelcursas=="Ávila"
replace SocioeconStatHog = 28431 if LocalidadInstitutoenelcursas=="Ávila"
replace poblacion = 57744 if LocalidadInstitutoenelcursas=="Ávila"
// Avila
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Avila"
replace SocioeconStatPers = 11990 if LocalidadInstitutoenelcursas=="Avila"
replace SocioeconStatHog = 28431 if LocalidadInstitutoenelcursas=="Avila"
replace poblacion = 57744 if LocalidadInstitutoenelcursas=="Avila"
// Ávila
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ávila"
replace SocioeconStatPers = 11990 if LocalidadInstitutoenelcursas=="Ávila"
replace SocioeconStatHog = 28431 if LocalidadInstitutoenelcursas=="Ávila"
replace poblacion = 57744 if LocalidadInstitutoenelcursas=="Ávila"
// Ávila 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ávila "
replace SocioeconStatPers = 11990 if LocalidadInstitutoenelcursas=="Ávila "
replace SocioeconStatHog = 28431 if LocalidadInstitutoenelcursas=="Ávila "
replace poblacion = 57744 if LocalidadInstitutoenelcursas=="Ávila "
// ÁVILA / IES VASCO DE LA ZARZA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÁVILA / IES VASCO DE LA ZARZA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÁVILA / IES VASCO DE LA ZARZA"
replace SocioeconStatPers = 11990 if LocalidadInstitutoenelcursas=="ÁVILA / IES VASCO DE LA ZARZA"
replace SocioeconStatHog = 28431 if LocalidadInstitutoenelcursas=="ÁVILA / IES VASCO DE LA ZARZA"
replace poblacion = 57744 if LocalidadInstitutoenelcursas=="ÁVILA / IES VASCO DE LA ZARZA"
// IES VASCO DE LA ZARZA (ÁVILA)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES VASCO DE LA ZARZA (ÁVILA)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES VASCO DE LA ZARZA (ÁVILA)"
replace SocioeconStatPers = 11990 if LocalidadInstitutoenelcursas=="IES VASCO DE LA ZARZA (ÁVILA)"
replace SocioeconStatHog = 28431 if LocalidadInstitutoenelcursas=="IES VASCO DE LA ZARZA (ÁVILA)"
replace poblacion = 57744 if LocalidadInstitutoenelcursas=="IES VASCO DE LA ZARZA (ÁVILA)"
// Avila, Colegio Diocesano Asunción de Nuestra Señora
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Avila, Colegio Diocesano Asunción de Nuestra Señora"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Avila, Colegio Diocesano Asunción de Nuestra Señora"
replace SocioeconStatPers = 11990 if LocalidadInstitutoenelcursas=="Avila, Colegio Diocesano Asunción de Nuestra Señora"
replace SocioeconStatHog = 28431 if LocalidadInstitutoenelcursas=="Avila, Colegio Diocesano Asunción de Nuestra Señora"
replace poblacion = 57744 if LocalidadInstitutoenelcursas=="Avila, Colegio Diocesano Asunción de Nuestra Señora"
********************************************************************************
*    Badajoz
********************************************************************************
// Almendralejo
// I.E.S Carolina Coronado (Almendralejo)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Carolina Coronado (Almendralejo)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Carolina Coronado (Almendralejo)"
replace SocioeconStatPers = 8316 if LocalidadInstitutoenelcursas=="I.E.S Carolina Coronado (Almendralejo)"
replace SocioeconStatHog = 21998 if LocalidadInstitutoenelcursas=="I.E.S Carolina Coronado (Almendralejo)"
replace poblacion = 33474 if LocalidadInstitutoenelcursas=="I.E.S Carolina Coronado (Almendralejo)"
// Badajoz
// badajoz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="badajoz"
replace SocioeconStatPers = 10728 if LocalidadInstitutoenelcursas=="badajoz"
replace SocioeconStatHog = 28630 if LocalidadInstitutoenelcursas=="badajoz"
replace poblacion = 150702 if LocalidadInstitutoenelcursas=="badajoz"
// Badajoz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Badajoz"
replace SocioeconStatPers = 10728 if LocalidadInstitutoenelcursas=="Badajoz"
replace SocioeconStatHog = 28630 if LocalidadInstitutoenelcursas=="Badajoz"
replace poblacion = 150702 if LocalidadInstitutoenelcursas=="Badajoz"
// Badajoz 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Badajoz "
replace SocioeconStatPers = 10728 if LocalidadInstitutoenelcursas=="Badajoz "
replace SocioeconStatHog = 28630 if LocalidadInstitutoenelcursas=="Badajoz "
replace poblacion = 150702 if LocalidadInstitutoenelcursas=="Badajoz "
// Colegio Santa María Assumpta, Badajoz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Santa María Assumpta, Badajoz"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Santa María Assumpta, Badajoz"
replace SocioeconStatPers = 10728 if LocalidadInstitutoenelcursas=="Colegio Santa María Assumpta, Badajoz"
replace SocioeconStatHog = 28630 if LocalidadInstitutoenelcursas=="Colegio Santa María Assumpta, Badajoz"
replace poblacion = 150702 if LocalidadInstitutoenelcursas=="Colegio Santa María Assumpta, Badajoz"
// Cabeza del Buey
// Cabeza del Buey
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cabeza del Buey"
replace SocioeconStatPers = 8685 if LocalidadInstitutoenelcursas=="Cabeza del Buey"
replace SocioeconStatHog = 21908 if LocalidadInstitutoenelcursas=="Cabeza del Buey"
replace poblacion = 4864 if LocalidadInstitutoenelcursas=="Cabeza del Buey"
// Don Benito
// Don Benito
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Don Benito"
replace SocioeconStatPers = 9654 if LocalidadInstitutoenelcursas=="Don Benito"
replace SocioeconStatHog = 25048 if LocalidadInstitutoenelcursas=="Don Benito"
replace poblacion = 37151 if LocalidadInstitutoenelcursas=="Don Benito"
// Don Benito 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Don Benito "
replace SocioeconStatPers = 9654 if LocalidadInstitutoenelcursas=="Don Benito "
replace SocioeconStatHog = 25048 if LocalidadInstitutoenelcursas=="Don Benito "
replace poblacion = 37151 if LocalidadInstitutoenelcursas=="Don Benito "
// Herrera del Duque
// IES BENAZAIRE, HERRERA DEL DUQUE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES BENAZAIRE, HERRERA DEL DUQUE"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES BENAZAIRE, HERRERA DEL DUQUE"
replace SocioeconStatPers = 8378 if LocalidadInstitutoenelcursas=="IES BENAZAIRE, HERRERA DEL DUQUE"
replace SocioeconStatHog = 20079 if LocalidadInstitutoenelcursas=="IES BENAZAIRE, HERRERA DEL DUQUE"
replace poblacion = 3482 if LocalidadInstitutoenelcursas=="IES BENAZAIRE, HERRERA DEL DUQUE"
// Montijo
// Montijo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Montijo"
replace SocioeconStatPers = 8084 if LocalidadInstitutoenelcursas=="Montijo"
replace SocioeconStatHog = 20481 if LocalidadInstitutoenelcursas=="Montijo"
replace poblacion = 15457 if LocalidadInstitutoenelcursas=="Montijo"
// Villanueva de la Serena
// Villanueva de la Serena
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Serena"
replace SocioeconStatPers = 9062 if LocalidadInstitutoenelcursas=="Villanueva de la Serena"
replace SocioeconStatHog = 23818 if LocalidadInstitutoenelcursas=="Villanueva de la Serena"
replace poblacion = 25667 if LocalidadInstitutoenelcursas=="Villanueva de la Serena"
// Villanueva de la Serena 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Serena "
replace SocioeconStatPers = 9062 if LocalidadInstitutoenelcursas=="Villanueva de la Serena "
replace SocioeconStatHog = 23818 if LocalidadInstitutoenelcursas=="Villanueva de la Serena "
replace poblacion = 25667 if LocalidadInstitutoenelcursas=="Villanueva de la Serena "
// Zarza, La
// IES TIERRA BLANCA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES TIERRA BLANCA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES TIERRA BLANCA"
replace SocioeconStatPers = 9248 if LocalidadInstitutoenelcursas=="IES TIERRA BLANCA"
replace SocioeconStatHog = 20273 if LocalidadInstitutoenelcursas=="IES TIERRA BLANCA"
replace poblacion = 3472 if LocalidadInstitutoenelcursas=="IES TIERRA BLANCA"
********************************************************************************
*    Baleares
********************************************************************************
// Alaior
// Alaior
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alaior"
replace SocioeconStatPers = 11686 if LocalidadInstitutoenelcursas=="Alaior"
replace SocioeconStatHog = 29798 if LocalidadInstitutoenelcursas=="Alaior"
replace poblacion = 9065 if LocalidadInstitutoenelcursas=="Alaior"
// Alaior 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alaior "
replace SocioeconStatPers = 11686 if LocalidadInstitutoenelcursas=="Alaior "
replace SocioeconStatHog = 29798 if LocalidadInstitutoenelcursas=="Alaior "
replace poblacion = 9065 if LocalidadInstitutoenelcursas=="Alaior "
// Ciutadella de Menorca / Ciudadela
// Ciutadella de Menorca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ciutadella de Menorca"
replace SocioeconStatPers = 11343 if LocalidadInstitutoenelcursas=="Ciutadella de Menorca"
replace SocioeconStatHog = 30140 if LocalidadInstitutoenelcursas=="Ciutadella de Menorca"
replace poblacion = 29840 if LocalidadInstitutoenelcursas=="Ciutadella de Menorca"
// IES Mª Àngels Cardona, Ciutadella de Menorca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Mª Àngels Cardona, Ciutadella de Menorca"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Mª Àngels Cardona, Ciutadella de Menorca"
replace SocioeconStatPers = 11343 if LocalidadInstitutoenelcursas=="IES Mª Àngels Cardona, Ciutadella de Menorca"
replace SocioeconStatHog = 30140 if LocalidadInstitutoenelcursas=="IES Mª Àngels Cardona, Ciutadella de Menorca"
replace poblacion = 29840 if LocalidadInstitutoenelcursas=="IES Mª Àngels Cardona, Ciutadella de Menorca"
// Eivissa
// Eivissa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Eivissa"
replace SocioeconStatPers = 13314 if LocalidadInstitutoenelcursas=="Eivissa"
replace SocioeconStatHog = 37822 if LocalidadInstitutoenelcursas=="Eivissa"
replace poblacion = 49783 if LocalidadInstitutoenelcursas=="Eivissa"
// Ibiza, CC Sa Real
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ibiza, CC Sa Real"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ibiza, CC Sa Real"
replace SocioeconStatPers = 13314 if LocalidadInstitutoenelcursas=="Ibiza, CC Sa Real"
replace SocioeconStatHog = 37822 if LocalidadInstitutoenelcursas=="Ibiza, CC Sa Real"
replace poblacion = 49783 if LocalidadInstitutoenelcursas=="Ibiza, CC Sa Real"
// Llucmajor / LLuchmayor
// Llucmajor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Llucmajor"
replace SocioeconStatPers = 12472 if LocalidadInstitutoenelcursas=="Llucmajor"
replace SocioeconStatHog = 33678 if LocalidadInstitutoenelcursas=="Llucmajor"
replace poblacion = 36914 if LocalidadInstitutoenelcursas=="Llucmajor"
// Manacor
// IES Manacor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Manacor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Manacor"
replace SocioeconStatPers = 11333 if LocalidadInstitutoenelcursas=="IES Manacor"
replace SocioeconStatHog = 32589 if LocalidadInstitutoenelcursas=="IES Manacor"
replace poblacion = 43808 if LocalidadInstitutoenelcursas=="IES Manacor"
// IES Mossèn Alcover, Manacor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Mossèn Alcover, Manacor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Mossèn Alcover, Manacor"
replace SocioeconStatPers = 11333 if LocalidadInstitutoenelcursas=="IES Mossèn Alcover, Manacor"
replace SocioeconStatHog = 32589 if LocalidadInstitutoenelcursas=="IES Mossèn Alcover, Manacor"
replace poblacion = 43808 if LocalidadInstitutoenelcursas=="IES Mossèn Alcover, Manacor"
// Palma
// Palma de Mallorca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Palma de Mallorca"
replace SocioeconStatPers = 12514 if LocalidadInstitutoenelcursas=="Palma de Mallorca"
replace SocioeconStatHog = 35110 if LocalidadInstitutoenelcursas=="Palma de Mallorca"
replace poblacion = 416065 if LocalidadInstitutoenelcursas=="Palma de Mallorca"
// Palma de Mallorca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Palma de Mallorca"
replace SocioeconStatPers = 12514 if LocalidadInstitutoenelcursas=="Palma de Mallorca"
replace SocioeconStatHog = 35110 if LocalidadInstitutoenelcursas=="Palma de Mallorca"
replace poblacion = 416065 if LocalidadInstitutoenelcursas=="Palma de Mallorca"
// Sant Josep Obrer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
replace SocioeconStatPers = 12514 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
replace SocioeconStatHog = 35110 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
replace poblacion = 416065 if LocalidadInstitutoenelcursas=="Sant Josep Obrer"
// IES SON PACS, PALMA DE MALLORCA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES SON PACS, PALMA DE MALLORCA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES SON PACS, PALMA DE MALLORCA"
replace SocioeconStatPers = 12514 if LocalidadInstitutoenelcursas=="IES SON PACS, PALMA DE MALLORCA"
replace SocioeconStatHog = 35110 if LocalidadInstitutoenelcursas=="IES SON PACS, PALMA DE MALLORCA"
replace poblacion = 416065 if LocalidadInstitutoenelcursas=="IES SON PACS, PALMA DE MALLORCA"
// Palma de Mallorca/ Colegio Madre Alberta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Palma de Mallorca/ Colegio Madre Alberta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Palma de Mallorca/ Colegio Madre Alberta"
replace SocioeconStatPers = 12514 if LocalidadInstitutoenelcursas=="Palma de Mallorca/ Colegio Madre Alberta"
replace SocioeconStatHog = 35110 if LocalidadInstitutoenelcursas=="Palma de Mallorca/ Colegio Madre Alberta"
replace poblacion = 416065 if LocalidadInstitutoenelcursas=="Palma de Mallorca/ Colegio Madre Alberta"
// Palma de Mallorca/Colegio Nuestra Señora de Montesion
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion"
replace SocioeconStatPers = 12514 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion"
replace SocioeconStatHog = 35110 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion"
replace poblacion = 416065 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion"
// Palma de Mallorca/Colegio Nuestra Señora de Montesion 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion "
replace SocioeconStatPers = 12514 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion "
replace SocioeconStatHog = 35110 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion "
replace poblacion = 416065 if LocalidadInstitutoenelcursas=="Palma de Mallorca/Colegio Nuestra Señora de Montesion "
// Sa Pobla / La puebla
// Sa Pobla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sa Pobla"
replace SocioeconStatPers = 10437 if LocalidadInstitutoenelcursas=="Sa Pobla"
replace SocioeconStatHog = 30275 if LocalidadInstitutoenelcursas=="Sa Pobla"
replace poblacion = 13475 if LocalidadInstitutoenelcursas=="Sa Pobla"
********************************************************************************
*    Barcelona
********************************************************************************
// Arenys de Mar
// arenys de mar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="arenys de mar"
replace SocioeconStatPers = 12969 if LocalidadInstitutoenelcursas=="arenys de mar"
replace SocioeconStatHog = 32698 if LocalidadInstitutoenelcursas=="arenys de mar"
replace poblacion = 15776 if LocalidadInstitutoenelcursas=="arenys de mar"
// La Presentació Arenys de Mar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Presentació Arenys de Mar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Presentació Arenys de Mar"
replace SocioeconStatPers = 12969 if LocalidadInstitutoenelcursas=="La Presentació Arenys de Mar"
replace SocioeconStatHog = 32698 if LocalidadInstitutoenelcursas=="La Presentació Arenys de Mar"
replace poblacion = 15776 if LocalidadInstitutoenelcursas=="La Presentació Arenys de Mar"
// Badalona
// Badalona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Badalona"
replace SocioeconStatPers = 11495 if LocalidadInstitutoenelcursas=="Badalona"
replace SocioeconStatHog = 30705 if LocalidadInstitutoenelcursas=="Badalona"
replace poblacion = 220440 if LocalidadInstitutoenelcursas=="Badalona"
// Badalona 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Badalona "
replace SocioeconStatPers = 11495 if LocalidadInstitutoenelcursas=="Badalona "
replace SocioeconStatHog = 30705 if LocalidadInstitutoenelcursas=="Badalona "
replace poblacion = 220440 if LocalidadInstitutoenelcursas=="Badalona "
// Badalona/Maristes Champanyat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
replace SocioeconStatPers = 11495 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
replace SocioeconStatHog = 30705 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
replace poblacion = 220440 if LocalidadInstitutoenelcursas=="Badalona/Maristes Champanyat"
// Grabi Pere Verges, Badalona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Grabi Pere Verges, Badalona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Grabi Pere Verges, Badalona"
replace SocioeconStatPers = 11495 if LocalidadInstitutoenelcursas=="Grabi Pere Verges, Badalona"
replace SocioeconStatHog = 30705 if LocalidadInstitutoenelcursas=="Grabi Pere Verges, Badalona"
replace poblacion = 220440 if LocalidadInstitutoenelcursas=="Grabi Pere Verges, Badalona"
// Barcelona
// Barclona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barclona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barclona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barclona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barclona"
//  Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas==" Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas==" Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas==" Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas==" Barcelona"
// BARCELONA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BARCELONA"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="BARCELONA"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="BARCELONA"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="BARCELONA"
// Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona"
// Barcelona 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona "
// barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="barcelona"
// Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona"
// Lestonnac
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lestonnac"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lestonnac"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Lestonnac"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Lestonnac"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Lestonnac"
// Teresianas Ganduxer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Teresianas Ganduxer"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Teresianas Ganduxer"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Teresianas Ganduxer"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Teresianas Ganduxer"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Teresianas Ganduxer"
// Lestonnac Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lestonnac Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lestonnac Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Lestonnac Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Lestonnac Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Lestonnac Barcelona"
// Virgen de Atocha
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Virgen de Atocha"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Virgen de Atocha"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Virgen de Atocha"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Virgen de Atocha"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Virgen de Atocha"
// Salesians Rocafort
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesians Rocafort"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesians Rocafort"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Salesians Rocafort"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Salesians Rocafort"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Salesians Rocafort"
// Salesians de Sarrià
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesians de Sarrià"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesians de Sarrià"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Salesians de Sarrià"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Salesians de Sarrià"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Salesians de Sarrià"
// Sagrat Cor Diputació
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrat Cor Diputació"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrat Cor Diputació"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sagrat Cor Diputació"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sagrat Cor Diputació"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sagrat Cor Diputació"
// Pedralbes- Liceo francés
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pedralbes- Liceo francés"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Pedralbes- Liceo francés"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Pedralbes- Liceo francés"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Pedralbes- Liceo francés"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Pedralbes- Liceo francés"
// Pare Manyanet Les Corts
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pare Manyanet Les Corts"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Pare Manyanet Les Corts"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Pare Manyanet Les Corts"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Pare Manyanet Les Corts"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Pare Manyanet Les Corts"
// IES l'Alzina
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES l'Alzina"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES l'Alzina"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="IES l'Alzina"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="IES l'Alzina"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="IES l'Alzina"
// La Salle Bonanova
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Bonanova"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Bonanova"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="La Salle Bonanova"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="La Salle Bonanova"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="La Salle Bonanova"
// Jesuitas Clot
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jesuitas Clot"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jesuitas Clot"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Jesuitas Clot"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Jesuitas Clot"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Jesuitas Clot"
// Jesuïtes Sarrià Sant Ignasi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jesuïtes Sarrià Sant Ignasi"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jesuïtes Sarrià Sant Ignasi"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Jesuïtes Sarrià Sant Ignasi"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Jesuïtes Sarrià Sant Ignasi"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Jesuïtes Sarrià Sant Ignasi"
// Jesuïtes-Sarrià Sant Ignasi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jesuïtes-Sarrià Sant Ignasi"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jesuïtes-Sarrià Sant Ignasi"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Jesuïtes-Sarrià Sant Ignasi"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Jesuïtes-Sarrià Sant Ignasi"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Jesuïtes-Sarrià Sant Ignasi"
// Campus de la Ciutadella, Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Campus de la Ciutadella, Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Campus de la Ciutadella, Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Campus de la Ciutadella, Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Campus de la Ciutadella, Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Campus de la Ciutadella, Barcelona"
// Providencia del Corazón de Jesús
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Providencia del Corazón de Jesús"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Providencia del Corazón de Jesús"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Providencia del Corazón de Jesús"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Providencia del Corazón de Jesús"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Providencia del Corazón de Jesús"
// Barcelona / Jesuïtes el Clot
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona / Jesuïtes el Clot"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona / Jesuïtes el Clot"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona / Jesuïtes el Clot"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona / Jesuïtes el Clot"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona / Jesuïtes el Clot"
// Moisès Broggi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Moisès Broggi"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Moisès Broggi"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Moisès Broggi"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Moisès Broggi"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Moisès Broggi"
// Barcelona, Jesuitas de Casp
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona, Jesuitas de Casp"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona, Jesuitas de Casp"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona, Jesuitas de Casp"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona, Jesuitas de Casp"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona, Jesuitas de Casp"
// Barcelona, Real Monasterio de Santa Isabel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona, Real Monasterio de Santa Isabel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona, Real Monasterio de Santa Isabel"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona, Real Monasterio de Santa Isabel"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona, Real Monasterio de Santa Isabel"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona, Real Monasterio de Santa Isabel"
// Barcelona-EMDN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona-EMDN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona-EMDN"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona-EMDN"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona-EMDN"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona-EMDN"
// Barcelona/ IES JAUME BALMES
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/ IES JAUME BALMES"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/ IES JAUME BALMES"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/ IES JAUME BALMES"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/ IES JAUME BALMES"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/ IES JAUME BALMES"
// Centre d'Estudis Jaume Balmes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Centre d'Estudis Jaume Balmes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Centre d'Estudis Jaume Balmes"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Centre d'Estudis Jaume Balmes"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Centre d'Estudis Jaume Balmes"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Centre d'Estudis Jaume Balmes"
// Barcelona/Institut Jaume Balmes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Institut Jaume Balmes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Institut Jaume Balmes"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Institut Jaume Balmes"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Institut Jaume Balmes"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Institut Jaume Balmes"
// Barcelona/Monlau
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Monlau"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Monlau"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Monlau"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Monlau"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Monlau"
// Barcelona/Sant Ignasi Sarrià
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Sant Ignasi Sarrià"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Sant Ignasi Sarrià"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Sant Ignasi Sarrià"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Sant Ignasi Sarrià"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Sant Ignasi Sarrià"
// Institut Jaume Balmes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Jaume Balmes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Jaume Balmes"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Institut Jaume Balmes"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Institut Jaume Balmes"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Institut Jaume Balmes"
// Barcelona/Súnion
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Súnion"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Súnion"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Súnion"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Súnion"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Súnion"
// Barcelona/Vedruna Gracia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Vedruna Gracia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Vedruna Gracia"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Vedruna Gracia"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Vedruna Gracia"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Vedruna Gracia"
// Institut Moisès Broggi, Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Moisès Broggi, Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Moisès Broggi, Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Institut Moisès Broggi, Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Institut Moisès Broggi, Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Institut Moisès Broggi, Barcelona"
// Abat Oliba Spinola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Abat Oliba Spinola"
// Barcelona/Abat Oliba Cardenal Spínola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Abat Oliba Cardenal Spínola"
// Cardenal Spinola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cardenal Spinola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cardenal Spinola"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Cardenal Spinola"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Cardenal Spinola"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Cardenal Spinola"
// Cardenal Spinola 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cardenal Spinola "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cardenal Spinola "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Cardenal Spinola "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Cardenal Spinola "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Cardenal Spinola "
// CIC
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CIC"
replace instituto = 1 if LocalidadInstitutoenelcursas=="CIC"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="CIC"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="CIC"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="CIC"
// cic
replace cuidad = 1 if LocalidadInstitutoenelcursas=="cic"
replace instituto = 1 if LocalidadInstitutoenelcursas=="cic"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="cic"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="cic"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="cic"
// Barcelona, betania patmos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona, betania patmos"
// Betania Patmos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Betania Patmos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Betania Patmos"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Betania Patmos"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Betania Patmos"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Betania Patmos"
// Betania Patmos Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Betania Patmos Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Betania Patmos Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Betania Patmos Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Betania Patmos Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Betania Patmos Barcelona"
// Cic Batxillerats, Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Cic Batxillerats, Barcelona"
// Cic batxillerats
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cic batxillerats"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cic batxillerats"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Cic batxillerats"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Cic batxillerats"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Cic batxillerats"
// Barcelona/Dominicas Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Dominicas Barcelona"
// Barcelona, Frederic Mistral Tecnica Eulalia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral Tecnica Eulalia"
// Barcelona, Frederic Mistral-Tècnic Eulàlia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona, Frederic Mistral-Tècnic Eulàlia"
// Barcelona / Jesus María Sant Andreu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona / Jesus María Sant Andreu"
// Institut Menéndez y Pelayo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Institut Menéndez y Pelayo"
* Assuming Escola Pia Balmes
// Escola Ia Balmes (Barcelona)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona)"
// Escola Ia Balmes (Barcelona) 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Escola Ia Balmes (Barcelona) "
// Escola Pia Sarrià
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Escola Pia Sarrià"
// Barcelona/Príncep de Viana
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Barcelona/Príncep de Viana"
// Frederic mistral tècnic eulàlia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Frederic mistral tècnic eulàlia"
* Assuming Sant Ignasi
// Dant Ignasi
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Dant Ignasi"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Dant Ignasi"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Dant Ignasi"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Dant Ignasi"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Dant Ignasi"
// Escuela Suiza de Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Escuela Suiza de Barcelona"
// IPSI
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IPSI"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IPSI"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="IPSI"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="IPSI"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="IPSI"
// Jesuïtes Casp
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Jesuïtes Casp"
// ípsilons Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="ípsilons Barcelona"
// ípsilons Barcelona 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
replace instituto = 1 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="ípsilons Barcelona "
// Sagrado Corazón Sarria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria"
// Sagrado Corazón Sarria 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sagrado Corazón Sarria "
// Sagrado Corazon de Sarria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazon de Sarria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazon de Sarria"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sagrado Corazon de Sarria"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sagrado Corazon de Sarria"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sagrado Corazon de Sarria"
// Sarrià
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sarrià"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sarrià"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sarrià"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sarrià"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sarrià"
// Sarrià 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sarrià "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sarrià "
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Sarrià "
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Sarrià "
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Sarrià "
// Santa Teresa Ganduxer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Santa Teresa Ganduxer"
// Centre Escolar Valldaura
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Centre Escolar Valldaura"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Centre Escolar Valldaura"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Centre Escolar Valldaura"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Centre Escolar Valldaura"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Centre Escolar Valldaura"
// Col•legi Claret Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Col•legi Claret Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Col•legi Claret Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Col•legi Claret Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Col•legi Claret Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Col•legi Claret Barcelona"
// Escola Pia Nostra Senyora
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Pia Nostra Senyora"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Pia Nostra Senyora"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Escola Pia Nostra Senyora"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Escola Pia Nostra Senyora"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Escola Pia Nostra Senyora"
// FREDERIC MISTRAL TECNIC EULALIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TECNIC EULALIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TECNIC EULALIA"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TECNIC EULALIA"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TECNIC EULALIA"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TECNIC EULALIA"
// FREDERIC MISTRAL TÈCNIC EULÀLIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TÈCNIC EULÀLIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TÈCNIC EULÀLIA"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TÈCNIC EULÀLIA"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TÈCNIC EULÀLIA"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="FREDERIC MISTRAL TÈCNIC EULÀLIA"
// Fert Bachillerato Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Fert Bachillerato Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Fert Bachillerato Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Fert Bachillerato Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Fert Bachillerato Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Fert Bachillerato Barcelona"
// Frederic Mistral Tècnic Eulàlia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Frederic Mistral Tècnic Eulàlia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Frederic Mistral Tècnic Eulàlia"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Frederic Mistral Tècnic Eulàlia"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Frederic Mistral Tècnic Eulàlia"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Frederic Mistral Tècnic Eulàlia"
// Frederic Mistral, Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Frederic Mistral, Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Frederic Mistral, Barcelona"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Frederic Mistral, Barcelona"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Frederic Mistral, Barcelona"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Frederic Mistral, Barcelona"
// Frederic Mistral-Tècnic Eulàlia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Frederic Mistral-Tècnic Eulàlia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Frederic Mistral-Tècnic Eulàlia"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="Frederic Mistral-Tècnic Eulàlia"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="Frederic Mistral-Tècnic Eulàlia"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="Frederic Mistral-Tècnic Eulàlia"
// INS MENÉNDEZ Y PELAYO / BCN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS MENÉNDEZ Y PELAYO / BCN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS MENÉNDEZ Y PELAYO / BCN"
replace SocioeconStatPers = 15755 if LocalidadInstitutoenelcursas=="INS MENÉNDEZ Y PELAYO / BCN"
replace SocioeconStatHog = 37881 if LocalidadInstitutoenelcursas=="INS MENÉNDEZ Y PELAYO / BCN"
replace poblacion = 1636762 if LocalidadInstitutoenelcursas=="INS MENÉNDEZ Y PELAYO / BCN"
// Berga
// Berga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Berga"
replace SocioeconStatPers = 12641 if LocalidadInstitutoenelcursas=="Berga"
replace SocioeconStatHog = 30611 if LocalidadInstitutoenelcursas=="Berga"
replace poblacion = 16494 if LocalidadInstitutoenelcursas=="Berga"
// Calaf
// Calaf/Institut Alexandre de Riquer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Calaf/Institut Alexandre de Riquer"
replace SocioeconStatPers = 11242 if LocalidadInstitutoenelcursas=="Calaf/Institut Alexandre de Riquer"
replace SocioeconStatHog = 29353 if LocalidadInstitutoenelcursas=="Calaf/Institut Alexandre de Riquer"
replace poblacion = 3535 if LocalidadInstitutoenelcursas=="Calaf/Institut Alexandre de Riquer"
// Calella
// Calella/IES Bisbe Sivilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Calella/IES Bisbe Sivilla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Calella/IES Bisbe Sivilla"
replace SocioeconStatPers = 11071 if LocalidadInstitutoenelcursas=="Calella/IES Bisbe Sivilla"
replace SocioeconStatHog = 28122 if LocalidadInstitutoenelcursas=="Calella/IES Bisbe Sivilla"
replace poblacion = 19069 if LocalidadInstitutoenelcursas=="Calella/IES Bisbe Sivilla"
// Escola Freta Calella
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Freta Calella"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Freta Calella"
replace SocioeconStatPers = 11071 if LocalidadInstitutoenelcursas=="Escola Freta Calella"
replace SocioeconStatHog = 28122 if LocalidadInstitutoenelcursas=="Escola Freta Calella"
replace poblacion = 19069 if LocalidadInstitutoenelcursas=="Escola Freta Calella"
// Escola Pia Calella
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Pia Calella"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Pia Calella"
replace SocioeconStatPers = 11071 if LocalidadInstitutoenelcursas=="Escola Pia Calella"
replace SocioeconStatHog = 28122 if LocalidadInstitutoenelcursas=="Escola Pia Calella"
replace poblacion = 19069 if LocalidadInstitutoenelcursas=="Escola Pia Calella"
// Canet de Mar
// Canet de Mar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Canet de Mar"
replace SocioeconStatPers = 12359 if LocalidadInstitutoenelcursas=="Canet de Mar"
replace SocioeconStatHog = 31895 if LocalidadInstitutoenelcursas=="Canet de Mar"
replace poblacion = 14526 if LocalidadInstitutoenelcursas=="Canet de Mar"
// Cardedeu
// Cardedeu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cardedeu"
replace SocioeconStatPers = 13640 if LocalidadInstitutoenelcursas=="Cardedeu"
replace SocioeconStatHog = 37447 if LocalidadInstitutoenelcursas=="Cardedeu"
replace poblacion = 18357 if LocalidadInstitutoenelcursas=="Cardedeu"
// Cardona
// Ins Sant Ramon, Cardona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ins Sant Ramon, Cardona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ins Sant Ramon, Cardona"
replace SocioeconStatPers = 13377 if LocalidadInstitutoenelcursas=="Ins Sant Ramon, Cardona"
replace SocioeconStatHog = 33076 if LocalidadInstitutoenelcursas=="Ins Sant Ramon, Cardona"
replace poblacion = 4636 if LocalidadInstitutoenelcursas=="Ins Sant Ramon, Cardona"
// Castellar del Vallès
// Castellar del Vallès/ Ins Puig de la Creu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Castellar del Vallès/ Ins Puig de la Creu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Castellar del Vallès/ Ins Puig de la Creu"
replace SocioeconStatPers = 13565 if LocalidadInstitutoenelcursas=="Castellar del Vallès/ Ins Puig de la Creu"
replace SocioeconStatHog = 37568 if LocalidadInstitutoenelcursas=="Castellar del Vallès/ Ins Puig de la Creu"
replace poblacion = 24187 if LocalidadInstitutoenelcursas=="Castellar del Vallès/ Ins Puig de la Creu"
// Castellbisbal
// Castellbisbal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Castellbisbal"
replace SocioeconStatPers = 13238 if LocalidadInstitutoenelcursas=="Castellbisbal"
replace SocioeconStatHog = 38386 if LocalidadInstitutoenelcursas=="Castellbisbal"
replace poblacion = 12390 if LocalidadInstitutoenelcursas=="Castellbisbal"
// Castelldefels
// INS Les Marines
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS Les Marines"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS Les Marines"
replace SocioeconStatPers = 16555 if LocalidadInstitutoenelcursas=="INS Les Marines"
replace SocioeconStatHog = 45885 if LocalidadInstitutoenelcursas=="INS Les Marines"
replace poblacion = 67004 if LocalidadInstitutoenelcursas=="INS Les Marines"
// Esparreguera
// Esparreguera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Esparreguera"
replace SocioeconStatPers = 12619 if LocalidadInstitutoenelcursas=="Esparreguera"
replace SocioeconStatHog = 34130 if LocalidadInstitutoenelcursas=="Esparreguera"
replace poblacion = 22251 if LocalidadInstitutoenelcursas=="Esparreguera"
// Esplugues del Llobregat / Esplugas
// Esplugues del Llobregat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Esplugues del Llobregat"
replace SocioeconStatPers = 14857 if LocalidadInstitutoenelcursas=="Esplugues del Llobregat"
replace SocioeconStatHog = 39457 if LocalidadInstitutoenelcursas=="Esplugues del Llobregat"
replace poblacion = 46680 if LocalidadInstitutoenelcursas=="Esplugues del Llobregat"
// La Miranda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Miranda"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Miranda"
replace SocioeconStatPers = 14857 if LocalidadInstitutoenelcursas=="La Miranda"
replace SocioeconStatHog = 39457 if LocalidadInstitutoenelcursas=="La Miranda"
replace poblacion = 46680 if LocalidadInstitutoenelcursas=="La Miranda"
// Garriga, La
// La Garriga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Garriga"
replace SocioeconStatPers = 14110 if LocalidadInstitutoenelcursas=="La Garriga"
replace SocioeconStatHog = 38100 if LocalidadInstitutoenelcursas=="La Garriga"
replace poblacion = 16514 if LocalidadInstitutoenelcursas=="La Garriga"
// Gavà
// Gavà
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gavà"
replace SocioeconStatPers = 13723 if LocalidadInstitutoenelcursas=="Gavà"
replace SocioeconStatHog = 37370 if LocalidadInstitutoenelcursas=="Gavà"
replace poblacion = 46771 if LocalidadInstitutoenelcursas=="Gavà"
// Gironella
// Ies Pere Fontdevila
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace SocioeconStatPers = 12203 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace SocioeconStatHog = 30035 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace poblacion = 4826 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
// Ies Pere Fontdevila
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace SocioeconStatPers = 12203 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace SocioeconStatHog = 30035 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
replace poblacion = 4826 if LocalidadInstitutoenelcursas=="Ies Pere Fontdevila"
// Granollers
// Granollers
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Granollers"
replace SocioeconStatPers = 12798 if LocalidadInstitutoenelcursas=="Granollers"
replace SocioeconStatHog = 34496 if LocalidadInstitutoenelcursas=="Granollers"
replace poblacion = 61275 if LocalidadInstitutoenelcursas=="Granollers"
// Carles Vallbona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Carles Vallbona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Carles Vallbona"
replace SocioeconStatPers = 12798 if LocalidadInstitutoenelcursas=="Carles Vallbona"
replace SocioeconStatHog = 34496 if LocalidadInstitutoenelcursas=="Carles Vallbona"
replace poblacion = 61275 if LocalidadInstitutoenelcursas=="Carles Vallbona"
// Hospitalet de Llobregat
// Hospitalet de Llobregat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Hospitalet de Llobregat"
replace SocioeconStatPers = 10917 if LocalidadInstitutoenelcursas=="Hospitalet de Llobregat"
replace SocioeconStatHog = 28985 if LocalidadInstitutoenelcursas=="Hospitalet de Llobregat"
replace poblacion = 264923 if LocalidadInstitutoenelcursas=="Hospitalet de Llobregat"
// Tecla Sala
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tecla Sala"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Tecla Sala"
replace SocioeconStatPers = 10917 if LocalidadInstitutoenelcursas=="Tecla Sala"
replace SocioeconStatHog = 28985 if LocalidadInstitutoenelcursas=="Tecla Sala"
replace poblacion = 264923 if LocalidadInstitutoenelcursas=="Tecla Sala"
// Tecla Sala (Hospitalet de Llobregat)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tecla Sala (Hospitalet de Llobregat)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Tecla Sala (Hospitalet de Llobregat)"
replace SocioeconStatPers = 10917 if LocalidadInstitutoenelcursas=="Tecla Sala (Hospitalet de Llobregat)"
replace SocioeconStatHog = 28985 if LocalidadInstitutoenelcursas=="Tecla Sala (Hospitalet de Llobregat)"
replace poblacion = 264923 if LocalidadInstitutoenelcursas=="Tecla Sala (Hospitalet de Llobregat)"
// Col·legi Tecla Sala
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Col·legi Tecla Sala"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Col·legi Tecla Sala"
replace SocioeconStatPers = 10917 if LocalidadInstitutoenelcursas=="Col·legi Tecla Sala"
replace SocioeconStatHog = 28985 if LocalidadInstitutoenelcursas=="Col·legi Tecla Sala"
replace poblacion = 264923 if LocalidadInstitutoenelcursas=="Col·legi Tecla Sala"
// Xaloc
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Xaloc"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Xaloc"
replace SocioeconStatPers = 10917 if LocalidadInstitutoenelcursas=="Xaloc"
replace SocioeconStatHog = 28985 if LocalidadInstitutoenelcursas=="Xaloc"
replace poblacion = 264923 if LocalidadInstitutoenelcursas=="Xaloc"
// Igualada
// Igualada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Igualada"
replace SocioeconStatPers = 12505 if LocalidadInstitutoenelcursas=="Igualada"
replace SocioeconStatHog = 32369 if LocalidadInstitutoenelcursas=="Igualada"
replace poblacion = 39967 if LocalidadInstitutoenelcursas=="Igualada"
// Igualada/ Pere Vives Vich
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Igualada/ Pere Vives Vich"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Igualada/ Pere Vives Vich"
replace SocioeconStatPers = 12505 if LocalidadInstitutoenelcursas=="Igualada/ Pere Vives Vich"
replace SocioeconStatHog = 32369 if LocalidadInstitutoenelcursas=="Igualada/ Pere Vives Vich"
replace poblacion = 39967 if LocalidadInstitutoenelcursas=="Igualada/ Pere Vives Vich"
// LLinars del Vallès
// Escola Ginebró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Ginebró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Ginebró"
replace SocioeconStatPers = 13023 if LocalidadInstitutoenelcursas=="Escola Ginebró"
replace SocioeconStatHog = 35332 if LocalidadInstitutoenelcursas=="Escola Ginebró"
replace poblacion = 9938 if LocalidadInstitutoenelcursas=="Escola Ginebró"
// Malgrat de Mar
// INS Ramon Turro i Darder
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
replace SocioeconStatPers = 11251 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
replace SocioeconStatHog = 28985 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
replace poblacion = 18579 if LocalidadInstitutoenelcursas=="INS Ramon Turro i Darder"
// Manlleu
// La Salle Manlleu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Manlleu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Manlleu"
replace SocioeconStatPers = 11221 if LocalidadInstitutoenelcursas=="La Salle Manlleu"
replace SocioeconStatHog = 31866 if LocalidadInstitutoenelcursas=="La Salle Manlleu"
replace poblacion = 20573 if LocalidadInstitutoenelcursas=="La Salle Manlleu"
// Manresa
// Manresa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Manresa"
replace SocioeconStatPers = 12389 if LocalidadInstitutoenelcursas=="Manresa"
replace SocioeconStatHog = 31422 if LocalidadInstitutoenelcursas=="Manresa"
replace poblacion = 77174 if LocalidadInstitutoenelcursas=="Manresa"
// Joviat Manresa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Joviat Manresa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Joviat Manresa"
replace SocioeconStatPers = 12389 if LocalidadInstitutoenelcursas=="Joviat Manresa"
replace SocioeconStatHog = 31422 if LocalidadInstitutoenelcursas=="Joviat Manresa"
replace poblacion = 77174 if LocalidadInstitutoenelcursas=="Joviat Manresa"
// Manresa, La Salle
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Manresa, La Salle"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Manresa, La Salle"
replace SocioeconStatPers = 12389 if LocalidadInstitutoenelcursas=="Manresa, La Salle"
replace SocioeconStatHog = 31422 if LocalidadInstitutoenelcursas=="Manresa, La Salle"
replace poblacion = 77174 if LocalidadInstitutoenelcursas=="Manresa, La Salle"
// Martorell
// Martorell
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Martorell"
replace SocioeconStatPers = 11891 if LocalidadInstitutoenelcursas=="Martorell"
replace SocioeconStatHog = 34056 if LocalidadInstitutoenelcursas=="Martorell"
replace poblacion = 28189 if LocalidadInstitutoenelcursas=="Martorell"
// Masnou, El
// El Masnou
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Masnou"
replace SocioeconStatPers = 15135 if LocalidadInstitutoenelcursas=="El Masnou"
replace SocioeconStatHog = 39690 if LocalidadInstitutoenelcursas=="El Masnou"
replace poblacion = 23515 if LocalidadInstitutoenelcursas=="El Masnou"
// Masquefa
// Instituto de Masquefa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Instituto de Masquefa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Instituto de Masquefa"
replace SocioeconStatPers = 12127 if LocalidadInstitutoenelcursas=="Instituto de Masquefa"
replace SocioeconStatHog = 33903 if LocalidadInstitutoenelcursas=="Instituto de Masquefa"
replace poblacion = 9211 if LocalidadInstitutoenelcursas=="Instituto de Masquefa"
// Mataró
// Mataró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mataró"
replace SocioeconStatPers = 11125 if LocalidadInstitutoenelcursas=="Mataró"
replace SocioeconStatHog = 30250 if LocalidadInstitutoenelcursas=="Mataró"
replace poblacion = 128265 if LocalidadInstitutoenelcursas=="Mataró"
// Mataró, Escola Pia Mataró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mataró, Escola Pia Mataró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Mataró, Escola Pia Mataró"
replace SocioeconStatPers = 11125 if LocalidadInstitutoenelcursas=="Mataró, Escola Pia Mataró"
replace SocioeconStatHog = 30250 if LocalidadInstitutoenelcursas=="Mataró, Escola Pia Mataró"
replace poblacion = 128265 if LocalidadInstitutoenelcursas=="Mataró, Escola Pia Mataró"
// Escola Pia Mataró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Pia Mataró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Pia Mataró"
replace SocioeconStatPers = 11125 if LocalidadInstitutoenelcursas=="Escola Pia Mataró"
replace SocioeconStatHog = 30250 if LocalidadInstitutoenelcursas=="Escola Pia Mataró"
replace poblacion = 128265 if LocalidadInstitutoenelcursas=="Escola Pia Mataró"
// Escola Pia Santa Anna Mataró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escola Pia Santa Anna Mataró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escola Pia Santa Anna Mataró"
replace SocioeconStatPers = 11125 if LocalidadInstitutoenelcursas=="Escola Pia Santa Anna Mataró"
replace SocioeconStatHog = 30250 if LocalidadInstitutoenelcursas=="Escola Pia Santa Anna Mataró"
replace poblacion = 128265 if LocalidadInstitutoenelcursas=="Escola Pia Santa Anna Mataró"
// Maristes Valldemia Mataró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Maristes Valldemia Mataró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Maristes Valldemia Mataró"
replace SocioeconStatPers = 11125 if LocalidadInstitutoenelcursas=="Maristes Valldemia Mataró"
replace SocioeconStatHog = 30250 if LocalidadInstitutoenelcursas=="Maristes Valldemia Mataró"
replace poblacion = 128265 if LocalidadInstitutoenelcursas=="Maristes Valldemia Mataró"
// Mataró/Maristes Valldemia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mataró/Maristes Valldemia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Mataró/Maristes Valldemia"
replace SocioeconStatPers = 11125 if LocalidadInstitutoenelcursas=="Mataró/Maristes Valldemia"
replace SocioeconStatHog = 30250 if LocalidadInstitutoenelcursas=="Mataró/Maristes Valldemia"
replace poblacion = 128265 if LocalidadInstitutoenelcursas=="Mataró/Maristes Valldemia"
// Mollet del Vallès
// mollet del valles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="mollet del valles"
replace SocioeconStatPers = 12075 if LocalidadInstitutoenelcursas=="mollet del valles"
replace SocioeconStatHog = 32350 if LocalidadInstitutoenelcursas=="mollet del valles"
replace poblacion = 51318 if LocalidadInstitutoenelcursas=="mollet del valles"
// mollet/vicenç plantada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
replace instituto = 1 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
replace SocioeconStatPers = 12075 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
replace SocioeconStatHog = 32350 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
replace poblacion = 51318 if LocalidadInstitutoenelcursas=="mollet/vicenç plantada"
// Mollet del Vallés, Barcelona/ Ins Gallecs
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mollet del Vallés, Barcelona/ Ins Gallecs"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Mollet del Vallés, Barcelona/ Ins Gallecs"
replace SocioeconStatPers = 12075 if LocalidadInstitutoenelcursas=="Mollet del Vallés, Barcelona/ Ins Gallecs"
replace SocioeconStatHog = 32350 if LocalidadInstitutoenelcursas=="Mollet del Vallés, Barcelona/ Ins Gallecs"
replace poblacion = 51318 if LocalidadInstitutoenelcursas=="Mollet del Vallés, Barcelona/ Ins Gallecs"
// Montgat
// montgat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="montgat"
replace SocioeconStatPers = 14346 if LocalidadInstitutoenelcursas=="montgat"
replace SocioeconStatHog = 37657 if LocalidadInstitutoenelcursas=="montgat"
replace poblacion = 12041 if LocalidadInstitutoenelcursas=="montgat"
// Moyá / Moà
// Moià
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Moià"
replace SocioeconStatPers = 12337 if LocalidadInstitutoenelcursas=="Moià"
replace SocioeconStatHog = 32858 if LocalidadInstitutoenelcursas=="Moià"
replace poblacion = 6267 if LocalidadInstitutoenelcursas=="Moià"
// Navàs
// Navàs
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Navàs"
replace SocioeconStatPers = 12276 if LocalidadInstitutoenelcursas=="Navàs"
replace SocioeconStatHog = 30979 if LocalidadInstitutoenelcursas=="Navàs"
replace poblacion = 6022 if LocalidadInstitutoenelcursas=="Navàs"
// Palau-solità i Plegamans
// Palau-Solità i Plegamans
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Palau-Solità i Plegamans"
replace SocioeconStatPers = 13621 if LocalidadInstitutoenelcursas=="Palau-Solità i Plegamans"
replace SocioeconStatHog = 39722 if LocalidadInstitutoenelcursas=="Palau-Solità i Plegamans"
replace poblacion = 14771 if LocalidadInstitutoenelcursas=="Palau-Solità i Plegamans"
// INS Ramon Casas i Carbó
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS Ramon Casas i Carbó"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS Ramon Casas i Carbó"
replace SocioeconStatPers = 13621 if LocalidadInstitutoenelcursas=="INS Ramon Casas i Carbó"
replace SocioeconStatHog = 39722 if LocalidadInstitutoenelcursas=="INS Ramon Casas i Carbó"
replace poblacion = 14771 if LocalidadInstitutoenelcursas=="INS Ramon Casas i Carbó"
// Sabadell
// Sabadell
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sabadell"
replace SocioeconStatPers = 12533 if LocalidadInstitutoenelcursas=="Sabadell"
replace SocioeconStatHog = 32494 if LocalidadInstitutoenelcursas=="Sabadell"
replace poblacion = 213644 if LocalidadInstitutoenelcursas=="Sabadell"
// SABADELL
replace cuidad = 1 if LocalidadInstitutoenelcursas=="SABADELL"
replace SocioeconStatPers = 12533 if LocalidadInstitutoenelcursas=="SABADELL"
replace SocioeconStatHog = 32494 if LocalidadInstitutoenelcursas=="SABADELL"
replace poblacion = 213644 if LocalidadInstitutoenelcursas=="SABADELL"
// Instituto Sagrada Familia de Sabadell
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Instituto Sagrada Familia de Sabadell"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Instituto Sagrada Familia de Sabadell"
replace SocioeconStatPers = 12533 if LocalidadInstitutoenelcursas=="Instituto Sagrada Familia de Sabadell"
replace SocioeconStatHog = 32494 if LocalidadInstitutoenelcursas=="Instituto Sagrada Familia de Sabadell"
replace poblacion = 213644 if LocalidadInstitutoenelcursas=="Instituto Sagrada Familia de Sabadell"
// Joaquim Blume
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Joaquim Blume"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Joaquim Blume"
replace SocioeconStatPers = 12533 if LocalidadInstitutoenelcursas=="Joaquim Blume"
replace SocioeconStatHog = 32494 if LocalidadInstitutoenelcursas=="Joaquim Blume"
replace poblacion = 213644 if LocalidadInstitutoenelcursas=="Joaquim Blume"
// Sant Adrià de Besòs
// Institut Manuel Vazquez Montalban (Sant Adrià de Besòs)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Manuel Vazquez Montalban (Sant Adrià de Besòs)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Manuel Vazquez Montalban (Sant Adrià de Besòs)"
replace SocioeconStatPers = 10466 if LocalidadInstitutoenelcursas=="Institut Manuel Vazquez Montalban (Sant Adrià de Besòs)"
replace SocioeconStatHog = 28845 if LocalidadInstitutoenelcursas=="Institut Manuel Vazquez Montalban (Sant Adrià de Besòs)"
replace poblacion = 37097 if LocalidadInstitutoenelcursas=="Institut Manuel Vazquez Montalban (Sant Adrià de Besòs)"
// Sant Andreu de Llavaneres
// Institut Llavaneres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Llavaneres"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Llavaneres"
replace SocioeconStatPers = 15952 if LocalidadInstitutoenelcursas=="Institut Llavaneres"
replace SocioeconStatHog = 43884 if LocalidadInstitutoenelcursas=="Institut Llavaneres"
replace poblacion = 10968 if LocalidadInstitutoenelcursas=="Institut Llavaneres"
// Santa Coloma de Gramanet / Santa Coloma de Gramenet
// Santa Coloma de Gramenet
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Coloma de Gramenet"
replace SocioeconStatPers = 9910 if LocalidadInstitutoenelcursas=="Santa Coloma de Gramenet"
replace SocioeconStatHog = 27520 if LocalidadInstitutoenelcursas=="Santa Coloma de Gramenet"
replace poblacion = 119215 if LocalidadInstitutoenelcursas=="Santa Coloma de Gramenet"
// Instituto Numancia, Santa Coloma de Gramanet
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Instituto Numancia, Santa Coloma de Gramanet"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Instituto Numancia, Santa Coloma de Gramanet"
replace SocioeconStatPers = 9910 if LocalidadInstitutoenelcursas=="Instituto Numancia, Santa Coloma de Gramanet"
replace SocioeconStatHog = 27520 if LocalidadInstitutoenelcursas=="Instituto Numancia, Santa Coloma de Gramanet"
replace poblacion = 119215 if LocalidadInstitutoenelcursas=="Instituto Numancia, Santa Coloma de Gramanet"
// Sant Cugat
// Sant Cugat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Cugat"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="Sant Cugat"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="Sant Cugat"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="Sant Cugat"
// Sant Cugat del Vallès
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallès"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallès"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallès"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallès"
// Sant Cugat del Vallés
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallés"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallés"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallés"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="Sant Cugat del Vallés"
// sant CUGAT
replace cuidad = 1 if LocalidadInstitutoenelcursas=="sant CUGAT"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="sant CUGAT"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="sant CUGAT"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="sant CUGAT"
// La Farga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Farga"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Farga"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="La Farga"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="La Farga"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="La Farga"
// La Farga, Sant Cugat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="La Farga, Sant Cugat"
// Viaró Global School
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Viaró Global School"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Viaró Global School"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="Viaró Global School"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="Viaró Global School"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="Viaró Global School"
// Viaró
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Viaró"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Viaró"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="Viaró"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="Viaró"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="Viaró"
// European International School of Barcelona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="European International School of Barcelona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="European International School of Barcelona"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="European International School of Barcelona"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="European International School of Barcelona"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="European International School of Barcelona"
// Institut Angeleta Ferrer, Sant Cugat del Vallès
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Angeleta Ferrer, Sant Cugat del Vallès"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Angeleta Ferrer, Sant Cugat del Vallès"
replace SocioeconStatPers = 19591 if LocalidadInstitutoenelcursas=="Institut Angeleta Ferrer, Sant Cugat del Vallès"
replace SocioeconStatHog = 59606 if LocalidadInstitutoenelcursas=="Institut Angeleta Ferrer, Sant Cugat del Vallès"
replace poblacion = 91006 if LocalidadInstitutoenelcursas=="Institut Angeleta Ferrer, Sant Cugat del Vallès"
// Sant Pere de Ribes
// INS Can Puig
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS Can Puig"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS Can Puig"
replace SocioeconStatPers = 11918 if LocalidadInstitutoenelcursas=="INS Can Puig"
replace SocioeconStatHog = 32766 if LocalidadInstitutoenelcursas=="INS Can Puig"
replace poblacion = 30719 if LocalidadInstitutoenelcursas=="INS Can Puig"
// Les Roquetes del Garraf, Sant Pere de Ribes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Les Roquetes del Garraf, Sant Pere de Ribes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Les Roquetes del Garraf, Sant Pere de Ribes"
replace SocioeconStatPers = 11918 if LocalidadInstitutoenelcursas=="Les Roquetes del Garraf, Sant Pere de Ribes"
replace SocioeconStatHog = 32766 if LocalidadInstitutoenelcursas=="Les Roquetes del Garraf, Sant Pere de Ribes"
replace poblacion = 30719 if LocalidadInstitutoenelcursas=="Les Roquetes del Garraf, Sant Pere de Ribes"
// Santa Eulàlia de Ronçana
// INS La Vall del Tenes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
replace SocioeconStatPers = 13219 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
replace SocioeconStatHog = 37517 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
replace poblacion = 7288 if LocalidadInstitutoenelcursas=="INS La Vall del Tenes"
// Sant Feliu de Llobregat
// Sant feliu de llobregat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant feliu de llobregat"
replace SocioeconStatPers = 13897 if LocalidadInstitutoenelcursas=="Sant feliu de llobregat"
replace SocioeconStatHog = 37265 if LocalidadInstitutoenelcursas=="Sant feliu de llobregat"
replace poblacion = 44860 if LocalidadInstitutoenelcursas=="Sant feliu de llobregat"
// Sant Feliu de Llobregat/ Verge de la Salut
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
replace SocioeconStatPers = 13897 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
replace SocioeconStatHog = 37265 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
replace poblacion = 44860 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut"
// Sant Feliu de Llobregat/ Verge de la Salut 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
replace SocioeconStatPers = 13897 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
replace SocioeconStatHog = 37265 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
replace poblacion = 44860 if LocalidadInstitutoenelcursas=="Sant Feliu de Llobregat/ Verge de la Salut "
// Sant Joan de Vilatorrada / San Juan de Torruella
// Sant Joan de Vilatorrada/Institut Quercus
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Joan de Vilatorrada/Institut Quercus"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Joan de Vilatorrada/Institut Quercus"
replace SocioeconStatPers = 12405 if LocalidadInstitutoenelcursas=="Sant Joan de Vilatorrada/Institut Quercus"
replace SocioeconStatHog = 33037 if LocalidadInstitutoenelcursas=="Sant Joan de Vilatorrada/Institut Quercus"
replace poblacion = 10936 if LocalidadInstitutoenelcursas=="Sant Joan de Vilatorrada/Institut Quercus"
//Sant Joan Despí
// Sant Joan Despí
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Joan Despí"
replace SocioeconStatPers = 14565 if LocalidadInstitutoenelcursas=="Sant Joan Despí"
replace SocioeconStatHog = 39846 if LocalidadInstitutoenelcursas=="Sant Joan Despí"
replace poblacion = 34123 if LocalidadInstitutoenelcursas=="Sant Joan Despí"
// Sant Just Desvern / San justo Desvern
// IES Sant Just Desvern
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Sant Just Desvern"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Sant Just Desvern"
replace SocioeconStatPers = 20152 if LocalidadInstitutoenelcursas=="IES Sant Just Desvern"
replace SocioeconStatHog = 54474 if LocalidadInstitutoenelcursas=="IES Sant Just Desvern"
replace poblacion = 17805 if LocalidadInstitutoenelcursas=="IES Sant Just Desvern"
// Santa Margarida de Montbui / Santa Margarita de Montbuy
// santa margarida de montgui
replace cuidad = 1 if LocalidadInstitutoenelcursas=="santa margarida de montgui"
replace SocioeconStatPers = 10011 if LocalidadInstitutoenelcursas=="santa margarida de montgui"
replace SocioeconStatHog = 27060 if LocalidadInstitutoenelcursas=="santa margarida de montgui"
replace poblacion = 9980 if LocalidadInstitutoenelcursas=="santa margarida de montgui"
// Santa Margarita y Monjós / Santa Margarida i els Monjos
// Santa Margarida i els Monjos/ IES El Foix
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Margarida i els Monjos/ IES El Foix"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Margarida i els Monjos/ IES El Foix"
replace SocioeconStatPers = 11276 if LocalidadInstitutoenelcursas=="Santa Margarida i els Monjos/ IES El Foix"
replace SocioeconStatHog = 31233 if LocalidadInstitutoenelcursas=="Santa Margarida i els Monjos/ IES El Foix"
replace poblacion = 7611 if LocalidadInstitutoenelcursas=="Santa Margarida i els Monjos/ IES El Foix"
// Santa Perpetua de Moguda / Santa Perpètua de Mogoda
// santa perpetua
replace cuidad = 1 if LocalidadInstitutoenelcursas=="santa perpetua"
replace SocioeconStatPers = 12276 if LocalidadInstitutoenelcursas=="santa perpetua"
replace SocioeconStatHog = 34531 if LocalidadInstitutoenelcursas=="santa perpetua"
replace poblacion = 25799 if LocalidadInstitutoenelcursas=="santa perpetua"
// Sitges
// Sitges/Ins Vinyet
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sitges/Ins Vinyet"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sitges/Ins Vinyet"
replace SocioeconStatPers = 16143 if LocalidadInstitutoenelcursas=="Sitges/Ins Vinyet"
replace SocioeconStatHog = 39373 if LocalidadInstitutoenelcursas=="Sitges/Ins Vinyet"
replace poblacion = 29307 if LocalidadInstitutoenelcursas=="Sitges/Ins Vinyet"
// escola pia sitges
replace cuidad = 1 if LocalidadInstitutoenelcursas=="escola pia sitges"
replace instituto = 1 if LocalidadInstitutoenelcursas=="escola pia sitges"
replace SocioeconStatPers = 16143 if LocalidadInstitutoenelcursas=="escola pia sitges"
replace SocioeconStatHog = 39373 if LocalidadInstitutoenelcursas=="escola pia sitges"
replace poblacion = 29307 if LocalidadInstitutoenelcursas=="escola pia sitges"
// Terrassa
// Terrassa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Terrassa"
replace SocioeconStatPers = 12098 if LocalidadInstitutoenelcursas=="Terrassa"
replace SocioeconStatHog = 32184 if LocalidadInstitutoenelcursas=="Terrassa"
replace poblacion = 220556 if LocalidadInstitutoenelcursas=="Terrassa"
// Terrassa, Sagrat Cor de Jesus
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Terrassa, Sagrat Cor de Jesus"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Terrassa, Sagrat Cor de Jesus"
replace SocioeconStatPers = 12098 if LocalidadInstitutoenelcursas=="Terrassa, Sagrat Cor de Jesus"
replace SocioeconStatHog = 32184 if LocalidadInstitutoenelcursas=="Terrassa, Sagrat Cor de Jesus"
replace poblacion = 220556 if LocalidadInstitutoenelcursas=="Terrassa, Sagrat Cor de Jesus"
// Vic
// Vic
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vic"
replace SocioeconStatPers = 12708 if LocalidadInstitutoenelcursas=="Vic"
replace SocioeconStatHog = 34720 if LocalidadInstitutoenelcursas=="Vic"
replace poblacion = 46214 if LocalidadInstitutoenelcursas=="Vic"
// Sant Miquel dels Sants de Vic
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Miquel dels Sants de Vic"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Miquel dels Sants de Vic"
replace SocioeconStatPers = 12708 if LocalidadInstitutoenelcursas=="Sant Miquel dels Sants de Vic"
replace SocioeconStatHog = 34720 if LocalidadInstitutoenelcursas=="Sant Miquel dels Sants de Vic"
replace poblacion = 46214 if LocalidadInstitutoenelcursas=="Sant Miquel dels Sants de Vic"
// Sant Miquel dels sants
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sant Miquel dels sants"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sant Miquel dels sants"
replace SocioeconStatPers = 12708 if LocalidadInstitutoenelcursas=="Sant Miquel dels sants"
replace SocioeconStatHog = 34720 if LocalidadInstitutoenelcursas=="Sant Miquel dels sants"
replace poblacion = 46214 if LocalidadInstitutoenelcursas=="Sant Miquel dels sants"
// Vilafranca del Penedès
// Institut Alt Penedès en Vilafranca del Penedès
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Alt Penedès en Vilafranca del Penedès"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Alt Penedès en Vilafranca del Penedès"
replace SocioeconStatPers = 12266 if LocalidadInstitutoenelcursas=="Institut Alt Penedès en Vilafranca del Penedès"
replace SocioeconStatHog = 31880 if LocalidadInstitutoenelcursas=="Institut Alt Penedès en Vilafranca del Penedès"
replace poblacion = 39746 if LocalidadInstitutoenelcursas=="Institut Alt Penedès en Vilafranca del Penedès"
//Vilanova i la Geltrú
// vilanova
replace cuidad = 1 if LocalidadInstitutoenelcursas=="vilanova"
replace SocioeconStatPers = 12777 if LocalidadInstitutoenelcursas=="vilanova"
replace SocioeconStatHog = 32551 if LocalidadInstitutoenelcursas=="vilanova"
replace poblacion = 67086 if LocalidadInstitutoenelcursas=="vilanova"
// Vilanova i la Geltrú, IES baixamar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace SocioeconStatPers = 12777 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace SocioeconStatHog = 32551 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace poblacion = 67086 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
// Vilanova i la Geltrú, IES baixamar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace SocioeconStatPers = 12777 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace SocioeconStatHog = 32551 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
replace poblacion = 67086 if LocalidadInstitutoenelcursas=="Vilanova i la Geltrú, IES baixamar"
// Insitut Joaquim Mir
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Insitut Joaquim Mir"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Insitut Joaquim Mir"
replace SocioeconStatPers = 12777 if LocalidadInstitutoenelcursas=="Insitut Joaquim Mir"
replace SocioeconStatHog = 32551 if LocalidadInstitutoenelcursas=="Insitut Joaquim Mir"
replace poblacion = 67086 if LocalidadInstitutoenelcursas=="Insitut Joaquim Mir"
// Institut Manuel de Cabanyes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Manuel de Cabanyes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Manuel de Cabanyes"
replace SocioeconStatPers = 12777 if LocalidadInstitutoenelcursas=="Institut Manuel de Cabanyes"
replace SocioeconStatHog = 32551 if LocalidadInstitutoenelcursas=="Institut Manuel de Cabanyes"
replace poblacion = 67086 if LocalidadInstitutoenelcursas=="Institut Manuel de Cabanyes"
// Vilassar de Dalt
// Institut Jaume Almera de Vilassar de Dalt
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Jaume Almera de Vilassar de Dalt"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Jaume Almera de Vilassar de Dalt"
replace SocioeconStatPers = 15251 if LocalidadInstitutoenelcursas=="Institut Jaume Almera de Vilassar de Dalt"
replace SocioeconStatHog = 42116 if LocalidadInstitutoenelcursas=="Institut Jaume Almera de Vilassar de Dalt"
replace poblacion = 9043 if LocalidadInstitutoenelcursas=="Institut Jaume Almera de Vilassar de Dalt"
********************************************************************************
*    Burgos
********************************************************************************
// Aranda de Duero
// Aranda de Duero
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aranda de Duero"
replace SocioeconStatPers = 11939 if LocalidadInstitutoenelcursas=="Aranda de Duero"
replace SocioeconStatHog = 29101 if LocalidadInstitutoenelcursas=="Aranda de Duero"
replace poblacion = 32856 if LocalidadInstitutoenelcursas=="Aranda de Duero"
// Burgos
// Burgos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Burgos"
replace SocioeconStatPers = 13347 if LocalidadInstitutoenelcursas=="Burgos"
replace SocioeconStatHog = 32285 if LocalidadInstitutoenelcursas=="Burgos"
replace poblacion = 175821 if LocalidadInstitutoenelcursas=="Burgos"
// Burgos, IES Comuneros de Castilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Burgos, IES Comuneros de Castilla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Burgos, IES Comuneros de Castilla"
replace SocioeconStatPers = 13347 if LocalidadInstitutoenelcursas=="Burgos, IES Comuneros de Castilla"
replace SocioeconStatHog = 32285 if LocalidadInstitutoenelcursas=="Burgos, IES Comuneros de Castilla"
replace poblacion = 175821 if LocalidadInstitutoenelcursas=="Burgos, IES Comuneros de Castilla"
// MM. Concepcionistas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="MM. Concepcionistas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="MM. Concepcionistas"
replace SocioeconStatPers = 13347 if LocalidadInstitutoenelcursas=="MM. Concepcionistas"
replace SocioeconStatHog = 32285 if LocalidadInstitutoenelcursas=="MM. Concepcionistas"
replace poblacion = 175821 if LocalidadInstitutoenelcursas=="MM. Concepcionistas"
********************************************************************************
*    Cáceres
********************************************************************************
// Cáceres
// Cáceres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cáceres"
replace SocioeconStatPers = 11649 if LocalidadInstitutoenelcursas=="Cáceres"
replace SocioeconStatHog = 29356 if LocalidadInstitutoenelcursas=="Cáceres"
replace poblacion = 96126 if LocalidadInstitutoenelcursas=="Cáceres"
// Cáceres 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cáceres "
replace SocioeconStatPers = 11649 if LocalidadInstitutoenelcursas=="Cáceres "
replace SocioeconStatHog = 29356 if LocalidadInstitutoenelcursas=="Cáceres "
replace poblacion = 96126 if LocalidadInstitutoenelcursas=="Cáceres "
// CÁCERES
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CÁCERES"
replace SocioeconStatPers = 11649 if LocalidadInstitutoenelcursas=="CÁCERES"
replace SocioeconStatHog = 29356 if LocalidadInstitutoenelcursas=="CÁCERES"
replace poblacion = 96126 if LocalidadInstitutoenelcursas=="CÁCERES"
// Cáceres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cáceres"
replace SocioeconStatPers = 11649 if LocalidadInstitutoenelcursas=="Cáceres"
replace SocioeconStatHog = 29356 if LocalidadInstitutoenelcursas=="Cáceres"
replace poblacion = 96126 if LocalidadInstitutoenelcursas=="Cáceres"
// IES Norba Caesarina
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
replace SocioeconStatPers = 11649 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
replace SocioeconStatHog = 29356 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
replace poblacion = 96126 if LocalidadInstitutoenelcursas=="IES Norba Caesarina"
// Caceres/ IES Hernández Pacheco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Caceres/ IES Hernández Pacheco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Caceres/ IES Hernández Pacheco"
replace SocioeconStatPers = 11649 if LocalidadInstitutoenelcursas=="Caceres/ IES Hernández Pacheco"
replace SocioeconStatHog = 29356 if LocalidadInstitutoenelcursas=="Caceres/ IES Hernández Pacheco"
replace poblacion = 96126 if LocalidadInstitutoenelcursas=="Caceres/ IES Hernández Pacheco"
// Colegio Licenciados Reunidos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Licenciados Reunidos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Licenciados Reunidos"
replace SocioeconStatPers = 11649 if LocalidadInstitutoenelcursas=="Colegio Licenciados Reunidos"
replace SocioeconStatHog = 29356 if LocalidadInstitutoenelcursas=="Colegio Licenciados Reunidos"
replace poblacion = 96126 if LocalidadInstitutoenelcursas=="Colegio Licenciados Reunidos"
// Cáceres/santa Cecilia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cáceres/santa Cecilia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cáceres/santa Cecilia"
replace SocioeconStatPers = 11649 if LocalidadInstitutoenelcursas=="Cáceres/santa Cecilia"
replace SocioeconStatHog = 29356 if LocalidadInstitutoenelcursas=="Cáceres/santa Cecilia"
replace poblacion = 96126 if LocalidadInstitutoenelcursas=="Cáceres/santa Cecilia"
// Jarandilla de la Vera
// jarandilla de la vera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="jarandilla de la vera"
replace SocioeconStatPers = 9363 if LocalidadInstitutoenelcursas=="jarandilla de la vera"
replace SocioeconStatHog = 21381 if LocalidadInstitutoenelcursas=="jarandilla de la vera"
replace poblacion = 2828 if LocalidadInstitutoenelcursas=="jarandilla de la vera"
// IES Jaranda (Jarandilla De la Vera)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Jaranda (Jarandilla De la Vera)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Jaranda (Jarandilla De la Vera)"
replace SocioeconStatPers = 9363 if LocalidadInstitutoenelcursas=="IES Jaranda (Jarandilla De la Vera)"
replace SocioeconStatHog = 21381 if LocalidadInstitutoenelcursas=="IES Jaranda (Jarandilla De la Vera)"
replace poblacion = 2828 if LocalidadInstitutoenelcursas=="IES Jaranda (Jarandilla De la Vera)"
// Moraleja
// moraleja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="moraleja"
replace SocioeconStatPers = 8363 if LocalidadInstitutoenelcursas=="moraleja"
replace SocioeconStatHog = 19648 if LocalidadInstitutoenelcursas=="moraleja"
replace poblacion = 6750 if LocalidadInstitutoenelcursas=="moraleja"
// Navalmoral de la Mata
// Navalmoral de la mata
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Navalmoral de la mata"
replace SocioeconStatPers = 10404 if LocalidadInstitutoenelcursas=="Navalmoral de la mata"
replace SocioeconStatHog = 27365 if LocalidadInstitutoenelcursas=="Navalmoral de la mata"
replace poblacion = 17129 if LocalidadInstitutoenelcursas=="Navalmoral de la mata"
// Navalmoral de la Mata
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
replace SocioeconStatPers = 10404 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
replace SocioeconStatHog = 27365 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
replace poblacion = 17129 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
// Navalmoral de la Mata
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
replace SocioeconStatPers = 10404 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
replace SocioeconStatHog = 27365 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
replace poblacion = 17129 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata"
// Navalmoral de la Mata 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata "
replace SocioeconStatPers = 10404 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata "
replace SocioeconStatHog = 27365 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata "
replace poblacion = 17129 if LocalidadInstitutoenelcursas=="Navalmoral de la Mata "
// ÍES ZURBARAN DE NAVALMORAL DE LA MATA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA"
replace SocioeconStatPers = 10404 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA"
replace SocioeconStatHog = 27365 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA"
replace poblacion = 17129 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA"
// ÍES ZURBARAN DE NAVALMORAL DE LA MATA 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA "
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA "
replace SocioeconStatPers = 10404 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA "
replace SocioeconStatHog = 27365 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA "
replace poblacion = 17129 if LocalidadInstitutoenelcursas=="ÍES ZURBARAN DE NAVALMORAL DE LA MATA "
// Plasencia
// Plasencia (Cáceres)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Plasencia (Cáceres)"
replace SocioeconStatPers = 10194 if LocalidadInstitutoenelcursas=="Plasencia (Cáceres)"
replace SocioeconStatHog = 25485 if LocalidadInstitutoenelcursas=="Plasencia (Cáceres)"
replace poblacion = 39913 if LocalidadInstitutoenelcursas=="Plasencia (Cáceres)"
// Colegio Santísima Trinidad
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad"
replace SocioeconStatPers = 10194 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad"
replace SocioeconStatHog = 25485 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad"
replace poblacion = 39913 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad"
// Colegio Santísima Trinidad 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad "
replace SocioeconStatPers = 10194 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad "
replace SocioeconStatHog = 25485 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad "
replace poblacion = 39913 if LocalidadInstitutoenelcursas=="Colegio Santísima Trinidad "
// IES Valle del Jerte (Navaconcejo)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Valle del Jerte (Navaconcejo)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Valle del Jerte (Navaconcejo)"
replace SocioeconStatPers = 10194 if LocalidadInstitutoenelcursas=="IES Valle del Jerte (Navaconcejo)"
replace SocioeconStatHog = 25485 if LocalidadInstitutoenelcursas=="IES Valle del Jerte (Navaconcejo)"
replace poblacion = 39913 if LocalidadInstitutoenelcursas=="IES Valle del Jerte (Navaconcejo)"
********************************************************************************
*    Cádiz
********************************************************************************
// Barbate
// BARBATE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BARBATE"
replace SocioeconStatPers = 7200 if LocalidadInstitutoenelcursas=="BARBATE"
replace SocioeconStatHog = 19785 if LocalidadInstitutoenelcursas=="BARBATE"
replace poblacion = 22518 if LocalidadInstitutoenelcursas=="BARBATE"
// Cádiz
// Cádiz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cádiz"
replace SocioeconStatPers = 11698 if LocalidadInstitutoenelcursas=="Cádiz"
replace SocioeconStatHog = 30181 if LocalidadInstitutoenelcursas=="Cádiz"
replace poblacion = 116027 if LocalidadInstitutoenelcursas=="Cádiz"
// Puerto de Santa María
// El Puerto de Santa Maria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Puerto de Santa Maria"
replace SocioeconStatPers = 9741 if LocalidadInstitutoenelcursas=="El Puerto de Santa Maria"
replace SocioeconStatHog = 27732 if LocalidadInstitutoenelcursas=="El Puerto de Santa Maria"
replace poblacion = 88405 if LocalidadInstitutoenelcursas=="El Puerto de Santa Maria"
// El Puerto de Santa María
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Puerto de Santa María"
replace SocioeconStatPers = 9741 if LocalidadInstitutoenelcursas=="El Puerto de Santa María"
replace SocioeconStatHog = 27732 if LocalidadInstitutoenelcursas=="El Puerto de Santa María"
replace poblacion = 88405 if LocalidadInstitutoenelcursas=="El Puerto de Santa María"
// IES Jose Luis Tejada/ El Puerto de Santa María
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Jose Luis Tejada/ El Puerto de Santa María"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Jose Luis Tejada/ El Puerto de Santa María"
replace SocioeconStatPers = 9741 if LocalidadInstitutoenelcursas=="IES Jose Luis Tejada/ El Puerto de Santa María"
replace SocioeconStatHog = 27732 if LocalidadInstitutoenelcursas=="IES Jose Luis Tejada/ El Puerto de Santa María"
replace poblacion = 88405 if LocalidadInstitutoenelcursas=="IES Jose Luis Tejada/ El Puerto de Santa María"
// Puerto de Santa María/ IES Jose Luis Tejada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Puerto de Santa María/ IES Jose Luis Tejada"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Puerto de Santa María/ IES Jose Luis Tejada"
replace SocioeconStatPers = 9741 if LocalidadInstitutoenelcursas=="Puerto de Santa María/ IES Jose Luis Tejada"
replace SocioeconStatHog = 27732 if LocalidadInstitutoenelcursas=="Puerto de Santa María/ IES Jose Luis Tejada"
replace poblacion = 88405 if LocalidadInstitutoenelcursas=="Puerto de Santa María/ IES Jose Luis Tejada"
// Colegio Grazalema
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Grazalema"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Grazalema"
replace SocioeconStatPers = 9741 if LocalidadInstitutoenelcursas=="Colegio Grazalema"
replace SocioeconStatHog = 27732 if LocalidadInstitutoenelcursas=="Colegio Grazalema"
replace poblacion = 88405 if LocalidadInstitutoenelcursas=="Colegio Grazalema"
// San roque
// Sotogrande
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sotogrande"
replace SocioeconStatPers = 9205 if LocalidadInstitutoenelcursas=="Sotogrande"
replace SocioeconStatHog = 26487 if LocalidadInstitutoenelcursas=="Sotogrande"
replace poblacion = 31218 if LocalidadInstitutoenelcursas=="Sotogrande"
********************************************************************************
*    Cantabria
********************************************************************************
// Medio Cudeyo
// Torreanaz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torreanaz"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Torreanaz"
replace SocioeconStatPers = 11304 if LocalidadInstitutoenelcursas=="Torreanaz"
replace SocioeconStatHog = 31192 if LocalidadInstitutoenelcursas=="Torreanaz"
replace poblacion = 7556 if LocalidadInstitutoenelcursas=="Torreanaz"
// Torreanaz 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torreanaz "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Torreanaz "
replace SocioeconStatPers = 11304 if LocalidadInstitutoenelcursas=="Torreanaz "
replace SocioeconStatHog = 31192 if LocalidadInstitutoenelcursas=="Torreanaz "
replace poblacion = 7556 if LocalidadInstitutoenelcursas=="Torreanaz "
// Miengo
// Santander, colegio torrevelo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santander, colegio torrevelo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santander, colegio torrevelo"
replace SocioeconStatPers = 12122 if LocalidadInstitutoenelcursas=="Santander, colegio torrevelo"
replace SocioeconStatHog = 27801 if LocalidadInstitutoenelcursas=="Santander, colegio torrevelo"
replace poblacion = 4741 if LocalidadInstitutoenelcursas=="Santander, colegio torrevelo"
// Reinosa
// IES Montesclaros
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Montesclaros"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Montesclaros"
replace SocioeconStatPers = 12591 if LocalidadInstitutoenelcursas=="IES Montesclaros"
replace SocioeconStatHog = 27480 if LocalidadInstitutoenelcursas=="IES Montesclaros"
replace poblacion = 9003 if LocalidadInstitutoenelcursas=="IES Montesclaros"
// Santander
// santander
replace cuidad = 1 if LocalidadInstitutoenelcursas=="santander"
replace SocioeconStatPers = 13272 if LocalidadInstitutoenelcursas=="santander"
replace SocioeconStatHog = 32102 if LocalidadInstitutoenelcursas=="santander"
replace poblacion = 172539 if LocalidadInstitutoenelcursas=="santander"
// Colegio San Agustín de Santander
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio San Agustín de Santander"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio San Agustín de Santander"
replace SocioeconStatPers = 13272 if LocalidadInstitutoenelcursas=="Colegio San Agustín de Santander"
replace SocioeconStatHog = 32102 if LocalidadInstitutoenelcursas=="Colegio San Agustín de Santander"
replace poblacion = 172539 if LocalidadInstitutoenelcursas=="Colegio San Agustín de Santander"
// IES Santa Clara, Santander, Cantabria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Santa Clara, Santander, Cantabria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Santa Clara, Santander, Cantabria"
replace SocioeconStatPers = 13272 if LocalidadInstitutoenelcursas=="IES Santa Clara, Santander, Cantabria"
replace SocioeconStatHog = 32102 if LocalidadInstitutoenelcursas=="IES Santa Clara, Santander, Cantabria"
replace poblacion = 172539 if LocalidadInstitutoenelcursas=="IES Santa Clara, Santander, Cantabria"
// Santander/ IES SANTA CLARA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santander/ IES SANTA CLARA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santander/ IES SANTA CLARA"
replace SocioeconStatPers = 13272 if LocalidadInstitutoenelcursas=="Santander/ IES SANTA CLARA"
replace SocioeconStatHog = 32102 if LocalidadInstitutoenelcursas=="Santander/ IES SANTA CLARA"
replace poblacion = 172539 if LocalidadInstitutoenelcursas=="Santander/ IES SANTA CLARA"
// Torrelavega
// Torrelavega
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrelavega"
replace SocioeconStatPers = 10806 if LocalidadInstitutoenelcursas=="Torrelavega"
replace SocioeconStatHog = 26466 if LocalidadInstitutoenelcursas=="Torrelavega"
replace poblacion = 51494 if LocalidadInstitutoenelcursas=="Torrelavega"
********************************************************************************
*    Castellón
********************************************************************************
// Almazroza / Almassora
// IES Vila-roja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Vila-roja"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Vila-roja"
replace SocioeconStatPers = 10367 if LocalidadInstitutoenelcursas=="IES Vila-roja"
replace SocioeconStatHog = 26015 if LocalidadInstitutoenelcursas=="IES Vila-roja"
replace poblacion = 26270 if LocalidadInstitutoenelcursas=="IES Vila-roja"
// Benicasim / Benicàssim
// BENICÀSSIM
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BENICÀSSIM"
replace SocioeconStatPers = 14690 if LocalidadInstitutoenelcursas=="BENICÀSSIM"
replace SocioeconStatHog = 36555 if LocalidadInstitutoenelcursas=="BENICÀSSIM"
replace poblacion = 18192 if LocalidadInstitutoenelcursas=="BENICÀSSIM"
// ies violant de casalduch
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies violant de casalduch"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies violant de casalduch"
replace SocioeconStatPers = 14690 if LocalidadInstitutoenelcursas=="ies violant de casalduch"
replace SocioeconStatHog = 36555 if LocalidadInstitutoenelcursas=="ies violant de casalduch"
replace poblacion = 18192 if LocalidadInstitutoenelcursas=="ies violant de casalduch"
// Betxi
// Bechí, Colegio de Fomento Torrenova
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova"
replace SocioeconStatPers = 11428 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova"
replace SocioeconStatHog = 28744 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova"
replace poblacion = 5645 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova"
// Bechí, Colegio de Fomento Torrenova 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova "
replace SocioeconStatPers = 11428 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova "
replace SocioeconStatHog = 28744 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova "
replace poblacion = 5645 if LocalidadInstitutoenelcursas=="Bechí, Colegio de Fomento Torrenova "
// Castelló de la Plana
// castellón consolación
replace cuidad = 1 if LocalidadInstitutoenelcursas=="castellón consolación"
replace instituto = 1 if LocalidadInstitutoenelcursas=="castellón consolación"
replace SocioeconStatPers = 11790 if LocalidadInstitutoenelcursas=="castellón consolación"
replace SocioeconStatHog = 29751 if LocalidadInstitutoenelcursas=="castellón consolación"
replace poblacion = 171728 if LocalidadInstitutoenelcursas=="castellón consolación"
// Onda
// Ies serra d’espada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies serra d’espada"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies serra d’espada"
replace SocioeconStatPers = 10735 if LocalidadInstitutoenelcursas=="Ies serra d’espada"
replace SocioeconStatHog = 27474 if LocalidadInstitutoenelcursas=="Ies serra d’espada"
replace poblacion = 24859 if LocalidadInstitutoenelcursas=="Ies serra d’espada"
// Vinaroz / Vinaròs
// IES José VilaplN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES José VilaplN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES José VilaplN"
replace SocioeconStatPers = 9911 if LocalidadInstitutoenelcursas=="IES José VilaplN"
replace SocioeconStatHog = 25116 if LocalidadInstitutoenelcursas=="IES José VilaplN"
replace poblacion = 28682 if LocalidadInstitutoenelcursas=="IES José VilaplN"
********************************************************************************
*    Ceuta
********************************************************************************
********************************************************************************
*    Ciudad Real
********************************************************************************
// Argamasilla de Calat rava
// Argamasilla de Calatrava
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Argamasilla de Calatrava"
replace SocioeconStatPers = 10183 if LocalidadInstitutoenelcursas=="Argamasilla de Calatrava"
replace SocioeconStatHog = 25030 if LocalidadInstitutoenelcursas=="Argamasilla de Calatrava"
replace poblacion = 5943 if LocalidadInstitutoenelcursas=="Argamasilla de Calatrava"
// Campo de Criptana
// Ies Isabel Perillán y Quirós
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Isabel Perillán y Quirós"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Isabel Perillán y Quirós"
replace SocioeconStatPers = 8636 if LocalidadInstitutoenelcursas=="Ies Isabel Perillán y Quirós"
replace SocioeconStatHog = 23831 if LocalidadInstitutoenelcursas=="Ies Isabel Perillán y Quirós"
replace poblacion = 13414 if LocalidadInstitutoenelcursas=="Ies Isabel Perillán y Quirós"
// Ciudad Real
// ciudad real
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ciudad real"
replace SocioeconStatPers = 12368 if LocalidadInstitutoenelcursas=="ciudad real"
replace SocioeconStatHog = 31931 if LocalidadInstitutoenelcursas=="ciudad real"
replace poblacion = 74746 if LocalidadInstitutoenelcursas=="ciudad real"
// Nuestra Señora Del Prado
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado"
replace SocioeconStatPers = 12368 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado"
replace SocioeconStatHog = 31931 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado"
replace poblacion = 74746 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado"
// Nuestra Señora Del Prado 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado "
replace SocioeconStatPers = 12368 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado "
replace SocioeconStatHog = 31931 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado "
replace poblacion = 74746 if LocalidadInstitutoenelcursas=="Nuestra Señora Del Prado "
// Herencia
// IES HERMÓGENES RODRÍGUEZ
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES HERMÓGENES RODRÍGUEZ"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES HERMÓGENES RODRÍGUEZ"
replace SocioeconStatPers = 7316 if LocalidadInstitutoenelcursas=="IES HERMÓGENES RODRÍGUEZ"
replace SocioeconStatHog = 21410 if LocalidadInstitutoenelcursas=="IES HERMÓGENES RODRÍGUEZ"
replace poblacion = 8390 if LocalidadInstitutoenelcursas=="IES HERMÓGENES RODRÍGUEZ"
// IES Hermógenes Rodríguez
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Hermógenes Rodríguez"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Hermógenes Rodríguez"
replace SocioeconStatPers = 7316 if LocalidadInstitutoenelcursas=="IES Hermógenes Rodríguez"
replace SocioeconStatHog = 21410 if LocalidadInstitutoenelcursas=="IES Hermógenes Rodríguez"
replace poblacion = 8390 if LocalidadInstitutoenelcursas=="IES Hermógenes Rodríguez"
// Pedro Muñoz
// Pedro Muñoz (Ciudad Real)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pedro Muñoz (Ciudad Real)"
replace SocioeconStatPers = 8044 if LocalidadInstitutoenelcursas=="Pedro Muñoz (Ciudad Real)"
replace SocioeconStatHog = 20815 if LocalidadInstitutoenelcursas=="Pedro Muñoz (Ciudad Real)"
replace poblacion = 7293 if LocalidadInstitutoenelcursas=="Pedro Muñoz (Ciudad Real)"
// Puertollano
// IES Fray Andrés de Puertollano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano"
replace SocioeconStatPers = 10622 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano"
replace SocioeconStatHog = 25919 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano"
replace poblacion = 47035 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano"
// IES Fray Andrés de Puertollano 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano "
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano "
replace SocioeconStatPers = 10622 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano "
replace SocioeconStatHog = 25919 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano "
replace poblacion = 47035 if LocalidadInstitutoenelcursas=="IES Fray Andrés de Puertollano "
// Socuéllamos
// IES Fernando de Mena (Socuéllamos)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Fernando de Mena (Socuéllamos)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Fernando de Mena (Socuéllamos)"
replace SocioeconStatPers = 8036 if LocalidadInstitutoenelcursas=="IES Fernando de Mena (Socuéllamos)"
replace SocioeconStatHog = 22253 if LocalidadInstitutoenelcursas=="IES Fernando de Mena (Socuéllamos)"
replace poblacion = 12139 if LocalidadInstitutoenelcursas=="IES Fernando de Mena (Socuéllamos)"
// I.E.S. Fernando de Mena
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S. Fernando de Mena"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S. Fernando de Mena"
replace SocioeconStatPers = 8036 if LocalidadInstitutoenelcursas=="I.E.S. Fernando de Mena"
replace SocioeconStatHog = 22253 if LocalidadInstitutoenelcursas=="I.E.S. Fernando de Mena"
replace poblacion = 12139 if LocalidadInstitutoenelcursas=="I.E.S. Fernando de Mena"
// Tomelloso
// Tomelloso
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tomelloso"
replace SocioeconStatPers = 8271 if LocalidadInstitutoenelcursas=="Tomelloso"
replace SocioeconStatHog = 22652 if LocalidadInstitutoenelcursas=="Tomelloso"
replace poblacion = 35873 if LocalidadInstitutoenelcursas=="Tomelloso"
// tomelloso
replace cuidad = 1 if LocalidadInstitutoenelcursas=="tomelloso"
replace SocioeconStatPers = 8271 if LocalidadInstitutoenelcursas=="tomelloso"
replace SocioeconStatHog = 22652 if LocalidadInstitutoenelcursas=="tomelloso"
replace poblacion = 35873 if LocalidadInstitutoenelcursas=="tomelloso"
// Tomelloso/Eladio Cabañero
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tomelloso/Eladio Cabañero"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Tomelloso/Eladio Cabañero"
replace SocioeconStatPers = 8271 if LocalidadInstitutoenelcursas=="Tomelloso/Eladio Cabañero"
replace SocioeconStatHog = 22652 if LocalidadInstitutoenelcursas=="Tomelloso/Eladio Cabañero"
replace poblacion = 35873 if LocalidadInstitutoenelcursas=="Tomelloso/Eladio Cabañero"
// Valdepeñas
// Valdepeñas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valdepeñas"
replace SocioeconStatPers = 9542 if LocalidadInstitutoenelcursas=="Valdepeñas"
replace SocioeconStatHog = 25208 if LocalidadInstitutoenelcursas=="Valdepeñas"
replace poblacion = 30077 if LocalidadInstitutoenelcursas=="Valdepeñas"
********************************************************************************
*    Córdoba
********************************************************************************
// Córdoba
// Córdoba, instituto Virgen del Carmen
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Córdoba, instituto Virgen del Carmen"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Córdoba, instituto Virgen del Carmen"
replace SocioeconStatPers = 10629 if LocalidadInstitutoenelcursas=="Córdoba, instituto Virgen del Carmen"
replace SocioeconStatHog = 28676 if LocalidadInstitutoenelcursas=="Córdoba, instituto Virgen del Carmen"
replace poblacion = 325701 if LocalidadInstitutoenelcursas=="Córdoba, instituto Virgen del Carmen"
// Fernan Núñez
// Fernan Núñez
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Fernan Núñez"
replace SocioeconStatPers = 9029 if LocalidadInstitutoenelcursas=="Fernan Núñez"
replace SocioeconStatHog = 23397 if LocalidadInstitutoenelcursas=="Fernan Núñez"
replace poblacion = 9663 if LocalidadInstitutoenelcursas=="Fernan Núñez"
// Montilla
// Montilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Montilla"
replace SocioeconStatPers = 8891 if LocalidadInstitutoenelcursas=="Montilla"
replace SocioeconStatHog = 23526 if LocalidadInstitutoenelcursas=="Montilla"
replace poblacion = 22859 if LocalidadInstitutoenelcursas=="Montilla"
// Montilla 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Montilla "
replace SocioeconStatPers = 8891 if LocalidadInstitutoenelcursas=="Montilla "
replace SocioeconStatHog = 23526 if LocalidadInstitutoenelcursas=="Montilla "
replace poblacion = 22859 if LocalidadInstitutoenelcursas=="Montilla "
// Montilla/ Inca Garcilaso
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Montilla/ Inca Garcilaso"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Montilla/ Inca Garcilaso"
replace SocioeconStatPers = 8891 if LocalidadInstitutoenelcursas=="Montilla/ Inca Garcilaso"
replace SocioeconStatHog = 23526 if LocalidadInstitutoenelcursas=="Montilla/ Inca Garcilaso"
replace poblacion = 22859 if LocalidadInstitutoenelcursas=="Montilla/ Inca Garcilaso"
********************************************************************************
*    Cuenca
********************************************************************************
// Belmonte
// ies San Juan del Castillo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies San Juan del Castillo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies San Juan del Castillo"
replace SocioeconStatPers = 9243 if LocalidadInstitutoenelcursas=="ies San Juan del Castillo"
replace SocioeconStatHog = 22716 if LocalidadInstitutoenelcursas=="ies San Juan del Castillo"
replace poblacion = 1903 if LocalidadInstitutoenelcursas=="ies San Juan del Castillo"
// Cuenca
// Cuenca/ IES Alfonso VIII
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
replace SocioeconStatPers = 11827 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
replace SocioeconStatHog = 29877 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
replace poblacion = 54690 if LocalidadInstitutoenelcursas=="Cuenca/ IES Alfonso VIII"
// Alfonso VIII, cuenca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alfonso VIII, cuenca"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alfonso VIII, cuenca"
replace SocioeconStatPers = 11827 if LocalidadInstitutoenelcursas=="Alfonso VIII, cuenca"
replace SocioeconStatHog = 29877 if LocalidadInstitutoenelcursas=="Alfonso VIII, cuenca"
replace poblacion = 54690 if LocalidadInstitutoenelcursas=="Alfonso VIII, cuenca"
// IES PEDRO MERCEDES
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES PEDRO MERCEDES"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES PEDRO MERCEDES"
replace SocioeconStatPers = 11827 if LocalidadInstitutoenelcursas=="IES PEDRO MERCEDES"
replace SocioeconStatHog = 29877 if LocalidadInstitutoenelcursas=="IES PEDRO MERCEDES"
replace poblacion = 54690 if LocalidadInstitutoenelcursas=="IES PEDRO MERCEDES"
// Pedroñeras, Las
// Las Pedroñeras
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Pedroñeras"
replace SocioeconStatPers = 8849 if LocalidadInstitutoenelcursas=="Las Pedroñeras"
replace SocioeconStatHog = 24638 if LocalidadInstitutoenelcursas=="Las Pedroñeras"
replace poblacion = 6638 if LocalidadInstitutoenelcursas=="Las Pedroñeras"
// Tarancón
// IES La Hontanilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES La Hontanilla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES La Hontanilla"
replace SocioeconStatPers = 9848 if LocalidadInstitutoenelcursas=="IES La Hontanilla"
replace SocioeconStatHog = 26630 if LocalidadInstitutoenelcursas=="IES La Hontanilla"
replace poblacion = 15271 if LocalidadInstitutoenelcursas=="IES La Hontanilla"
********************************************************************************
*    Girona / Gerona
********************************************************************************
// Amer
// Amer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Amer"
replace SocioeconStatPers = 13538 if LocalidadInstitutoenelcursas=="Amer"
replace SocioeconStatHog = 35024 if LocalidadInstitutoenelcursas=="Amer"
replace poblacion = 2298 if LocalidadInstitutoenelcursas=="Amer"
// Banyoles / Bañolas
// Banyoles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Banyoles"
replace SocioeconStatPers = 12495 if LocalidadInstitutoenelcursas=="Banyoles"
replace SocioeconStatHog = 34143 if LocalidadInstitutoenelcursas=="Banyoles"
replace poblacion = 19826 if LocalidadInstitutoenelcursas=="Banyoles"
// Banyoles 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Banyoles "
replace SocioeconStatPers = 12495 if LocalidadInstitutoenelcursas=="Banyoles "
replace SocioeconStatHog = 34143 if LocalidadInstitutoenelcursas=="Banyoles "
replace poblacion = 19826 if LocalidadInstitutoenelcursas=="Banyoles "
// Instituto Pla de l'Estany
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Instituto Pla de l'Estany"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Instituto Pla de l'Estany"
replace SocioeconStatPers = 12495 if LocalidadInstitutoenelcursas=="Instituto Pla de l'Estany"
replace SocioeconStatHog = 34143 if LocalidadInstitutoenelcursas=="Instituto Pla de l'Estany"
replace poblacion = 19826 if LocalidadInstitutoenelcursas=="Instituto Pla de l'Estany"
// Figueres / Figueras
// Alexandre Deulofeu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alexandre Deulofeu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alexandre Deulofeu"
replace SocioeconStatPers = 10104 if LocalidadInstitutoenelcursas=="Alexandre Deulofeu"
replace SocioeconStatHog = 27583 if LocalidadInstitutoenelcursas=="Alexandre Deulofeu"
replace poblacion = 46654 if LocalidadInstitutoenelcursas=="Alexandre Deulofeu"
// Institut Olivar Gran
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Olivar Gran"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Olivar Gran"
replace SocioeconStatPers = 10104 if LocalidadInstitutoenelcursas=="Institut Olivar Gran"
replace SocioeconStatHog = 27583 if LocalidadInstitutoenelcursas=="Institut Olivar Gran"
replace poblacion = 46654 if LocalidadInstitutoenelcursas=="Institut Olivar Gran"
// Girona
// Girona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Girona"
replace SocioeconStatPers = 13527 if LocalidadInstitutoenelcursas=="Girona"
replace SocioeconStatHog = 35968 if LocalidadInstitutoenelcursas=="Girona"
replace poblacion = 101852 if LocalidadInstitutoenelcursas=="Girona"
// Bell-lloc
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Bell-lloc"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Bell-lloc"
replace SocioeconStatPers = 13527 if LocalidadInstitutoenelcursas=="Bell-lloc"
replace SocioeconStatHog = 35968 if LocalidadInstitutoenelcursas=="Bell-lloc"
replace poblacion = 101852 if LocalidadInstitutoenelcursas=="Bell-lloc"
// La Salle Girona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Girona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Girona"
replace SocioeconStatPers = 13527 if LocalidadInstitutoenelcursas=="La Salle Girona"
replace SocioeconStatHog = 35968 if LocalidadInstitutoenelcursas=="La Salle Girona"
replace poblacion = 101852 if LocalidadInstitutoenelcursas=="La Salle Girona"
// Maristes Girona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Maristes Girona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Maristes Girona"
replace SocioeconStatPers = 13527 if LocalidadInstitutoenelcursas=="Maristes Girona"
replace SocioeconStatHog = 35968 if LocalidadInstitutoenelcursas=="Maristes Girona"
replace poblacion = 101852 if LocalidadInstitutoenelcursas=="Maristes Girona"
// Montessori Palau Girona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Montessori Palau Girona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Montessori Palau Girona"
replace SocioeconStatPers = 13527 if LocalidadInstitutoenelcursas=="Montessori Palau Girona"
replace SocioeconStatHog = 35968 if LocalidadInstitutoenelcursas=="Montessori Palau Girona"
replace poblacion = 101852 if LocalidadInstitutoenelcursas=="Montessori Palau Girona"
// Lloret de Mar
// lloret de mar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="lloret de mar"
replace SocioeconStatPers = 9596 if LocalidadInstitutoenelcursas=="lloret de mar"
replace SocioeconStatHog = 25229 if LocalidadInstitutoenelcursas=="lloret de mar"
replace poblacion = 38373 if LocalidadInstitutoenelcursas=="lloret de mar"
// Olot
// Olot
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Olot"
replace SocioeconStatPers = 12540 if LocalidadInstitutoenelcursas=="Olot"
replace SocioeconStatHog = 32806 if LocalidadInstitutoenelcursas=="Olot"
replace poblacion = 35228 if LocalidadInstitutoenelcursas=="Olot"
// Sant Feliu de Guíxols
// Institut Sant Elm, Sant Feliu de Guíxols
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Sant Elm, Sant Feliu de Guíxols"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Sant Elm, Sant Feliu de Guíxols"
replace SocioeconStatPers = 10701 if LocalidadInstitutoenelcursas=="Institut Sant Elm, Sant Feliu de Guíxols"
replace SocioeconStatHog = 27300 if LocalidadInstitutoenelcursas=="Institut Sant Elm, Sant Feliu de Guíxols"
replace poblacion = 21925 if LocalidadInstitutoenelcursas=="Institut Sant Elm, Sant Feliu de Guíxols"
// Santa Coloma de Farners / Santa Coloma de Farner
// Santa Coloma de Farners
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Coloma de Farners"
replace SocioeconStatPers = 11841 if LocalidadInstitutoenelcursas=="Santa Coloma de Farners"
replace SocioeconStatHog = 32897 if LocalidadInstitutoenelcursas=="Santa Coloma de Farners"
replace poblacion = 13143 if LocalidadInstitutoenelcursas=="Santa Coloma de Farners"
********************************************************************************
*    Granada
********************************************************************************
// Atarfe
// GRANADA COLLEGE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="GRANADA COLLEGE"
replace instituto = 1 if LocalidadInstitutoenelcursas=="GRANADA COLLEGE"
replace SocioeconStatPers = 8181 if LocalidadInstitutoenelcursas=="GRANADA COLLEGE"
replace SocioeconStatHog = 22377 if LocalidadInstitutoenelcursas=="GRANADA COLLEGE"
replace poblacion = 18706 if LocalidadInstitutoenelcursas=="GRANADA COLLEGE"
// Baza
// IES JOSÉ DE MORA (BAZA)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES JOSÉ DE MORA (BAZA)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES JOSÉ DE MORA (BAZA)"
replace SocioeconStatPers = 8688 if LocalidadInstitutoenelcursas=="IES JOSÉ DE MORA (BAZA)"
replace SocioeconStatHog = 23589 if LocalidadInstitutoenelcursas=="IES JOSÉ DE MORA (BAZA)"
replace poblacion = 20412 if LocalidadInstitutoenelcursas=="IES JOSÉ DE MORA (BAZA)"
// Granada
// Granada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Granada"
replace SocioeconStatPers = 12127 if LocalidadInstitutoenelcursas=="Granada"
replace SocioeconStatHog = 29373 if LocalidadInstitutoenelcursas=="Granada"
replace poblacion = 232462 if LocalidadInstitutoenelcursas=="Granada"
// granada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="granada"
replace SocioeconStatPers = 12127 if LocalidadInstitutoenelcursas=="granada"
replace SocioeconStatHog = 29373 if LocalidadInstitutoenelcursas=="granada"
replace poblacion = 232462 if LocalidadInstitutoenelcursas=="granada"
// GRANADA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="GRANADA"
replace SocioeconStatPers = 12127 if LocalidadInstitutoenelcursas=="GRANADA"
replace SocioeconStatHog = 29373 if LocalidadInstitutoenelcursas=="GRANADA"
replace poblacion = 232462 if LocalidadInstitutoenelcursas=="GRANADA"
// IES FRAY LUIS DE GRANADA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES FRAY LUIS DE GRANADA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES FRAY LUIS DE GRANADA"
replace SocioeconStatPers = 12127 if LocalidadInstitutoenelcursas=="IES FRAY LUIS DE GRANADA"
replace SocioeconStatHog = 29373 if LocalidadInstitutoenelcursas=="IES FRAY LUIS DE GRANADA"
replace poblacion = 232462 if LocalidadInstitutoenelcursas=="IES FRAY LUIS DE GRANADA"
// Íllora
// IES Diego de Siloe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Diego de Siloe"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Diego de Siloe"
replace SocioeconStatPers = 7430 if LocalidadInstitutoenelcursas=="IES Diego de Siloe"
replace SocioeconStatHog = 17716 if LocalidadInstitutoenelcursas=="IES Diego de Siloe"
replace poblacion = 10054 if LocalidadInstitutoenelcursas=="IES Diego de Siloe"
// Motril
// Motril. IES Francisco Giner de los Ríos.
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Motril. IES Francisco Giner de los Ríos."
replace instituto = 1 if LocalidadInstitutoenelcursas=="Motril. IES Francisco Giner de los Ríos."
replace SocioeconStatPers = 8485 if LocalidadInstitutoenelcursas=="Motril. IES Francisco Giner de los Ríos."
replace SocioeconStatHog = 23766 if LocalidadInstitutoenelcursas=="Motril. IES Francisco Giner de los Ríos."
replace poblacion = 58020 if LocalidadInstitutoenelcursas=="Motril. IES Francisco Giner de los Ríos."
// Peligros
// Peligros (Granada)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Peligros (Granada)"
replace SocioeconStatPers = 9250 if LocalidadInstitutoenelcursas=="Peligros (Granada)"
replace SocioeconStatHog = 26468 if LocalidadInstitutoenelcursas=="Peligros (Granada)"
replace poblacion = 11394 if LocalidadInstitutoenelcursas=="Peligros (Granada)"
********************************************************************************
*    Guadalajara
********************************************************************************
// Azuqueca de Henares
// Azuqueca de Henares, Profesor Domínguez Ortiz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Azuqueca de Henares, Profesor Domínguez Ortiz"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Azuqueca de Henares, Profesor Domínguez Ortiz"
replace SocioeconStatPers = 10864 if LocalidadInstitutoenelcursas=="Azuqueca de Henares, Profesor Domínguez Ortiz"
replace SocioeconStatHog = 31449 if LocalidadInstitutoenelcursas=="Azuqueca de Henares, Profesor Domínguez Ortiz"
replace poblacion = 35009 if LocalidadInstitutoenelcursas=="Azuqueca de Henares, Profesor Domínguez Ortiz"
// Guadalajara
// Guadalajara
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Guadalajara"
replace SocioeconStatPers = 12466 if LocalidadInstitutoenelcursas=="Guadalajara"
replace SocioeconStatHog = 32442 if LocalidadInstitutoenelcursas=="Guadalajara"
replace poblacion = 85871 if LocalidadInstitutoenelcursas=="Guadalajara"
// GUADALAJARA/ I.E.S CASTILLA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
replace SocioeconStatPers = 12466 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
replace SocioeconStatHog = 32442 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
replace poblacion = 85871 if LocalidadInstitutoenelcursas=="GUADALAJARA/ I.E.S CASTILLA"
// I.E.S Aguas Vivas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Aguas Vivas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Aguas Vivas"
replace SocioeconStatPers = 12466 if LocalidadInstitutoenelcursas=="I.E.S Aguas Vivas"
replace SocioeconStatHog = 32442 if LocalidadInstitutoenelcursas=="I.E.S Aguas Vivas"
replace poblacion = 85871 if LocalidadInstitutoenelcursas=="I.E.S Aguas Vivas"
// Molina de Aragón
// Molina de Aragón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Molina de Aragón"
replace SocioeconStatPers = 10273 if LocalidadInstitutoenelcursas=="Molina de Aragón"
replace SocioeconStatHog = 24550 if LocalidadInstitutoenelcursas=="Molina de Aragón"
replace poblacion = 3275 if LocalidadInstitutoenelcursas=="Molina de Aragón"
// Sigüenza
// Sigüenza
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sigüenza"
replace SocioeconStatPers = 10989 if LocalidadInstitutoenelcursas=="Sigüenza"
replace SocioeconStatHog = 23283 if LocalidadInstitutoenelcursas=="Sigüenza"
replace poblacion = 4309 if LocalidadInstitutoenelcursas=="Sigüenza"
********************************************************************************
*    Gipuzkoa / Guipúzcoa
********************************************************************************
// Donostia/San Sebastián
// Donostia/San Sebastián
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Donostia/San Sebastián"
replace SocioeconStatPers = 17187 if LocalidadInstitutoenelcursas=="Donostia/San Sebastián"
replace SocioeconStatHog = 41051 if LocalidadInstitutoenelcursas=="Donostia/San Sebastián"
replace poblacion = 184415 if LocalidadInstitutoenelcursas=="Donostia/San Sebastián"
// Donostia - San Sebastián
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Donostia - San Sebastián"
replace SocioeconStatPers = 17187 if LocalidadInstitutoenelcursas=="Donostia - San Sebastián"
replace SocioeconStatHog = 41051 if LocalidadInstitutoenelcursas=="Donostia - San Sebastián"
replace poblacion = 184415 if LocalidadInstitutoenelcursas=="Donostia - San Sebastián"
// Ordizia / Villafranca de Ordizia
// Jakintza Ikastola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jakintza Ikastola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jakintza Ikastola"
replace SocioeconStatPers = 14432 if LocalidadInstitutoenelcursas=="Jakintza Ikastola"
replace SocioeconStatHog = 35920 if LocalidadInstitutoenelcursas=="Jakintza Ikastola"
replace poblacion = 10394 if LocalidadInstitutoenelcursas=="Jakintza Ikastola"
********************************************************************************
*    Huelva
********************************************************************************
// Huelva
// Huelva
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Huelva"
replace SocioeconStatPers = 10165 if LocalidadInstitutoenelcursas=="Huelva"
replace SocioeconStatHog = 26857 if LocalidadInstitutoenelcursas=="Huelva"
replace poblacion = 143663 if LocalidadInstitutoenelcursas=="Huelva"
********************************************************************************
*    Huesca
********************************************************************************
// Fraga
// ÍES BAJO CINCA DE FRAGA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES BAJO CINCA DE FRAGA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES BAJO CINCA DE FRAGA"
replace SocioeconStatPers = 10327 if LocalidadInstitutoenelcursas=="ÍES BAJO CINCA DE FRAGA"
replace SocioeconStatHog = 29524 if LocalidadInstitutoenelcursas=="ÍES BAJO CINCA DE FRAGA"
replace poblacion = 15033 if LocalidadInstitutoenelcursas=="ÍES BAJO CINCA DE FRAGA"
// Fraga (Huesca), IES Bajo Cinca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Fraga (Huesca), IES Bajo Cinca"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Fraga (Huesca), IES Bajo Cinca"
replace SocioeconStatPers = 10327 if LocalidadInstitutoenelcursas=="Fraga (Huesca), IES Bajo Cinca"
replace SocioeconStatHog = 29524 if LocalidadInstitutoenelcursas=="Fraga (Huesca), IES Bajo Cinca"
replace poblacion = 15033 if LocalidadInstitutoenelcursas=="Fraga (Huesca), IES Bajo Cinca"
// Huesca
// huesca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="huesca"
replace SocioeconStatPers = 12665 if LocalidadInstitutoenelcursas=="huesca"
replace SocioeconStatHog = 31278 if LocalidadInstitutoenelcursas=="huesca"
replace poblacion = 53132 if LocalidadInstitutoenelcursas=="huesca"
// Huesca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Huesca"
replace SocioeconStatPers = 12665 if LocalidadInstitutoenelcursas=="Huesca"
replace SocioeconStatHog = 31278 if LocalidadInstitutoenelcursas=="Huesca"
replace poblacion = 53132 if LocalidadInstitutoenelcursas=="Huesca"
// Huesca-Colegio Santa Rosa Altoaragón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Huesca-Colegio Santa Rosa Altoaragón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Huesca-Colegio Santa Rosa Altoaragón"
replace SocioeconStatPers = 12665 if LocalidadInstitutoenelcursas=="Huesca-Colegio Santa Rosa Altoaragón"
replace SocioeconStatHog = 31278 if LocalidadInstitutoenelcursas=="Huesca-Colegio Santa Rosa Altoaragón"
replace poblacion = 53132 if LocalidadInstitutoenelcursas=="Huesca-Colegio Santa Rosa Altoaragón"
// Monzón
// Monzón (Huesca)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Monzón (Huesca)"
replace SocioeconStatPers = 11181 if LocalidadInstitutoenelcursas=="Monzón (Huesca)"
replace SocioeconStatHog = 28622 if LocalidadInstitutoenelcursas=="Monzón (Huesca)"
replace poblacion = 17236 if LocalidadInstitutoenelcursas=="Monzón (Huesca)"
********************************************************************************
*    Jaén
********************************************************************************
// Baeza
// IES Santísima Trinidad, Baeza, Jaén
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Santísima Trinidad, Baeza, Jaén"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Santísima Trinidad, Baeza, Jaén"
replace SocioeconStatPers = 9013 if LocalidadInstitutoenelcursas=="IES Santísima Trinidad, Baeza, Jaén"
replace SocioeconStatHog = 24498 if LocalidadInstitutoenelcursas=="IES Santísima Trinidad, Baeza, Jaén"
replace poblacion = 15841 if LocalidadInstitutoenelcursas=="IES Santísima Trinidad, Baeza, Jaén"
// Jaén
// el valle
replace cuidad = 1 if LocalidadInstitutoenelcursas=="el valle"
replace instituto = 1 if LocalidadInstitutoenelcursas=="el valle"
replace SocioeconStatPers = 11366 if LocalidadInstitutoenelcursas=="el valle"
replace SocioeconStatHog = 30452 if LocalidadInstitutoenelcursas=="el valle"
replace poblacion = 112999 if LocalidadInstitutoenelcursas=="el valle"
// Jaén /Maristss
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jaén /Maristss"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jaén /Maristss"
replace SocioeconStatPers = 11366 if LocalidadInstitutoenelcursas=="Jaén /Maristss"
replace SocioeconStatHog = 30452 if LocalidadInstitutoenelcursas=="Jaén /Maristss"
replace poblacion = 112999 if LocalidadInstitutoenelcursas=="Jaén /Maristss"
// Lupión
// Jaén/Guadalimar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jaén/Guadalimar"
replace SocioeconStatPers = 8148 if LocalidadInstitutoenelcursas=="Jaén/Guadalimar"
replace SocioeconStatHog = 18470 if LocalidadInstitutoenelcursas=="Jaén/Guadalimar"
replace poblacion = 842 if LocalidadInstitutoenelcursas=="Jaén/Guadalimar"
// Jaén/Guadalimar 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jaén/Guadalimar "
replace SocioeconStatPers = 8148 if LocalidadInstitutoenelcursas=="Jaén/Guadalimar "
replace SocioeconStatHog = 18470 if LocalidadInstitutoenelcursas=="Jaén/Guadalimar "
replace poblacion = 842 if LocalidadInstitutoenelcursas=="Jaén/Guadalimar "
// Marmolejo
// IES Viegen de la Cabeza (Marmolejo)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Viegen de la Cabeza (Marmolejo)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Viegen de la Cabeza (Marmolejo)"
replace SocioeconStatPers = 8177 if LocalidadInstitutoenelcursas=="IES Viegen de la Cabeza (Marmolejo)"
replace SocioeconStatHog = 21758 if LocalidadInstitutoenelcursas=="IES Viegen de la Cabeza (Marmolejo)"
replace poblacion = 6812 if LocalidadInstitutoenelcursas=="IES Viegen de la Cabeza (Marmolejo)"
// IES Virgen de la Cabeza, Marmolejo (Jaén)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Virgen de la Cabeza, Marmolejo (Jaén)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Virgen de la Cabeza, Marmolejo (Jaén)"
replace SocioeconStatPers = 8177 if LocalidadInstitutoenelcursas=="IES Virgen de la Cabeza, Marmolejo (Jaén)"
replace SocioeconStatHog = 21758 if LocalidadInstitutoenelcursas=="IES Virgen de la Cabeza, Marmolejo (Jaén)"
replace poblacion = 6812 if LocalidadInstitutoenelcursas=="IES Virgen de la Cabeza, Marmolejo (Jaén)"
********************************************************************************
*    La Rioja
********************************************************************************
// Logroño
// Logroño
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Logroño"
replace SocioeconStatPers = 12854 if LocalidadInstitutoenelcursas=="Logroño"
replace SocioeconStatHog = 31406 if LocalidadInstitutoenelcursas=="Logroño"
replace poblacion = 151136 if LocalidadInstitutoenelcursas=="Logroño"
// Logroño
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Logroño"
replace SocioeconStatPers = 12854 if LocalidadInstitutoenelcursas=="Logroño"
replace SocioeconStatHog = 31406 if LocalidadInstitutoenelcursas=="Logroño"
replace poblacion = 151136 if LocalidadInstitutoenelcursas=="Logroño"
// Logroño/Bachillerato Santa María
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Logroño/Bachillerato Santa María"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Logroño/Bachillerato Santa María"
replace SocioeconStatPers = 12854 if LocalidadInstitutoenelcursas=="Logroño/Bachillerato Santa María"
replace SocioeconStatHog = 31406 if LocalidadInstitutoenelcursas=="Logroño/Bachillerato Santa María"
replace poblacion = 151136 if LocalidadInstitutoenelcursas=="Logroño/Bachillerato Santa María"
// Instituto Tomás Mingot de Logroño
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Instituto Tomás Mingot de Logroño"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Instituto Tomás Mingot de Logroño"
replace SocioeconStatPers = 12854 if LocalidadInstitutoenelcursas=="Instituto Tomás Mingot de Logroño"
replace SocioeconStatHog = 31406 if LocalidadInstitutoenelcursas=="Instituto Tomás Mingot de Logroño"
replace poblacion = 151136 if LocalidadInstitutoenelcursas=="Instituto Tomás Mingot de Logroño"
********************************************************************************
*    Las Palmas
********************************************************************************
// Arrecife
// Arrecife/IES Las Salinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Arrecife/IES Las Salinas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Arrecife/IES Las Salinas"
replace SocioeconStatPers = 9267 if LocalidadInstitutoenelcursas=="Arrecife/IES Las Salinas"
replace SocioeconStatHog = 26992 if LocalidadInstitutoenelcursas=="Arrecife/IES Las Salinas"
replace poblacion = 62988 if LocalidadInstitutoenelcursas=="Arrecife/IES Las Salinas"
// ESCUELA DE ARTE PANCHO LASSO
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ESCUELA DE ARTE PANCHO LASSO"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ESCUELA DE ARTE PANCHO LASSO"
replace SocioeconStatPers = 9267 if LocalidadInstitutoenelcursas=="ESCUELA DE ARTE PANCHO LASSO"
replace SocioeconStatHog = 26992 if LocalidadInstitutoenelcursas=="ESCUELA DE ARTE PANCHO LASSO"
replace poblacion = 62988 if LocalidadInstitutoenelcursas=="ESCUELA DE ARTE PANCHO LASSO"
// Arucas
// Arucas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Arucas"
replace SocioeconStatPers = 10193 if LocalidadInstitutoenelcursas=="Arucas"
replace SocioeconStatHog = 28707 if LocalidadInstitutoenelcursas=="Arucas"
replace poblacion = 38138 if LocalidadInstitutoenelcursas=="Arucas"
// Las Palmas de Gran Canaria
// LAS PALMAS DE GRAN CANARIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="LAS PALMAS DE GRAN CANARIA"
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="LAS PALMAS DE GRAN CANARIA"
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="LAS PALMAS DE GRAN CANARIA"
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="LAS PALMAS DE GRAN CANARIA"
// Las Palmas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Palmas"
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="Las Palmas"
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="Las Palmas"
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="Las Palmas"
// Las palmas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las palmas"
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="Las palmas"
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="Las palmas"
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="Las palmas"
// Las Palmas de Gran Canaria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria"
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria"
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria"
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria"
// Las Palmas de Gran Canaria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria"
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria"
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria"
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria"
// Las Palmas de Gran Canaria 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria "
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria "
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria "
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="Las Palmas de Gran Canaria "
// IES Guanarteme (Las Palmas de Gran Canaria)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Guanarteme (Las Palmas de Gran Canaria)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Guanarteme (Las Palmas de Gran Canaria)"
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="IES Guanarteme (Las Palmas de Gran Canaria)"
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="IES Guanarteme (Las Palmas de Gran Canaria)"
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="IES Guanarteme (Las Palmas de Gran Canaria)"
// IES Santa Teresa de Jesus, Las Palmas de Gran Canaria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Santa Teresa de Jesus, Las Palmas de Gran Canaria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Santa Teresa de Jesus, Las Palmas de Gran Canaria"
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="IES Santa Teresa de Jesus, Las Palmas de Gran Canaria"
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="IES Santa Teresa de Jesus, Las Palmas de Gran Canaria"
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="IES Santa Teresa de Jesus, Las Palmas de Gran Canaria"
// IES Tafira
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Tafira"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Tafira"
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="IES Tafira"
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="IES Tafira"
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="IES Tafira"
// IES Tafira 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Tafira "
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Tafira "
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="IES Tafira "
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="IES Tafira "
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="IES Tafira "
// Las Palmas/Colegio teresiana Las Palmas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Palmas/Colegio teresiana Las Palmas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Las Palmas/Colegio teresiana Las Palmas"
replace SocioeconStatPers = 11365 if LocalidadInstitutoenelcursas=="Las Palmas/Colegio teresiana Las Palmas"
replace SocioeconStatHog = 31172 if LocalidadInstitutoenelcursas=="Las Palmas/Colegio teresiana Las Palmas"
replace poblacion = 379925 if LocalidadInstitutoenelcursas=="Las Palmas/Colegio teresiana Las Palmas"
// Mogán
// ARGUINEGUÍN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN"
replace SocioeconStatPers = 9009 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN"
replace SocioeconStatHog = 24581 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN"
replace poblacion = 20072 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN"
// ARGUINEGUÍN 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN "
replace instituto = 1 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN "
replace SocioeconStatPers = 9009 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN "
replace SocioeconStatHog = 24581 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN "
replace poblacion = 20072 if LocalidadInstitutoenelcursas=="ARGUINEGUÍN "
// Puerto del Rosario
// Puerto del Rosario
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Puerto del Rosario"
replace SocioeconStatPers = 10380 if LocalidadInstitutoenelcursas=="Puerto del Rosario"
replace SocioeconStatHog = 28654 if LocalidadInstitutoenelcursas=="Puerto del Rosario"
replace poblacion = 40753 if LocalidadInstitutoenelcursas=="Puerto del Rosario"
// San Diego de Alcalá, Puerto del Rosario
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Diego de Alcalá, Puerto del Rosario"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Diego de Alcalá, Puerto del Rosario"
replace SocioeconStatPers = 10380 if LocalidadInstitutoenelcursas=="San Diego de Alcalá, Puerto del Rosario"
replace SocioeconStatHog = 28654 if LocalidadInstitutoenelcursas=="San Diego de Alcalá, Puerto del Rosario"
replace poblacion = 40753 if LocalidadInstitutoenelcursas=="San Diego de Alcalá, Puerto del Rosario"
// Santa Brígida
// Santa Brígida
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Brígida"
replace SocioeconStatPers = 14072 if LocalidadInstitutoenelcursas=="Santa Brígida"
replace SocioeconStatHog = 41367 if LocalidadInstitutoenelcursas=="Santa Brígida"
replace poblacion = 18263 if LocalidadInstitutoenelcursas=="Santa Brígida"
// American School of Las Palmas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
replace SocioeconStatPers = 14072 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
replace SocioeconStatHog = 41367 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
replace poblacion = 18263 if LocalidadInstitutoenelcursas=="American School of Las Palmas"
// Santa Lucía de Tirajana
// IES EL DOCTORAL
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES EL DOCTORAL"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES EL DOCTORAL"
replace SocioeconStatPers = 8613 if LocalidadInstitutoenelcursas=="IES EL DOCTORAL"
replace SocioeconStatHog = 25164 if LocalidadInstitutoenelcursas=="IES EL DOCTORAL"
replace poblacion = 73328 if LocalidadInstitutoenelcursas=="IES EL DOCTORAL"
// IES Santa Lucía
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Santa Lucía"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Santa Lucía"
replace SocioeconStatPers = 8613 if LocalidadInstitutoenelcursas=="IES Santa Lucía"
replace SocioeconStatHog = 25164 if LocalidadInstitutoenelcursas=="IES Santa Lucía"
replace poblacion = 73328 if LocalidadInstitutoenelcursas=="IES Santa Lucía"
// Teguise
// Colegio Arenas Internacional
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Arenas Internacional"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Arenas Internacional"
replace SocioeconStatPers = 11074 if LocalidadInstitutoenelcursas=="Colegio Arenas Internacional"
replace SocioeconStatHog = 31753 if LocalidadInstitutoenelcursas=="Colegio Arenas Internacional"
replace poblacion = 22342 if LocalidadInstitutoenelcursas=="Colegio Arenas Internacional"
// IES Teguise
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Teguise"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Teguise"
replace SocioeconStatPers = 11074 if LocalidadInstitutoenelcursas=="IES Teguise"
replace SocioeconStatHog = 31753 if LocalidadInstitutoenelcursas=="IES Teguise"
replace poblacion = 22342 if LocalidadInstitutoenelcursas=="IES Teguise"
// Telde
// Ies Casas Nuevas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Casas Nuevas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Casas Nuevas"
replace SocioeconStatPers = 9731 if LocalidadInstitutoenelcursas=="Ies Casas Nuevas"
replace SocioeconStatHog = 26858 if LocalidadInstitutoenelcursas=="Ies Casas Nuevas"
replace poblacion = 102647 if LocalidadInstitutoenelcursas=="Ies Casas Nuevas"
// Telde, I.E.S José Arencibia Gil
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Telde, I.E.S José Arencibia Gil"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Telde, I.E.S José Arencibia Gil"
replace SocioeconStatPers = 9731 if LocalidadInstitutoenelcursas=="Telde, I.E.S José Arencibia Gil"
replace SocioeconStatHog = 26858 if LocalidadInstitutoenelcursas=="Telde, I.E.S José Arencibia Gil"
replace poblacion = 102647 if LocalidadInstitutoenelcursas=="Telde, I.E.S José Arencibia Gil"
// Valsequillo
// Valsequillo, Gran Canaria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valsequillo, Gran Canaria"
replace SocioeconStatPers = 8521 if LocalidadInstitutoenelcursas=="Valsequillo, Gran Canaria"
replace SocioeconStatHog = 20175 if LocalidadInstitutoenelcursas=="Valsequillo, Gran Canaria"
replace poblacion = 9340 if LocalidadInstitutoenelcursas=="Valsequillo, Gran Canaria"
********************************************************************************
*    León
********************************************************************************
// Cistierna
// IES VADINIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES VADINIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES VADINIA"
replace SocioeconStatPers = 10857 if LocalidadInstitutoenelcursas=="IES VADINIA"
replace SocioeconStatHog = 23882 if LocalidadInstitutoenelcursas=="IES VADINIA"
replace poblacion = 3135 if LocalidadInstitutoenelcursas=="IES VADINIA"
// León
// León
replace cuidad = 1 if LocalidadInstitutoenelcursas=="León"
replace SocioeconStatPers = 13354 if LocalidadInstitutoenelcursas=="León"
replace SocioeconStatHog = 30238 if LocalidadInstitutoenelcursas=="León"
replace poblacion = 124303 if LocalidadInstitutoenelcursas=="León"
// Leon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Leon"
replace SocioeconStatPers = 13354 if LocalidadInstitutoenelcursas=="Leon"
replace SocioeconStatHog = 30238 if LocalidadInstitutoenelcursas=="Leon"
replace poblacion = 124303 if LocalidadInstitutoenelcursas=="Leon"
// León 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="León "
replace SocioeconStatPers = 13354 if LocalidadInstitutoenelcursas=="León "
replace SocioeconStatHog = 30238 if LocalidadInstitutoenelcursas=="León "
replace poblacion = 124303 if LocalidadInstitutoenelcursas=="León "
// León, Agustinas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="León, Agustinas"
replace SocioeconStatPers = 13354 if LocalidadInstitutoenelcursas=="León, Agustinas"
replace SocioeconStatHog = 30238 if LocalidadInstitutoenelcursas=="León, Agustinas"
replace poblacion = 124303 if LocalidadInstitutoenelcursas=="León, Agustinas"
// Ponferrada
// Gil y Carrasco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gil y Carrasco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Gil y Carrasco"
replace SocioeconStatPers = 11261 if LocalidadInstitutoenelcursas=="Gil y Carrasco"
replace SocioeconStatHog = 26800 if LocalidadInstitutoenelcursas=="Gil y Carrasco"
replace poblacion = 64674 if LocalidadInstitutoenelcursas=="Gil y Carrasco"
// Villafranca del Bierzo
// IES Padre Sarmiento (Villafranca del Bierzo)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Padre Sarmiento (Villafranca del Bierzo)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Padre Sarmiento (Villafranca del Bierzo)"
replace SocioeconStatPers = 10418 if LocalidadInstitutoenelcursas=="IES Padre Sarmiento (Villafranca del Bierzo)"
replace SocioeconStatHog = 22325 if LocalidadInstitutoenelcursas=="IES Padre Sarmiento (Villafranca del Bierzo)"
replace poblacion = 2899 if LocalidadInstitutoenelcursas=="IES Padre Sarmiento (Villafranca del Bierzo)"
********************************************************************************
*    Lérida
********************************************************************************
// Borjas Blancas / Borges Blanques, Les
// INS. JOSEP VALLVERDÚ
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS. JOSEP VALLVERDÚ"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS. JOSEP VALLVERDÚ"
replace SocioeconStatPers = 11301 if LocalidadInstitutoenelcursas=="INS. JOSEP VALLVERDÚ"
replace SocioeconStatHog = 28109 if LocalidadInstitutoenelcursas=="INS. JOSEP VALLVERDÚ"
replace poblacion = 6087 if LocalidadInstitutoenelcursas=="INS. JOSEP VALLVERDÚ"
// Lérida / LLeida
// Lleida
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lleida"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Lleida"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Lleida"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Lleida"
// Lleida / Màrius Torres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Lleida / Màrius Torres"
// Lleida/Ins Marius Torres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Lleida/Ins Marius Torres"
// Lleida Marius Torres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lleida Marius Torres"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lleida Marius Torres"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Lleida Marius Torres"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Lleida Marius Torres"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Lleida Marius Torres"
// Lleida/Ins Márius Torres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lleida/Ins Márius Torres"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lleida/Ins Márius Torres"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Lleida/Ins Márius Torres"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Lleida/Ins Márius Torres"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Lleida/Ins Márius Torres"
// Gili gaya
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gili gaya"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Gili gaya"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Gili gaya"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Gili gaya"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Gili gaya"
// Ins. Samuel Gili i Gaya, de Lleida
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ins. Samuel Gili i Gaya, de Lleida"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ins. Samuel Gili i Gaya, de Lleida"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="Ins. Samuel Gili i Gaya, de Lleida"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="Ins. Samuel Gili i Gaya, de Lleida"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="Ins. Samuel Gili i Gaya, de Lleida"
// INS Guissona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS Guissona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS Guissona"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="INS Guissona"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="INS Guissona"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="INS Guissona"
// INS Marius Torres
replace cuidad = 1 if LocalidadInstitutoenelcursas=="INS Marius Torres"
replace instituto = 1 if LocalidadInstitutoenelcursas=="INS Marius Torres"
replace SocioeconStatPers = 12059 if LocalidadInstitutoenelcursas=="INS Marius Torres"
replace SocioeconStatHog = 31206 if LocalidadInstitutoenelcursas=="INS Marius Torres"
replace poblacion = 138956 if LocalidadInstitutoenelcursas=="INS Marius Torres"
// Talavera
// Hh Maristas Santa María del Prado (Talavera)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Hh Maristas Santa María del Prado (Talavera)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Hh Maristas Santa María del Prado (Talavera)"
replace SocioeconStatPers = 11610 if LocalidadInstitutoenelcursas=="Hh Maristas Santa María del Prado (Talavera)"
replace SocioeconStatHog = 30354 if LocalidadInstitutoenelcursas=="Hh Maristas Santa María del Prado (Talavera)"
replace poblacion = 283 if LocalidadInstitutoenelcursas=="Hh Maristas Santa María del Prado (Talavera)"
// Tàrrega / Tárrega
// TARREGA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="TARREGA"
replace SocioeconStatPers = 10915 if LocalidadInstitutoenelcursas=="TARREGA"
replace SocioeconStatHog = 28778 if LocalidadInstitutoenelcursas=="TARREGA"
replace poblacion = 17098 if LocalidadInstitutoenelcursas=="TARREGA"
// Manuel de Pedrolo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Manuel de Pedrolo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Manuel de Pedrolo"
replace SocioeconStatPers = 10915 if LocalidadInstitutoenelcursas=="Manuel de Pedrolo"
replace SocioeconStatHog = 28778 if LocalidadInstitutoenelcursas=="Manuel de Pedrolo"
replace poblacion = 17098 if LocalidadInstitutoenelcursas=="Manuel de Pedrolo"
// Vedruna Tàrrega
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vedruna Tàrrega"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Vedruna Tàrrega"
replace SocioeconStatPers = 10915 if LocalidadInstitutoenelcursas=="Vedruna Tàrrega"
replace SocioeconStatHog = 28778 if LocalidadInstitutoenelcursas=="Vedruna Tàrrega"
replace poblacion = 17098 if LocalidadInstitutoenelcursas=="Vedruna Tàrrega"
// Torrefarrera
// Institut Joan Solà
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
replace SocioeconStatPers = 12748 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
replace SocioeconStatHog = 35673 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
replace poblacion = 4605 if LocalidadInstitutoenelcursas=="Institut Joan Solà"
// Tremp
// Tremp
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tremp"
replace SocioeconStatPers = 11674 if LocalidadInstitutoenelcursas=="Tremp"
replace SocioeconStatHog = 27434 if LocalidadInstitutoenelcursas=="Tremp"
replace poblacion = 5844 if LocalidadInstitutoenelcursas=="Tremp"
// Seu d'Urgell / Seo de Urgel
// La Seu d’Urgell/Institut Joan Brudieu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Seu d’Urgell/Institut Joan Brudieu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Seu d’Urgell/Institut Joan Brudieu"
replace SocioeconStatPers = 10183 if LocalidadInstitutoenelcursas=="La Seu d’Urgell/Institut Joan Brudieu"
replace SocioeconStatHog = 25635 if LocalidadInstitutoenelcursas=="La Seu d’Urgell/Institut Joan Brudieu"
replace poblacion = 12089 if LocalidadInstitutoenelcursas=="La Seu d’Urgell/Institut Joan Brudieu"
********************************************************************************
*    Lugo
********************************************************************************
// Lugo
// Lugo, Galicia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lugo, Galicia"
replace SocioeconStatPers = 11884 if LocalidadInstitutoenelcursas=="Lugo, Galicia"
replace SocioeconStatHog = 28404 if LocalidadInstitutoenelcursas=="Lugo, Galicia"
replace poblacion = 98276 if LocalidadInstitutoenelcursas=="Lugo, Galicia"
// Lugo, IES Nosa Señora dos Ollos Grandes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lugo, IES Nosa Señora dos Ollos Grandes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lugo, IES Nosa Señora dos Ollos Grandes"
replace SocioeconStatPers = 11884 if LocalidadInstitutoenelcursas=="Lugo, IES Nosa Señora dos Ollos Grandes"
replace SocioeconStatHog = 28404 if LocalidadInstitutoenelcursas=="Lugo, IES Nosa Señora dos Ollos Grandes"
replace poblacion = 98276 if LocalidadInstitutoenelcursas=="Lugo, IES Nosa Señora dos Ollos Grandes"
// Galén, Lugo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Galén, Lugo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Galén, Lugo"
replace SocioeconStatPers = 11884 if LocalidadInstitutoenelcursas=="Galén, Lugo"
replace SocioeconStatHog = 28404 if LocalidadInstitutoenelcursas=="Galén, Lugo"
replace poblacion = 98276 if LocalidadInstitutoenelcursas=="Galén, Lugo"
// Monterroso
// Monterroso, Lugo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Monterroso, Lugo"
replace SocioeconStatPers = 9598 if LocalidadInstitutoenelcursas=="Monterroso, Lugo"
replace SocioeconStatHog = 22374 if LocalidadInstitutoenelcursas=="Monterroso, Lugo"
replace poblacion = 3616 if LocalidadInstitutoenelcursas=="Monterroso, Lugo"
********************************************************************************
*    Madrid
********************************************************************************
// Alcalá de Henares
// Alcalá de Henares
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcalá de Henares"
replace SocioeconStatPers = 12097 if LocalidadInstitutoenelcursas=="Alcalá de Henares"
replace SocioeconStatHog = 33655 if LocalidadInstitutoenelcursas=="Alcalá de Henares"
replace poblacion = 195649 if LocalidadInstitutoenelcursas=="Alcalá de Henares"
// Alcalá de Henares, Lope de Vega
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcalá de Henares, Lope de Vega"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcalá de Henares, Lope de Vega"
replace SocioeconStatPers = 12097 if LocalidadInstitutoenelcursas=="Alcalá de Henares, Lope de Vega"
replace SocioeconStatHog = 33655 if LocalidadInstitutoenelcursas=="Alcalá de Henares, Lope de Vega"
replace poblacion = 195649 if LocalidadInstitutoenelcursas=="Alcalá de Henares, Lope de Vega"
// Alcobendas
// alcobendas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="alcobendas"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="alcobendas"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="alcobendas"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="alcobendas"
// Alcobendas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcobendas"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Alcobendas"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Alcobendas"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Alcobendas"
// La Moraleja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Moraleja"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="La Moraleja"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="La Moraleja"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="La Moraleja"
// La Moraleja 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Moraleja "
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="La Moraleja "
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="La Moraleja "
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="La Moraleja "
// Liceo Europeo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Liceo Europeo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Liceo Europeo"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Liceo Europeo"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Liceo Europeo"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Liceo Europeo"
// Runnymede College
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Runnymede College"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Runnymede College"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Runnymede College"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Runnymede College"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Runnymede College"
// liceo europeo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="liceo europeo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="liceo europeo"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="liceo europeo"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="liceo europeo"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="liceo europeo"
// ALCOBENDAS/LOS SAUCES
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ALCOBENDAS/LOS SAUCES"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ALCOBENDAS/LOS SAUCES"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="ALCOBENDAS/LOS SAUCES"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="ALCOBENDAS/LOS SAUCES"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="ALCOBENDAS/LOS SAUCES"
// Aldeafuente
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aldeafuente"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Aldeafuente"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Aldeafuente"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Aldeafuente"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Aldeafuente"
// Aldeafuente 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aldeafuente "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Aldeafuente "
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Aldeafuente "
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Aldeafuente "
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Aldeafuente "
// Alcobendas, IES Francisco Giner de los Ríos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcobendas, IES Francisco Giner de los Ríos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcobendas, IES Francisco Giner de los Ríos"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Alcobendas, IES Francisco Giner de los Ríos"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Alcobendas, IES Francisco Giner de los Ríos"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Alcobendas, IES Francisco Giner de los Ríos"
// B. V.M Irlandesas el Soto de la Moraleja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="B. V.M Irlandesas el Soto de la Moraleja"
replace instituto = 1 if LocalidadInstitutoenelcursas=="B. V.M Irlandesas el Soto de la Moraleja"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="B. V.M Irlandesas el Soto de la Moraleja"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="B. V.M Irlandesas el Soto de la Moraleja"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="B. V.M Irlandesas el Soto de la Moraleja"
// B.V.M Irlandesas el Soto
replace cuidad = 1 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto"
replace instituto = 1 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto"
// B.V.M Irlandesas el Soto 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto "
replace instituto = 1 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto "
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto "
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto "
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="B.V.M Irlandesas el Soto "
// Colegio Base
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Base"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Base"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Colegio Base"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Colegio Base"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Colegio Base"
// Colegio San Patricio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio San Patricio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio San Patricio"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Colegio San Patricio"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Colegio San Patricio"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Colegio San Patricio"
// Colegio base Alcobendas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio base Alcobendas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio base Alcobendas"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Colegio base Alcobendas"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Colegio base Alcobendas"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Colegio base Alcobendas"
// Colegio padre manyanet
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio padre manyanet"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio padre manyanet"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Colegio padre manyanet"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Colegio padre manyanet"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Colegio padre manyanet"
// IES Severo Ochoa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Severo Ochoa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Severo Ochoa"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="IES Severo Ochoa"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="IES Severo Ochoa"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="IES Severo Ochoa"
// Ies Severo Ochoa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Severo Ochoa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Severo Ochoa"
replace SocioeconStatPers = 18925 if LocalidadInstitutoenelcursas=="Ies Severo Ochoa"
replace SocioeconStatHog = 57678 if LocalidadInstitutoenelcursas=="Ies Severo Ochoa"
replace poblacion = 117040 if LocalidadInstitutoenelcursas=="Ies Severo Ochoa"
// Alcorcón
// Alcorcón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcón"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcón"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcón"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcón"
// Alcorcon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcon"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcon"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcon"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcon"
// Alcorcoón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcoón"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcoón"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcoón"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcoón"
// Alcorcón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcón"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcón"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcón"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcón"
// alcorcon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="alcorcon"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="alcorcon"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="alcorcon"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="alcorcon"
// Alcorcón 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcón "
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcón "
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcón "
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcón "
// Alcorcon-Josefina aldecoa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa"
// Alcorcon-Josefina aldecoa 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa "
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa "
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa "
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcon-Josefina aldecoa "
// San Pablo Ceu Montepríncipe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Pablo Ceu Montepríncipe"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Pablo Ceu Montepríncipe"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="San Pablo Ceu Montepríncipe"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="San Pablo Ceu Montepríncipe"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="San Pablo Ceu Montepríncipe"
// Alcorcón/Fuenllana
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcón/Fuenllana"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcorcón/Fuenllana"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcón/Fuenllana"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcón/Fuenllana"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcón/Fuenllana"
// Alcorcón/IES Los Castillos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcón/IES Los Castillos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcorcón/IES Los Castillos"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcón/IES Los Castillos"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcón/IES Los Castillos"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcón/IES Los Castillos"
// Alcorcón/Colegio Amanecer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Alcorcón/Colegio Amanecer"
// Amor De Dios Alcorcón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Amor De Dios Alcorcón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Amor De Dios Alcorcón"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Amor De Dios Alcorcón"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Amor De Dios Alcorcón"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Amor De Dios Alcorcón"
// Villalkor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villalkor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Villalkor"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Villalkor"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Villalkor"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Villalkor"
// Colegio santísima trinidad, Alcorcón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio santísima trinidad, Alcorcón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio santísima trinidad, Alcorcón"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="Colegio santísima trinidad, Alcorcón"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="Colegio santísima trinidad, Alcorcón"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="Colegio santísima trinidad, Alcorcón"
// IES ITACA ALCORCÓN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES ITACA ALCORCÓN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES ITACA ALCORCÓN"
replace SocioeconStatPers = 12437 if LocalidadInstitutoenelcursas=="IES ITACA ALCORCÓN"
replace SocioeconStatHog = 33100 if LocalidadInstitutoenelcursas=="IES ITACA ALCORCÓN"
replace poblacion = 170514 if LocalidadInstitutoenelcursas=="IES ITACA ALCORCÓN"
// Aranjuez
// Aranjuez
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aranjuez"
replace SocioeconStatPers = 11057 if LocalidadInstitutoenelcursas=="Aranjuez"
replace SocioeconStatHog = 31497 if LocalidadInstitutoenelcursas=="Aranjuez"
replace poblacion = 59607 if LocalidadInstitutoenelcursas=="Aranjuez"
// Aranjuez 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aranjuez "
replace SocioeconStatPers = 11057 if LocalidadInstitutoenelcursas=="Aranjuez "
replace SocioeconStatHog = 31497 if LocalidadInstitutoenelcursas=="Aranjuez "
replace poblacion = 59607 if LocalidadInstitutoenelcursas=="Aranjuez "
// Aranjuez/ IES Domenico Scarlatti
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aranjuez/ IES Domenico Scarlatti"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Aranjuez/ IES Domenico Scarlatti"
replace SocioeconStatPers = 11057 if LocalidadInstitutoenelcursas=="Aranjuez/ IES Domenico Scarlatti"
replace SocioeconStatHog = 31497 if LocalidadInstitutoenelcursas=="Aranjuez/ IES Domenico Scarlatti"
replace poblacion = 59607 if LocalidadInstitutoenelcursas=="Aranjuez/ IES Domenico Scarlatti"
// Arganda del rey
// Arganda del Rey
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Arganda del Rey"
replace SocioeconStatPers = 10646 if LocalidadInstitutoenelcursas=="Arganda del Rey"
replace SocioeconStatHog = 31495 if LocalidadInstitutoenelcursas=="Arganda del Rey"
replace poblacion = 55389 if LocalidadInstitutoenelcursas=="Arganda del Rey"
// arganda del rey
replace cuidad = 1 if LocalidadInstitutoenelcursas=="arganda del rey"
replace SocioeconStatPers = 10646 if LocalidadInstitutoenelcursas=="arganda del rey"
replace SocioeconStatHog = 31495 if LocalidadInstitutoenelcursas=="arganda del rey"
replace poblacion = 55389 if LocalidadInstitutoenelcursas=="arganda del rey"
// Arroyomolinos
// Arroyomolinos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Arroyomolinos"
replace SocioeconStatPers = 8529 if LocalidadInstitutoenelcursas=="Arroyomolinos"
replace SocioeconStatHog = 19107 if LocalidadInstitutoenelcursas=="Arroyomolinos"
replace poblacion = 31396 if LocalidadInstitutoenelcursas=="Arroyomolinos"
// Boadilla del Monte
// Boadilla del Monte
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
// Boadilla del monte
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Boadilla del monte"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Boadilla del monte"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Boadilla del monte"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Boadilla del monte"
// Boafilla del Monte
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Boafilla del Monte"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Boafilla del Monte"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Boafilla del Monte"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Boafilla del Monte"
// Boadilla del Monte
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Boadilla del Monte"
// BOADILLA DE MONTE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BOADILLA DE MONTE"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="BOADILLA DE MONTE"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="BOADILLA DE MONTE"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="BOADILLA DE MONTE"
// BOADILLA DEL MONTE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BOADILLA DEL MONTE"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="BOADILLA DEL MONTE"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="BOADILLA DEL MONTE"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="BOADILLA DEL MONTE"
// BOADILLA DEL MONTE 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="BOADILLA DEL MONTE "
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="BOADILLA DEL MONTE "
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="BOADILLA DEL MONTE "
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="BOADILLA DEL MONTE "
// Boadilla del Monte 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Boadilla del Monte "
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Boadilla del Monte "
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Boadilla del Monte "
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Boadilla del Monte "
// Boadilla del monte 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Boadilla del monte "
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Boadilla del monte "
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Boadilla del monte "
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Boadilla del monte "
// Monteprincipe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Monteprincipe"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Monteprincipe"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Monteprincipe"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Monteprincipe"
// Boadilla del Monte/ Mirabal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Boadilla del Monte/ Mirabal"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Boadilla del Monte/ Mirabal"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Boadilla del Monte/ Mirabal"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Boadilla del Monte/ Mirabal"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Boadilla del Monte/ Mirabal"
// Mirabal International school
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mirabal International school"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Mirabal International school"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Mirabal International school"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Mirabal International school"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Mirabal International school"
// Profesor Máximo Trueba
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Profesor Máximo Trueba"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Profesor Máximo Trueba"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Profesor Máximo Trueba"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Profesor Máximo Trueba"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Profesor Máximo Trueba"
// St Michales school boadilla del monte
replace cuidad = 1 if LocalidadInstitutoenelcursas=="St Michales school boadilla del monte"
replace instituto = 1 if LocalidadInstitutoenelcursas=="St Michales school boadilla del monte"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="St Michales school boadilla del monte"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="St Michales school boadilla del monte"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="St Michales school boadilla del monte"
// St. Michaels School
replace cuidad = 1 if LocalidadInstitutoenelcursas=="St. Michaels School"
replace instituto = 1 if LocalidadInstitutoenelcursas=="St. Michaels School"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="St. Michaels School"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="St. Michaels School"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="St. Michaels School"
// Saint Michael’s school
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Saint Michael’s school"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Saint Michael’s school"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Saint Michael’s school"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Saint Michael’s school"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Saint Michael’s school"
// St Michael’s School
replace cuidad = 1 if LocalidadInstitutoenelcursas=="St Michael’s School"
replace instituto = 1 if LocalidadInstitutoenelcursas=="St Michael’s School"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="St Michael’s School"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="St Michael’s School"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="St Michael’s School"
// Casvi Boadilla del monte
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Casvi Boadilla del monte"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Casvi Boadilla del monte"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Casvi Boadilla del monte"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Casvi Boadilla del monte"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Casvi Boadilla del monte"
// Colegio Mirabal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Mirabal"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Mirabal"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Colegio Mirabal"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Colegio Mirabal"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Colegio Mirabal"
// Colegio Virgen de Europa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa"
// Colegio Virgen de Europa, Boadilla del Monte
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa, Boadilla del Monte"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa, Boadilla del Monte"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa, Boadilla del Monte"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa, Boadilla del Monte"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Colegio Virgen de Europa, Boadilla del Monte"
// Colegio mirabal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio mirabal"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio mirabal"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Colegio mirabal"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Colegio mirabal"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Colegio mirabal"
// Colegio nuestra señora de la victoria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio nuestra señora de la victoria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio nuestra señora de la victoria"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Colegio nuestra señora de la victoria"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Colegio nuestra señora de la victoria"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Colegio nuestra señora de la victoria"
// Eurocolegio Casvi Boadilla del Monte
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi Boadilla del Monte"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi Boadilla del Monte"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi Boadilla del Monte"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi Boadilla del Monte"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi Boadilla del Monte"
// IES Profesor Máximo Trueba
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Profesor Máximo Trueba"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Profesor Máximo Trueba"
replace SocioeconStatPers = 20103 if LocalidadInstitutoenelcursas=="IES Profesor Máximo Trueba"
replace SocioeconStatHog = 67389 if LocalidadInstitutoenelcursas=="IES Profesor Máximo Trueba"
replace poblacion = 54570 if LocalidadInstitutoenelcursas=="IES Profesor Máximo Trueba"
// Collado Villalba
// Collado villalba
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Collado villalba"
replace SocioeconStatPers = 11435 if LocalidadInstitutoenelcursas=="Collado villalba"
replace SocioeconStatHog = 32912 if LocalidadInstitutoenelcursas=="Collado villalba"
replace poblacion = 63679 if LocalidadInstitutoenelcursas=="Collado villalba"
// Collado Villalba
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Collado Villalba"
replace SocioeconStatPers = 11435 if LocalidadInstitutoenelcursas=="Collado Villalba"
replace SocioeconStatHog = 32912 if LocalidadInstitutoenelcursas=="Collado Villalba"
replace poblacion = 63679 if LocalidadInstitutoenelcursas=="Collado Villalba"
// Collado Villalba ies María Guerrero
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Collado Villalba ies María Guerrero"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Collado Villalba ies María Guerrero"
replace SocioeconStatPers = 11435 if LocalidadInstitutoenelcursas=="Collado Villalba ies María Guerrero"
replace SocioeconStatHog = 32912 if LocalidadInstitutoenelcursas=="Collado Villalba ies María Guerrero"
replace poblacion = 63679 if LocalidadInstitutoenelcursas=="Collado Villalba ies María Guerrero"
// IES María Guerrero
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES María Guerrero"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES María Guerrero"
replace SocioeconStatPers = 11435 if LocalidadInstitutoenelcursas=="IES María Guerrero"
replace SocioeconStatHog = 32912 if LocalidadInstitutoenelcursas=="IES María Guerrero"
replace poblacion = 63679 if LocalidadInstitutoenelcursas=="IES María Guerrero"
// IES María Guerrero, Collado Villalba
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES María Guerrero, Collado Villalba"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES María Guerrero, Collado Villalba"
replace SocioeconStatPers = 11435 if LocalidadInstitutoenelcursas=="IES María Guerrero, Collado Villalba"
replace SocioeconStatHog = 32912 if LocalidadInstitutoenelcursas=="IES María Guerrero, Collado Villalba"
replace poblacion = 63679 if LocalidadInstitutoenelcursas=="IES María Guerrero, Collado Villalba"
// IES MARIA GUERRERO COLLADO VILLALBA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES MARIA GUERRERO COLLADO VILLALBA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES MARIA GUERRERO COLLADO VILLALBA"
replace SocioeconStatPers = 11435 if LocalidadInstitutoenelcursas=="IES MARIA GUERRERO COLLADO VILLALBA"
replace SocioeconStatHog = 32912 if LocalidadInstitutoenelcursas=="IES MARIA GUERRERO COLLADO VILLALBA"
replace poblacion = 63679 if LocalidadInstitutoenelcursas=="IES MARIA GUERRERO COLLADO VILLALBA"
// IES Las Canteras
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Las Canteras"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Las Canteras"
replace SocioeconStatPers = 11435 if LocalidadInstitutoenelcursas=="IES Las Canteras"
replace SocioeconStatHog = 32912 if LocalidadInstitutoenelcursas=="IES Las Canteras"
replace poblacion = 63679 if LocalidadInstitutoenelcursas=="IES Las Canteras"
// ÍES JAIME FERRÁN en Collado Villalba
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES JAIME FERRÁN en Collado Villalba"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES JAIME FERRÁN en Collado Villalba"
replace SocioeconStatPers = 11435 if LocalidadInstitutoenelcursas=="ÍES JAIME FERRÁN en Collado Villalba"
replace SocioeconStatHog = 32912 if LocalidadInstitutoenelcursas=="ÍES JAIME FERRÁN en Collado Villalba"
replace poblacion = 63679 if LocalidadInstitutoenelcursas=="ÍES JAIME FERRÁN en Collado Villalba"
// Colmenar Viejo
// Colmenar Viejo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colmenar Viejo"
replace SocioeconStatPers = 13632 if LocalidadInstitutoenelcursas=="Colmenar Viejo"
replace SocioeconStatHog = 40270 if LocalidadInstitutoenelcursas=="Colmenar Viejo"
replace poblacion = 50752 if LocalidadInstitutoenelcursas=="Colmenar Viejo"
// Colmenar Viejo/IES Rosa Chacel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colmenar Viejo/IES Rosa Chacel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colmenar Viejo/IES Rosa Chacel"
replace SocioeconStatPers = 13632 if LocalidadInstitutoenelcursas=="Colmenar Viejo/IES Rosa Chacel"
replace SocioeconStatHog = 40270 if LocalidadInstitutoenelcursas=="Colmenar Viejo/IES Rosa Chacel"
replace poblacion = 50752 if LocalidadInstitutoenelcursas=="Colmenar Viejo/IES Rosa Chacel"
// Coslada
// Coslada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Coslada"
replace SocioeconStatPers = 12029 if LocalidadInstitutoenelcursas=="Coslada"
replace SocioeconStatHog = 34348 if LocalidadInstitutoenelcursas=="Coslada"
replace poblacion = 81661 if LocalidadInstitutoenelcursas=="Coslada"
// Escorial, El
// EL Escorial
replace cuidad = 1 if LocalidadInstitutoenelcursas=="EL Escorial"
replace SocioeconStatPers = 13627 if LocalidadInstitutoenelcursas=="EL Escorial"
replace SocioeconStatHog = 36469 if LocalidadInstitutoenelcursas=="EL Escorial"
replace poblacion = 16162 if LocalidadInstitutoenelcursas=="EL Escorial"
// El Escorial
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Escorial"
replace SocioeconStatPers = 13627 if LocalidadInstitutoenelcursas=="El Escorial"
replace SocioeconStatHog = 36469 if LocalidadInstitutoenelcursas=="El Escorial"
replace poblacion = 16162 if LocalidadInstitutoenelcursas=="El Escorial"
// Fuenlabrada
// Fuenlabrada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Fuenlabrada"
replace SocioeconStatPers = 10062 if LocalidadInstitutoenelcursas=="Fuenlabrada"
replace SocioeconStatHog = 30034 if LocalidadInstitutoenelcursas=="Fuenlabrada"
replace poblacion = 193700 if LocalidadInstitutoenelcursas=="Fuenlabrada"
// Galapagar
// Galapagar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Galapagar"
replace SocioeconStatPers = 13145 if LocalidadInstitutoenelcursas=="Galapagar"
replace SocioeconStatHog = 38710 if LocalidadInstitutoenelcursas=="Galapagar"
replace poblacion = 33742 if LocalidadInstitutoenelcursas=="Galapagar"
// colegio parque
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio parque"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio parque"
replace SocioeconStatPers = 13145 if LocalidadInstitutoenelcursas=="colegio parque"
replace SocioeconStatHog = 38710 if LocalidadInstitutoenelcursas=="colegio parque"
replace poblacion = 33742 if LocalidadInstitutoenelcursas=="colegio parque"
// Getafe
// Getafe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Getafe"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="Getafe"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="Getafe"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="Getafe"
// getafe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="getafe"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="getafe"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="getafe"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="getafe"
// Getafe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Getafe"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="Getafe"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="Getafe"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="Getafe"
// GETAFE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="GETAFE"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="GETAFE"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="GETAFE"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="GETAFE"
// IES León Felipe Getafe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES León Felipe Getafe"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES León Felipe Getafe"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="IES León Felipe Getafe"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="IES León Felipe Getafe"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="IES León Felipe Getafe"
// José hierro getafe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="José hierro getafe"
replace instituto = 1 if LocalidadInstitutoenelcursas=="José hierro getafe"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="José hierro getafe"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="José hierro getafe"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="José hierro getafe"
// Getafe, Colegio Jesús Nazareno
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Getafe, Colegio Jesús Nazareno"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Getafe, Colegio Jesús Nazareno"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="Getafe, Colegio Jesús Nazareno"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="Getafe, Colegio Jesús Nazareno"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="Getafe, Colegio Jesús Nazareno"
// Matemático Puig Adam
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Matemático Puig Adam"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Matemático Puig Adam"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="Matemático Puig Adam"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="Matemático Puig Adam"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="Matemático Puig Adam"
// Matemático Puig Adam 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Matemático Puig Adam "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Matemático Puig Adam "
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="Matemático Puig Adam "
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="Matemático Puig Adam "
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="Matemático Puig Adam "
// Colegio Europeo Aristos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Europeo Aristos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Europeo Aristos"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="Colegio Europeo Aristos"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="Colegio Europeo Aristos"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="Colegio Europeo Aristos"
// Colegio Los Angeles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Los Angeles"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Los Angeles"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="Colegio Los Angeles"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="Colegio Los Angeles"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="Colegio Los Angeles"
// IES La Senda, Getafe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES La Senda, Getafe"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES La Senda, Getafe"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="IES La Senda, Getafe"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="IES La Senda, Getafe"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="IES La Senda, Getafe"
// IES SATAFI
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES SATAFI"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES SATAFI"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="IES SATAFI"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="IES SATAFI"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="IES SATAFI"
// LA INMACULADA PADRES ESCOLAPIOS
replace cuidad = 1 if LocalidadInstitutoenelcursas=="LA INMACULADA PADRES ESCOLAPIOS"
replace instituto = 1 if LocalidadInstitutoenelcursas=="LA INMACULADA PADRES ESCOLAPIOS"
replace SocioeconStatPers = 12250 if LocalidadInstitutoenelcursas=="LA INMACULADA PADRES ESCOLAPIOS"
replace SocioeconStatHog = 32545 if LocalidadInstitutoenelcursas=="LA INMACULADA PADRES ESCOLAPIOS"
replace poblacion = 183374 if LocalidadInstitutoenelcursas=="LA INMACULADA PADRES ESCOLAPIOS"
// Griñón
// Griñon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Griñon"
replace SocioeconStatPers = 12472 if LocalidadInstitutoenelcursas=="Griñon"
replace SocioeconStatHog = 38627 if LocalidadInstitutoenelcursas=="Griñon"
replace poblacion = 10319 if LocalidadInstitutoenelcursas=="Griñon"
// La Salle Griñón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Griñón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Griñón"
replace SocioeconStatPers = 12472 if LocalidadInstitutoenelcursas=="La Salle Griñón"
replace SocioeconStatHog = 38627 if LocalidadInstitutoenelcursas=="La Salle Griñón"
replace poblacion = 10319 if LocalidadInstitutoenelcursas=="La Salle Griñón"
// Colegio Villa de Griñón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
replace SocioeconStatPers = 12472 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
replace SocioeconStatHog = 38627 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
replace poblacion = 10319 if LocalidadInstitutoenelcursas=="Colegio Villa de Griñón"
// Colegio villa de griñon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio villa de griñon"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio villa de griñon"
replace SocioeconStatPers = 12472 if LocalidadInstitutoenelcursas=="Colegio villa de griñon"
replace SocioeconStatHog = 38627 if LocalidadInstitutoenelcursas=="Colegio villa de griñon"
replace poblacion = 10319 if LocalidadInstitutoenelcursas=="Colegio villa de griñon"
// Guadarrama
// Guadarrama
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Guadarrama"
replace SocioeconStatPers = 12645 if LocalidadInstitutoenelcursas=="Guadarrama"
replace SocioeconStatHog = 35024 if LocalidadInstitutoenelcursas=="Guadarrama"
replace poblacion = 16032 if LocalidadInstitutoenelcursas=="Guadarrama"
// guadarrama
replace cuidad = 1 if LocalidadInstitutoenelcursas=="guadarrama"
replace SocioeconStatPers = 12645 if LocalidadInstitutoenelcursas=="guadarrama"
replace SocioeconStatHog = 35024 if LocalidadInstitutoenelcursas=="guadarrama"
replace poblacion = 16032 if LocalidadInstitutoenelcursas=="guadarrama"
// CSA Los Negrales
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CSA Los Negrales"
replace instituto = 1 if LocalidadInstitutoenelcursas=="CSA Los Negrales"
replace SocioeconStatPers = 12645 if LocalidadInstitutoenelcursas=="CSA Los Negrales"
replace SocioeconStatHog = 35024 if LocalidadInstitutoenelcursas=="CSA Los Negrales"
replace poblacion = 16032 if LocalidadInstitutoenelcursas=="CSA Los Negrales"
// Humanes de Madrid
// Humanes de Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Humanes de Madrid"
replace SocioeconStatPers = 9262 if LocalidadInstitutoenelcursas=="Humanes de Madrid"
replace SocioeconStatHog = 28436 if LocalidadInstitutoenelcursas=="Humanes de Madrid"
replace poblacion = 19743 if LocalidadInstitutoenelcursas=="Humanes de Madrid"
// Leganés
// Leganés
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Leganés"
replace SocioeconStatPers = 11546 if LocalidadInstitutoenelcursas=="Leganés"
replace SocioeconStatHog = 30833 if LocalidadInstitutoenelcursas=="Leganés"
replace poblacion =  189861 if LocalidadInstitutoenelcursas=="Leganés"
// Leganes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Leganes"
replace SocioeconStatPers = 11546 if LocalidadInstitutoenelcursas=="Leganes"
replace SocioeconStatHog = 30833 if LocalidadInstitutoenelcursas=="Leganes"
replace poblacion =  189861 if LocalidadInstitutoenelcursas=="Leganes"
// Leganés
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Leganés"
replace SocioeconStatPers = 11546 if LocalidadInstitutoenelcursas=="Leganés"
replace SocioeconStatHog = 30833 if LocalidadInstitutoenelcursas=="Leganés"
replace poblacion =  189861 if LocalidadInstitutoenelcursas=="Leganés"
// IES ARQUITECTO PERIDIS- LEGANÉS
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES ARQUITECTO PERIDIS- LEGANÉS"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES ARQUITECTO PERIDIS- LEGANÉS"
replace SocioeconStatPers = 11546 if LocalidadInstitutoenelcursas=="IES ARQUITECTO PERIDIS- LEGANÉS"
replace SocioeconStatHog = 30833 if LocalidadInstitutoenelcursas=="IES ARQUITECTO PERIDIS- LEGANÉS"
replace poblacion =  189861 if LocalidadInstitutoenelcursas=="IES ARQUITECTO PERIDIS- LEGANÉS"
// María Zambrano, Leganés
replace cuidad = 1 if LocalidadInstitutoenelcursas=="María Zambrano, Leganés"
replace instituto = 1 if LocalidadInstitutoenelcursas=="María Zambrano, Leganés"
replace SocioeconStatPers = 11546 if LocalidadInstitutoenelcursas=="María Zambrano, Leganés"
replace SocioeconStatHog = 30833 if LocalidadInstitutoenelcursas=="María Zambrano, Leganés"
replace poblacion =  189861 if LocalidadInstitutoenelcursas=="María Zambrano, Leganés"
// IES Pablo Neruda, Leganés
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Pablo Neruda, Leganés"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Pablo Neruda, Leganés"
replace SocioeconStatPers = 11546 if LocalidadInstitutoenelcursas=="IES Pablo Neruda, Leganés"
replace SocioeconStatHog = 30833 if LocalidadInstitutoenelcursas=="IES Pablo Neruda, Leganés"
replace poblacion =  189861 if LocalidadInstitutoenelcursas=="IES Pablo Neruda, Leganés"
// Majadahonda
// Majadahonda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Majadahonda"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Majadahonda"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Majadahonda"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Majadahonda"
// Majadahonda 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Majadahonda "
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Majadahonda "
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Majadahonda "
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Majadahonda "
// Majadahonda, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Majadahonda, Madrid"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Majadahonda, Madrid"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Majadahonda, Madrid"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Majadahonda, Madrid"
// Majadahonda, ies margarita salas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Majadahonda, ies margarita salas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Majadahonda, ies margarita salas"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Majadahonda, ies margarita salas"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Majadahonda, ies margarita salas"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Majadahonda, ies margarita salas"
// Majadahonda/San Jaime
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Majadahonda/San Jaime"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Majadahonda/San Jaime"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Majadahonda/San Jaime"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Majadahonda/San Jaime"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Majadahonda/San Jaime"
// San jaime Majadahonda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San jaime Majadahonda"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San jaime Majadahonda"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="San jaime Majadahonda"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="San jaime Majadahonda"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="San jaime Majadahonda"
// Sagrado Corazón Reparadoras
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras"
// Sagrado Corazón Reparadoras 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras "
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras "
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras "
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Sagrado Corazón Reparadoras "
// Colegio San Jaime
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio San Jaime"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio San Jaime"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="Colegio San Jaime"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="Colegio San Jaime"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="Colegio San Jaime"
// IES Leonardo Da Vinci
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci"
// IES Leonardo Da Vinci, Majadahonda, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci, Majadahonda, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci, Majadahonda, Madrid"
replace SocioeconStatPers = 19282 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci, Majadahonda, Madrid"
replace SocioeconStatHog = 61838 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci, Majadahonda, Madrid"
replace poblacion = 71826 if LocalidadInstitutoenelcursas=="IES Leonardo Da Vinci, Majadahonda, Madrid"
// Móstoles
// mostoles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="mostoles"
replace SocioeconStatPers = 11004 if LocalidadInstitutoenelcursas=="mostoles"
replace SocioeconStatHog = 30826 if LocalidadInstitutoenelcursas=="mostoles"
replace poblacion = 209184 if LocalidadInstitutoenelcursas=="mostoles"
// Móstoles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Móstoles"
replace SocioeconStatPers = 11004 if LocalidadInstitutoenelcursas=="Móstoles"
replace SocioeconStatHog = 30826 if LocalidadInstitutoenelcursas=="Móstoles"
replace poblacion = 209184 if LocalidadInstitutoenelcursas=="Móstoles"
// Móstoles, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Móstoles, Madrid"
replace SocioeconStatPers = 11004 if LocalidadInstitutoenelcursas=="Móstoles, Madrid"
replace SocioeconStatHog = 30826 if LocalidadInstitutoenelcursas=="Móstoles, Madrid"
replace poblacion = 209184 if LocalidadInstitutoenelcursas=="Móstoles, Madrid"
// Móstoles/ IES Antonio de Nebrija
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Móstoles/ IES Antonio de Nebrija"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Móstoles/ IES Antonio de Nebrija"
replace SocioeconStatPers = 11004 if LocalidadInstitutoenelcursas=="Móstoles/ IES Antonio de Nebrija"
replace SocioeconStatHog = 30826 if LocalidadInstitutoenelcursas=="Móstoles/ IES Antonio de Nebrija"
replace poblacion = 209184 if LocalidadInstitutoenelcursas=="Móstoles/ IES Antonio de Nebrija"
// Colegio Villa de Móstoles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Villa de Móstoles"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Villa de Móstoles"
replace SocioeconStatPers = 11004 if LocalidadInstitutoenelcursas=="Colegio Villa de Móstoles"
replace SocioeconStatHog = 30826 if LocalidadInstitutoenelcursas=="Colegio Villa de Móstoles"
replace poblacion = 209184 if LocalidadInstitutoenelcursas=="Colegio Villa de Móstoles"
// Navalcarnero
// Navalcarnero
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Navalcarnero"
replace SocioeconStatPers = 10935 if LocalidadInstitutoenelcursas=="Navalcarnero"
replace SocioeconStatHog = 32669 if LocalidadInstitutoenelcursas=="Navalcarnero"
replace poblacion = 29298 if LocalidadInstitutoenelcursas=="Navalcarnero"
// Paracuellos de Jarama
// Paracuellos de Jarama
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
replace poblacion = 25269 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
// Paracuellos de Jarama
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
replace poblacion = 25269 if LocalidadInstitutoenelcursas=="Paracuellos de Jarama"
// Paracuellos De jarama
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Paracuellos De jarama"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Paracuellos De jarama"
replace SocioeconStatHog = 23409 if LocalidadInstitutoenelcursas=="Paracuellos De jarama"
replace poblacion = 25269 if LocalidadInstitutoenelcursas=="Paracuellos De jarama"
// Parla
// Parla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Parla"
replace SocioeconStatPers = 9004 if LocalidadInstitutoenelcursas=="Parla"
replace SocioeconStatHog = 27142 if LocalidadInstitutoenelcursas=="Parla"
replace poblacion = 130124 if LocalidadInstitutoenelcursas=="Parla"
// Parla/La Laguna
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Parla/La Laguna"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Parla/La Laguna"
replace SocioeconStatPers = 9004 if LocalidadInstitutoenelcursas=="Parla/La Laguna"
replace SocioeconStatHog = 27142 if LocalidadInstitutoenelcursas=="Parla/La Laguna"
replace poblacion = 130124 if LocalidadInstitutoenelcursas=="Parla/La Laguna"
// Nicolás Copernico, Parla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Nicolás Copernico, Parla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Nicolás Copernico, Parla"
replace SocioeconStatPers = 9004 if LocalidadInstitutoenelcursas=="Nicolás Copernico, Parla"
replace SocioeconStatHog = 27142 if LocalidadInstitutoenelcursas=="Nicolás Copernico, Parla"
replace poblacion = 130124 if LocalidadInstitutoenelcursas=="Nicolás Copernico, Parla"
// Parla/ Torrente Ballester
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Parla/ Torrente Ballester"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Parla/ Torrente Ballester"
replace SocioeconStatPers = 9004 if LocalidadInstitutoenelcursas=="Parla/ Torrente Ballester"
replace SocioeconStatHog = 27142 if LocalidadInstitutoenelcursas=="Parla/ Torrente Ballester"
replace poblacion = 130124 if LocalidadInstitutoenelcursas=="Parla/ Torrente Ballester"
// Pinto
// Pinto
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pinto"
replace SocioeconStatPers = 11957 if LocalidadInstitutoenelcursas=="Pinto"
replace SocioeconStatHog = 33342 if LocalidadInstitutoenelcursas=="Pinto"
replace poblacion = 52526 if LocalidadInstitutoenelcursas=="Pinto"
// Pinto
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pinto"
replace SocioeconStatPers = 11957 if LocalidadInstitutoenelcursas=="Pinto"
replace SocioeconStatHog = 33342 if LocalidadInstitutoenelcursas=="Pinto"
replace poblacion = 52526 if LocalidadInstitutoenelcursas=="Pinto"
// Mirasur
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mirasur"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Mirasur"
replace SocioeconStatPers = 11957 if LocalidadInstitutoenelcursas=="Mirasur"
replace SocioeconStatHog = 33342 if LocalidadInstitutoenelcursas=="Mirasur"
replace poblacion = 52526 if LocalidadInstitutoenelcursas=="Mirasur"
// Colegio Mirasur. Pinto (Madrid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
replace SocioeconStatPers = 11957 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
replace SocioeconStatHog = 33342 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
replace poblacion = 52526 if LocalidadInstitutoenelcursas=="Colegio Mirasur. Pinto (Madrid)"
// IES PABLO PICASSO
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES PABLO PICASSO"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES PABLO PICASSO"
replace SocioeconStatPers = 11957 if LocalidadInstitutoenelcursas=="IES PABLO PICASSO"
replace SocioeconStatHog = 33342 if LocalidadInstitutoenelcursas=="IES PABLO PICASSO"
replace poblacion = 52526 if LocalidadInstitutoenelcursas=="IES PABLO PICASSO"
// Pozuelo de Alarcón
// Pozuelo de Alarcón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón"
// Pozuelo de Alarcon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcon"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcon"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcon"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcon"
// Pozuelo de alarcon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pozuelo de alarcon"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Pozuelo de alarcon"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Pozuelo de alarcon"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Pozuelo de alarcon"
// POZUELO, MADRID
replace cuidad = 1 if LocalidadInstitutoenelcursas=="POZUELO, MADRID"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="POZUELO, MADRID"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="POZUELO, MADRID"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="POZUELO, MADRID"
// Pozuelo de Alarcón,Mater Salvatoris
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón,Mater Salvatoris"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón,Mater Salvatoris"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón,Mater Salvatoris"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón,Mater Salvatoris"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón,Mater Salvatoris"
// Mater salvatoris
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mater salvatoris"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Mater salvatoris"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Mater salvatoris"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Mater salvatoris"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Mater salvatoris"
// escuelas pías de san fernando, pozuelo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="escuelas pías de san fernando, pozuelo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="escuelas pías de san fernando, pozuelo"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="escuelas pías de san fernando, pozuelo"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="escuelas pías de san fernando, pozuelo"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="escuelas pías de san fernando, pozuelo"
// Liceo Sorolla C
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Liceo Sorolla C"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Liceo Sorolla C"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Liceo Sorolla C"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Liceo Sorolla C"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Liceo Sorolla C"
// Liceo Sorolla C, Pozuelo de Alarcón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Liceo Sorolla C, Pozuelo de Alarcón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Liceo Sorolla C, Pozuelo de Alarcón"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Liceo Sorolla C, Pozuelo de Alarcón"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Liceo Sorolla C, Pozuelo de Alarcón"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Liceo Sorolla C, Pozuelo de Alarcón"
// Pozuelo de Alarcón/ colegio instituto Veritas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón/ colegio instituto Veritas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón/ colegio instituto Veritas"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón/ colegio instituto Veritas"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón/ colegio instituto Veritas"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Pozuelo de Alarcón/ colegio instituto Veritas"
// Instituto Veritas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Instituto Veritas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Instituto Veritas"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Instituto Veritas"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Instituto Veritas"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Instituto Veritas"
// Instituto Veritas (Pozuelo de Alarcón)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Instituto Veritas (Pozuelo de Alarcón)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Instituto Veritas (Pozuelo de Alarcón)"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Instituto Veritas (Pozuelo de Alarcón)"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Instituto Veritas (Pozuelo de Alarcón)"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Instituto Veritas (Pozuelo de Alarcón)"
// Everest School Monteclaro
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Everest School Monteclaro"
// IES Gerardo Diego
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="IES Gerardo Diego"
// ies gerardo diego
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies gerardo diego"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies gerardo diego"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="ies gerardo diego"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="ies gerardo diego"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="ies gerardo diego"
// Colegio Retamar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Retamar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Retamar"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Colegio Retamar"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Colegio Retamar"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Colegio Retamar"
// Retamar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Retamar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Retamar"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Retamar"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Retamar"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Retamar"
// I.E.S SAN JUAN DE LA CRUZ
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ"
// I.E.S SAN JUAN DE LA CRUZ 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ "
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ "
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ "
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ "
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="I.E.S SAN JUAN DE LA CRUZ "
// Aquinas American School
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aquinas American School"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Aquinas American School"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Aquinas American School"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Aquinas American School"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Aquinas American School"
// Colegio San Luis de los Franceses (Pozuelo de Alarcón, Madrid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio San Luis de los Franceses (Pozuelo de Alarcón, Madrid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio San Luis de los Franceses (Pozuelo de Alarcón, Madrid)"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Colegio San Luis de los Franceses (Pozuelo de Alarcón, Madrid)"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Colegio San Luis de los Franceses (Pozuelo de Alarcón, Madrid)"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Colegio San Luis de los Franceses (Pozuelo de Alarcón, Madrid)"
// Colegio Everest (monteclaro, pozuelo)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Everest (monteclaro, pozuelo)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Everest (monteclaro, pozuelo)"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Colegio Everest (monteclaro, pozuelo)"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Colegio Everest (monteclaro, pozuelo)"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Colegio Everest (monteclaro, pozuelo)"
// Everest school
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Everest school"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Everest school"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Everest school"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Everest school"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Everest school"
// Escuelas Pías de San Fernando
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escuelas Pías de San Fernando"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escuelas Pías de San Fernando"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Escuelas Pías de San Fernando"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Escuelas Pías de San Fernando"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Escuelas Pías de San Fernando"
// Gerardo Diego Pozuelo de Alarcón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gerardo Diego Pozuelo de Alarcón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Gerardo Diego Pozuelo de Alarcón"
replace SocioeconStatPers = 25903 if LocalidadInstitutoenelcursas=="Gerardo Diego Pozuelo de Alarcón"
replace SocioeconStatHog = 83388 if LocalidadInstitutoenelcursas=="Gerardo Diego Pozuelo de Alarcón"
replace poblacion = 86422 if LocalidadInstitutoenelcursas=="Gerardo Diego Pozuelo de Alarcón"
// Rivas
// Rivas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Rivas"
replace SocioeconStatPers = 14002 if LocalidadInstitutoenelcursas=="Rivas"
replace SocioeconStatHog = 42234 if LocalidadInstitutoenelcursas=="Rivas"
replace poblacion = 88150 if LocalidadInstitutoenelcursas=="Rivas"
// Rivas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Rivas"
replace SocioeconStatPers = 14002 if LocalidadInstitutoenelcursas=="Rivas"
replace SocioeconStatHog = 42234 if LocalidadInstitutoenelcursas=="Rivas"
replace poblacion = 88150 if LocalidadInstitutoenelcursas=="Rivas"
// Rivas 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Rivas "
replace SocioeconStatPers = 14002 if LocalidadInstitutoenelcursas=="Rivas "
replace SocioeconStatHog = 42234 if LocalidadInstitutoenelcursas=="Rivas "
replace poblacion = 88150 if LocalidadInstitutoenelcursas=="Rivas "
// Rivas Vaciamadrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Rivas Vaciamadrid"
replace SocioeconStatPers = 14002 if LocalidadInstitutoenelcursas=="Rivas Vaciamadrid"
replace SocioeconStatHog = 42234 if LocalidadInstitutoenelcursas=="Rivas Vaciamadrid"
replace poblacion = 88150 if LocalidadInstitutoenelcursas=="Rivas Vaciamadrid"
// RIVAS-VACIAMADRID
replace cuidad = 1 if LocalidadInstitutoenelcursas=="RIVAS-VACIAMADRID"
replace SocioeconStatPers = 14002 if LocalidadInstitutoenelcursas=="RIVAS-VACIAMADRID"
replace SocioeconStatHog = 42234 if LocalidadInstitutoenelcursas=="RIVAS-VACIAMADRID"
replace poblacion = 88150 if LocalidadInstitutoenelcursas=="RIVAS-VACIAMADRID"
// Luyferivas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Luyferivas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Luyferivas"
replace SocioeconStatPers = 14002 if LocalidadInstitutoenelcursas=="Luyferivas"
replace SocioeconStatHog = 42234 if LocalidadInstitutoenelcursas=="Luyferivas"
replace poblacion = 88150 if LocalidadInstitutoenelcursas=="Luyferivas"
// rivas vaciamadrid/luyferivas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="rivas vaciamadrid/luyferivas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="rivas vaciamadrid/luyferivas"
replace SocioeconStatPers = 14002 if LocalidadInstitutoenelcursas=="rivas vaciamadrid/luyferivas"
replace SocioeconStatHog = 42234 if LocalidadInstitutoenelcursas=="rivas vaciamadrid/luyferivas"
replace poblacion = 88150 if LocalidadInstitutoenelcursas=="rivas vaciamadrid/luyferivas"
// Rivas Vaciamadrid/IES Las Lagunas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Rivas Vaciamadrid/IES Las Lagunas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Rivas Vaciamadrid/IES Las Lagunas"
replace SocioeconStatPers = 14002 if LocalidadInstitutoenelcursas=="Rivas Vaciamadrid/IES Las Lagunas"
replace SocioeconStatHog = 42234 if LocalidadInstitutoenelcursas=="Rivas Vaciamadrid/IES Las Lagunas"
replace poblacion = 88150 if LocalidadInstitutoenelcursas=="Rivas Vaciamadrid/IES Las Lagunas"
// Rozas de Madrid, las
// Las Rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las Rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las Rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las Rozas"
// Las rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las rozas"
// Las Rozas 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Rozas "
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las Rozas "
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las Rozas "
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las Rozas "
// Las Rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las Rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las Rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las Rozas"
// Las  rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las  rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las  rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las  rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las  rozas"
// Las rozas de madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las rozas de madrid"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las rozas de madrid"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las rozas de madrid"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las rozas de madrid"
// Las Rozas de Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Rozas de Madrid"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las Rozas de Madrid"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las Rozas de Madrid"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las Rozas de Madrid"
// Colegio Logos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Logos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Logos"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Colegio Logos"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Colegio Logos"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Colegio Logos"
// Jose García nieto, las rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jose García nieto, las rozas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jose García nieto, las rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Jose García nieto, las rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Jose García nieto, las rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Jose García nieto, las rozas"
// IES LAS ROZAS 1
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES LAS ROZAS 1"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES LAS ROZAS 1"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="IES LAS ROZAS 1"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="IES LAS ROZAS 1"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="IES LAS ROZAS 1"
// IES EL BURGO
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES EL BURGO"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES EL BURGO"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="IES EL BURGO"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="IES EL BURGO"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="IES EL BURGO"
// IES El Burgo de Las Rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES El Burgo de Las Rozas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES El Burgo de Las Rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="IES El Burgo de Las Rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="IES El Burgo de Las Rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="IES El Burgo de Las Rozas"
// Centro educativo Zola, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Centro educativo Zola, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Centro educativo Zola, Madrid"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Centro educativo Zola, Madrid"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Centro educativo Zola, Madrid"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Centro educativo Zola, Madrid"
// Zola las Rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Zola las Rozas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Zola las Rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Zola las Rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Zola las Rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Zola las Rozas"
// Colegio Berriz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Berriz"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Berriz"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Colegio Berriz"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Colegio Berriz"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Colegio Berriz"
// Colegio Europeo de Madrid , Las Rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Europeo de Madrid , Las Rozas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Europeo de Madrid , Las Rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Colegio Europeo de Madrid , Las Rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Colegio Europeo de Madrid , Las Rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Colegio Europeo de Madrid , Las Rozas"
// Colegio GSD Las Rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio GSD Las Rozas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio GSD Las Rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Colegio GSD Las Rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Colegio GSD Las Rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Colegio GSD Las Rozas"
// Colegio Orvalle
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Orvalle"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Orvalle"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Colegio Orvalle"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Colegio Orvalle"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Colegio Orvalle"
// Colegio Orvalle, Las Rozas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Orvalle, Las Rozas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Orvalle, Las Rozas"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Colegio Orvalle, Las Rozas"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Colegio Orvalle, Las Rozas"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Colegio Orvalle, Las Rozas"
// Las Rozas de Madrid/Colegio Logos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Rozas de Madrid/Colegio Logos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Las Rozas de Madrid/Colegio Logos"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las Rozas de Madrid/Colegio Logos"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las Rozas de Madrid/Colegio Logos"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las Rozas de Madrid/Colegio Logos"
// Las Rozas, IES El Burgo Ignacio Echevarría
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Rozas, IES El Burgo Ignacio Echevarría"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Las Rozas, IES El Burgo Ignacio Echevarría"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Las Rozas, IES El Burgo Ignacio Echevarría"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Las Rozas, IES El Burgo Ignacio Echevarría"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Las Rozas, IES El Burgo Ignacio Echevarría"
// IES El burgo-Ignacio Echeverría
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES El burgo-Ignacio Echeverría"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES El burgo-Ignacio Echeverría"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="IES El burgo-Ignacio Echeverría"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="IES El burgo-Ignacio Echeverría"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="IES El burgo-Ignacio Echeverría"
// IES José García Nieto
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES José García Nieto"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES José García Nieto"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="IES José García Nieto"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="IES José García Nieto"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="IES José García Nieto"
// IES José García Nieto en Las Rozas de Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES José García Nieto en Las Rozas de Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES José García Nieto en Las Rozas de Madrid"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="IES José García Nieto en Las Rozas de Madrid"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="IES José García Nieto en Las Rozas de Madrid"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="IES José García Nieto en Las Rozas de Madrid"
// Ies carmen conde
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies carmen conde"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies carmen conde"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Ies carmen conde"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Ies carmen conde"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Ies carmen conde"
// Ies el Burgo-Ignacio Echeverría
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies el Burgo-Ignacio Echeverría"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies el Burgo-Ignacio Echeverría"
replace SocioeconStatPers = 19202 if LocalidadInstitutoenelcursas=="Ies el Burgo-Ignacio Echeverría"
replace SocioeconStatHog = 60236 if LocalidadInstitutoenelcursas=="Ies el Burgo-Ignacio Echeverría"
replace poblacion = 95814 if LocalidadInstitutoenelcursas=="Ies el Burgo-Ignacio Echeverría"
// San Agustín del Guadalix
// IES San Agustín del Guadalix
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES San Agustín del Guadalix"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES San Agustín del Guadalix"
replace SocioeconStatPers = 14973 if LocalidadInstitutoenelcursas=="IES San Agustín del Guadalix"
replace SocioeconStatHog = 48337 if LocalidadInstitutoenelcursas=="IES San Agustín del Guadalix"
replace poblacion = 13379 if LocalidadInstitutoenelcursas=="IES San Agustín del Guadalix"
// San Martín de la Vega
// Napoleon High School (Ohio,USA)//ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Napoleon High School (Ohio,USA)//ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Napoleon High School (Ohio,USA)//ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
replace SocioeconStatPers = 9577 if LocalidadInstitutoenelcursas=="Napoleon High School (Ohio,USA)//ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
replace SocioeconStatHog = 29035 if LocalidadInstitutoenelcursas=="Napoleon High School (Ohio,USA)//ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
replace poblacion = 19170 if LocalidadInstitutoenelcursas=="Napoleon High School (Ohio,USA)//ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
// ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
replace SocioeconStatPers = 9577 if LocalidadInstitutoenelcursas=="ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
replace SocioeconStatHog = 29035 if LocalidadInstitutoenelcursas=="ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
replace poblacion = 19170 if LocalidadInstitutoenelcursas=="ÍES Anselmo Lorenzo (San martin De la Vega, Madrid)"
// San Lorenzo de el Escorial
// san lorenzo de el escorial
replace cuidad = 1 if LocalidadInstitutoenelcursas=="san lorenzo de el escorial"
replace SocioeconStatPers = 13030 if LocalidadInstitutoenelcursas=="san lorenzo de el escorial"
replace SocioeconStatHog = 34915 if LocalidadInstitutoenelcursas=="san lorenzo de el escorial"
replace poblacion = 18369 if LocalidadInstitutoenelcursas=="san lorenzo de el escorial"
// San Sebastián de los Reyes
// S.S. de los Reyes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="S.S. de los Reyes"
replace SocioeconStatPers = 14323 if LocalidadInstitutoenelcursas=="S.S. de los Reyes"
replace SocioeconStatHog = 40526 if LocalidadInstitutoenelcursas=="S.S. de los Reyes"
replace poblacion = 89276 if LocalidadInstitutoenelcursas=="S.S. de los Reyes"
// Torrejón de Ardoz
// Torrejon de Ardoz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrejon de Ardoz"
replace SocioeconStatPers = 11159 if LocalidadInstitutoenelcursas=="Torrejon de Ardoz"
replace SocioeconStatHog = 32266 if LocalidadInstitutoenelcursas=="Torrejon de Ardoz"
replace poblacion = 131376 if LocalidadInstitutoenelcursas=="Torrejon de Ardoz"
// Torrejon de Ardoz 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrejon de Ardoz "
replace SocioeconStatPers = 11159 if LocalidadInstitutoenelcursas=="Torrejon de Ardoz "
replace SocioeconStatHog = 32266 if LocalidadInstitutoenelcursas=="Torrejon de Ardoz "
replace poblacion = 131376 if LocalidadInstitutoenelcursas=="Torrejon de Ardoz "
// Torrejón de Ardoz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrejón de Ardoz"
replace SocioeconStatPers = 11159 if LocalidadInstitutoenelcursas=="Torrejón de Ardoz"
replace SocioeconStatHog = 32266 if LocalidadInstitutoenelcursas=="Torrejón de Ardoz"
replace poblacion = 131376 if LocalidadInstitutoenelcursas=="Torrejón de Ardoz"
// COLEGIO SAN JUAN BOSCO (TORREJÓN DE ARDOZ)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="COLEGIO SAN JUAN BOSCO (TORREJÓN DE ARDOZ)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="COLEGIO SAN JUAN BOSCO (TORREJÓN DE ARDOZ)"
replace SocioeconStatPers = 11159 if LocalidadInstitutoenelcursas=="COLEGIO SAN JUAN BOSCO (TORREJÓN DE ARDOZ)"
replace SocioeconStatHog = 32266 if LocalidadInstitutoenelcursas=="COLEGIO SAN JUAN BOSCO (TORREJÓN DE ARDOZ)"
replace poblacion = 131376 if LocalidadInstitutoenelcursas=="COLEGIO SAN JUAN BOSCO (TORREJÓN DE ARDOZ)"
// IES Luis de Góngora, Torrejón de Ardoz (Madrid).
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Luis de Góngora, Torrejón de Ardoz (Madrid)."
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Luis de Góngora, Torrejón de Ardoz (Madrid)."
replace SocioeconStatPers = 11159 if LocalidadInstitutoenelcursas=="IES Luis de Góngora, Torrejón de Ardoz (Madrid)."
replace SocioeconStatHog = 32266 if LocalidadInstitutoenelcursas=="IES Luis de Góngora, Torrejón de Ardoz (Madrid)."
replace poblacion = 131376 if LocalidadInstitutoenelcursas=="IES Luis de Góngora, Torrejón de Ardoz (Madrid)."
// Torrejón de la calzada
// Torrejón de la calzada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrejón de la calzada"
replace SocioeconStatPers = 11620 if LocalidadInstitutoenelcursas=="Torrejón de la calzada"
replace SocioeconStatHog = 35143 if LocalidadInstitutoenelcursas=="Torrejón de la calzada"
replace poblacion = 8872 if LocalidadInstitutoenelcursas=="Torrejón de la calzada"
// Torrelaguna
// IES ALTO JARAMA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA"
replace SocioeconStatPers = 10845 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA"
replace SocioeconStatHog = 29731 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA"
replace poblacion = 4760 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA"
// IES ALTO JARAMA 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA "
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA "
replace SocioeconStatPers = 10845 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA "
replace SocioeconStatHog = 29731 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA "
replace poblacion = 4760 if LocalidadInstitutoenelcursas=="IES ALTO JARAMA "
// Torrelodones
// Torrelodones
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrelodones"
replace SocioeconStatPers = 19041 if LocalidadInstitutoenelcursas=="Torrelodones"
replace SocioeconStatHog = 63326 if LocalidadInstitutoenelcursas=="Torrelodones"
replace poblacion = 23717 if LocalidadInstitutoenelcursas=="Torrelodones"
// Torrrelodones
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrrelodones"
replace SocioeconStatPers = 19041 if LocalidadInstitutoenelcursas=="Torrrelodones"
replace SocioeconStatHog = 63326 if LocalidadInstitutoenelcursas=="Torrrelodones"
replace poblacion = 23717 if LocalidadInstitutoenelcursas=="Torrrelodones"
// Colegio Peñalar, Torrelodones
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Peñalar, Torrelodones"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Peñalar, Torrelodones"
replace SocioeconStatPers = 19041 if LocalidadInstitutoenelcursas=="Colegio Peñalar, Torrelodones"
replace SocioeconStatHog = 63326 if LocalidadInstitutoenelcursas=="Colegio Peñalar, Torrelodones"
replace poblacion = 23717 if LocalidadInstitutoenelcursas=="Colegio Peñalar, Torrelodones"
// Torrelodones. IES Diego Velázquez
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrelodones. IES Diego Velázquez"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Torrelodones. IES Diego Velázquez"
replace SocioeconStatPers = 19041 if LocalidadInstitutoenelcursas=="Torrelodones. IES Diego Velázquez"
replace SocioeconStatHog = 63326 if LocalidadInstitutoenelcursas=="Torrelodones. IES Diego Velázquez"
replace poblacion = 23717 if LocalidadInstitutoenelcursas=="Torrelodones. IES Diego Velázquez"
// Tres Cantos
// Tres cantos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tres cantos"
replace SocioeconStatPers = 18227 if LocalidadInstitutoenelcursas=="Tres cantos"
replace SocioeconStatHog = 51944 if LocalidadInstitutoenelcursas=="Tres cantos"
replace poblacion = 47722 if LocalidadInstitutoenelcursas=="Tres cantos"
// Tres Cantos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tres Cantos"
replace SocioeconStatPers = 18227 if LocalidadInstitutoenelcursas=="Tres Cantos"
replace SocioeconStatHog = 51944 if LocalidadInstitutoenelcursas=="Tres Cantos"
replace poblacion = 47722 if LocalidadInstitutoenelcursas=="Tres Cantos"
// Tres Cantos/Ies Jose Luis Sampedro
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tres Cantos/Ies Jose Luis Sampedro"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Tres Cantos/Ies Jose Luis Sampedro"
replace SocioeconStatPers = 18227 if LocalidadInstitutoenelcursas=="Tres Cantos/Ies Jose Luis Sampedro"
replace SocioeconStatHog = 51944 if LocalidadInstitutoenelcursas=="Tres Cantos/Ies Jose Luis Sampedro"
replace poblacion = 47722 if LocalidadInstitutoenelcursas=="Tres Cantos/Ies Jose Luis Sampedro"
// King's College British School of Madrid (Tres Cantos)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="King's College British School of Madrid (Tres Cantos)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="King's College British School of Madrid (Tres Cantos)"
replace SocioeconStatPers = 18227 if LocalidadInstitutoenelcursas=="King's College British School of Madrid (Tres Cantos)"
replace SocioeconStatHog = 51944 if LocalidadInstitutoenelcursas=="King's College British School of Madrid (Tres Cantos)"
replace poblacion = 47722 if LocalidadInstitutoenelcursas=="King's College British School of Madrid (Tres Cantos)"
// Valdemorillo
// Valdemorillo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valdemorillo"
replace SocioeconStatPers = 13257 if LocalidadInstitutoenelcursas=="Valdemorillo"
replace SocioeconStatHog = 36588 if LocalidadInstitutoenelcursas=="Valdemorillo"
replace poblacion = 12518 if LocalidadInstitutoenelcursas=="Valdemorillo"
// Valdemoro
// Valdemoro
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valdemoro"
replace SocioeconStatPers = 11593 if LocalidadInstitutoenelcursas=="Valdemoro"
replace SocioeconStatHog = 34829 if LocalidadInstitutoenelcursas=="Valdemoro"
replace poblacion = 75983 if LocalidadInstitutoenelcursas=="Valdemoro"
// Juncarejo Marqués de Vallejo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
replace SocioeconStatPers = 11593 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
replace SocioeconStatHog = 34829 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
replace poblacion = 75983 if LocalidadInstitutoenelcursas=="Juncarejo Marqués de Vallejo"
// Villa de Madrid
// Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid"
// madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="madrid"
// MADRID
replace cuidad = 1 if LocalidadInstitutoenelcursas=="MADRID"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="MADRID"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="MADRID"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="MADRID"
// Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid"
// Madrid 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid "
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid "
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid "
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid "
// usera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="usera"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="usera"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="usera"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="usera"
// Aravaca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aravaca"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Aravaca"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Aravaca"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Aravaca"
// Aravaca 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aravaca "
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Aravaca "
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Aravaca "
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Aravaca "
// aravaca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="aravaca"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="aravaca"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="aravaca"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="aravaca"
// Vicálvaro
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vicálvaro"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Vicálvaro"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Vicálvaro"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Vicálvaro"
// Colegio Mater Salvatoris Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Mater Salvatoris Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Mater Salvatoris Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Mater Salvatoris Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Mater Salvatoris Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Mater Salvatoris Madrid"
// uam acceso +25
replace cuidad = 1 if LocalidadInstitutoenelcursas=="uam acceso +25"
replace instituto = 1 if LocalidadInstitutoenelcursas=="uam acceso +25"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="uam acceso +25"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="uam acceso +25"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="uam acceso +25"
// Santa Maria de los Rosales / Aravaca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Maria de los Rosales / Aravaca"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Maria de los Rosales / Aravaca"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Santa Maria de los Rosales / Aravaca"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Santa Maria de los Rosales / Aravaca"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Santa Maria de los Rosales / Aravaca"
// maristas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="maristas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="maristas"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="maristas"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="maristas"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="maristas"
// Santa Maria del bosque
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Maria del bosque"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Maria del bosque"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Santa Maria del bosque"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Santa Maria del bosque"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Santa Maria del bosque"
// sagrado corazon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="sagrado corazon"
replace instituto = 1 if LocalidadInstitutoenelcursas=="sagrado corazon"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="sagrado corazon"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="sagrado corazon"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="sagrado corazon"
// nuestra señora del pilar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="nuestra señora del pilar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="nuestra señora del pilar"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="nuestra señora del pilar"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="nuestra señora del pilar"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="nuestra señora del pilar"
// colegio agora
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio agora"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio agora"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="colegio agora"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="colegio agora"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="colegio agora"
// Asumiendo Colegio Brains International School María Lombillo
// brains
replace cuidad = 1 if LocalidadInstitutoenelcursas=="brains"
replace instituto = 1 if LocalidadInstitutoenelcursas=="brains"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="brains"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="brains"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="brains"
// colegios Ramón y Cajal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegios Ramón y Cajal"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegios Ramón y Cajal"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="colegios Ramón y Cajal"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="colegios Ramón y Cajal"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="colegios Ramón y Cajal"
// Senara
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Senara"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Senara"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Senara"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Senara"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Senara"
// calasanco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="calasanco"
replace instituto = 1 if LocalidadInstitutoenelcursas=="calasanco"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="calasanco"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="calasanco"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="calasanco"
// Valdecás
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valdecás"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Valdecás"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Valdecás"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Valdecás"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Valdecás"
// Liceo Italiano de Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Liceo Italiano de Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Liceo Italiano de Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Liceo Italiano de Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Liceo Italiano de Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Liceo Italiano de Madrid"
// San Isidoro de Sevilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Isidoro de Sevilla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Isidoro de Sevilla"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="San Isidoro de Sevilla"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="San Isidoro de Sevilla"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="San Isidoro de Sevilla"
// Sagrado Corazón de Jesús
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús"
// San Miguel II
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Miguel II"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Miguel II"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="San Miguel II"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="San Miguel II"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="San Miguel II"
// Sagrado Corazón de Jesús de Ferraz (Madrid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús de Ferraz (Madrid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús de Ferraz (Madrid)"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús de Ferraz (Madrid)"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús de Ferraz (Madrid)"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Sagrado Corazón de Jesús de Ferraz (Madrid)"
// San Gabriel Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Gabriel Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San Gabriel Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="San Gabriel Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="San Gabriel Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="San Gabriel Madrid"
// Santa Maria de la Hispanidad
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad"
// Santa Maria de la Hispanidad 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad "
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad "
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad "
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Santa Maria de la Hispanidad "
// Colegio Bernadette
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Bernadette"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Bernadette"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Bernadette"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Bernadette"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Bernadette"
// SALESIANOS SAN MIGUEL ARCANGEL
replace cuidad = 1 if LocalidadInstitutoenelcursas=="SALESIANOS SAN MIGUEL ARCANGEL"
replace instituto = 1 if LocalidadInstitutoenelcursas=="SALESIANOS SAN MIGUEL ARCANGEL"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="SALESIANOS SAN MIGUEL ARCANGEL"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="SALESIANOS SAN MIGUEL ARCANGEL"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="SALESIANOS SAN MIGUEL ARCANGEL"
// SAN AGUSTIN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="SAN AGUSTIN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="SAN AGUSTIN"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="SAN AGUSTIN"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="SAN AGUSTIN"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="SAN AGUSTIN"
// Real Monasterio de Santa Isabel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Real Monasterio de Santa Isabel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Real Monasterio de Santa Isabel"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Real Monasterio de Santa Isabel"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Real Monasterio de Santa Isabel"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Real Monasterio de Santa Isabel"
// Nuestra Señora de los Ángeles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Nuestra Señora de los Ángeles"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Nuestra Señora de los Ángeles"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Nuestra Señora de los Ángeles"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Nuestra Señora de los Ángeles"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Nuestra Señora de los Ángeles"
// Madrid / IES San Juan Bautista
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid / IES San Juan Bautista"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid / IES San Juan Bautista"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid / IES San Juan Bautista"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid / IES San Juan Bautista"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid / IES San Juan Bautista"
// ÍES SAN JUAN BAUTISTA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES SAN JUAN BAUTISTA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES SAN JUAN BAUTISTA"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="ÍES SAN JUAN BAUTISTA"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="ÍES SAN JUAN BAUTISTA"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="ÍES SAN JUAN BAUTISTA"
// IES Crdenal Cisneros
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Crdenal Cisneros"
// Colegio Jesus Maria Garcia Noblejas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Jesus Maria Garcia Noblejas"
// Colegio Santa Maria de la Hispanidad, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Santa Maria de la Hispanidad, Madrid"
// Colegio Valdefuentes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Valdefuentes"
// IES Ciudad de los poetas, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Ciudad de los poetas, Madrid"
// las rosas 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="las rosas "
replace instituto = 1 if LocalidadInstitutoenelcursas=="las rosas "
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="las rosas "
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="las rosas "
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="las rosas "
// COLEGIO LAS ROSAS, MADRID
replace cuidad = 1 if LocalidadInstitutoenelcursas=="COLEGIO LAS ROSAS, MADRID"
replace instituto = 1 if LocalidadInstitutoenelcursas=="COLEGIO LAS ROSAS, MADRID"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="COLEGIO LAS ROSAS, MADRID"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="COLEGIO LAS ROSAS, MADRID"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="COLEGIO LAS ROSAS, MADRID"
// Liceo Francés de Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Liceo Francés de Madrid"
// Liceo Frances de Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Liceo Frances de Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Liceo Frances de Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Liceo Frances de Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Liceo Frances de Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Liceo Frances de Madrid"
// Colegio Lourdes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Lourdes"
// Lourdes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lourdes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Lourdes"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Lourdes"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Lourdes"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Lourdes"
// Madrid, Nuestra Señora del Recuerdo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, Nuestra Señora del Recuerdo"
// Madrid/I.E.S. Ramiro de Maeztu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/I.E.S. Ramiro de Maeztu"
// Madrid / IES Ramiro de Maeztu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid / IES Ramiro de Maeztu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid / IES Ramiro de Maeztu"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid / IES Ramiro de Maeztu"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid / IES Ramiro de Maeztu"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid / IES Ramiro de Maeztu"
// Ramiro de Maeztu, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu, Madrid"
// Ramiro de maeztu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ramiro de maeztu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ramiro de maeztu"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Ramiro de maeztu"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Ramiro de maeztu"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Ramiro de maeztu"
// IES Ramiro de Maeztu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Ramiro de Maeztu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Ramiro de Maeztu"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Ramiro de Maeztu"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Ramiro de Maeztu"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Ramiro de Maeztu"
// Ramiro de Maeztu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu"
// Ramiro de Maeztu 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu "
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu "
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu "
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Ramiro de Maeztu "
// Madrid/San Mateo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
// IES San Mateo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES San Mateo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES San Mateo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES San Mateo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES San Mateo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES San Mateo"
// Madrid/San Mateo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/San Mateo"
// San José del Parque
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San José del Parque"
replace instituto = 1 if LocalidadInstitutoenelcursas=="San José del Parque"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="San José del Parque"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="San José del Parque"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="San José del Parque"
// Madrid, San Saturio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, San Saturio"
// IES Madrid Sur
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Madrid Sur"
// Madrid / Jesús-María
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid / Jesús-María"
// Asunción Vallecas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Asunción Vallecas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Asunción Vallecas"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Asunción Vallecas"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Asunción Vallecas"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Asunción Vallecas"
// Beatriz Galindo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Beatriz Galindo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Beatriz Galindo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Beatriz Galindo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Beatriz Galindo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Beatriz Galindo"
// CEU San Pablo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CEU San Pablo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="CEU San Pablo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="CEU San Pablo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="CEU San Pablo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="CEU San Pablo"
// CEU San Pablo 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CEU San Pablo "
replace instituto = 1 if LocalidadInstitutoenelcursas=="CEU San Pablo "
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="CEU San Pablo "
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="CEU San Pablo "
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="CEU San Pablo "
// Colegio CEU San Pablo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo"
// Colegio CEU San Pablo de Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo de Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo de Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo de Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo de Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio CEU San Pablo de Madrid"
// Colegio Hispano-Alemán (Madrid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Hispano-Alemán (Madrid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Hispano-Alemán (Madrid)"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Hispano-Alemán (Madrid)"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Hispano-Alemán (Madrid)"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Hispano-Alemán (Madrid)"
// Colegio Joyfe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Joyfe"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Joyfe"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Joyfe"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Joyfe"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Joyfe"
// Colegio Sagrado Corazón, Av. Alfonso XXIII
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón, Av. Alfonso XXIII"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón, Av. Alfonso XXIII"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón, Av. Alfonso XXIII"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón, Av. Alfonso XXIII"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón, Av. Alfonso XXIII"
// Vallecas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vallecas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Vallecas"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Vallecas"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Vallecas"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Vallecas"
// Ies Madrid sur, vallecas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Madrid sur, vallecas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Madrid sur, vallecas"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Ies Madrid sur, vallecas"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Ies Madrid sur, vallecas"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Ies Madrid sur, vallecas"
// Madrid San Ramón y San Antonio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid San Ramón y San Antonio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid San Ramón y San Antonio"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid San Ramón y San Antonio"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid San Ramón y San Antonio"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid San Ramón y San Antonio"
// Madrid/IES San Mateo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/IES San Mateo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/IES San Mateo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/IES San Mateo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/IES San Mateo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/IES San Mateo"
// Madrid/Instituto San Isidro
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/Instituto San Isidro"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/Instituto San Isidro"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/Instituto San Isidro"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/Instituto San Isidro"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/Instituto San Isidro"
// Madrid/Nuestra Señora del recuerdo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/Nuestra Señora del recuerdo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/Nuestra Señora del recuerdo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/Nuestra Señora del recuerdo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/Nuestra Señora del recuerdo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/Nuestra Señora del recuerdo"
// Madrid/Santa María de los Apóstoles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/Santa María de los Apóstoles"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/Santa María de los Apóstoles"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/Santa María de los Apóstoles"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/Santa María de los Apóstoles"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/Santa María de los Apóstoles"
// ÍES FORTUNY
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES FORTUNY"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES FORTUNY"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="ÍES FORTUNY"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="ÍES FORTUNY"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="ÍES FORTUNY"
// IES FORTUNY
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES FORTUNY"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES FORTUNY"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES FORTUNY"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES FORTUNY"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES FORTUNY"
// IES Fortuny
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Fortuny"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Fortuny"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Fortuny"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Fortuny"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Fortuny"
// Madrid, IES Fortuny
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, IES Fortuny"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, IES Fortuny"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, IES Fortuny"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, IES Fortuny"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, IES Fortuny"
// Escolapias de Carabanchel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Escolapias de Carabanchel"
// Salesianos de Carabanchel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesianos de Carabanchel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesianos de Carabanchel"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Salesianos de Carabanchel"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Salesianos de Carabanchel"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Salesianos de Carabanchel"
// Salesianos Atocha
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesianos Atocha"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesianos Atocha"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Salesianos Atocha"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Salesianos Atocha"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Salesianos Atocha"
// Salesianos Atocha, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesianos Atocha, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesianos Atocha, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Salesianos Atocha, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Salesianos Atocha, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Salesianos Atocha, Madrid"
// Salesianos Carabanchel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salesianos Carabanchel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salesianos Carabanchel"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Salesianos Carabanchel"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Salesianos Carabanchel"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Salesianos Carabanchel"
// colegio internacional nuevo centro
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro"
// colegio internacional nuevo centro 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro "
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro "
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro "
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro "
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="colegio internacional nuevo centro "
// Santa María de yermo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa María de yermo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa María de yermo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Santa María de yermo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Santa María de yermo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Santa María de yermo"
// IES Cervantes (Madrid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Cervantes (Madrid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Cervantes (Madrid)"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Cervantes (Madrid)"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Cervantes (Madrid)"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Cervantes (Madrid)"
// Instituto Cervantes de Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Instituto Cervantes de Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Instituto Cervantes de Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Instituto Cervantes de Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Instituto Cervantes de Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Instituto Cervantes de Madrid"
// Ies Cervantes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Cervantes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Cervantes"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Ies Cervantes"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Ies Cervantes"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Ies Cervantes"
// Las tablas Valverde
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las tablas Valverde"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Las tablas Valverde"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Las tablas Valverde"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Las tablas Valverde"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Las tablas Valverde"
// Las Tablas Valverde
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Las Tablas Valverde"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Las Tablas Valverde"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Las Tablas Valverde"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Las Tablas Valverde"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Las Tablas Valverde"
// Joyfe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Joyfe"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Joyfe"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Joyfe"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Joyfe"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Joyfe"
// Inmaculada Marillac, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Inmaculada Marillac, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Inmaculada Marillac, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Inmaculada Marillac, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Inmaculada Marillac, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Inmaculada Marillac, Madrid"
// IES La Estrella
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES La Estrella"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES La Estrella"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES La Estrella"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES La Estrella"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES La Estrella"
// Instituto la estrella; Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Instituto la estrella; Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Instituto la estrella; Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Instituto la estrella; Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Instituto la estrella; Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Instituto la estrella; Madrid"
// Colegio huérfanos de la Armada Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio huérfanos de la Armada Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio huérfanos de la Armada Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio huérfanos de la Armada Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio huérfanos de la Armada Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio huérfanos de la Armada Madrid"
// Colegio Santa María Marianistas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Santa María Marianistas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Santa María Marianistas"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Santa María Marianistas"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Santa María Marianistas"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Santa María Marianistas"
// Aluche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Aluche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Aluche"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Aluche"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Aluche"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Aluche"
// CIEM Federico Moreno Torroba, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="CIEM Federico Moreno Torroba, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="CIEM Federico Moreno Torroba, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="CIEM Federico Moreno Torroba, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="CIEM Federico Moreno Torroba, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="CIEM Federico Moreno Torroba, Madrid"
// COLEGIO ESTUDIO
replace cuidad = 1 if LocalidadInstitutoenelcursas=="COLEGIO ESTUDIO"
replace instituto = 1 if LocalidadInstitutoenelcursas=="COLEGIO ESTUDIO"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="COLEGIO ESTUDIO"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="COLEGIO ESTUDIO"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="COLEGIO ESTUDIO"
// colegio Estudio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio Estudio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio Estudio"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="colegio Estudio"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="colegio Estudio"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="colegio Estudio"
// Calasancio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Calasancio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Calasancio"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Calasancio"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Calasancio"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Calasancio"
// Centro de estudios Díaz-Balaguer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Centro de estudios Díaz-Balaguer"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Centro de estudios Díaz-Balaguer"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Centro de estudios Díaz-Balaguer"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Centro de estudios Díaz-Balaguer"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Centro de estudios Díaz-Balaguer"
// Ceu claudio coello Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ceu claudio coello Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ceu claudio coello Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Ceu claudio coello Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Ceu claudio coello Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Ceu claudio coello Madrid"
// Madrid, Colegio CEU Claudio Coello
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, Colegio CEU Claudio Coello"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, Colegio CEU Claudio Coello"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, Colegio CEU Claudio Coello"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, Colegio CEU Claudio Coello"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, Colegio CEU Claudio Coello"
// colegio FEM
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio FEM"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio FEM"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="colegio FEM"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="colegio FEM"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="colegio FEM"
// IES Lope de Vega Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Lope de Vega Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Lope de Vega Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Lope de Vega Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Lope de Vega Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Lope de Vega Madrid"
// IES Lope de Vega, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Lope de Vega, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Lope de Vega, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Lope de Vega, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Lope de Vega, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Lope de Vega, Madrid"
// Madrid Colegio Patrocinio de San jose
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid Colegio Patrocinio de San jose"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid Colegio Patrocinio de San jose"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid Colegio Patrocinio de San jose"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid Colegio Patrocinio de San jose"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid Colegio Patrocinio de San jose"
// Madrid Fomento fundación
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid Fomento fundación"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid Fomento fundación"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid Fomento fundación"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid Fomento fundación"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid Fomento fundación"
// Madrid, Argüelles
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, Argüelles"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, Argüelles"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, Argüelles"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, Argüelles"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, Argüelles"
// Madrid, Colegio Claret
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, Colegio Claret"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, Colegio Claret"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, Colegio Claret"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, Colegio Claret"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, Colegio Claret"
// Madrid, San juan Bautista
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, San juan Bautista"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, San juan Bautista"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, San juan Bautista"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, San juan Bautista"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, San juan Bautista"
// Madrid, colegio Sagrados Corazones
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, colegio Sagrados Corazones"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, colegio Sagrados Corazones"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, colegio Sagrados Corazones"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, colegio Sagrados Corazones"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, colegio Sagrados Corazones"
// Madrid, ies cervantes
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid, ies cervantes"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid, ies cervantes"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid, ies cervantes"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid, ies cervantes"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid, ies cervantes"
// Madrid-Sagrado corazón de Jesus (Esclavas)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid-Sagrado corazón de Jesus (Esclavas)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid-Sagrado corazón de Jesus (Esclavas)"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid-Sagrado corazón de Jesus (Esclavas)"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid-Sagrado corazón de Jesus (Esclavas)"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid-Sagrado corazón de Jesus (Esclavas)"
// Madrid. Central
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid. Central"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid. Central"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid. Central"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid. Central"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid. Central"
// Madrid. Nuestra Señora de la Consolación.
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid. Nuestra Señora de la Consolación."
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid. Nuestra Señora de la Consolación."
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid. Nuestra Señora de la Consolación."
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid. Nuestra Señora de la Consolación."
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid. Nuestra Señora de la Consolación."
// Madrid/ Colegio Menesiano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Menesiano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Menesiano"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Menesiano"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Menesiano"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Menesiano"
// Madrid/ Colegio Paraíso Sagrados Corazones
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Paraíso Sagrados Corazones"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Paraíso Sagrados Corazones"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Paraíso Sagrados Corazones"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Paraíso Sagrados Corazones"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/ Colegio Paraíso Sagrados Corazones"
// Madrid/ IES Arcipreste de Hita
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/ IES Arcipreste de Hita"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/ IES Arcipreste de Hita"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/ IES Arcipreste de Hita"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/ IES Arcipreste de Hita"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/ IES Arcipreste de Hita"
// Madrid/Colegio Claret
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/Colegio Claret"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/Colegio Claret"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/Colegio Claret"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/Colegio Claret"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/Colegio Claret"
// Madrid/Colegio Tajamar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/Colegio Tajamar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/Colegio Tajamar"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/Colegio Tajamar"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/Colegio Tajamar"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/Colegio Tajamar"
// Madrid/Colegios Ramón y Cajal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/Colegios Ramón y Cajal"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/Colegios Ramón y Cajal"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/Colegios Ramón y Cajal"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/Colegios Ramón y Cajal"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/Colegios Ramón y Cajal"
// IES RAMON Y CAJAL MADRID
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES RAMON Y CAJAL MADRID"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES RAMON Y CAJAL MADRID"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES RAMON Y CAJAL MADRID"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES RAMON Y CAJAL MADRID"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES RAMON Y CAJAL MADRID"
// IES SAN CRISTÓBAL DE LOS ÁNGELES
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES SAN CRISTÓBAL DE LOS ÁNGELES"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES SAN CRISTÓBAL DE LOS ÁNGELES"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES SAN CRISTÓBAL DE LOS ÁNGELES"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES SAN CRISTÓBAL DE LOS ÁNGELES"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES SAN CRISTÓBAL DE LOS ÁNGELES"
// Madrid/IES Blas de Otero
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/IES Blas de Otero"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/IES Blas de Otero"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/IES Blas de Otero"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/IES Blas de Otero"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/IES Blas de Otero"
// Madrid/IES Francisco Ayala
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/IES Francisco Ayala"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/IES Francisco Ayala"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/IES Francisco Ayala"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/IES Francisco Ayala"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/IES Francisco Ayala"
// Madrid/Instituto Emperatriz María de Austria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/Instituto Emperatriz María de Austria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/Instituto Emperatriz María de Austria"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/Instituto Emperatriz María de Austria"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/Instituto Emperatriz María de Austria"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/Instituto Emperatriz María de Austria"
// IES EMPERATRIZ MARIA DE AUSTRIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES EMPERATRIZ MARIA DE AUSTRIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES EMPERATRIZ MARIA DE AUSTRIA"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES EMPERATRIZ MARIA DE AUSTRIA"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES EMPERATRIZ MARIA DE AUSTRIA"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES EMPERATRIZ MARIA DE AUSTRIA"
// Madrid/Sagrada Familia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madrid/Sagrada Familia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Madrid/Sagrada Familia"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Madrid/Sagrada Familia"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Madrid/Sagrada Familia"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Madrid/Sagrada Familia"
// LICEO FRANCÉS DE MADRID
replace cuidad = 1 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE MADRID"
replace instituto = 1 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE MADRID"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE MADRID"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE MADRID"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE MADRID"
// Colegio Arturo Soria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria"
// Colegio Arturo Soria 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria "
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria "
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria "
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Arturo Soria "
// Colegio Bristol
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Bristol"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Bristol"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Bristol"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Bristol"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Bristol"
// Colegio Corazón Inmaculado
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Corazón Inmaculado"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Corazón Inmaculado"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Corazón Inmaculado"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Corazón Inmaculado"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Corazón Inmaculado"
// Colegio Estudio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Estudio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Estudio"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Estudio"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Estudio"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Estudio"
// Colegio GSD
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio GSD"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio GSD"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio GSD"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio GSD"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio GSD"
// Colegio Nuestra Señora de Loreto
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de Loreto"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de Loreto"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de Loreto"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de Loreto"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de Loreto"
// Colegio Nuestra Señora de la Providencia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de la Providencia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de la Providencia"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de la Providencia"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de la Providencia"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de la Providencia"
// Colegio Nuestra Señora de las Maravillas, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de las Maravillas, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de las Maravillas, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de las Maravillas, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de las Maravillas, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora de las Maravillas, Madrid"
// Colegio Nuestra Señora del Pilar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora del Pilar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora del Pilar"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora del Pilar"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora del Pilar"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Nuestra Señora del Pilar"
// Colegio Patrocinio San José
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José"
// Colegio San Agustín
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio San Agustín"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio San Agustín"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio San Agustín"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio San Agustín"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio San Agustín"
// Colegio Santa Maria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Santa Maria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Santa Maria"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Santa Maria"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Santa Maria"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Santa Maria"
// Colegio Santa María del Carmen
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Santa María del Carmen"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Santa María del Carmen"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Santa María del Carmen"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Santa María del Carmen"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Santa María del Carmen"
// Colegio Santa María del Pilar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Santa María del Pilar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Santa María del Pilar"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Santa María del Pilar"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Santa María del Pilar"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Santa María del Pilar"
// Colegio Torrevilano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Torrevilano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Torrevilano"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio Torrevilano"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio Torrevilano"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio Torrevilano"
// Colegio de Fomento el Prqdo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio de Fomento el Prqdo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio de Fomento el Prqdo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio de Fomento el Prqdo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio de Fomento el Prqdo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio de Fomento el Prqdo"
// Colegio de fomento El Prado
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio de fomento El Prado"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio de fomento El Prado"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Colegio de fomento El Prado"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Colegio de fomento El Prado"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Colegio de fomento El Prado"
// Corazonistas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Corazonistas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Corazonistas"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Corazonistas"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Corazonistas"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Corazonistas"
// Corazón de maría (madrid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Corazón de maría (madrid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Corazón de maría (madrid)"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Corazón de maría (madrid)"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Corazón de maría (madrid)"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Corazón de maría (madrid)"
// El prado
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El prado"
replace instituto = 1 if LocalidadInstitutoenelcursas=="El prado"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="El prado"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="El prado"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="El prado"
// Felipe II
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Felipe II"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Felipe II"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Felipe II"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Felipe II"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Felipe II"
// Fomento Fundación
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Fomento Fundación"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Fomento Fundación"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Fomento Fundación"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Fomento Fundación"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Fomento Fundación"
// Fomento Fundación, Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Fomento Fundación, Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Fomento Fundación, Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Fomento Fundación, Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Fomento Fundación, Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Fomento Fundación, Madrid"
// Gamo Diana
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gamo Diana"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Gamo Diana"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Gamo Diana"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Gamo Diana"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Gamo Diana"
// Gamo Diana en Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gamo Diana en Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Gamo Diana en Madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Gamo Diana en Madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Gamo Diana en Madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Gamo Diana en Madrid"
// Gran Capitán, Madrid.
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gran Capitán, Madrid."
replace instituto = 1 if LocalidadInstitutoenelcursas=="Gran Capitán, Madrid."
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="Gran Capitán, Madrid."
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="Gran Capitán, Madrid."
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="Gran Capitán, Madrid."
// IES Gran Capitán
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Gran Capitán"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Gran Capitán"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Gran Capitán"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Gran Capitán"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Gran Capitán"
// IES Avenida de los Toreros
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Avenida de los Toreros"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Avenida de los Toreros"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Avenida de los Toreros"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Avenida de los Toreros"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Avenida de los Toreros"
// IES Cervantes en Embajadores
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Cervantes en Embajadores"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Cervantes en Embajadores"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Cervantes en Embajadores"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Cervantes en Embajadores"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Cervantes en Embajadores"
// IES Ciudad de Jaén
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Ciudad de Jaén"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Ciudad de Jaén"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Ciudad de Jaén"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Ciudad de Jaén"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Ciudad de Jaén"
// IES Conde de Orgaz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Conde de Orgaz"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Conde de Orgaz"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Conde de Orgaz"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Conde de Orgaz"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Conde de Orgaz"
// IES El Espinillo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES El Espinillo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES El Espinillo"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES El Espinillo"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES El Espinillo"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES El Espinillo"
// IES Juana de Castilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Juana de Castilla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Juana de Castilla"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES Juana de Castilla"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES Juana de Castilla"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES Juana de Castilla"
// IES La Serna
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES La Serna"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES La Serna"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES La Serna"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES La Serna"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES La Serna"
// IES la serna
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES la serna"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES la serna"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES la serna"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES la serna"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES la serna"
// IES TIRSO DE MOLINA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES TIRSO DE MOLINA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES TIRSO DE MOLINA"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES TIRSO DE MOLINA"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES TIRSO DE MOLINA"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES TIRSO DE MOLINA"
// IES santa Teresa de Jesús madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES santa Teresa de Jesús madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES santa Teresa de Jesús madrid"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IES santa Teresa de Jesús madrid"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IES santa Teresa de Jesús madrid"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IES santa Teresa de Jesús madrid"
// IPS Salesianos Carabanchel
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IPS Salesianos Carabanchel"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IPS Salesianos Carabanchel"
replace SocioeconStatPers = 15930 if LocalidadInstitutoenelcursas=="IPS Salesianos Carabanchel"
replace SocioeconStatHog = 40195 if LocalidadInstitutoenelcursas=="IPS Salesianos Carabanchel"
replace poblacion = 3266126 if LocalidadInstitutoenelcursas=="IPS Salesianos Carabanchel"
// Villalbilla
// Villalbilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villalbilla"
replace SocioeconStatPers = 14197 if LocalidadInstitutoenelcursas=="Villalbilla"
replace SocioeconStatHog = 42091 if LocalidadInstitutoenelcursas=="Villalbilla"
replace poblacion = 13878 if LocalidadInstitutoenelcursas=="Villalbilla"
// Villanueva de la cañada
// villanueva de la cañada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="villanueva de la cañada"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="villanueva de la cañada"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="villanueva de la cañada"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="villanueva de la cañada"
// Villanueva de la Cañada/Liceo Francés Molière
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/Liceo Francés Molière"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/Liceo Francés Molière"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/Liceo Francés Molière"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/Liceo Francés Molière"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/Liceo Francés Molière"
// Villanueva de la Cañada
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada"
// Villanueva de la Cañada ( Madri)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada ( Madri)"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada ( Madri)"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada ( Madri)"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada ( Madri)"
// Villanueva de la Cañada, Villafranca del Castillo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada, Villafranca del Castillo"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada, Villafranca del Castillo"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada, Villafranca del Castillo"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada, Villafranca del Castillo"
// SEK El Castillo Madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="SEK El Castillo Madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="SEK El Castillo Madrid"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="SEK El Castillo Madrid"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="SEK El Castillo Madrid"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="SEK El Castillo Madrid"
// Sek el Castillo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sek el Castillo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sek el Castillo"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="Sek el Castillo"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="Sek el Castillo"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="Sek el Castillo"
// Colegio Internacional Kolbe
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Internacional Kolbe"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Internacional Kolbe"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="Colegio Internacional Kolbe"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="Colegio Internacional Kolbe"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="Colegio Internacional Kolbe"
// Villanueva de la Cañada/ Centro Educativo Zola
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ Centro Educativo Zola"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ Centro Educativo Zola"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ Centro Educativo Zola"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ Centro Educativo Zola"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ Centro Educativo Zola"
// Zola ,Villafranca del Castillo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Zola ,Villafranca del Castillo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Zola ,Villafranca del Castillo"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="Zola ,Villafranca del Castillo"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="Zola ,Villafranca del Castillo"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="Zola ,Villafranca del Castillo"
// Villanueva de la Cañada/ liceo francés moliere
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ liceo francés moliere"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ liceo francés moliere"
replace SocioeconStatPers = 16141 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ liceo francés moliere"
replace SocioeconStatHog = 55118 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ liceo francés moliere"
replace poblacion = 21445 if LocalidadInstitutoenelcursas=="Villanueva de la Cañada/ liceo francés moliere"
// Villaviciosa de Odón
// Villaviciosa de Odón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villaviciosa de Odón"
replace SocioeconStatPers = 17294 if LocalidadInstitutoenelcursas=="Villaviciosa de Odón"
replace SocioeconStatHog = 53347 if LocalidadInstitutoenelcursas=="Villaviciosa de Odón"
replace poblacion = 27835 if LocalidadInstitutoenelcursas=="Villaviciosa de Odón"
// Villaviciosa de Odón, Colegio Alcalá
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Villaviciosa de Odón, Colegio Alcalá"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Villaviciosa de Odón, Colegio Alcalá"
replace SocioeconStatPers = 17294 if LocalidadInstitutoenelcursas=="Villaviciosa de Odón, Colegio Alcalá"
replace SocioeconStatHog = 53347 if LocalidadInstitutoenelcursas=="Villaviciosa de Odón, Colegio Alcalá"
replace poblacion = 27835 if LocalidadInstitutoenelcursas=="Villaviciosa de Odón, Colegio Alcalá"
// Calatalifa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Calatalifa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Calatalifa"
replace SocioeconStatPers = 17294 if LocalidadInstitutoenelcursas=="Calatalifa"
replace SocioeconStatHog = 53347 if LocalidadInstitutoenelcursas=="Calatalifa"
replace poblacion = 27835 if LocalidadInstitutoenelcursas=="Calatalifa"
// Eurocolegio Casvi de Villaviciosa de Odón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi de Villaviciosa de Odón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi de Villaviciosa de Odón"
replace SocioeconStatPers = 17294 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi de Villaviciosa de Odón"
replace SocioeconStatHog = 53347 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi de Villaviciosa de Odón"
replace poblacion = 27835 if LocalidadInstitutoenelcursas=="Eurocolegio Casvi de Villaviciosa de Odón"
// Eurocolegio casvi, Villaviciosa de Odon
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Eurocolegio casvi, Villaviciosa de Odon"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Eurocolegio casvi, Villaviciosa de Odon"
replace SocioeconStatPers = 17294 if LocalidadInstitutoenelcursas=="Eurocolegio casvi, Villaviciosa de Odon"
replace SocioeconStatHog = 53347 if LocalidadInstitutoenelcursas=="Eurocolegio casvi, Villaviciosa de Odon"
replace poblacion = 27835 if LocalidadInstitutoenelcursas=="Eurocolegio casvi, Villaviciosa de Odon"
// casvi villaviciosa de odón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón"
replace SocioeconStatPers = 17294 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón"
replace SocioeconStatHog = 53347 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón"
replace poblacion = 27835 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón"
// casvi villaviciosa de odón 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón "
replace instituto = 1 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón "
replace SocioeconStatPers = 17294 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón "
replace SocioeconStatHog = 53347 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón "
replace poblacion = 27835 if LocalidadInstitutoenelcursas=="casvi villaviciosa de odón "
// IES Calatalifa, Villaviciosa de Odón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Calatalifa, Villaviciosa de Odón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Calatalifa, Villaviciosa de Odón"
replace SocioeconStatPers = 17294 if LocalidadInstitutoenelcursas=="IES Calatalifa, Villaviciosa de Odón"
replace SocioeconStatHog = 53347 if LocalidadInstitutoenelcursas=="IES Calatalifa, Villaviciosa de Odón"
replace poblacion = 27835 if LocalidadInstitutoenelcursas=="IES Calatalifa, Villaviciosa de Odón"
// Ágora international school madrid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ágora international school madrid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ágora international school madrid"
replace SocioeconStatPers = 17294 if LocalidadInstitutoenelcursas=="Ágora international school madrid"
replace SocioeconStatHog = 53347 if LocalidadInstitutoenelcursas=="Ágora international school madrid"
replace poblacion = 27835 if LocalidadInstitutoenelcursas=="Ágora international school madrid"
********************************************************************************
*    Málaga
********************************************************************************
// Benalmádena
// Benalmádena (Málaga)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Benalmádena (Málaga)"
replace SocioeconStatPers = 9830 if LocalidadInstitutoenelcursas=="Benalmádena (Málaga)"
replace SocioeconStatHog = 24329 if LocalidadInstitutoenelcursas=="Benalmádena (Málaga)"
replace poblacion = 68128 if LocalidadInstitutoenelcursas=="Benalmádena (Málaga)"
// Estepona
// Estepona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Estepona"
replace SocioeconStatPers = 8808 if LocalidadInstitutoenelcursas=="Estepona"
replace SocioeconStatHog = 23760 if LocalidadInstitutoenelcursas=="Estepona"
replace poblacion = 68286 if LocalidadInstitutoenelcursas=="Estepona"
// Estepona, Málaga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Estepona, Málaga"
replace SocioeconStatPers = 8808 if LocalidadInstitutoenelcursas=="Estepona, Málaga"
replace SocioeconStatHog = 23760 if LocalidadInstitutoenelcursas=="Estepona, Málaga"
replace poblacion = 68286 if LocalidadInstitutoenelcursas=="Estepona, Málaga"
// Estepona/ Colegio San José
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José"
replace SocioeconStatPers = 8808 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José"
replace SocioeconStatHog = 23760 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José"
replace poblacion = 68286 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José"
// Estepona/ Colegio San José 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José "
replace SocioeconStatPers = 8808 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José "
replace SocioeconStatHog = 23760 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José "
replace poblacion = 68286 if LocalidadInstitutoenelcursas=="Estepona/ Colegio San José "
// Colegio Patrocinio San José (Estepona)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José (Estepona)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José (Estepona)"
replace SocioeconStatPers = 8808 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José (Estepona)"
replace SocioeconStatHog = 23760 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José (Estepona)"
replace poblacion = 68286 if LocalidadInstitutoenelcursas=="Colegio Patrocinio San José (Estepona)"
// IES Monterroso, Estepona, Málaga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Monterroso, Estepona, Málaga"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Monterroso, Estepona, Málaga"
replace SocioeconStatPers = 8808 if LocalidadInstitutoenelcursas=="IES Monterroso, Estepona, Málaga"
replace SocioeconStatHog = 23760 if LocalidadInstitutoenelcursas=="IES Monterroso, Estepona, Málaga"
replace poblacion = 68286 if LocalidadInstitutoenelcursas=="IES Monterroso, Estepona, Málaga"
// Fuengirola
// Salliver
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salliver"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salliver"
replace SocioeconStatPers = 8754 if LocalidadInstitutoenelcursas=="Salliver"
replace SocioeconStatHog = 22523 if LocalidadInstitutoenelcursas=="Salliver"
replace poblacion = 80309 if LocalidadInstitutoenelcursas=="Salliver"
// Marbella
// Marbella
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Marbella"
replace SocioeconStatPers = 9324 if LocalidadInstitutoenelcursas=="Marbella"
replace SocioeconStatHog = 26120 if LocalidadInstitutoenelcursas=="Marbella"
replace poblacion = 143386 if LocalidadInstitutoenelcursas=="Marbella"
// Marbella Colegio Atalaya
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Marbella Colegio Atalaya"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Marbella Colegio Atalaya"
replace SocioeconStatPers = 9324 if LocalidadInstitutoenelcursas=="Marbella Colegio Atalaya"
replace SocioeconStatHog = 26120 if LocalidadInstitutoenelcursas=="Marbella Colegio Atalaya"
replace poblacion = 143386 if LocalidadInstitutoenelcursas=="Marbella Colegio Atalaya"
// IES VEGA DE MAR
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES VEGA DE MAR"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES VEGA DE MAR"
replace SocioeconStatPers = 9324 if LocalidadInstitutoenelcursas=="IES VEGA DE MAR"
replace SocioeconStatHog = 26120 if LocalidadInstitutoenelcursas=="IES VEGA DE MAR"
replace poblacion = 143386 if LocalidadInstitutoenelcursas=="IES VEGA DE MAR"
// Málaga
// Málaga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Málaga"
replace SocioeconStatPers = 10119 if LocalidadInstitutoenelcursas=="Málaga"
replace SocioeconStatHog = 27525 if LocalidadInstitutoenelcursas=="Málaga"
replace poblacion = 574654 if LocalidadInstitutoenelcursas=="Málaga"
// Málaga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Málaga"
replace SocioeconStatPers = 10119 if LocalidadInstitutoenelcursas=="Málaga"
replace SocioeconStatHog = 27525 if LocalidadInstitutoenelcursas=="Málaga"
replace poblacion = 574654 if LocalidadInstitutoenelcursas=="Málaga"
// Esclavas Sagrado Corazon Bachillerato Málaga
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Esclavas Sagrado Corazon Bachillerato Málaga"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Esclavas Sagrado Corazon Bachillerato Málaga"
replace SocioeconStatPers = 10119 if LocalidadInstitutoenelcursas=="Esclavas Sagrado Corazon Bachillerato Málaga"
replace SocioeconStatHog = 27525 if LocalidadInstitutoenelcursas=="Esclavas Sagrado Corazon Bachillerato Málaga"
replace poblacion = 574654 if LocalidadInstitutoenelcursas=="Esclavas Sagrado Corazon Bachillerato Málaga"
// Mijas
// Mijas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Mijas"
replace SocioeconStatPers = 7937 if LocalidadInstitutoenelcursas=="Mijas"
replace SocioeconStatHog = 21578 if LocalidadInstitutoenelcursas=="Mijas"
replace poblacion = 82742 if LocalidadInstitutoenelcursas=="Mijas"
// Nerja
// Nerja
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Nerja"
replace SocioeconStatPers = 8388 if LocalidadInstitutoenelcursas=="Nerja"
replace SocioeconStatHog = 21966 if LocalidadInstitutoenelcursas=="Nerja"
replace poblacion = 21091 if LocalidadInstitutoenelcursas=="Nerja"
// Nerja 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Nerja "
replace SocioeconStatPers = 8388 if LocalidadInstitutoenelcursas=="Nerja "
replace SocioeconStatHog = 21966 if LocalidadInstitutoenelcursas=="Nerja "
replace poblacion = 21091 if LocalidadInstitutoenelcursas=="Nerja "
// Nerja (Málaga)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Nerja (Málaga)"
replace SocioeconStatPers = 8388 if LocalidadInstitutoenelcursas=="Nerja (Málaga)"
replace SocioeconStatHog = 21966 if LocalidadInstitutoenelcursas=="Nerja (Málaga)"
replace poblacion = 21091 if LocalidadInstitutoenelcursas=="Nerja (Málaga)"
// IES El Chaparil ( Nerja, Málaga)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES El Chaparil ( Nerja, Málaga)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES El Chaparil ( Nerja, Málaga)"
replace SocioeconStatPers = 8388 if LocalidadInstitutoenelcursas=="IES El Chaparil ( Nerja, Málaga)"
replace SocioeconStatHog = 21966 if LocalidadInstitutoenelcursas=="IES El Chaparil ( Nerja, Málaga)"
replace poblacion = 21091 if LocalidadInstitutoenelcursas=="IES El Chaparil ( Nerja, Málaga)"
// Rincón de la Victoria
// Novaschool Añoreta
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Novaschool Añoreta"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Novaschool Añoreta"
replace SocioeconStatPers = 11104 if LocalidadInstitutoenelcursas=="Novaschool Añoreta"
replace SocioeconStatHog = 30007 if LocalidadInstitutoenelcursas=="Novaschool Añoreta"
replace poblacion = 47179 if LocalidadInstitutoenelcursas=="Novaschool Añoreta"
// Ronda
// Ronda
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ronda"
replace SocioeconStatPers = 9723 if LocalidadInstitutoenelcursas=="Ronda"
replace SocioeconStatHog = 25258 if LocalidadInstitutoenelcursas=="Ronda"
replace poblacion = 33877 if LocalidadInstitutoenelcursas=="Ronda"
********************************************************************************
*    Melilla
********************************************************************************
********************************************************************************
*    Murcia
********************************************************************************
// Abanilla
// Abanilla/Murcia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Abanilla/Murcia"
replace SocioeconStatPers = 9044 if LocalidadInstitutoenelcursas=="Abanilla/Murcia"
replace SocioeconStatHog = 22820 if LocalidadInstitutoenelcursas=="Abanilla/Murcia"
replace poblacion = 6127 if LocalidadInstitutoenelcursas=="Abanilla/Murcia"
// Cartagena
// Cartagena/IES El Bohio
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cartagena/IES El Bohio"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cartagena/IES El Bohio"
replace SocioeconStatPers = 10051 if LocalidadInstitutoenelcursas=="Cartagena/IES El Bohio"
replace SocioeconStatHog = 28835 if LocalidadInstitutoenelcursas=="Cartagena/IES El Bohio"
replace poblacion = 214802 if LocalidadInstitutoenelcursas=="Cartagena/IES El Bohio"
// Cehegín
// IES Alquipir
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Alquipir"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Alquipir"
replace SocioeconStatPers = 7884 if LocalidadInstitutoenelcursas=="IES Alquipir"
replace SocioeconStatHog = 21654 if LocalidadInstitutoenelcursas=="IES Alquipir"
replace poblacion = 14983 if LocalidadInstitutoenelcursas=="IES Alquipir"
// Vega del Argos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vega del Argos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Vega del Argos"
replace SocioeconStatPers = 7884 if LocalidadInstitutoenelcursas=="Vega del Argos"
replace SocioeconStatHog = 21654 if LocalidadInstitutoenelcursas=="Vega del Argos"
replace poblacion = 14983 if LocalidadInstitutoenelcursas=="Vega del Argos"
// Cieza
// Cieza
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cieza"
replace SocioeconStatPers = 8695 if LocalidadInstitutoenelcursas=="Cieza"
replace SocioeconStatHog = 25350 if LocalidadInstitutoenelcursas=="Cieza"
replace poblacion = 34988 if LocalidadInstitutoenelcursas=="Cieza"
// Cieza/IES Los Albares
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cieza/IES Los Albares"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cieza/IES Los Albares"
replace SocioeconStatPers = 8695 if LocalidadInstitutoenelcursas=="Cieza/IES Los Albares"
replace SocioeconStatHog = 25350 if LocalidadInstitutoenelcursas=="Cieza/IES Los Albares"
replace poblacion = 34988 if LocalidadInstitutoenelcursas=="Cieza/IES Los Albares"
// Ies Diego Tortosa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Diego Tortosa"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Diego Tortosa"
replace SocioeconStatPers = 8695 if LocalidadInstitutoenelcursas=="Ies Diego Tortosa"
replace SocioeconStatHog = 25350 if LocalidadInstitutoenelcursas=="Ies Diego Tortosa"
replace poblacion = 34988 if LocalidadInstitutoenelcursas=="Ies Diego Tortosa"
// Jumilla
// Jumilla/IES infanta Elena
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jumilla/IES infanta Elena"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jumilla/IES infanta Elena"
replace SocioeconStatPers = 8113 if LocalidadInstitutoenelcursas=="Jumilla/IES infanta Elena"
replace SocioeconStatHog = 23512 if LocalidadInstitutoenelcursas=="Jumilla/IES infanta Elena"
replace poblacion = 25600 if LocalidadInstitutoenelcursas=="Jumilla/IES infanta Elena"
// Lorca
// Lorca/Murcia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lorca/Murcia"
replace SocioeconStatPers = 8794 if LocalidadInstitutoenelcursas=="Lorca/Murcia"
replace SocioeconStatHog = 27149 if LocalidadInstitutoenelcursas=="Lorca/Murcia"
replace poblacion = 94404 if LocalidadInstitutoenelcursas=="Lorca/Murcia"
// IES JOSÉ IBAÑEZ MARTÍN
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES JOSÉ IBAÑEZ MARTÍN"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES JOSÉ IBAÑEZ MARTÍN"
replace SocioeconStatPers = 8794 if LocalidadInstitutoenelcursas=="IES JOSÉ IBAÑEZ MARTÍN"
replace SocioeconStatHog = 27149 if LocalidadInstitutoenelcursas=="IES JOSÉ IBAÑEZ MARTÍN"
replace poblacion = 94404 if LocalidadInstitutoenelcursas=="IES JOSÉ IBAÑEZ MARTÍN"
// Molina de Segura
// Molina de Segura
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Molina de Segura"
replace SocioeconStatPers = 10417 if LocalidadInstitutoenelcursas=="Molina de Segura"
replace SocioeconStatHog = 30226 if LocalidadInstitutoenelcursas=="Molina de Segura"
replace poblacion = 71890 if LocalidadInstitutoenelcursas=="Molina de Segura"
// Murcia 
// Murcia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Murcia"
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Murcia"
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Murcia"
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Murcia"
// Murcia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Murcia"
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Murcia"
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Murcia"
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Murcia"
// Murcia/ ies alquibla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Murcia/ ies alquibla"
// Colegio Nelva
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Nelva"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Nelva"
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Colegio Nelva"
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Colegio Nelva"
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Colegio Nelva"
// Colegio Nelva 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Nelva "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Nelva "
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Colegio Nelva "
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Colegio Nelva "
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Colegio Nelva "
// Licenciado Francisco Cascales/ Murcia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia"
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia"
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia"
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia"
// Licenciado Francisco Cascales/ Murcia 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia "
replace SocioeconStatPers = 10703 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia "
replace SocioeconStatHog = 30582 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia "
replace poblacion = 453258 if LocalidadInstitutoenelcursas=="Licenciado Francisco Cascales/ Murcia "
// San Javier
// ies mar menor
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies mar menor"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies mar menor"
replace SocioeconStatPers = 9560 if LocalidadInstitutoenelcursas=="ies mar menor"
replace SocioeconStatHog = 26969 if LocalidadInstitutoenelcursas=="ies mar menor"
replace poblacion = 32489 if LocalidadInstitutoenelcursas=="ies mar menor"
// Yecla
// Yecla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Yecla"
replace SocioeconStatPers = 8407 if LocalidadInstitutoenelcursas=="Yecla"
replace SocioeconStatHog = 23608 if LocalidadInstitutoenelcursas=="Yecla"
replace poblacion = 34432 if LocalidadInstitutoenelcursas=="Yecla"
// yecla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="yecla"
replace SocioeconStatPers = 8407 if LocalidadInstitutoenelcursas=="yecla"
replace SocioeconStatHog = 23608 if LocalidadInstitutoenelcursas=="yecla"
replace poblacion = 34432 if LocalidadInstitutoenelcursas=="yecla"
// Yecla(Murcia)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Yecla(Murcia)"
replace SocioeconStatPers = 8407 if LocalidadInstitutoenelcursas=="Yecla(Murcia)"
replace SocioeconStatHog = 23608 if LocalidadInstitutoenelcursas=="Yecla(Murcia)"
replace poblacion = 34432 if LocalidadInstitutoenelcursas=="Yecla(Murcia)"
// YECLA/IES JOSE LUIS CASTILLO PUCHE
replace cuidad = 1 if LocalidadInstitutoenelcursas=="YECLA/IES JOSE LUIS CASTILLO PUCHE"
replace instituto = 1 if LocalidadInstitutoenelcursas=="YECLA/IES JOSE LUIS CASTILLO PUCHE"
replace SocioeconStatPers = 8407 if LocalidadInstitutoenelcursas=="YECLA/IES JOSE LUIS CASTILLO PUCHE"
replace SocioeconStatHog = 23608 if LocalidadInstitutoenelcursas=="YECLA/IES JOSE LUIS CASTILLO PUCHE"
replace poblacion = 34432 if LocalidadInstitutoenelcursas=="YECLA/IES JOSE LUIS CASTILLO PUCHE"
// Yecla/IES Jose Luis Castillo Puche
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Yecla/IES Jose Luis Castillo Puche"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Yecla/IES Jose Luis Castillo Puche"
replace SocioeconStatPers = 8407 if LocalidadInstitutoenelcursas=="Yecla/IES Jose Luis Castillo Puche"
replace SocioeconStatHog = 23608 if LocalidadInstitutoenelcursas=="Yecla/IES Jose Luis Castillo Puche"
replace poblacion = 34432 if LocalidadInstitutoenelcursas=="Yecla/IES Jose Luis Castillo Puche"
// Yecla/ I.E.S. Azorin
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Yecla/ I.E.S. Azorin"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Yecla/ I.E.S. Azorin"
replace SocioeconStatPers = 8407 if LocalidadInstitutoenelcursas=="Yecla/ I.E.S. Azorin"
replace SocioeconStatHog = 23608 if LocalidadInstitutoenelcursas=="Yecla/ I.E.S. Azorin"
replace poblacion = 34432 if LocalidadInstitutoenelcursas=="Yecla/ I.E.S. Azorin"
// IES J. MARTÍNEZ RUIZ (AZORÍN)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES J. MARTÍNEZ RUIZ (AZORÍN)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES J. MARTÍNEZ RUIZ (AZORÍN)"
replace SocioeconStatPers = 8407 if LocalidadInstitutoenelcursas=="IES J. MARTÍNEZ RUIZ (AZORÍN)"
replace SocioeconStatHog = 23608 if LocalidadInstitutoenelcursas=="IES J. MARTÍNEZ RUIZ (AZORÍN)"
replace poblacion = 34432 if LocalidadInstitutoenelcursas=="IES J. MARTÍNEZ RUIZ (AZORÍN)"
//  IES J. MARTÍNEZ RUIZ (AZORÍN)
replace cuidad = 1 if LocalidadInstitutoenelcursas==" IES J. MARTÍNEZ RUIZ (AZORÍN)"
replace instituto = 1 if LocalidadInstitutoenelcursas==" IES J. MARTÍNEZ RUIZ (AZORÍN)"
replace SocioeconStatPers = 8407 if LocalidadInstitutoenelcursas==" IES J. MARTÍNEZ RUIZ (AZORÍN)"
replace SocioeconStatHog = 23608 if LocalidadInstitutoenelcursas==" IES J. MARTÍNEZ RUIZ (AZORÍN)"
replace poblacion = 34432 if LocalidadInstitutoenelcursas==" IES J. MARTÍNEZ RUIZ (AZORÍN)"
// IES Martínez Ruiz Azorin
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Martínez Ruiz Azorin"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Martínez Ruiz Azorin"
replace SocioeconStatPers = 8407 if LocalidadInstitutoenelcursas=="IES Martínez Ruiz Azorin"
replace SocioeconStatHog = 23608 if LocalidadInstitutoenelcursas=="IES Martínez Ruiz Azorin"
replace poblacion = 34432 if LocalidadInstitutoenelcursas=="IES Martínez Ruiz Azorin"
********************************************************************************
*    Navarra
********************************************************************************
// Pamplona/Iruña
// Pamplona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pamplona"
replace SocioeconStatPers = 13216 if LocalidadInstitutoenelcursas=="Pamplona"
replace SocioeconStatHog = 33882 if LocalidadInstitutoenelcursas=="Pamplona"
replace poblacion = 210653 if LocalidadInstitutoenelcursas=="Pamplona"
// Tafalla
// Tafalla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tafalla"
replace SocioeconStatPers = 12451 if LocalidadInstitutoenelcursas=="Tafalla"
replace SocioeconStatHog = 32039 if LocalidadInstitutoenelcursas=="Tafalla"
replace poblacion = 10595 if LocalidadInstitutoenelcursas=="Tafalla"
// Tudela
// TUDELA/ IES BENJAMIN DE TUDELA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="TUDELA/ IES BENJAMIN DE TUDELA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="TUDELA/ IES BENJAMIN DE TUDELA"
replace SocioeconStatPers = 11488 if LocalidadInstitutoenelcursas=="TUDELA/ IES BENJAMIN DE TUDELA"
replace SocioeconStatHog = 29866 if LocalidadInstitutoenelcursas=="TUDELA/ IES BENJAMIN DE TUDELA"
replace poblacion = 36258 if LocalidadInstitutoenelcursas=="TUDELA/ IES BENJAMIN DE TUDELA"
********************************************************************************
*    Ourense
********************************************************************************
********************************************************************************
*    Palencia
********************************************************************************
// Palencia
// La Salle Palencia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Palencia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Palencia"
replace SocioeconStatPers = 12650 if LocalidadInstitutoenelcursas=="La Salle Palencia"
replace SocioeconStatHog = 29581 if LocalidadInstitutoenelcursas=="La Salle Palencia"
replace poblacion = 78412 if LocalidadInstitutoenelcursas=="La Salle Palencia"
// Palencia/IES Alonso Berruguete
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Palencia/IES Alonso Berruguete"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Palencia/IES Alonso Berruguete"
replace SocioeconStatPers = 12650 if LocalidadInstitutoenelcursas=="Palencia/IES Alonso Berruguete"
replace SocioeconStatHog = 29581 if LocalidadInstitutoenelcursas=="Palencia/IES Alonso Berruguete"
replace poblacion = 78412 if LocalidadInstitutoenelcursas=="Palencia/IES Alonso Berruguete"
********************************************************************************
*    Pontevedra
********************************************************************************
// Lalín
// Lalín
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Lalín"
replace SocioeconStatPers = 9344 if LocalidadInstitutoenelcursas=="Lalín"
replace SocioeconStatHog = 24476 if LocalidadInstitutoenelcursas=="Lalín"
replace poblacion = 20218 if LocalidadInstitutoenelcursas=="Lalín"
// Pontevedra
// Pontevedra
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pontevedra"
replace SocioeconStatPers = 11831 if LocalidadInstitutoenelcursas=="Pontevedra"
replace SocioeconStatHog = 29618 if LocalidadInstitutoenelcursas=="Pontevedra"
replace poblacion = 83029 if LocalidadInstitutoenelcursas=="Pontevedra"
// Pontevedra, Sek Atlántico
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pontevedra, Sek Atlántico"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Pontevedra, Sek Atlántico"
replace SocioeconStatPers = 11831 if LocalidadInstitutoenelcursas=="Pontevedra, Sek Atlántico"
replace SocioeconStatHog = 29618 if LocalidadInstitutoenelcursas=="Pontevedra, Sek Atlántico"
replace poblacion = 83029 if LocalidadInstitutoenelcursas=="Pontevedra, Sek Atlántico"
// Pontevedra/Ies Torrente Ballester
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Pontevedra/Ies Torrente Ballester"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Pontevedra/Ies Torrente Ballester"
replace SocioeconStatPers = 11831 if LocalidadInstitutoenelcursas=="Pontevedra/Ies Torrente Ballester"
replace SocioeconStatHog = 29618 if LocalidadInstitutoenelcursas=="Pontevedra/Ies Torrente Ballester"
replace poblacion = 83029 if LocalidadInstitutoenelcursas=="Pontevedra/Ies Torrente Ballester"
// Rosal, O / Rosal, El
// O Rosal
replace cuidad = 1 if LocalidadInstitutoenelcursas=="O Rosal"
replace SocioeconStatPers = 9191 if LocalidadInstitutoenelcursas=="O Rosal"
replace SocioeconStatHog = 24051 if LocalidadInstitutoenelcursas=="O Rosal"
replace poblacion = 6234 if LocalidadInstitutoenelcursas=="O Rosal"
// Tomiño
// Tomiño, IES Antón Alonso Ríos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tomiño, IES Antón Alonso Ríos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Tomiño, IES Antón Alonso Ríos"
replace SocioeconStatPers = 9469 if LocalidadInstitutoenelcursas=="Tomiño, IES Antón Alonso Ríos"
replace SocioeconStatHog = 22162 if LocalidadInstitutoenelcursas=="Tomiño, IES Antón Alonso Ríos"
replace poblacion = 13499 if LocalidadInstitutoenelcursas=="Tomiño, IES Antón Alonso Ríos"
// Tui / Tuy
// IES San Paio, Tui
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES San Paio, Tui"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES San Paio, Tui"
replace SocioeconStatPers = 9457 if LocalidadInstitutoenelcursas=="IES San Paio, Tui"
replace SocioeconStatHog = 25263 if LocalidadInstitutoenelcursas=="IES San Paio, Tui"
replace poblacion = 16701 if LocalidadInstitutoenelcursas=="IES San Paio, Tui"
// Vigo
// Vigo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vigo"
replace SocioeconStatPers = 12083 if LocalidadInstitutoenelcursas=="Vigo"
replace SocioeconStatHog = 31827 if LocalidadInstitutoenelcursas=="Vigo"
replace poblacion = 295364 if LocalidadInstitutoenelcursas=="Vigo"
// Vigo 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vigo "
replace SocioeconStatPers = 12083 if LocalidadInstitutoenelcursas=="Vigo "
replace SocioeconStatHog = 31827 if LocalidadInstitutoenelcursas=="Vigo "
replace poblacion = 295364 if LocalidadInstitutoenelcursas=="Vigo "
// Vigo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vigo"
replace SocioeconStatPers = 12083 if LocalidadInstitutoenelcursas=="Vigo"
replace SocioeconStatHog = 31827 if LocalidadInstitutoenelcursas=="Vigo"
replace poblacion = 295364 if LocalidadInstitutoenelcursas=="Vigo"
// Vigo, Pontevedra
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Vigo, Pontevedra"
replace SocioeconStatPers = 12083 if LocalidadInstitutoenelcursas=="Vigo, Pontevedra"
replace SocioeconStatHog = 31827 if LocalidadInstitutoenelcursas=="Vigo, Pontevedra"
replace poblacion = 295364 if LocalidadInstitutoenelcursas=="Vigo, Pontevedra"
// Colegio Rosalía de Castro, Vigo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Rosalía de Castro, Vigo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Rosalía de Castro, Vigo"
replace SocioeconStatPers = 12083 if LocalidadInstitutoenelcursas=="Colegio Rosalía de Castro, Vigo"
replace SocioeconStatHog = 31827 if LocalidadInstitutoenelcursas=="Colegio Rosalía de Castro, Vigo"
replace poblacion = 295364 if LocalidadInstitutoenelcursas=="Colegio Rosalía de Castro, Vigo"
// Vilagarcía de Arousa
// ies iguel angel gonzalez esteves
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ies iguel angel gonzalez esteves"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ies iguel angel gonzalez esteves"
replace SocioeconStatPers = 9855 if LocalidadInstitutoenelcursas=="ies iguel angel gonzalez esteves"
replace SocioeconStatHog = 25758 if LocalidadInstitutoenelcursas=="ies iguel angel gonzalez esteves"
replace poblacion = 37456 if LocalidadInstitutoenelcursas=="ies iguel angel gonzalez esteves"
********************************************************************************
*    Salamanca
********************************************************************************
// Salamanca
// Salamanca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salamanca"
replace SocioeconStatPers = 12225 if LocalidadInstitutoenelcursas=="Salamanca"
replace SocioeconStatHog = 27811 if LocalidadInstitutoenelcursas=="Salamanca"
replace poblacion = 144228 if LocalidadInstitutoenelcursas=="Salamanca"
// IES vaguada de la palma
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
replace SocioeconStatPers = 12225 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
replace SocioeconStatHog = 27811 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
replace poblacion = 144228 if LocalidadInstitutoenelcursas=="IES vaguada de la palma"
// IES vaguada de la palma 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
replace SocioeconStatPers = 12225 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
replace SocioeconStatHog = 27811 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
replace poblacion = 144228 if LocalidadInstitutoenelcursas=="IES vaguada de la palma "
// IES Francisco Salinas, Salamanca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Francisco Salinas, Salamanca"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Francisco Salinas, Salamanca"
replace SocioeconStatPers = 12225 if LocalidadInstitutoenelcursas=="IES Francisco Salinas, Salamanca"
replace SocioeconStatHog = 27811 if LocalidadInstitutoenelcursas=="IES Francisco Salinas, Salamanca"
replace poblacion = 144228 if LocalidadInstitutoenelcursas=="IES Francisco Salinas, Salamanca"
//  IES Francisco Salinas, Salamanca
replace cuidad = 1 if LocalidadInstitutoenelcursas==" IES Francisco Salinas, Salamanca"
replace instituto = 1 if LocalidadInstitutoenelcursas==" IES Francisco Salinas, Salamanca"
replace SocioeconStatPers = 12225 if LocalidadInstitutoenelcursas==" IES Francisco Salinas, Salamanca"
replace SocioeconStatHog = 27811 if LocalidadInstitutoenelcursas==" IES Francisco Salinas, Salamanca"
replace poblacion = 144228 if LocalidadInstitutoenelcursas==" IES Francisco Salinas, Salamanca"
// IES Francisco Salinsa, Salamanca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Francisco Salinsa, Salamanca"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Francisco Salinsa, Salamanca"
replace SocioeconStatPers = 12225 if LocalidadInstitutoenelcursas=="IES Francisco Salinsa, Salamanca"
replace SocioeconStatHog = 27811 if LocalidadInstitutoenelcursas=="IES Francisco Salinsa, Salamanca"
replace poblacion = 144228 if LocalidadInstitutoenelcursas=="IES Francisco Salinsa, Salamanca"
// IES Fray Luis de León
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Fray Luis de León"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Fray Luis de León"
replace SocioeconStatPers = 12225 if LocalidadInstitutoenelcursas=="IES Fray Luis de León"
replace SocioeconStatHog = 27811 if LocalidadInstitutoenelcursas=="IES Fray Luis de León"
replace poblacion = 144228 if LocalidadInstitutoenelcursas=="IES Fray Luis de León"
********************************************************************************
*    Segovia
********************************************************************************
// Cuéllar
// IES Duque de Alburquerque, Cuéllar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Duque de Alburquerque, Cuéllar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Duque de Alburquerque, Cuéllar"
replace SocioeconStatPers = 10381 if LocalidadInstitutoenelcursas=="IES Duque de Alburquerque, Cuéllar"
replace SocioeconStatHog = 25945 if LocalidadInstitutoenelcursas=="IES Duque de Alburquerque, Cuéllar"
replace poblacion = 9583 if LocalidadInstitutoenelcursas=="IES Duque de Alburquerque, Cuéllar"
// Segovia
// Segovia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Segovia"
replace SocioeconStatPers = 12252 if LocalidadInstitutoenelcursas=="Segovia"
replace SocioeconStatHog = 30781 if LocalidadInstitutoenelcursas=="Segovia"
replace poblacion = 51674 if LocalidadInstitutoenelcursas=="Segovia"
// segovia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="segovia"
replace SocioeconStatPers = 12252 if LocalidadInstitutoenelcursas=="segovia"
replace SocioeconStatHog = 30781 if LocalidadInstitutoenelcursas=="segovia"
replace poblacion = 51674 if LocalidadInstitutoenelcursas=="segovia"
// Segovia 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Segovia "
replace SocioeconStatPers = 12252 if LocalidadInstitutoenelcursas=="Segovia "
replace SocioeconStatHog = 30781 if LocalidadInstitutoenelcursas=="Segovia "
replace poblacion = 51674 if LocalidadInstitutoenelcursas=="Segovia "
// Segovia/ IES Peñalara
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Segovia/ IES Peñalara"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Segovia/ IES Peñalara"
replace SocioeconStatPers = 12252 if LocalidadInstitutoenelcursas=="Segovia/ IES Peñalara"
replace SocioeconStatHog = 30781 if LocalidadInstitutoenelcursas=="Segovia/ IES Peñalara"
replace poblacion = 51674 if LocalidadInstitutoenelcursas=="Segovia/ IES Peñalara"
// Segovia/ies Giner de los Rios
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Segovia/ies Giner de los Rios"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Segovia/ies Giner de los Rios"
replace SocioeconStatPers = 12252 if LocalidadInstitutoenelcursas=="Segovia/ies Giner de los Rios"
replace SocioeconStatHog = 30781 if LocalidadInstitutoenelcursas=="Segovia/ies Giner de los Rios"
replace poblacion = 51674 if LocalidadInstitutoenelcursas=="Segovia/ies Giner de los Rios"
// Segovia/Mariano Quintanilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Segovia/Mariano Quintanilla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Segovia/Mariano Quintanilla"
replace SocioeconStatPers = 12252 if LocalidadInstitutoenelcursas=="Segovia/Mariano Quintanilla"
replace SocioeconStatHog = 30781 if LocalidadInstitutoenelcursas=="Segovia/Mariano Quintanilla"
replace poblacion = 51674 if LocalidadInstitutoenelcursas=="Segovia/Mariano Quintanilla"
// SEGOVIA. IES ANDRÉS LAGUNA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="SEGOVIA. IES ANDRÉS LAGUNA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="SEGOVIA. IES ANDRÉS LAGUNA"
replace SocioeconStatPers = 12252 if LocalidadInstitutoenelcursas=="SEGOVIA. IES ANDRÉS LAGUNA"
replace SocioeconStatHog = 30781 if LocalidadInstitutoenelcursas=="SEGOVIA. IES ANDRÉS LAGUNA"
replace poblacion = 51674 if LocalidadInstitutoenelcursas=="SEGOVIA. IES ANDRÉS LAGUNA"
//  SEGOVIA. IES ANDRÉS LAGUNA
replace cuidad = 1 if LocalidadInstitutoenelcursas==" SEGOVIA. IES ANDRÉS LAGUNA"
replace instituto = 1 if LocalidadInstitutoenelcursas==" SEGOVIA. IES ANDRÉS LAGUNA"
replace SocioeconStatPers = 12252 if LocalidadInstitutoenelcursas==" SEGOVIA. IES ANDRÉS LAGUNA"
replace SocioeconStatHog = 30781 if LocalidadInstitutoenelcursas==" SEGOVIA. IES ANDRÉS LAGUNA"
replace poblacion = 51674 if LocalidadInstitutoenelcursas==" SEGOVIA. IES ANDRÉS LAGUNA"
// IES ANDRES LAGUNA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES ANDRES LAGUNA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES ANDRES LAGUNA"
replace SocioeconStatPers = 12252 if LocalidadInstitutoenelcursas=="IES ANDRES LAGUNA"
replace SocioeconStatHog = 30781 if LocalidadInstitutoenelcursas=="IES ANDRES LAGUNA"
replace poblacion = 51674 if LocalidadInstitutoenelcursas=="IES ANDRES LAGUNA"
********************************************************************************
*    Sevilla
********************************************************************************
// Mairena del Alcor
// I.E.S. Los Alcores
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S. Los Alcores"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S. Los Alcores"
replace SocioeconStatPers = 8634 if LocalidadInstitutoenelcursas=="I.E.S. Los Alcores"
replace SocioeconStatHog = 25605 if LocalidadInstitutoenelcursas=="I.E.S. Los Alcores"
replace poblacion = 23550 if LocalidadInstitutoenelcursas=="I.E.S. Los Alcores"
// Osuna
// Osuna-IES Sierra Sur
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Osuna-IES Sierra Sur"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Osuna-IES Sierra Sur"
replace SocioeconStatPers = 8236 if LocalidadInstitutoenelcursas=="Osuna-IES Sierra Sur"
replace SocioeconStatHog = 23008 if LocalidadInstitutoenelcursas=="Osuna-IES Sierra Sur"
replace poblacion = 17560 if LocalidadInstitutoenelcursas=="Osuna-IES Sierra Sur"
// Sevilla
// Sevilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sevilla"
replace SocioeconStatPers = 11346 if LocalidadInstitutoenelcursas=="Sevilla"
replace SocioeconStatHog = 29911 if LocalidadInstitutoenelcursas=="Sevilla"
replace poblacion = 688592 if LocalidadInstitutoenelcursas=="Sevilla"
// Sevilla / Colegio Portaceli
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sevilla / Colegio Portaceli"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sevilla / Colegio Portaceli"
replace SocioeconStatPers = 11346 if LocalidadInstitutoenelcursas=="Sevilla / Colegio Portaceli"
replace SocioeconStatHog = 29911 if LocalidadInstitutoenelcursas=="Sevilla / Colegio Portaceli"
replace poblacion = 688592 if LocalidadInstitutoenelcursas=="Sevilla / Colegio Portaceli"
// Sevilla/IES Martínez Montañés
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sevilla/IES Martínez Montañés"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Sevilla/IES Martínez Montañés"
replace SocioeconStatPers = 11346 if LocalidadInstitutoenelcursas=="Sevilla/IES Martínez Montañés"
replace SocioeconStatHog = 29911 if LocalidadInstitutoenelcursas=="Sevilla/IES Martínez Montañés"
replace poblacion = 688592 if LocalidadInstitutoenelcursas=="Sevilla/IES Martínez Montañés"
********************************************************************************
*    Soria
********************************************************************************
// Almazán
// ALMAZÁN, SORIA/ IES GAYA NUÑO
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ALMAZÁN, SORIA/ IES GAYA NUÑO"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ALMAZÁN, SORIA/ IES GAYA NUÑO"
replace SocioeconStatPers = 11297 if LocalidadInstitutoenelcursas=="ALMAZÁN, SORIA/ IES GAYA NUÑO"
replace SocioeconStatHog = 27356 if LocalidadInstitutoenelcursas=="ALMAZÁN, SORIA/ IES GAYA NUÑO"
replace poblacion = 5489 if LocalidadInstitutoenelcursas=="ALMAZÁN, SORIA/ IES GAYA NUÑO"
// Soria
// Soria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Soria"
replace SocioeconStatPers = 13021 if LocalidadInstitutoenelcursas=="Soria"
replace SocioeconStatHog = 31546 if LocalidadInstitutoenelcursas=="Soria"
replace poblacion = 39398 if LocalidadInstitutoenelcursas=="Soria"
********************************************************************************
*    Tarragona
********************************************************************************
// Alcanar
// Alcanar/Institut Sòl-de-Riu
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Alcanar/Institut Sòl-de-Riu"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Alcanar/Institut Sòl-de-Riu"
replace SocioeconStatPers = 9348 if LocalidadInstitutoenelcursas=="Alcanar/Institut Sòl-de-Riu"
replace SocioeconStatHog = 24074 if LocalidadInstitutoenelcursas=="Alcanar/Institut Sòl-de-Riu"
replace poblacion = 9418 if LocalidadInstitutoenelcursas=="Alcanar/Institut Sòl-de-Riu"
// Altafulla
// ALTAFULLA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ALTAFULLA"
replace SocioeconStatPers = 14843 if LocalidadInstitutoenelcursas=="ALTAFULLA"
replace SocioeconStatHog = 37944 if LocalidadInstitutoenelcursas=="ALTAFULLA"
replace poblacion = 5243 if LocalidadInstitutoenelcursas=="ALTAFULLA"
// Amposta
// Amposta/Ins. Ramon Berenguer IV
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV"
replace SocioeconStatPers = 9965 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV"
replace SocioeconStatHog = 25631 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV"
replace poblacion = 20738 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV"
// Amposta/Ins. Ramon Berenguer IV 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV "
replace SocioeconStatPers = 9965 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV "
replace SocioeconStatHog = 25631 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV "
replace poblacion = 20738 if LocalidadInstitutoenelcursas=="Amposta/Ins. Ramon Berenguer IV "
// Flix
// Flix
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Flix"
replace SocioeconStatPers = 14717 if LocalidadInstitutoenelcursas=="Flix"
replace SocioeconStatHog = 34019 if LocalidadInstitutoenelcursas=="Flix"
replace poblacion = 3408 if LocalidadInstitutoenelcursas=="Flix"
// Reus
// Reus
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Reus"
replace SocioeconStatPers = 11530 if LocalidadInstitutoenelcursas=="Reus"
replace SocioeconStatHog = 30179 if LocalidadInstitutoenelcursas=="Reus"
replace poblacion = 104373 if LocalidadInstitutoenelcursas=="Reus"
// reus/ Gabriel i Ferrater
replace cuidad = 1 if LocalidadInstitutoenelcursas=="reus/ Gabriel i Ferrater"
replace instituto = 1 if LocalidadInstitutoenelcursas=="reus/ Gabriel i Ferrater"
replace SocioeconStatPers = 11530 if LocalidadInstitutoenelcursas=="reus/ Gabriel i Ferrater"
replace SocioeconStatHog = 30179 if LocalidadInstitutoenelcursas=="reus/ Gabriel i Ferrater"
replace poblacion = 104373 if LocalidadInstitutoenelcursas=="reus/ Gabriel i Ferrater"
// Salou
// Salou/Escola Elisabeth
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
replace SocioeconStatPers = 11091 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
replace SocioeconStatHog = 28351 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
replace poblacion = 27476 if LocalidadInstitutoenelcursas=="Salou/Escola Elisabeth"
// Tarragona
// TARRAGONA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="TARRAGONA"
replace SocioeconStatPers = 12967 if LocalidadInstitutoenelcursas=="TARRAGONA"
replace SocioeconStatHog = 33547 if LocalidadInstitutoenelcursas=="TARRAGONA"
replace poblacion = 134515 if LocalidadInstitutoenelcursas=="TARRAGONA"
// Tarragona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tarragona"
replace SocioeconStatPers = 12967 if LocalidadInstitutoenelcursas=="Tarragona"
replace SocioeconStatHog = 33547 if LocalidadInstitutoenelcursas=="Tarragona"
replace poblacion = 134515 if LocalidadInstitutoenelcursas=="Tarragona"
// La Salle Tarragona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
replace SocioeconStatPers = 12967 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
replace SocioeconStatHog = 33547 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
replace poblacion = 134515 if LocalidadInstitutoenelcursas=="La Salle Tarragona"
// Tarragona/ Instituto Martí i Franquès
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tarragona/ Instituto Martí i Franquès"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Tarragona/ Instituto Martí i Franquès"
replace SocioeconStatPers = 12967 if LocalidadInstitutoenelcursas=="Tarragona/ Instituto Martí i Franquès"
replace SocioeconStatHog = 33547 if LocalidadInstitutoenelcursas=="Tarragona/ Instituto Martí i Franquès"
replace poblacion = 134515 if LocalidadInstitutoenelcursas=="Tarragona/ Instituto Martí i Franquès"
// Institut Antoni de Martí Franqués
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Antoni de Martí Franqués"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Antoni de Martí Franqués"
replace SocioeconStatPers = 12967 if LocalidadInstitutoenelcursas=="Institut Antoni de Martí Franqués"
replace SocioeconStatHog = 33547 if LocalidadInstitutoenelcursas=="Institut Antoni de Martí Franqués"
replace poblacion = 134515 if LocalidadInstitutoenelcursas=="Institut Antoni de Martí Franqués"
// Institut Pons d'Icart, Tarragona
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Pons d'Icart, Tarragona"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Pons d'Icart, Tarragona"
replace SocioeconStatPers = 12967 if LocalidadInstitutoenelcursas=="Institut Pons d'Icart, Tarragona"
replace SocioeconStatHog = 33547 if LocalidadInstitutoenelcursas=="Institut Pons d'Icart, Tarragona"
replace poblacion = 134515 if LocalidadInstitutoenelcursas=="Institut Pons d'Icart, Tarragona"
// Torredembarra
// Ins Torredembarra
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ins Torredembarra"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ins Torredembarra"
replace SocioeconStatPers = 11189 if LocalidadInstitutoenelcursas=="Ins Torredembarra"
replace SocioeconStatHog = 27666 if LocalidadInstitutoenelcursas=="Ins Torredembarra"
replace poblacion = 16184 if LocalidadInstitutoenelcursas=="Ins Torredembarra"
// Valls
// Valls
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valls"
replace SocioeconStatPers = 11995 if LocalidadInstitutoenelcursas=="Valls"
replace SocioeconStatHog = 31870 if LocalidadInstitutoenelcursas=="Valls"
replace poblacion = 24359 if LocalidadInstitutoenelcursas=="Valls"
// Vendrell, El
// Institut Baix Penedès
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Institut Baix Penedès"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Institut Baix Penedès"
replace SocioeconStatPers = 10828 if LocalidadInstitutoenelcursas=="Institut Baix Penedès"
replace SocioeconStatHog = 27364 if LocalidadInstitutoenelcursas=="Institut Baix Penedès"
replace poblacion = 37606 if LocalidadInstitutoenelcursas=="Institut Baix Penedès"
********************************************************************************
*    Santa Cruz de Tenerife
********************************************************************************
// Adeje
// Tenerife  colegio costa Adeje
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Tenerife  colegio costa Adeje"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Tenerife  colegio costa Adeje"
replace SocioeconStatPers = 8706 if LocalidadInstitutoenelcursas=="Tenerife  colegio costa Adeje"
replace SocioeconStatHog = 23721 if LocalidadInstitutoenelcursas=="Tenerife  colegio costa Adeje"
replace poblacion = 19461 if LocalidadInstitutoenelcursas=="Tenerife  colegio costa Adeje"
// colegio internacional costa adeje
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio internacional costa adeje"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio internacional costa adeje"
replace SocioeconStatPers = 8706 if LocalidadInstitutoenelcursas=="colegio internacional costa adeje"
replace SocioeconStatHog = 23721 if LocalidadInstitutoenelcursas=="colegio internacional costa adeje"
replace poblacion = 19461 if LocalidadInstitutoenelcursas=="colegio internacional costa adeje"
// Breña Baja
// IES LAS BREÑAS
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES LAS BREÑAS"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES LAS BREÑAS"
replace SocioeconStatPers = 10979 if LocalidadInstitutoenelcursas=="IES LAS BREÑAS"
replace SocioeconStatHog = 30253 if LocalidadInstitutoenelcursas=="IES LAS BREÑAS"
replace poblacion = 5690 if LocalidadInstitutoenelcursas=="IES LAS BREÑAS"
// Güímar
// S/C de Tenerife, Güímar, IES Mencey Acaymo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="S/C de Tenerife, Güímar, IES Mencey Acaymo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="S/C de Tenerife, Güímar, IES Mencey Acaymo"
replace SocioeconStatPers = 9275 if LocalidadInstitutoenelcursas=="S/C de Tenerife, Güímar, IES Mencey Acaymo"
replace SocioeconStatHog = 25081 if LocalidadInstitutoenelcursas=="S/C de Tenerife, Güímar, IES Mencey Acaymo"
replace poblacion = 20190 if LocalidadInstitutoenelcursas=="S/C de Tenerife, Güímar, IES Mencey Acaymo"
// Icod de los Vinos
// Icod de los Vinos/IES Lucas Martin Espino
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino"
replace SocioeconStatPers = 8083 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino"
replace SocioeconStatHog = 23013 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino"
replace poblacion = 23254 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino"
// Icod de los Vinos/IES Lucas Martin Espino 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino "
replace SocioeconStatPers = 8083 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino "
replace SocioeconStatHog = 23013 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino "
replace poblacion = 23254 if LocalidadInstitutoenelcursas=="Icod de los Vinos/IES Lucas Martin Espino "
// San Cristóbal de La Laguna
// San Cristóbal de La Laguna
replace cuidad = 1 if LocalidadInstitutoenelcursas=="San Cristóbal de La Laguna"
replace SocioeconStatPers = 10251 if LocalidadInstitutoenelcursas=="San Cristóbal de La Laguna"
replace SocioeconStatHog = 28379 if LocalidadInstitutoenelcursas=="San Cristóbal de La Laguna"
replace poblacion = 157503 if LocalidadInstitutoenelcursas=="San Cristóbal de La Laguna"
// La Laguna (Tenerife)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Laguna (Tenerife)"
replace SocioeconStatPers = 10251 if LocalidadInstitutoenelcursas=="La Laguna (Tenerife)"
replace SocioeconStatHog = 28379 if LocalidadInstitutoenelcursas=="La Laguna (Tenerife)"
replace poblacion = 157503 if LocalidadInstitutoenelcursas=="La Laguna (Tenerife)"
// Santa Cruz de la Palma
// Santa Cruz de La Palma
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Cruz de La Palma"
replace SocioeconStatPers = 10475 if LocalidadInstitutoenelcursas=="Santa Cruz de La Palma"
replace SocioeconStatHog = 29555 if LocalidadInstitutoenelcursas=="Santa Cruz de La Palma"
replace poblacion = 15716 if LocalidadInstitutoenelcursas=="Santa Cruz de La Palma"
// Santa Cruz de La Palma/ IES Luis Cobiella Cuevas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Cruz de La Palma/ IES Luis Cobiella Cuevas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Cruz de La Palma/ IES Luis Cobiella Cuevas"
replace SocioeconStatPers = 10475 if LocalidadInstitutoenelcursas=="Santa Cruz de La Palma/ IES Luis Cobiella Cuevas"
replace SocioeconStatHog = 29555 if LocalidadInstitutoenelcursas=="Santa Cruz de La Palma/ IES Luis Cobiella Cuevas"
replace poblacion = 15716 if LocalidadInstitutoenelcursas=="Santa Cruz de La Palma/ IES Luis Cobiella Cuevas"
// Luis Cobiella Cuevas (S/C de La Palma)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
replace SocioeconStatPers = 10475 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
replace SocioeconStatHog = 29555 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
replace poblacion = 15716 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas (S/C de La Palma)"
// Luis Cobiella Cuevas/ S/C de La Palma
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
replace SocioeconStatPers = 10475 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
replace SocioeconStatHog = 29555 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
replace poblacion = 15716 if LocalidadInstitutoenelcursas=="Luis Cobiella Cuevas/ S/C de La Palma"
// Santa Cruz de Tenerife
// Santa Cruz de Tenerife
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife"
replace SocioeconStatPers = 11337 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife"
replace SocioeconStatHog = 29162 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife"
replace poblacion = 207312 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife"
// Santa Cruz de Tenerife 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife "
replace SocioeconStatPers = 11337 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife "
replace SocioeconStatHog = 29162 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife "
replace poblacion = 207312 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife "
// Santa Cruz de Tenerife/ Colegio Pureza de María
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María"
replace SocioeconStatPers = 11337 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María"
replace SocioeconStatHog = 29162 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María"
replace poblacion = 207312 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María"
// Santa Cruz de Tenerife/ Colegio Pureza de María 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María "
replace SocioeconStatPers = 11337 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María "
replace SocioeconStatHog = 29162 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María "
replace poblacion = 207312 if LocalidadInstitutoenelcursas=="Santa Cruz de Tenerife/ Colegio Pureza de María "
// La Salle San Ildefonso
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Salle San Ildefonso"
replace instituto = 1 if LocalidadInstitutoenelcursas=="La Salle San Ildefonso"
replace SocioeconStatPers = 11337 if LocalidadInstitutoenelcursas=="La Salle San Ildefonso"
replace SocioeconStatHog = 29162 if LocalidadInstitutoenelcursas=="La Salle San Ildefonso"
replace poblacion = 207312 if LocalidadInstitutoenelcursas=="La Salle San Ildefonso"
// Dominicas Vistabella
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Dominicas Vistabella"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Dominicas Vistabella"
replace SocioeconStatPers = 11337 if LocalidadInstitutoenelcursas=="Dominicas Vistabella"
replace SocioeconStatHog = 29162 if LocalidadInstitutoenelcursas=="Dominicas Vistabella"
replace poblacion = 207312 if LocalidadInstitutoenelcursas=="Dominicas Vistabella"
// Villa de Mazo
// villa de mazo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="villa de mazo"
replace SocioeconStatPers = 9515 if LocalidadInstitutoenelcursas=="villa de mazo"
replace SocioeconStatHog = 26688 if LocalidadInstitutoenelcursas=="villa de mazo"
replace poblacion = 4843 if LocalidadInstitutoenelcursas=="villa de mazo"
// villa de mazo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="villa de mazo"
replace SocioeconStatPers = 9515 if LocalidadInstitutoenelcursas=="villa de mazo"
replace SocioeconStatHog = 26688 if LocalidadInstitutoenelcursas=="villa de mazo"
replace poblacion = 4843 if LocalidadInstitutoenelcursas=="villa de mazo"
********************************************************************************
*    Teruel
********************************************************************************
// Alcañiz
// IES Bajo Aragón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Bajo Aragón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Bajo Aragón"
replace SocioeconStatPers = 10834 if LocalidadInstitutoenelcursas=="IES Bajo Aragón"
replace SocioeconStatHog = 28543 if LocalidadInstitutoenelcursas=="IES Bajo Aragón"
replace poblacion = 15947 if LocalidadInstitutoenelcursas=="IES Bajo Aragón"
// Monreal del Campo
// Monreal del Campo/IES Salvador Victoria
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Monreal del Campo/IES Salvador Victoria"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Monreal del Campo/IES Salvador Victoria"
replace SocioeconStatPers = 10690 if LocalidadInstitutoenelcursas=="Monreal del Campo/IES Salvador Victoria"
replace SocioeconStatHog = 27331 if LocalidadInstitutoenelcursas=="Monreal del Campo/IES Salvador Victoria"
replace poblacion = 2434 if LocalidadInstitutoenelcursas=="Monreal del Campo/IES Salvador Victoria"
********************************************************************************
*    Toledo
********************************************************************************
// Carranque
// Carranque
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Carranque"
replace SocioeconStatPers = 10294 if LocalidadInstitutoenelcursas=="Carranque"
replace SocioeconStatHog = 27936 if LocalidadInstitutoenelcursas=="Carranque"
replace poblacion = 4846 if LocalidadInstitutoenelcursas=="Carranque"
// Illescas
// Illescas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Illescas"
replace SocioeconStatPers = 10106 if LocalidadInstitutoenelcursas=="Illescas"
replace SocioeconStatHog = 29905 if LocalidadInstitutoenelcursas=="Illescas"
replace poblacion = 29894 if LocalidadInstitutoenelcursas=="Illescas"
// Ies Juan de Padilla
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies Juan de Padilla"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies Juan de Padilla"
replace SocioeconStatPers = 10106 if LocalidadInstitutoenelcursas=="Ies Juan de Padilla"
replace SocioeconStatHog = 29905 if LocalidadInstitutoenelcursas=="Ies Juan de Padilla"
replace poblacion = 29894 if LocalidadInstitutoenelcursas=="Ies Juan de Padilla"
// Madridejos
// Madridejos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Madridejos"
replace SocioeconStatPers = 8274 if LocalidadInstitutoenelcursas=="Madridejos"
replace SocioeconStatHog = 23279 if LocalidadInstitutoenelcursas=="Madridejos"
replace poblacion = 10453 if LocalidadInstitutoenelcursas=="Madridejos"
// Ocaña
// Ocaña
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ocaña"
replace SocioeconStatPers = 8785 if LocalidadInstitutoenelcursas=="Ocaña"
replace SocioeconStatHog = 23921 if LocalidadInstitutoenelcursas=="Ocaña"
replace poblacion = 11597 if LocalidadInstitutoenelcursas=="Ocaña"
// Quintanar de la Orden
// Quintanar de la Orden
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Quintanar de la Orden"
replace SocioeconStatPers = 8295 if LocalidadInstitutoenelcursas=="Quintanar de la Orden"
replace SocioeconStatHog = 22622 if LocalidadInstitutoenelcursas=="Quintanar de la Orden"
replace poblacion = 11030 if LocalidadInstitutoenelcursas=="Quintanar de la Orden"
// Quintanar de la Orden 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Quintanar de la Orden "
replace SocioeconStatPers = 8295 if LocalidadInstitutoenelcursas=="Quintanar de la Orden "
replace SocioeconStatHog = 22622 if LocalidadInstitutoenelcursas=="Quintanar de la Orden "
replace poblacion = 11030 if LocalidadInstitutoenelcursas=="Quintanar de la Orden "
// quintanar de la orden/ IES Alonso Quijano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="quintanar de la orden/ IES Alonso Quijano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="quintanar de la orden/ IES Alonso Quijano"
replace SocioeconStatPers = 8295 if LocalidadInstitutoenelcursas=="quintanar de la orden/ IES Alonso Quijano"
replace SocioeconStatHog = 22622 if LocalidadInstitutoenelcursas=="quintanar de la orden/ IES Alonso Quijano"
replace poblacion = 11030 if LocalidadInstitutoenelcursas=="quintanar de la orden/ IES Alonso Quijano"
// Seseña
// Seseña
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Seseña"
replace SocioeconStatPers = 9219 if LocalidadInstitutoenelcursas=="Seseña"
replace SocioeconStatHog = 26614 if LocalidadInstitutoenelcursas=="Seseña"
replace poblacion = 25835 if LocalidadInstitutoenelcursas=="Seseña"
// IES Margatita Salas, Seseña
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Margatita Salas, Seseña"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Margatita Salas, Seseña"
replace SocioeconStatPers = 9219 if LocalidadInstitutoenelcursas=="IES Margatita Salas, Seseña"
replace SocioeconStatHog = 26614 if LocalidadInstitutoenelcursas=="IES Margatita Salas, Seseña"
replace poblacion = 25835 if LocalidadInstitutoenelcursas=="IES Margatita Salas, Seseña"
// Sonseca
// Sonseca
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sonseca"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Sonseca"
replace SocioeconStatHog = 25117 if LocalidadInstitutoenelcursas=="Sonseca"
replace poblacion = 11067 if LocalidadInstitutoenelcursas=="Sonseca"
// Sonseca 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Sonseca "
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="Sonseca "
replace SocioeconStatHog = 25117 if LocalidadInstitutoenelcursas=="Sonseca "
replace poblacion = 11067 if LocalidadInstitutoenelcursas=="Sonseca "
// IES LA SISLA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES LA SISLA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES LA SISLA"
replace SocioeconStatPers = 8870 if LocalidadInstitutoenelcursas=="IES LA SISLA"
replace SocioeconStatHog = 25117 if LocalidadInstitutoenelcursas=="IES LA SISLA"
replace poblacion = 11067 if LocalidadInstitutoenelcursas=="IES LA SISLA"
// Talavera de la Reina
// Talavera de la Reina
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Talavera de la Reina"
replace SocioeconStatPers = 9071 if LocalidadInstitutoenelcursas=="Talavera de la Reina"
replace SocioeconStatHog = 24565 if LocalidadInstitutoenelcursas=="Talavera de la Reina"
replace poblacion = 83417 if LocalidadInstitutoenelcursas=="Talavera de la Reina"
// Talavera de la Reina 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Talavera de la Reina "
replace SocioeconStatPers = 9071 if LocalidadInstitutoenelcursas=="Talavera de la Reina "
replace SocioeconStatHog = 24565 if LocalidadInstitutoenelcursas=="Talavera de la Reina "
replace poblacion = 83417 if LocalidadInstitutoenelcursas=="Talavera de la Reina "
// Talavera de la Reina / HH Maristas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Talavera de la Reina / HH Maristas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Talavera de la Reina / HH Maristas"
replace SocioeconStatPers = 9071 if LocalidadInstitutoenelcursas=="Talavera de la Reina / HH Maristas"
replace SocioeconStatHog = 24565 if LocalidadInstitutoenelcursas=="Talavera de la Reina / HH Maristas"
replace poblacion = 83417 if LocalidadInstitutoenelcursas=="Talavera de la Reina / HH Maristas"
// I.E.S Puerta de Cuartos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I.E.S Puerta de Cuartos"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I.E.S Puerta de Cuartos"
replace SocioeconStatPers = 9071 if LocalidadInstitutoenelcursas=="I.E.S Puerta de Cuartos"
replace SocioeconStatHog = 24565 if LocalidadInstitutoenelcursas=="I.E.S Puerta de Cuartos"
replace poblacion = 83417 if LocalidadInstitutoenelcursas=="I.E.S Puerta de Cuartos"
// Padre juan de mariana
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Padre juan de mariana"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Padre juan de mariana"
replace SocioeconStatPers = 9071 if LocalidadInstitutoenelcursas=="Padre juan de mariana"
replace SocioeconStatHog = 24565 if LocalidadInstitutoenelcursas=="Padre juan de mariana"
replace poblacion = 83417 if LocalidadInstitutoenelcursas=="Padre juan de mariana"
// IES Puerta de Cuartos (Talavera de la Reina)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Puerta de Cuartos (Talavera de la Reina)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Puerta de Cuartos (Talavera de la Reina)"
replace SocioeconStatPers = 9071 if LocalidadInstitutoenelcursas=="IES Puerta de Cuartos (Talavera de la Reina)"
replace SocioeconStatHog = 24565 if LocalidadInstitutoenelcursas=="IES Puerta de Cuartos (Talavera de la Reina)"
replace poblacion = 83417 if LocalidadInstitutoenelcursas=="IES Puerta de Cuartos (Talavera de la Reina)"
// Toledo
// TOLEDO
replace cuidad = 1 if LocalidadInstitutoenelcursas=="TOLEDO"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="TOLEDO"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="TOLEDO"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="TOLEDO"
// Toledo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Toledo"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="Toledo"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="Toledo"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="Toledo"
// Toledo 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Toledo "
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="Toledo "
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="Toledo "
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="Toledo "
// toledo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="toledo"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="toledo"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="toledo"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="toledo"
// Toledo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Toledo"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="Toledo"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="Toledo"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="Toledo"
// Toledo, IES Juanelo Turriano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Toledo, IES Juanelo Turriano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Toledo, IES Juanelo Turriano"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="Toledo, IES Juanelo Turriano"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="Toledo, IES Juanelo Turriano"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="Toledo, IES Juanelo Turriano"
// Santa Maria Maristas Toledo
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa Maria Maristas Toledo"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa Maria Maristas Toledo"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="Santa Maria Maristas Toledo"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="Santa Maria Maristas Toledo"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="Santa Maria Maristas Toledo"
// Toledo/ Hermanos Santa María Maristas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Toledo/ Hermanos Santa María Maristas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Toledo/ Hermanos Santa María Maristas"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="Toledo/ Hermanos Santa María Maristas"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="Toledo/ Hermanos Santa María Maristas"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="Toledo/ Hermanos Santa María Maristas"
// Toledo/ IES carlos III
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Toledo/ IES carlos III"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Toledo/ IES carlos III"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="Toledo/ IES carlos III"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="Toledo/ IES carlos III"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="Toledo/ IES carlos III"
// Toledo, IES Universidad Laboral
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Toledo, IES Universidad Laboral"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Toledo, IES Universidad Laboral"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="Toledo, IES Universidad Laboral"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="Toledo, IES Universidad Laboral"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="Toledo, IES Universidad Laboral"
// Nuestra señora de los infante
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Nuestra señora de los infante"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Nuestra señora de los infante"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="Nuestra señora de los infante"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="Nuestra señora de los infante"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="Nuestra señora de los infante"
// COLEGIO MAYOL
replace cuidad = 1 if LocalidadInstitutoenelcursas=="COLEGIO MAYOL"
replace instituto = 1 if LocalidadInstitutoenelcursas=="COLEGIO MAYOL"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="COLEGIO MAYOL"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="COLEGIO MAYOL"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="COLEGIO MAYOL"
// IES AZARQUIEL
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES AZARQUIEL"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES AZARQUIEL"
replace SocioeconStatPers = 13365 if LocalidadInstitutoenelcursas=="IES AZARQUIEL"
replace SocioeconStatHog = 35772 if LocalidadInstitutoenelcursas=="IES AZARQUIEL"
replace poblacion = 84873 if LocalidadInstitutoenelcursas=="IES AZARQUIEL"
// Torrijos
// Torrijos
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrijos"
replace SocioeconStatPers = 9096 if LocalidadInstitutoenelcursas=="Torrijos"
replace SocioeconStatHog = 25415 if LocalidadInstitutoenelcursas=="Torrijos"
replace poblacion = 13466 if LocalidadInstitutoenelcursas=="Torrijos"
// Torrijos 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Torrijos "
replace SocioeconStatPers = 9096 if LocalidadInstitutoenelcursas=="Torrijos "
replace SocioeconStatHog = 25415 if LocalidadInstitutoenelcursas=="Torrijos "
replace poblacion = 13466 if LocalidadInstitutoenelcursas=="Torrijos "
// Romeral, El
// El Romeral
replace cuidad = 1 if LocalidadInstitutoenelcursas=="El Romeral"
replace SocioeconStatPers = 9025 if LocalidadInstitutoenelcursas=="El Romeral"
replace SocioeconStatHog = 19748 if LocalidadInstitutoenelcursas=="El Romeral"
replace poblacion = 622 if LocalidadInstitutoenelcursas=="El Romeral"
// Valmojado
// Valmojado
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valmojado"
replace SocioeconStatPers = 9267 if LocalidadInstitutoenelcursas=="Valmojado"
replace SocioeconStatHog = 25534 if LocalidadInstitutoenelcursas=="Valmojado"
replace poblacion = 4303 if LocalidadInstitutoenelcursas=="Valmojado"
// Yepes
// IES CARPETANIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES CARPETANIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES CARPETANIA"
replace SocioeconStatPers = 8768 if LocalidadInstitutoenelcursas=="IES CARPETANIA"
replace SocioeconStatHog = 24293 if LocalidadInstitutoenelcursas=="IES CARPETANIA"
replace poblacion = 5198 if LocalidadInstitutoenelcursas=="IES CARPETANIA"
// Yuncos
// I. E. S la cañuela
replace cuidad = 1 if LocalidadInstitutoenelcursas=="I. E. S la cañuela"
replace instituto = 1 if LocalidadInstitutoenelcursas=="I. E. S la cañuela"
replace SocioeconStatPers = 8118 if LocalidadInstitutoenelcursas=="I. E. S la cañuela"
replace SocioeconStatHog = 23747 if LocalidadInstitutoenelcursas=="I. E. S la cañuela"
replace poblacion = 11222 if LocalidadInstitutoenelcursas=="I. E. S la cañuela"
********************************************************************************
*    Valencia
*******************************************************************************
// Alcúdia, l'
// IES Els Evóls (L'Alcúdia)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Els Evóls (L'Alcúdia)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Els Evóls (L'Alcúdia)"
replace SocioeconStatPers = 10877 if LocalidadInstitutoenelcursas=="IES Els Evóls (L'Alcúdia)"
replace SocioeconStatHog = 28112 if LocalidadInstitutoenelcursas=="IES Els Evóls (L'Alcúdia)"
replace poblacion = 12009 if LocalidadInstitutoenelcursas=="IES Els Evóls (L'Alcúdia)"
// Canals
// IES Francesc Gil, Canals.
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Francesc Gil, Canals."
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Francesc Gil, Canals."
replace SocioeconStatPers = 8834 if LocalidadInstitutoenelcursas=="IES Francesc Gil, Canals."
replace SocioeconStatHog = 24127 if LocalidadInstitutoenelcursas=="IES Francesc Gil, Canals."
replace poblacion = 13587 if LocalidadInstitutoenelcursas=="IES Francesc Gil, Canals."
// Castelló de Rugat
// Castello de Rugat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Castello de Rugat"
replace SocioeconStatPers = 9138 if LocalidadInstitutoenelcursas=="Castello de Rugat"
replace SocioeconStatHog = 23684 if LocalidadInstitutoenelcursas=="Castello de Rugat"
replace poblacion = 2274 if LocalidadInstitutoenelcursas=="Castello de Rugat"
// Castelló de Rugat
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Castelló de Rugat"
replace SocioeconStatPers = 9138 if LocalidadInstitutoenelcursas=="Castelló de Rugat"
replace SocioeconStatHog = 23684 if LocalidadInstitutoenelcursas=="Castelló de Rugat"
replace poblacion = 2274 if LocalidadInstitutoenelcursas=="Castelló de Rugat"
// Castelló de Rugat 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Castelló de Rugat "
replace SocioeconStatPers = 9138 if LocalidadInstitutoenelcursas=="Castelló de Rugat "
replace SocioeconStatHog = 23684 if LocalidadInstitutoenelcursas=="Castelló de Rugat "
replace poblacion = 2274 if LocalidadInstitutoenelcursas=="Castelló de Rugat "
// Cullera
// Cullera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cullera"
replace SocioeconStatPers = 9762 if LocalidadInstitutoenelcursas=="Cullera"
replace SocioeconStatHog = 24236 if LocalidadInstitutoenelcursas=="Cullera"
replace poblacion = 22145 if LocalidadInstitutoenelcursas=="Cullera"
// Foios o Foyos
// Foios
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Foios"
replace SocioeconStatPers = 11590 if LocalidadInstitutoenelcursas=="Foios"
replace SocioeconStatHog = 29111 if LocalidadInstitutoenelcursas=="Foios"
replace poblacion = 7367 if LocalidadInstitutoenelcursas=="Foios"
// Gandia
// Gandia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Gandia"
replace SocioeconStatPers = 9631 if LocalidadInstitutoenelcursas=="Gandia"
replace SocioeconStatHog = 25307 if LocalidadInstitutoenelcursas=="Gandia"
replace poblacion = 74562 if LocalidadInstitutoenelcursas=="Gandia"
// Godella
// Godella
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Godella"
replace SocioeconStatPers = 14468 if LocalidadInstitutoenelcursas=="Godella"
replace SocioeconStatHog = 43179 if LocalidadInstitutoenelcursas=="Godella"
replace poblacion = 13088 if LocalidadInstitutoenelcursas=="Godella"
// Godella 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Godella "
replace SocioeconStatPers = 14468 if LocalidadInstitutoenelcursas=="Godella "
replace SocioeconStatHog = 43179 if LocalidadInstitutoenelcursas=="Godella "
replace poblacion = 13088 if LocalidadInstitutoenelcursas=="Godella "
// Játiva / Xàtiva
// xativa
replace cuidad = 1 if LocalidadInstitutoenelcursas=="xativa"
replace SocioeconStatPers = 10459 if LocalidadInstitutoenelcursas=="xativa"
replace SocioeconStatHog = 26782 if LocalidadInstitutoenelcursas=="xativa"
replace poblacion = 29231 if LocalidadInstitutoenelcursas=="xativa"
// Ies josep de ribera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
replace SocioeconStatPers = 10459 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
replace SocioeconStatHog = 26782 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
replace poblacion = 29231 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
// Ies josep de ribera
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
replace SocioeconStatPers = 10459 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
replace SocioeconStatHog = 26782 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
replace poblacion = 29231 if LocalidadInstitutoenelcursas=="Ies josep de ribera"
// Mogente / Moixent
// IES Moixent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Moixent"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Moixent"
replace SocioeconStatPers = 8788 if LocalidadInstitutoenelcursas=="IES Moixent"
replace SocioeconStatHog = 22422 if LocalidadInstitutoenelcursas=="IES Moixent"
replace poblacion = 4302 if LocalidadInstitutoenelcursas=="IES Moixent"
// Oliva
// Oliva
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Oliva"
replace SocioeconStatPers = 9494 if LocalidadInstitutoenelcursas=="Oliva"
replace SocioeconStatHog = 24089 if LocalidadInstitutoenelcursas=="Oliva"
replace poblacion = 25101 if LocalidadInstitutoenelcursas=="Oliva"
// Oliva/Valencia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Oliva/Valencia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Oliva/Valencia"
replace SocioeconStatPers = 9494 if LocalidadInstitutoenelcursas=="Oliva/Valencia"
replace SocioeconStatHog = 24089 if LocalidadInstitutoenelcursas=="Oliva/Valencia"
replace poblacion = 25101 if LocalidadInstitutoenelcursas=="Oliva/Valencia"
// Ontinyent
// Ontinyent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Ontinyent"
replace SocioeconStatPers = 9784 if LocalidadInstitutoenelcursas=="Ontinyent"
replace SocioeconStatHog = 25570 if LocalidadInstitutoenelcursas=="Ontinyent"
replace poblacion = 35347 if LocalidadInstitutoenelcursas=="Ontinyent"
// Colegio La Concepción Ontinyent
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio La Concepción Ontinyent"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio La Concepción Ontinyent"
replace SocioeconStatPers = 9784 if LocalidadInstitutoenelcursas=="Colegio La Concepción Ontinyent"
replace SocioeconStatHog = 25570 if LocalidadInstitutoenelcursas=="Colegio La Concepción Ontinyent"
replace poblacion = 35347 if LocalidadInstitutoenelcursas=="Colegio La Concepción Ontinyent"
// Paterna
// LA SALLE VALENCIA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="LA SALLE VALENCIA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="LA SALLE VALENCIA"
replace SocioeconStatPers = 11136 if LocalidadInstitutoenelcursas=="LA SALLE VALENCIA"
replace SocioeconStatHog = 29538 if LocalidadInstitutoenelcursas=="LA SALLE VALENCIA"
replace poblacion = 70195 if LocalidadInstitutoenelcursas=="LA SALLE VALENCIA"
// Picaña / Picanya
// PICANYA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="PICANYA"
replace SocioeconStatPers = 11661 if LocalidadInstitutoenelcursas=="PICANYA"
replace SocioeconStatHog = 31794 if LocalidadInstitutoenelcursas=="PICANYA"
replace poblacion = 11513 if LocalidadInstitutoenelcursas=="PICANYA"
// Valencia
// Valencia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valencia"
replace SocioeconStatPers = 12453 if LocalidadInstitutoenelcursas=="Valencia"
replace SocioeconStatHog = 31456 if LocalidadInstitutoenelcursas=="Valencia"
replace poblacion = 794288 if LocalidadInstitutoenelcursas=="Valencia"
// Valencia, IES san Vicente Ferrer
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer"
replace SocioeconStatPers = 12453 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer"
replace SocioeconStatHog = 31456 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer"
replace poblacion = 794288 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer"
// Valencia, IES san Vicente Ferrer 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer "
replace SocioeconStatPers = 12453 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer "
replace SocioeconStatHog = 31456 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer "
replace poblacion = 794288 if LocalidadInstitutoenelcursas=="Valencia, IES san Vicente Ferrer "
// Valencia/ IES Serpis
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valencia/ IES Serpis"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Valencia/ IES Serpis"
replace SocioeconStatPers = 12453 if LocalidadInstitutoenelcursas=="Valencia/ IES Serpis"
replace SocioeconStatHog = 31456 if LocalidadInstitutoenelcursas=="Valencia/ IES Serpis"
replace poblacion = 794288 if LocalidadInstitutoenelcursas=="Valencia/ IES Serpis"
// Colegio Sagrado Corazón Hermanos Maristas
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Hermanos Maristas"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Hermanos Maristas"
replace SocioeconStatPers = 12453 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Hermanos Maristas"
replace SocioeconStatHog = 31456 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Hermanos Maristas"
replace poblacion = 794288 if LocalidadInstitutoenelcursas=="Colegio Sagrado Corazón Hermanos Maristas"
// La Vall d'Uixó
// La Vall d'Uixó
replace cuidad = 1 if LocalidadInstitutoenelcursas=="La Vall d'Uixó"
replace SocioeconStatPers = 9835 if LocalidadInstitutoenelcursas=="La Vall d'Uixó"
replace SocioeconStatHog = 25335 if LocalidadInstitutoenelcursas=="La Vall d'Uixó"
replace poblacion = 31660 if LocalidadInstitutoenelcursas=="La Vall d'Uixó"
********************************************************************************
*    Valladolid
********************************************************************************
// Laguna de Duero
// Laguna de Duero, Valladolid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Laguna de Duero, Valladolid"
replace SocioeconStatPers = 11751 if LocalidadInstitutoenelcursas=="Laguna de Duero, Valladolid"
replace SocioeconStatHog = 32182 if LocalidadInstitutoenelcursas=="Laguna de Duero, Valladolid"
replace poblacion = 22725 if LocalidadInstitutoenelcursas=="Laguna de Duero, Valladolid"
// LICEO FRANCÉS DE CASTILLA Y LEÓN, VALLADOLID, ESPAÑA
replace cuidad = 1 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE CASTILLA Y LEÓN, VALLADOLID, ESPAÑA"
replace instituto = 1 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE CASTILLA Y LEÓN, VALLADOLID, ESPAÑA"
replace SocioeconStatPers = 11751 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE CASTILLA Y LEÓN, VALLADOLID, ESPAÑA"
replace SocioeconStatHog = 32182 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE CASTILLA Y LEÓN, VALLADOLID, ESPAÑA"
replace poblacion = 22725 if LocalidadInstitutoenelcursas=="LICEO FRANCÉS DE CASTILLA Y LEÓN, VALLADOLID, ESPAÑA"
// Valladolid
// Valladolid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valladolid"
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="Valladolid"
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="Valladolid"
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="Valladolid"
// Valladolid 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Valladolid "
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="Valladolid "
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="Valladolid "
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="Valladolid "
// Colegio San José, Valladolid
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="Colegio San José, Valladolid"
// IES Jose Jiménez Lozano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="IES Jose Jiménez Lozano"
// Apostolado del sagrado corazón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón"
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón"
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón"
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón"
// Apostolado del sagrado corazón 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón "
replace instituto = 1 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón "
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón "
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón "
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="Apostolado del sagrado corazón "
// ÍES Pinar de la Rubia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="ÍES Pinar de la Rubia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="ÍES Pinar de la Rubia"
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="ÍES Pinar de la Rubia"
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="ÍES Pinar de la Rubia"
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="ÍES Pinar de la Rubia"
// Colegio La Inmaculada (Maristas Valladolid)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Colegio La Inmaculada (Maristas Valladolid)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Colegio La Inmaculada (Maristas Valladolid)"
replace SocioeconStatPers = 13230 if LocalidadInstitutoenelcursas=="Colegio La Inmaculada (Maristas Valladolid)"
replace SocioeconStatHog = 31453 if LocalidadInstitutoenelcursas=="Colegio La Inmaculada (Maristas Valladolid)"
replace poblacion = 298412 if LocalidadInstitutoenelcursas=="Colegio La Inmaculada (Maristas Valladolid)"
********************************************************************************
*    Vizcaya
********************************************************************************
// Bilbao
// Bilbao, País Vasco
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Bilbao, País Vasco"
replace SocioeconStatPers = 15137 if LocalidadInstitutoenelcursas=="Bilbao, País Vasco"
replace SocioeconStatHog = 35329 if LocalidadInstitutoenelcursas=="Bilbao, País Vasco"
replace poblacion = 346843 if LocalidadInstitutoenelcursas=="Bilbao, País Vasco"
// Bilbao, País Vasco 
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Bilbao, País Vasco "
replace SocioeconStatPers = 15137 if LocalidadInstitutoenelcursas=="Bilbao, País Vasco "
replace SocioeconStatHog = 35329 if LocalidadInstitutoenelcursas=="Bilbao, País Vasco "
replace poblacion = 346843 if LocalidadInstitutoenelcursas=="Bilbao, País Vasco "
// Bilbao
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Bilbao"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Bilbao"
replace SocioeconStatPers = 15137 if LocalidadInstitutoenelcursas=="Bilbao"
replace SocioeconStatHog = 35329 if LocalidadInstitutoenelcursas=="Bilbao"
replace poblacion = 346843 if LocalidadInstitutoenelcursas=="Bilbao"
// Bilbao/Colegio Pureza de María
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Bilbao/Colegio Pureza de María"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Bilbao/Colegio Pureza de María"
replace SocioeconStatPers = 15137 if LocalidadInstitutoenelcursas=="Bilbao/Colegio Pureza de María"
replace SocioeconStatHog = 35329 if LocalidadInstitutoenelcursas=="Bilbao/Colegio Pureza de María"
replace poblacion = 346843 if LocalidadInstitutoenelcursas=="Bilbao/Colegio Pureza de María"
// Durango
// Durango
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Durango"
replace SocioeconStatPers = 15487 if LocalidadInstitutoenelcursas=="Durango"
replace SocioeconStatHog = 39481 if LocalidadInstitutoenelcursas=="Durango"
replace poblacion = 29791 if LocalidadInstitutoenelcursas=="Durango"
// Getxo / Guecho
// colegio nuestra señora de Europa, Bizkaia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="colegio nuestra señora de Europa, Bizkaia"
replace instituto = 1 if LocalidadInstitutoenelcursas=="colegio nuestra señora de Europa, Bizkaia"
replace SocioeconStatPers = 20147 if LocalidadInstitutoenelcursas=="colegio nuestra señora de Europa, Bizkaia"
replace SocioeconStatHog = 52589 if LocalidadInstitutoenelcursas=="colegio nuestra señora de Europa, Bizkaia"
replace poblacion = 77946 if LocalidadInstitutoenelcursas=="colegio nuestra señora de Europa, Bizkaia"
// Leioa / Lejona
// Leioa, Bizkaia
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Leioa, Bizkaia"
replace SocioeconStatPers = 15005 if LocalidadInstitutoenelcursas=="Leioa, Bizkaia"
replace SocioeconStatHog = 38381 if LocalidadInstitutoenelcursas=="Leioa, Bizkaia"
replace poblacion = 31795 if LocalidadInstitutoenelcursas=="Leioa, Bizkaia"
// Muskiz / Musques
// Cf somorrostro, Muskiz
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Cf somorrostro, Muskiz"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Cf somorrostro, Muskiz"
replace SocioeconStatPers = 13483 if LocalidadInstitutoenelcursas=="Cf somorrostro, Muskiz"
replace SocioeconStatHog = 34127 if LocalidadInstitutoenelcursas=="Cf somorrostro, Muskiz"
replace poblacion = 7507 if LocalidadInstitutoenelcursas=="Cf somorrostro, Muskiz"
********************************************************************************
*    Zamora
********************************************************************************
// Benavente
// Benavente
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Benavente"
replace SocioeconStatPers = 9629 if LocalidadInstitutoenelcursas=="Benavente"
replace SocioeconStatHog = 23177 if LocalidadInstitutoenelcursas=="Benavente"
replace poblacion = 17935 if LocalidadInstitutoenelcursas=="Benavente"
// Zamora
// Claudio Moyano
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Claudio Moyano"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Claudio Moyano"
replace SocioeconStatPers = 13402 if LocalidadInstitutoenelcursas=="Claudio Moyano"
replace SocioeconStatHog = 30058 if LocalidadInstitutoenelcursas=="Claudio Moyano"
replace poblacion = 61406 if LocalidadInstitutoenelcursas=="Claudio Moyano"
// Zamora/Alfonso IX
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Zamora/Alfonso IX"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Zamora/Alfonso IX"
replace SocioeconStatPers = 13402 if LocalidadInstitutoenelcursas=="Zamora/Alfonso IX"
replace SocioeconStatHog = 30058 if LocalidadInstitutoenelcursas=="Zamora/Alfonso IX"
replace poblacion = 61406 if LocalidadInstitutoenelcursas=="Zamora/Alfonso IX"
********************************************************************************
*    Zaragoza
********************************************************************************
// Borja
// Borja, IES Juan de Lanuza
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Borja, IES Juan de Lanuza"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Borja, IES Juan de Lanuza"
replace SocioeconStatPers = 10836 if LocalidadInstitutoenelcursas=="Borja, IES Juan de Lanuza"
replace SocioeconStatHog = 27399 if LocalidadInstitutoenelcursas=="Borja, IES Juan de Lanuza"
replace poblacion = 4969 if LocalidadInstitutoenelcursas=="Borja, IES Juan de Lanuza"
// Zaragoza
// Zaragoza
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Zaragoza"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="Zaragoza"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="Zaragoza"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="Zaragoza"
// Zaragoza
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Zaragoza"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="Zaragoza"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="Zaragoza"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="Zaragoza"
// zaragoza
replace cuidad = 1 if LocalidadInstitutoenelcursas=="zaragoza"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="zaragoza"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="zaragoza"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="zaragoza"
// Zaragoza, Montearagón
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="Zaragoza, Montearagón"
// Nuestra Señora del Pilar
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Nuestra Señora del Pilar"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Nuestra Señora del Pilar"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="Nuestra Señora del Pilar"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="Nuestra Señora del Pilar"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="Nuestra Señora del Pilar"
// Jesuitas zaragoza
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Jesuitas zaragoza"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Jesuitas zaragoza"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="Jesuitas zaragoza"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="Jesuitas zaragoza"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="Jesuitas zaragoza"
// Santa María del Pilar - Marianistas, Zaragoza
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Santa María del Pilar - Marianistas, Zaragoza"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Santa María del Pilar - Marianistas, Zaragoza"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="Santa María del Pilar - Marianistas, Zaragoza"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="Santa María del Pilar - Marianistas, Zaragoza"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="Santa María del Pilar - Marianistas, Zaragoza"
// Fco Grande Covián (Zgz)
replace cuidad = 1 if LocalidadInstitutoenelcursas=="Fco Grande Covián (Zgz)"
replace instituto = 1 if LocalidadInstitutoenelcursas=="Fco Grande Covián (Zgz)"
replace SocioeconStatPers = 13211 if LocalidadInstitutoenelcursas=="Fco Grande Covián (Zgz)"
replace SocioeconStatHog = 32455 if LocalidadInstitutoenelcursas=="Fco Grande Covián (Zgz)"
replace poblacion = 674997 if LocalidadInstitutoenelcursas=="Fco Grande Covián (Zgz)"
********************************************************************************
********************************************************************************
*** Institutos Extranjeros, otras Regiones, desconocidos o ambiguos 
********************************************************************************
********************************************************************************
// .
// prueba
// No cursado
// uned
// UNED
// Andorra
// Colegio Holandés de Tres Arroyos, Argentina
// Colegio Holandés de Tres Arroyos, Argentina 
// Riobamba - Ecuador
// Catalunya
// Ceapucv
// Emiratos Árabes Unidos
// Future language school-egypt
// EL IES Jose Luis Sampedro puede ser tanto de Guadalajara como de Madrid
// IES Jose Luis Sampedro
// Marruecos
// Marruecos 
// Miro
// No cursado
// Rosenheim, Alemania
// Santa Teresa de Lisieux
// Santo Domingo
// St Pauls School
// Asturias
// Venezuela
// Venezuela
// ÍES SAN JUAN BAUTISTA puede ser de Navas de San Juan (Jaén) o Madrid
// ÍES SAN JUAN BAUTISTA
// IES san juan bautista
// IES san juan bautista
// San Miguel II
// San Antonio de Padua
// San Antonio de Padua
//Gredos San Diego: Hay distintos en diferenes areas: Alcalá, Buitrago, El Escorial, Guadarrama, Las Rozas, Las Suertes, Mortaláz y Vallecas
// Gredos San Diego
// Gredos San Diego
// AB
// ARGELIA
// Abu Dhabi, Emiratos Árabes
// Argel
// Asturias
// Bélgica
// CARACAS VENEZUELA
// Bristol
// Bulgaria
// Chile
// China
// uned
// v
// venezuela
// Alemania
// Andorra, Escuela andorrana de bachillerato
// Caracas Venezuela
// Caracas, Venezuela
// Carcas, Venezuela
// Carcas, Venezuela
// Colombia
// Cordoba (ARGENTINA)
// Ibiza puede referirse a la isla o a la cuidad de Eivissa.
// Ibiza
// Inglaterra
// Peru
// Peru, Lima
// Paris
// Venezuela
// Walt Whitman High School, MD, EE.UU.
// marruecos
// Insititutos y colegios de hermanos Amoròs en Villena y Madrid
// Amorós
// Atlantic School
// Bachillerato homologado por el ministerio de eduacion
// Bernadette
// Braders
// nicaragua, homologado en Barcelona
// U.E. COLEGIO SIMON BOLIVAR II (CARACAS, VENEZUELA)
// Ucrania
// Ucrania, Jarkov
// caracas venezuela
// caracas, venezuela
// Lycée Jean Zay, Jarny, France
// Kenosha Wisconsin
// LA PAZ, BOLIVIA
// Cantabria
// Varios Carmelitas a lo largo del país
// Carmelitas
// Celestino Mutis Colombia
// Celestino Mutis
// Ceu
// Honduras
// Hungría
// France
// Francia
// Ghana/ Kwame Nkrumah University of Science and Technology
// Guinea Ecuatorial
// liceo classico giovanni meli , palermo , italia
// Napoleon High School (Ohio,USA)
// México
// No hice BAT
// Northwood School (USA)
// Colegio CEU
// Colegio Emil Friedman (Caracas-Venezuela)
// Colegio Emil Friedman. Caracas, Venezuela
// Copenhague, Dinamarca
// Colegio Rudolf Steiner, Santiago de Chile
// Colegio San Gabriel Arcangel - Venezuela
// Colegio San Ignacio. Caracas
// Enköping
// Colegio St Michaels En Boadilla del monte y en San Lorenzo de El Escorial
// Colegio St Michaels
// Colegio virgen de Europa en Madrid y en Soria
// Colegio virgen de Europa
// Colegio virgen de Europa
//El Valle : Almería, Granada, León, Las Palmas, Murcia, Palencia,...
// El Valle
// El valle
// Escuela de Suboficiales del Ejército Sargento Cabral
// Extremadura
// Fray Bentos, Río Negro, Uruguay
// Londres
// Marruecos
// Mexico. Sierra Nevada Interlomas
// I.E Bertha Gedeon de Baladi Cartagena/Colombia
// I.S.N. Escuela Internacional de Nápoles
// IES Camilo José Cela En Pozuelo de Alarcón, Campillos y Padrón
// IES Camilo José Cela
// IES FEDERICO GARCIA LORCA en Albacete, Las Rozas y Churriana de la Vega
// IES FEDERICO GARCIA LORCA
// IES ISABEL LA CATÓLICA En Boadilla del Monte y Villa de Madrid
// IES Isabel La Católica
// IES ISABEL LA CATÓLICA
// colegio San Ignacio de Loyola (Caracas Venezuela)
// San Ignacio de Loyola en Madrid, Torrelodones, Carracas,..
// San Ignacio de Loyola
// IES Jorge Juan en Port de Sagunt, Alicante y San Fernando
// IES Jorge Juan
// Jorge Juan
// IES La Senda en Getafe o en Valencia
// IES La Senda
// I.E.S MARE NOSTRUM En Alicante o Torrevieja
// I.E.S MARE NOSTRUM
// IES MARE NOSTRUM
// IES Mare Nostrum
// IES MARGARITA SALAS en Majadahonda, Sevilla, Torre de Benalgabón y Seseña
// IES MARGARITA SALAS
// ÍES MARGARITA SALAS
// IES Margarita Salas
// Instituto D`Amicis en Puebla, México
// Escolapios varios por el territorio nacional
// Escolapios
// Escolapios
// Nueva york/ Cádiz
//IES San Isidro: Madrid, Azuqueca de Henares, Talavera de la Reina
// IES San Isidro
// Ibiza (IES Quartó de Portmany)/Berga (IES Guillem de Berguedà) Dos localidades
// Ibiza (IES Quartó de Portmany)/Berga (IES Guillem de Berguedà)
// Innova Schools
// Instituto Cumbres de Caracas
// Juan Pablo II en Alcorcón y Guadarrama
// Juan Pablo II
// La habana
// Liceo Franco-Peruano
// Liceo classico impallomeni
// Liceo linguistico G.A. De Cosmi
// Lincoln School, San José Costa Rica
// Los salesianos varios repartidos por la penínusla
// Los salesianos
// Luis Garcia Berlanga en Coslada, Guadalix de la sierra y San Juan de Alicante
// Luis Garcia Berlanga
// Maravillas
// Maristas
// Maristas Santa Maria
// Mueres
// Màrius Torres en LLeida y Hospitalet de Llobregar
// Màrius Torres
// Newton College
// Nuestra señora del buen consejo en Madrid, Logroño, Avilés
// Nuestra señora del buen consejo
// Peñalar en Segovia y Torrelodones
// Peñalar
// Punta Galea peuede ser un centro educativo en Madrid, o un lugar en Guecho (Vizcaya)
// Punta Galea
// Sagrado Corazón. Valencia, Venezuela.
// Saint Louis des Français
// Sainte-Marie Grand Lebrun, Burdeos, Francia
// San Agustín en Madrid y en Alicante
// San Agistin
// San Agustin
// San Agustín
// San Cayetano
// San Viator: coelgio provado en madrid o escuela en Valladolid
// San Viator
// San viator
// Sanaa
// Santa Marta en Badajoz y en Albacete
// Santa Marta
// colegio brader
// colegio luis vives En Madrid y en Alcalá de Henares
// colegio luis vives
// ies mediterraneo en Málaga, Cartagena, La Línea de la Concepción,..
// ies mediterraneo
// jesuitas
// ÍES Rosa chacel en Madrid y en Colmenar Viejo
// ÍES Rosa chacel
// ÍES lope de Vega en Madrid y Alcalá de Henares
// ÍES lope de Vega
// Venezuela


********************************************************************************
* Dummy para cuidad_grande si población > 50000
********************************************************************************
replace cuidad_grande = 1 if poblacion > 50000
********************************************************************************
********************************************************************************


