clear all

cd "/home/javiervg/QED/6 TrabajoStata/"

* If necessary import the excel files and convert them to dta:
* run ImportData.do


foreach x in "DATA/Quest-HL-WithoutBlock-201015" "DATA/Quest-HL-WithBlock-200916" "DATA/Quest-HL-WithBlock-201015" {
*foreach x in "DATA/Quest-HL-WithBlock-200916"{	// DONE
*foreach x in "DATA/Quest-HL-WithoutBlock-201015"  {	// DONE
*foreach x in "DATA/Quest-HL-WithBlock-201015" {	//DONE
	use `x', clear
	* Removes leading and trailing blanks from the field
	replace LocalidadInstitutoenelcursas = strltrim(LocalidadInstitutoenelcursas)
	replace LocalidadInstitutoenelcursas = strrtrim(LocalidadInstitutoenelcursas)
	do filter.do
	save `x'_filtered.dta, replace
}

** Code to perform some tests
*drop if cuidad == 1 & SocioeconStatPers >1 & SocioeconStatHog >1 & poblacion > 1
*drop if cuidad==1
*drop if LocalidadInstitutoenelcursas==""
