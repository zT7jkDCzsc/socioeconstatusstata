* This scripts imports the excel files and saves them as a .dta file afterwards.
* IMPORTANT: for the file "Questionnaire-HL-with-BlockE_October 15, 2020_18.38 ANONYM.xlsx"
* the number of rows imported have been limited to the 2055th row. This has been
* done because stata imported up to row 2124 even if it appears to be empty data.

*cd "/home/javiervg/QED/6 TrabajoCueva/data_orig/"

********************************************************************************
* Without economic bloc, October the 15th.
clear all
import excel "/home/javiervg/QED/6 TrabajoStata/DATA_XLSX/Questionnaire-HL-without-BlockE_October 15, 2020_18.40 ANONYM.xlsx", sheet("Sheet0") cellrange(A2:EH384) firstrow
save "/home/javiervg/QED/6 TrabajoStata/DATA/Quest-HL-WithoutBlock-201015.dta"


********************************************************************************
* With Economic Block, September the 16th
clear all
import excel "/home/javiervg/QED/6 TrabajoStata/DATA_XLSX/Questionnaire-HL-with-BlockE_September 16, 2020_22.57 ANONYM.xlsx", sheet("Sheet0") cellrange(A2:EX188) firstrow
save "/home/javiervg/QED/6 TrabajoStata/DATA/Quest-HL-WithBlock-200916.dta"

********************************************************************************
* With Economic Block, October the 15th
clear all
import excel "/home/javiervg/QED/6 TrabajoStata/DATA_XLSX/Questionnaire-HL-with-BlockE_October 15, 2020_18.38 ANONYM.xlsx", sheet("Sheet0") firstrow cellrange(A2:EX2055)
save "/home/javiervg/QED/6 TrabajoStata/DATA/Quest-HL-WithBlock-201015.dta"
